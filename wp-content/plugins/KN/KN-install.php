<?php

/**
플러그인에 필요한 테이블을 추가합니다.
*/
function _kn_install_db_table(){
	global $wpdb;

	include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$table_prefix_ = $wpdb->base_prefix;
	$table_charset_ = $wpdb->get_charset_collate();

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_gcode` (
		`CODE_ID` varchar(5) NOT NULL COMMENT '코드ID',
		`CODE_NM` varchar(50) DEFAULT NULL COMMENT '코드명',
		`CODE_DESC` varchar(100) DEFAULT NULL COMMENT '코드설명',
		`USE_YN` varchar(1) DEFAULT NULL COMMENT '사용여부',
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일자',
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '수정일자',
		PRIMARY KEY (`CODE_ID`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='공통코드';");

	dbDelta("
		INSERT INTO `{$table_prefix_}kn_gcode` VALUES ('P0001','놀이활동상태','놀이활동상태','Y','2015-10-13 11:32:32','2015-10-13 11:32:32'),('P0003','대분류','놀이활동대분류','Y','2015-10-13 11:32:58','2015-10-13 11:32:58'),('P0004','소분류','놀이활동소분류','Y','2015-10-13 11:33:18','2015-10-13 11:33:18'),('U0001','회원상태','회원상태','Y','2015-10-25 15:55:12','2015-10-25 15:55:12'),('U0003','회원구분','회원구분','Y','2015-10-25 15:56:33','2015-10-29 10:33:35'),('U0005','놀이진행경험','놀이진행경험','Y','2015-10-25 15:57:23','2015-10-25 15:57:23'),('U0007','전문가 유입경로질문','전문가 유입경로질문','Y','2015-10-27 10:57:32','2015-10-27 11:06:17'),('U0009','전문가 지원이유질문','전문가 지원이유질문','Y','2015-10-27 10:58:39','2015-10-27 11:06:36'),('U0011','전문가 분야질문','전문가 분야질문','Y','2015-10-27 11:00:05','2015-10-27 11:00:05'),('U0013','전문가 경력질문','전문가 경력질문','Y','2015-10-27 11:02:37','2015-10-27 11:02:37'),('U0015','전문가 활동형태질문','전문가 활동형태질문','Y','2015-10-27 11:04:42','2015-10-27 11:04:42'),('U0017','전문가 놀이장소질문','전문가 놀이장소질문','Y','2015-10-27 11:05:55','2015-10-27 11:05:55'),('U0019','전문가 상태','전문가 상태','Y','2015-10-27 18:17:19','2015-10-27 18:17:19'),('Z0001','휴대폰앞자리','휴대폰앞자리','Y','2015-10-10 11:56:43','2015-10-10 11:56:43'),('Z0003','성별','성별','Y','2015-10-14 12:52:13','2015-10-14 12:52:13');
		");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_gcode_dtl` (
		`CODE_ID` varchar(5) NOT NULL COMMENT '코드ID',
		`CODE_DID` varchar(5) NOT NULL COMMENT '코드상세ID',
		`CODE_DNM` varchar(50) DEFAULT NULL COMMENT '코드상세명',
		`CODE_DDESC` varchar(100) DEFAULT NULL COMMENT '코드상세설명',
		`USE_YN` varchar(1) DEFAULT NULL COMMENT '사용여부',
		`SORT_CHAR` varchar(5) DEFAULT NULL COMMENT '정령구분자',
		`REG_DATE` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '등록일자',
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '수정일자',
		PRIMARY KEY (`CODE_ID`,`CODE_DID`),
		CONSTRAINT `{$table_prefix_}kn_gcode_dtl_fk1` FOREIGN KEY (`CODE_ID`) REFERENCES `{$table_prefix_}kn_gcode` (`CODE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='공통코드상세';");

	dbDelta("
		INSERT INTO `{$table_prefix_}kn_gcode_dtl` VALUES ('P0001','00001','작성중','작성중','Y','1',NULL,'2015-10-13 12:48:33'),('P0001','00003','등록됨','등록됨','Y','2',NULL,'2015-10-13 12:48:49'),('P0001','00005','마감됨','마감됨','Y','5','2015-10-24 11:24:02','2015-10-24 11:24:02'),('P0001','00007','종료됨','종료됨','Y','7','2015-10-24 11:23:57','2015-10-24 11:23:57'),('P0001','00009','차단됨','차단됨','Y','9',NULL,'2015-10-13 12:49:27'),('P0003','11000','활동','활동','Y','0','2015-10-13 11:34:34','2015-10-13 11:34:34'),('P0003','13000','재미','재미','Y','1','2015-10-13 11:34:43','2015-10-13 11:34:43'),('P0003','15000','음악','음악','Y','2','2015-10-13 11:35:17','2015-10-13 11:35:17'),('P0003','17000','공연','공연','Y','3','2015-10-13 11:35:23','2015-10-13 11:35:23'),('P0003','19000','파티','파티','Y','4',NULL,'2015-10-13 11:36:30'),('P0003','21000','푸드','푸드','Y','5',NULL,'2015-10-13 11:36:41'),('P0003','23000','언어','언어','Y','6',NULL,'2015-10-13 11:36:54'),('P0003','99000','기타','기타','Y','99','2015-10-13 11:59:43','2015-10-13 11:59:43'),('P0004','11001','운동은 즐거워','운동은 즐거워','Y','1',NULL,'2015-10-13 11:52:25'),('P0004','11003','피크닉 고고씽','피크닉 고고씽','Y','2',NULL,'2015-10-13 11:52:37'),('P0004','11004','애완견과 자연속으로','애완견과 자연속으로','Y','3',NULL,'2015-10-13 11:52:50'),('P0004','13001','팝콘과 영화 산책','팝콘과 영화 산책','Y','4',NULL,'2015-10-13 11:53:06'),('P0004','13003','무작정 웃고 떠들기','무작정 웃고 떠들기','Y','5',NULL,'2015-10-13 11:53:21'),('P0004','13005','안 쓰는 물건의 재탄생','안 쓰는 물건의 재탄생','Y','6',NULL,'2015-10-13 11:53:36'),('P0004','13007','다(多)문화를 섞어섞어','다(多)문화를 섞어섞어','Y','7',NULL,'2015-10-13 11:53:47'),('P0004','13008','게임아 안녕','게임아 안녕','Y','8',NULL,'2015-10-13 11:53:58'),('P0004','13009','접고, 칠하고, 만들고','접고, 칠하고, 만들고','Y','9',NULL,'2015-10-13 11:54:13'),('P0004','15001','노래하며 감성 깨우기','노래하며 감성 깨우기','Y','10',NULL,'2015-10-13 11:54:24'),('P0004','15003','다 함께 댄스를','다 함께 댄스를','Y','11',NULL,'2015-10-13 11:56:40'),('P0004','17001','마술사의 쇼쇼쇼','마술사의 쇼쇼쇼','Y','12','2015-10-13 11:57:25','2015-10-13 11:57:25'),('P0004','17003','우리집에서 인형극','우리집에서 인형극','Y','13',NULL,'2015-10-13 11:57:21'),('P0004','19001','나는야 파티 전문가','나는야 파티 전문가','Y','14',NULL,'2015-10-13 11:57:43'),('P0004','19003','파티 초대해요!','파티 초대해요!','Y','15',NULL,'2015-10-13 11:57:54'),('P0004','21001','베이킹의 세계로','베이킹의 세계로','Y','16',NULL,'2015-10-13 11:58:05'),('P0004','21003','오감이 간질간질','오감이 간질간질','Y','17',NULL,'2015-10-13 11:58:20'),('P0004','21005','식재료 여행','식재료 여행','Y','19',NULL,'2015-10-13 11:58:33'),('P0004','23001','니하오~중국아 안녕','니하오~중국아 안녕','Y','20',NULL,'2015-10-13 11:58:56'),('P0004','23003','굿모닝~영어야 안녕','굿모닝~영어야 안녕','Y','21',NULL,'2015-10-13 11:59:08'),('P0004','99001','기타','기타','Y','99','2015-10-13 11:59:48','2015-10-13 11:59:48'),('U0001','00001','정상등록','정상등록','Y','1',NULL,'2015-10-25 15:55:30'),('U0001','00009','사용정지','사용정지','Y','9',NULL,'2015-10-25 15:56:02'),('U0003','00001','일반','일반','Y','1',NULL,'2015-10-25 15:56:48'),('U0003','00003','전문가','전문가','Y','3','2015-10-29 10:33:29','2015-10-29 10:33:29'),('U0005','00001','엄마표 놀이진행 경험있음','엄마표 놀이진행 경험있음','Y','1',NULL,'2015-10-25 15:58:18'),('U0005','00003','품앗이, 공동육아 경험있음','품앗이, 공동육아 경험있음','Y','3',NULL,'2015-10-25 15:58:36'),('U0005','00005','사교육 경험있음','사교육 경험있음','Y','5',NULL,'2015-10-25 15:58:50'),('U0005','00009','없음','없음','Y','9',NULL,'2015-10-25 15:59:08'),('U0007','00001','온라인 까페 및 블로그','온라인 까페 및 블로그','Y','1',NULL,'2015-10-27 10:57:45'),('U0007','00003','인터넷 검색','인터넷 검색','Y','3',NULL,'2015-10-27 10:57:53'),('U0007','00005','지인 추천','지인 추천','Y','5',NULL,'2015-10-27 10:58:07'),('U0007','00006','기타','기타','Y','9',NULL,'2015-10-27 10:58:17'),('U0009','00001','우리 아이(들)에게 친구를 만들어 주고 싶어서','우리 아이(들)에게 친구를 만들어 주고 싶어서','Y','1',NULL,'2015-10-27 10:58:52'),('U0009','00003','나의 재능과 능력을 시험해 보기 위해','나의 재능과 능력을 시험해 보기 위해','Y','3',NULL,'2015-10-27 10:59:02'),('U0009','00005','부수입을 창출하기 위해','부수입을 창출하기 위해','Y','5',NULL,'2015-10-27 10:59:11'),('U0009','00009','기타','기타','Y','9',NULL,'2015-10-27 10:59:23'),('U0011','00001','미술','미술','Y','1',NULL,'2015-10-27 11:00:17'),('U0011','00003','체육','체육','Y','3',NULL,'2015-10-27 11:00:26'),('U0011','00005','요리','요리','Y','5',NULL,'2015-10-27 11:00:35'),('U0011','00007','언어(영어, 중국어 등)','언어(영어, 중국어 등)','Y','7',NULL,'2015-10-27 11:00:45'),('U0011','00009','인성교육','인성교육','Y','9',NULL,'2015-10-27 11:00:56'),('U0011','00011','오감활동','오감활동','Y','11',NULL,'2015-10-27 11:01:04'),('U0011','00013','과학','과학','Y','13',NULL,'2015-10-27 11:01:13'),('U0011','00015','야외놀이','야외놀이','Y','15',NULL,'2015-10-27 11:01:22'),('U0011','00017','융합놀이(미술+영어 등)','융합놀이(미술+영어 등)','Y','17',NULL,'2015-10-27 11:01:32'),('U0011','00099','기타','기타','Y','99',NULL,'2015-10-27 11:01:46'),('U0013','00001','1년 미만','1년 미만','Y','1',NULL,'2015-10-27 11:02:48'),('U0013','00003','1년~3년','1년~3년','Y','3',NULL,'2015-10-27 11:02:58'),('U0013','00005','3년 이상','3년 이상','Y','5',NULL,'2015-10-27 11:03:14'),('U0013','00007','없음','없음','Y','7',NULL,'2015-10-27 11:03:25'),('U0013','00009','기타','기타','Y','9',NULL,'2015-10-27 11:03:34'),('U0015','00001','개인','개인','Y','1',NULL,'2015-10-27 11:04:56'),('U0015','00003','사업자','사업자','Y','3',NULL,'2015-10-27 11:05:04'),('U0015','00005','전문 프리랜서','전문 프리랜서','Y','5',NULL,'2015-10-27 11:05:15'),('U0015','00009','기타','기타','Y','9',NULL,'2015-10-27 11:05:28'),('U0017','00001','우리집','우리집','Y','1',NULL,'2015-10-27 11:07:29'),('U0017','00003','참여자의 집','참여자의 집','Y','3',NULL,'2015-10-27 11:07:37'),('U0017','00005','외부 장소(놀이터, 어린이 도서관 등)','외부 장소(놀이터, 어린이 도서관 등)','Y','5',NULL,'2015-10-27 11:07:44'),('U0017','00007','{site_name} 도움 필요','{site_name} 도움 필요','Y','7',NULL,'2015-10-27 11:08:16'),('U0017','00009','기타','기타','Y','9',NULL,'2015-10-27 11:08:24'),('U0019','00001','신청','신청','Y','1','2015-10-27 18:18:18','2015-10-27 18:18:18'),('U0019','00003','승인','승인','Y','3',NULL,'2015-10-27 18:18:13'),('Z0001','010','010','010','Y','',NULL,'2015-10-10 11:56:55'),('Z0003','00001','남자','남자','Y','1',NULL,'2015-10-14 12:52:25'),('Z0003','00003','여자','여자','Y','2',NULL,'2015-10-14 12:52:34'),('Z0003','00009','기타','기타','Y','9',NULL,'2015-10-14 12:52:44');
	");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_users` (
		`ID` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`USER_LOGIN` varchar(45) NOT NULL COMMENT '로그인ID',
		`USER_EMAIL` varchar(50) NOT NULL COMMENT '이메일',
		`USER_PASS` varchar(100) DEFAULT NULL COMMENT '암호',
		`USER_NAME` varchar(30) DEFAULT NULL COMMENT '이름',
		`NICK_NAME` varchar(40) DEFAULT NULL COMMENT '닉네임',
		`BIRTH_YYYY` varchar(4) DEFAULT NULL COMMENT '생년월일 - 연도',
		`BIRTH_MM` varchar(2) DEFAULT NULL COMMENT '생년월일 - 월',
		`BIRTH_DD` varchar(2) DEFAULT NULL COMMENT '생년월일 - 일',
		`PHONE1` varchar(5) DEFAULT NULL COMMENT '연락처',
		`PHONE2` varchar(5) DEFAULT NULL,
		`PHONE3` varchar(5) DEFAULT NULL,
		`SMS_REV_YN` varchar(1) DEFAULT NULL COMMENT 'SMS수신여부',
		`EMAIL_REV_YN` varchar(1) DEFAULT NULL COMMENT '이메일수신여부',
		`STATUS` varchar(5) DEFAULT NULL COMMENT '상태',
		`PROFILE_URL` varchar(700) DEFAULT NULL COMMENT '사용자구분',
		`INTRO_TXT` longtext COMMENT '자기소개',
		`PLAY_EXP` varchar(5) DEFAULT NULL COMMENT '놀이활동경험',
		`ADDRESS` varchar(300) DEFAULT NULL COMMENT '주소',
		`ADDRESS_DTL` varchar(300) DEFAULT NULL COMMENT '주소상세',
		`ADDR_LAT` double DEFAULT NULL,
		`ADDR_LNG` double DEFAULT NULL,
		`LEV_DATE` datetime DEFAULT NULL COMMENT '탈퇴일자',
		`BLK_DATE` datetime DEFAULT NULL COMMENT '정지일자',
		`BLK_RSN` longtext COMMENT '정지사유',
		`AUTH_DI` varchar(80) DEFAULT NULL COMMENT '본인인증_중복방지ID',
		`AUTH_CI1` varchar(100) DEFAULT NULL COMMENT '본인인증_연계ID1',
		`AUTH_CI2` varchar(100) DEFAULT NULL COMMENT '본인인증_연계ID2',
		`AUTH_CI_VER` varchar(10) DEFAULT NULL COMMENT '본인인증_연계정보버젼',
		`AUTH_SEX` varchar(5) DEFAULT NULL COMMENT '본인인증_성별',
		`AUTH_CELL_CORP` varchar(10) DEFAULT NULL COMMENT '본인인증_통신사',
		`AUTH_CELL_NUM` varchar(15) DEFAULT NULL COMMENT '본인인증_휴대폰번호',
		`CHANGE_KEY` varchar(120) DEFAULT NULL,
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`),
		UNIQUE KEY `USER_ID_UNIQUE` (`USER_LOGIN`),
		UNIQUE KEY `EMAIL_UNIQUE` (`USER_EMAIL`)
	) ENGINE=InnoDB {$table_charset_} COMMENT='회원';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_users_chd` (
		`ID` bigint(11) NOT NULL COMMENT '사용자ID',
		`SEQ` int(4) NOT NULL COMMENT '순번',
		`CHILD_NAME` varchar(45)  DEFAULT NULL COMMENT '아이이름',
		`CHILD_NICK` varchar(45)  DEFAULT NULL COMMENT '애칭',
		`SEX` varchar(5)  DEFAULT NULL COMMENT '성별',
		`AGE` int(3) DEFAULT NULL COMMENT '나이',
		`RMK` varchar(100)  DEFAULT NULL COMMENT '비고',
		`CHILD_IMG_URL` varchar(500)  DEFAULT NULL COMMENT '아이사진',
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`,`SEQ`),
		CONSTRAINT `{$table_prefix_}kn_users_chd_fk1` FOREIGN KEY (`ID`) REFERENCES `{$table_prefix_}kn_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='사용자정보 - 내아이';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_users_pro` (
		`ID` bigint(11) NOT NULL COMMENT '사용자ID',
		`STATUS` varchar(5) DEFAULT NULL,
		`QNA01` varchar(1000) DEFAULT NULL,
		`QNA02` varchar(1000)  DEFAULT NULL,
		`QNA03` varchar(1000)  DEFAULT NULL,
		`QNA04` varchar(1000)  DEFAULT NULL,
		`QNA05` varchar(1000)  DEFAULT NULL,
		`QNA06` varchar(1000)  DEFAULT NULL,
		`QNA07` varchar(1000)  DEFAULT NULL,
		`QNA08` varchar(1000)  DEFAULT NULL,
		`QNA09` varchar(1000)  DEFAULT NULL,
		`QNA10` varchar(1000)  DEFAULT NULL,
		`QNA11` varchar(1000)  DEFAULT NULL,
		`QNA12` varchar(1000)  DEFAULT NULL,
		`QNA13` varchar(1000)  DEFAULT NULL,
		`QNA14` varchar(1000) DEFAULT NULL,
		`ACCEPT_DATE` datetime DEFAULT NULL,
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`),
		CONSTRAINT `{$table_prefix_}kn_users_pro_fk1` FOREIGN KEY (`ID`) REFERENCES `{$table_prefix_}kn_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='사용자정보 - 프로';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_play` (
		`ID` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '놀이ID',
		`PLAY_TITLE` varchar(50)  DEFAULT NULL COMMENT '놀이명',
		`DESC_HTML` longtext  COMMENT '놀이설명',
		`STATUS` varchar(5)  DEFAULT NULL COMMENT '상태',
		`PLAY_DATE` date DEFAULT NULL COMMENT '놀이일자',
		`PLAY_SRT_TIME` time DEFAULT NULL COMMENT '시작시간',
		`PLAY_END_TIME` time DEFAULT NULL COMMENT '종료시간',
		`HOST_ID` bigint(11) DEFAULT NULL COMMENT '호스트ID',
		`ADDRESS` varchar(100)  DEFAULT NULL COMMENT '주소',
		`ADDRESS_DTL` varchar(100)  DEFAULT NULL COMMENT '주소상세',
		`ADDR_LAT` float(10,6) DEFAULT NULL COMMENT '위도',
		`ADDR_LNG` float(10,6) DEFAULT NULL COMMENT '경도',
		`MAIN_CTG` varchar(5)  DEFAULT NULL COMMENT '대분류',
		`SUB_CTG` varchar(5)  DEFAULT NULL COMMENT '소분류',
		`AV_SRT_AGE` int(3) DEFAULT NULL COMMENT '참가가능나이',
		`AV_END_AGE` int(3) DEFAULT NULL,
		`AV_MIN_CNT` int(5) DEFAULT NULL COMMENT '참가최소인원',
		`AV_MAX_CNT` int(5) DEFAULT NULL COMMENT '참가최대인원',
		`MAIN_IMG_URLS` longtext  COMMENT '메인이미지 URL(array 형식)',
		`REF_URL` varchar(500)  DEFAULT NULL COMMENT '관련링크',
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		`NEED_POINT` bigint(5) DEFAULT NULL,
		`NEED_COST` bigint(10) DEFAULT NULL,
		PRIMARY KEY (`ID`),
		KEY `{$table_prefix_}kn_play_fk1_idx` (`HOST_ID`),
		CONSTRAINT `{$table_prefix_}kn_play_fk1` FOREIGN KEY (`HOST_ID`) REFERENCES `{$table_prefix_}kn_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE 
	) ENGINE=InnoDB {$table_charset_} COMMENT='놀이활동';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_play_rev` (
		`ID` bigint(11) NOT NULL COMMENT '놀이ID',
		`WRITER_ID` bigint(11) NOT NULL COMMENT '작성자ID',
		`REV_TXT` varchar(500)  DEFAULT NULL COMMENT '리뷰',
		`REV_SCORE` int(1) DEFAULT NULL COMMENT '리뷰점수(1~5)',
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		KEY `{$table_prefix_}kn_play_rev_fk1_idx` (`ID`),
		CONSTRAINT `{$table_prefix_}kn_play_rev_fk1` FOREIGN KEY (`ID`) REFERENCES `{$table_prefix_}kn_play` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='놀이활동 - 리뷰';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_play_mem` (
		`ID` bigint(11) NOT NULL COMMENT '놀이ID',
		`MEM_ID` bigint(11) NOT NULL DEFAULT '0' COMMENT '참여자ID',
		`SEQ` int(3) NOT NULL COMMENT '참가순번',
		`CHILD_SEQ` int(4) DEFAULT NULL COMMENT '아이순번',
		`ETC_MEM_CNT` int(3) DEFAULT NULL,
		`STATUS` varchar(5)  DEFAULT NULL COMMENT '상태',
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`,`MEM_ID`,`SEQ`),
		KEY `{$table_prefix_}kn_play_mem_fk2_idx` (`MEM_ID`),
		KEY `{$table_prefix_}kn_play_mem_fk3_idx` (`MEM_ID`,`CHILD_SEQ`),
		CONSTRAINT `{$table_prefix_}kn_play_mem_fk1` FOREIGN KEY (`ID`) REFERENCES `{$table_prefix_}kn_play` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT `{$table_prefix_}kn_play_mem_fk2` FOREIGN KEY (`MEM_ID`) REFERENCES `{$table_prefix_}kn_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT `{$table_prefix_}kn_play_mem_fk3` FOREIGN KEY (`MEM_ID`, `CHILD_SEQ`) REFERENCES `{$table_prefix_}kn_users_chd` (`ID`, `SEQ`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE=InnoDB {$table_charset_} COMMENT='놀이활동 - 참여자';");

	dbDelta("CREATE TABLE IF NOT EXISTS `{$table_prefix_}kn_point` (
		`ID` bigint(15) NOT NULL AUTO_INCREMENT COMMENT 'ID',
		`POINT_CTG` varchar(5)  DEFAULT NULL COMMENT '포인트구분',
		`STATUS` varchar(5)  DEFAULT NULL COMMENT '상태',
		`USER_ID` bigint(11) DEFAULT NULL COMMENT '사용자ID',
		`PLAY_ID` bigint(11) DEFAULT NULL COMMENT '놀이ID',
		`POINT` bigint(10) DEFAULT NULL COMMENT '포인트',
		`RMK` varchar(100)  DEFAULT NULL,
		`REG_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
		`MOD_DATE` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' ',
		PRIMARY KEY (`ID`),
		KEY `{$table_prefix_}kn_point_fk1_idx` (`USER_ID`),
		KEY `{$table_prefix_}kn_point_fk2_idx` (`PLAY_ID`),
		CONSTRAINT `{$table_prefix_}kn_point_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `{$table_prefix_}kn_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT `{$table_prefix_}kn_point_fk2` FOREIGN KEY (`PLAY_ID`) REFERENCES `{$table_prefix_}kn_play` (`ID`) ON DELETE SET NULL ON UPDATE SET NULL
	) ENGINE=InnoDB AUTO_INCREMENT=26  COMMENT='킨스타';");
}

?>