<?php
/**
 * @package KN
 * @version 1.0
 */
/*
Plugin Name: KN
Plugin URI: http://jgroup.co.kr
Description: Kinclub 기본 플러그인입니다. 회원관리, 모임관리 기능을 포함하고 있습니다.
Author: 김형연
Version: 1.0
Author URI: http://jgroup.co.kr
*/

define("KN_PLUGIN_DIR_URL",plugin_dir_url( __FILE__));
define("KN_PLUGIN_DIR_PATH",plugin_dir_path( __FILE__));
define("KNDOMAIN", "kn");

class KN{

	static function _event_activate(){
		$can_activate_plugins_ = current_user_can("activate_plugins");		

		if(!$can_activate_plugins_){
			wp_die("플러그인을 활성화할 권한이 없습니다.");
		}

		_kn_install_db_table();
		
		add_option('KN_activated',True);
	}

	static function _event_deactivate(){
		delete_option('KN_activated');
	}

	static function _kn_initialize(){
		do_action("kn_initialize");
	}

	static function get_current_class_data(){
		global $_kn_class_map;
		if(!isset($_kn_class_map))
			return null;

		$page_ = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
		$current_class_data_ = null;
		foreach($_kn_class_map as $row_index_ => $class_data_){
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$equals_parameters_ = $class_data_["equals_parameters"];
			if(!current_user_can($class_data_["permission"])) continue;
			if(strlen($page_) && $page_ !== $class_data_["slug"]) continue;

			$checked_ = true;
			foreach($parameters_ as $row_index_ => $param_key_){
				if(!isset($_REQUEST[$param_key_]) || !strlen($_REQUEST[$param_key_])){
					$checked_ = false;
					break;
				}
			}

			if($checked_) foreach($equals_parameters_ as $key_ => $value_){
				if(!isset($_REQUEST[$key_]) || $_REQUEST[$key_] !== $value_){
					$checked_ = false;
					break;
				}
			}

			if($checked_){
				$before_param_count_ = count($current_class_data_["parameters"]) + count($current_class_data_["equals_parameters"]);
				$param_count_ = count($parameters_) + count($equals_parameters_);

				if($param_count_ >= $before_param_count_){
					$current_class_data_ = $class_data_;
				}
			}
		}

		return isset($current_class_data_) ? $current_class_data_ : null;
	}

	static function _get_root_class_data($page_){
		global $_kn_class_map;
		if(!isset($_kn_class_map))
			return null;

		foreach($_kn_class_map as $row_index_ => $class_data_){
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$equals_parameters_ = $class_data_["equals_parameters"];
			if(!current_user_can($class_data_["permission"])) continue;
			if(strlen($page_) && $page_ !== $class_data_["slug"]) continue;

			if(count($parameters_) == 0 && count($equals_parameters_) == 0) return $class_data_;
		}
		return null;
	}

	/**
	관리자화면의 플러그인 메뉴를 추가합니다.
	*/
	static function add_plugin_menu($attr_){
		$slug_ = isset($attr_["page"]) ? $attr_["page"] : "";
		$title_ = isset($attr_["title"]) ? $attr_["title"] : "";
		$class_ = isset($attr_["class"]) ? $attr_["class"] : "";

		if(!strlen($slug_) || !strlen($title_) || !strlen($class_)){
			wp_die("잘못된 접근");
			return;
		}

		$sub_title_ = isset($attr_["sub_title"]) ? $attr_["sub_title"] : "";
		$parameters_ = isset($attr_["param"]) ? $attr_["param"] : array();
		$equals_parameters_ = isset($attr_["eparam"]) ? $attr_["eparam"] : array();
		$permission_ = isset($attr_["permission"]) ? $attr_["permission"] : "read";
		$permission_class_ = isset($attr_["permission_class"]) ? $attr_["permission_class"] : null;
		$parent_ = isset($attr_["parent"]) ? $attr_["parent"] : null;
		$icon_ = isset($attr_["icon"]) ? $attr_["icon"] : "lib/img/admin_menu_icon.png";
		$screen_options_ = isset($attr_["screen_options"]) ? $attr_["screen_options"] : array();

		foreach($screen_options_ as $key_ => $row_data_){
			$screen_options_[$key_] = array_merge(array("option" => strtolower($class_."_".$key_)),$row_data_);
		}

		global $_kn_class_map;
		if(!isset($_kn_class_map)){
			$_kn_class_map = array();

			$defaults_map_ = apply_filters("KN_menu_defaults_map",array());
			foreach($defaults_map_ as $row_index_ => $default_){
				self::add_plugin_menu($default_);
			}
		}
			
		$menu_item_ = array(
			"slug" => $slug_,
			"title" => $title_,
			"sub_title" => $sub_title_,
			"class" => $class_,
			"parameters" => $parameters_,
			"equals_parameters" => $equals_parameters_,
			"permission" => $permission_,
			"permission_class" => $permission_class_,
			"parent" => $parent_,
			"icon" => $icon_,
			"screen_options" => $screen_options_,
		);

		array_push($_kn_class_map, $menu_item_);

		if(count($parameters_) == 0 && count($equals_parameters_) == 0){
			add_filter("set-screen-option",array("KN", "_set_screen_option"), 10, 3);
		}

		return $menu_item_;
	}

	static function _add_plugin_menu(){
		global $_kn_class_map;
		if(!isset($_kn_class_map))
			return;

		//add parent first
		foreach($_kn_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) > 0 || count($class_data_["equals_parameters"]) > 0) continue; //pass root class only

			$slug_ = $class_data_["slug"];
			$title_ = $class_data_["title"];
			$sub_title_ = $class_data_["sub_title"];
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$permission_ = $class_data_["permission"];
			$permission_class_ = $class_data_["permission_class"];
			$parent_ = $class_data_["parent"];
			$icon_ = $class_data_["icon"];
			$hook_ = null;

			if(!KN_settings::is_admin() && strlen($permission_class_) && call_user_func_array($permission_class_, array($slug_)) !== false){
				continue;
			}

			if(!strlen($parent_)){
				$hook_ = add_menu_page($title_, $title_, $permission_, $slug_, array("KN", "handle_plugin_menu"), (strpos($icon_,"/") == 0 ? $icon_ : KN_PLUGIN_DIR_URL.$icon_));
				$sub_title_ = strlen($sub_title_) ? $sub_title_ : $title_;
				add_submenu_page($slug_, $sub_title_, $sub_title_, $permission_, $slug_);
				add_action("load-$hook_",array("KN", "_initialize"));
				$_kn_class_map[$row_index_]["hook"] = $hook_;
			}
		}

		//add submenu
		foreach($_kn_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) > 0 || count($class_data_["equals_parameters"]) > 0) continue; //pass root class only

			$slug_ = $class_data_["slug"];
			$title_ = $class_data_["title"];
			$sub_title_ = $class_data_["sub_title"];
			$class_ = $class_data_["class"];
			$parameters_ = $class_data_["parameters"];
			$permission_ = $class_data_["permission"];
			$permission_class_ = $class_data_["permission_class"];
			$parent_ = $class_data_["parent"];
			$icon_ = $class_data_["icon"];
			$hook_ = null;

			if(!KN_settings::is_admin() && strlen($permission_class_) && call_user_func_array($permission_class_, array($slug_)) !== false){
				continue;
			}

			if(strlen($parent_)){
				$hook_ = add_submenu_page($parent_, $title_, $title_, $permission_, $slug_, array("KN", "handle_plugin_menu"));
				add_action("load-$hook_",array("KN", "_initialize"));
				$_kn_class_map[$row_index_]["hook"] = $hook_;
			}
		}

		//set hook to children
		foreach($_kn_class_map as $row_index_ => $class_data_){
			if(count($class_data_["parameters"]) && count($class_data_["equals_parameters"]) == 0) continue; //pass child class only

			$slug_ = $class_data_["slug"];
			$root_class_data_ = self::_get_root_class_data($slug_);
			$hook_ = $root_class_data_["hook"];

			$_kn_class_map[$row_index_]["hook"] = $hook_;
		}
	}

	static function _initialize(){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			$screen_options_ = $current_class_data_["screen_options"];
			foreach($screen_options_ as $key_ => $row_data_){
				add_screen_option($key_, $row_data_);
			}
			call_user_func(array($current_class_data_["class"], "_initialize"));
		}
	}
	static function _set_screen_option($status_, $option_, $value_){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			$screen_options_ = $current_class_data_["screen_options"];
			foreach($screen_options_ as $key_ => $row_data_){
				if($row_data_["option"] === $option_)
					return $value_;
			}
		}

		return $status_;
	}

	static function handle_plugin_menu(){
		$current_class_data_ = self::get_current_class_data();
		if(isset($current_class_data_)){
			call_user_func(array($current_class_data_["class"], "_draw_view"));
		}
	}

	static function _add_ajaxurl(){
	?>
	<script type="text/javascript">
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<?php
	}
}

register_activation_hook( __FILE__, array('KN','_event_activate')); //플러그인 활성화 hook
register_deactivation_hook( __FILE__, array('KN','_event_deactivate')); //플러그인 비활성화 hook

include_once(KN_PLUGIN_DIR_PATH.'KN-install.php'); //플러그인을 위한 설치 스크립트
include_once(KN_PLUGIN_DIR_PATH.'class.KN-common.php'); //기본 라이브러리

if(is_admin()){
	include_once(KN_PLUGIN_DIR_PATH.'class.KN-admin.php'); //관리자 기능 라이브러리
}

add_action('admin_menu', array('KN','_add_plugin_menu'));
add_action("wp_head", array('KN','_add_ajaxurl'));
add_action("plugins_loaded", array("KN", "_kn_initialize"));

?>