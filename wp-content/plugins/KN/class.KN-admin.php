<?php

if(is_admin()){
	include_once(KN_PLUGIN_DIR_PATH . 'modules/class.KNWPTable.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/class.KNAction.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-settings.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-mail.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-sms.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-crontab.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-maps.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-fileupload.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-siren24.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-admin-kakao.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/gcode/class.KN-admin-gcode.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-admin-user.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-admin-play.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/point/class.KN-admin-point.php');
	// include_once(KN_PLUGIN_DIR_PATH . 'modules/pro/class.KN-admin-pro.php');
	include_once(KN_PLUGIN_DIR_PATH . 'modules/mail-notice/class.KN-admin-mail-notice.php');
}

?>