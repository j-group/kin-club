<?php

// if(!function_exists('wp_get_current_user')){
// 	include(ABSPATH . "wp-includes/pluggable.php"); 
// }

include_once(KN_PLUGIN_DIR_PATH . 'class.KN-library.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/class.KNShortcode.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/class.KNListTable.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/class.KNSidemenu.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-utils.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-crypt.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-mobile-detect.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-session.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-settings.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-mail.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-sms.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-crontab.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-maps.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-fileupload.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-captcha.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-siren24.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-kakao.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/gcode/class.KN-gcode.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/point/class.KN-point.php');
// include_once(KN_PLUGIN_DIR_PATH . 'modules/pro/class.KN-pro.php');
// include_once(KN_PLUGIN_DIR_PATH . 'modules/pro/class.KN-play.php');

?>