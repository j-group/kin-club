<?php

global $kn_lib_map;
$kn_lib_map = array();

/**
Javascript & CSS 참조를 위한 클래스
*/
class KN_library{

	static function _initialize(){
		wp_register_style("kn-main", (KN_PLUGIN_DIR_URL . 'lib/css/main.css'));
		wp_register_style("sweet-alert", (KN_PLUGIN_DIR_URL . 'lib/js/sweet-alert/sweet-alert.css'));
		wp_register_style("jquery-datepicker", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.datepicker/zebra_datepicker.css'));
		wp_register_style("jquery-timepicker", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.timepicker/jquery.timepicker.css'));
		wp_register_style("jquery-magnific-popup", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.magnific-popup/magnific-popup.css'));
		wp_register_style("jquery-swiper", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.swiper/jquery.swiper.css'));
		
		wp_register_style("jquery-vex", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.vex/vex.css'));
		wp_register_style("jquery-vex-theme", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.vex/vex-theme-plain.css'));
		wp_register_style("jquery-jqpagination", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.jqpagination/main.css'));
		wp_register_style("jquery-modal", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.modal/jquery.modal.css'));
		wp_register_style("awesome-font", (KN_PLUGIN_DIR_URL . 'lib/css/awesome-font/css/font-awesome.min.css'));
		wp_register_style("jquery-ui", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.ui/jquery-ui.min.css'));
		wp_register_style("jquery-jtable", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.jtable/themes/lightcolor/gray/jtable.min.css'));
		wp_register_style("jquery-tokeninput", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.tokeninput/jquery.tokeninput.css'));
		wp_register_style("jquery-tokeninput-facebook", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.tokeninput/jquery.tokeninput.facebook.css'));
		wp_register_style("jquery-raty", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.raty/jquery.raty.css'));

		wp_register_script("jquery-blockui", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.blockUI.js') , array("jquery"));
		wp_register_script("jquery-validate", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.validate/jquery.validate.min.js') , array("jquery"));
		wp_register_script("jquery-datepicker", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.datepicker/zebra_datepicker.js') , array("jquery"));
		wp_register_script("jquery-timepicker", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.timepicker/jquery.timepicker.min.js') , array("jquery"));
		wp_register_script("jquery-magnific-popup", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.magnific-popup/jquery.magnific-popup.min.js'), array("jquery"));
		wp_register_script("jquery-portamento", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.portamento.js'), array("jquery"));
		wp_register_script("jquery-swiper", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.swiper/jquery.swiper.js'), array("jquery"));
		wp_register_script("jquery-placeholder", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.placeholder.min.js'), array("jquery"));
		
		wp_register_script("sweet-alert", (KN_PLUGIN_DIR_URL . 'lib/js/sweet-alert/sweet-alert.min.js'), array("jquery"));
		wp_register_script("jquery-vex", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.vex/vex.combined.min.js'), array("jquery"));
		wp_register_script("jquery-jqpagination", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.jqpagination/jquery.jqpagination.js'), array("jquery"));
		wp_register_script("jquery-modal", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.modal/jquery.modal.min.js'), array("jquery"));
		wp_register_script("jquery-input-mask", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.mask.min.js'), array("jquery"));
		wp_register_script("jquery-ui", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.ui/jquery-ui.min.js'),array("jquery"));
		wp_register_script("jquery-jtable", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.jtable/jquery.jtable.min.js'),array("jquery","jquery-ui"));
		wp_register_script("jquery-tokeninput", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.tokeninput/jquery.tokeninput.js'),array("jquery","jquery-ui"));
		wp_register_script("jquery-waypoints", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.waypoints/jquery.waypoints.min.js'));
		wp_register_script("jquery-waypoints-inview", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.waypoints/inview.min.js'), array("jquery-waypoints"));
		wp_register_script("jquery-raty", (KN_PLUGIN_DIR_URL . 'lib/js/jquery.raty/jquery.raty.js'), array("jquery"));
		
		
		wp_register_script("kn-main", (KN_PLUGIN_DIR_URL . 'lib/js/main.js'), array("jquery","jquery-validate"));
		wp_register_script("kn-admin", (KN_PLUGIN_DIR_URL . 'lib/js/admin.main.js'), array("jquery","kn-main"));


		do_action("kn_library_init");

		self::register("main",array(
			"script" => array(
				"jquery",
				"jquery-validate",
				"kn-main"
			),
			"style" => array(
				"kn-main",
				"awesome-font",
			)
		));
		self::register("admin",array(
			"script" => array(
				"kn-admin",
			),
			"library" => array("main")
		));
		self::register("blockui",array(
			"script" => array(
				"jquery-blockui",
			)
		));
		self::register("datepicker",array(
			"script" => array(
				"jquery-datepicker",
			),
			"style" => array(
				"jquery-datepicker",
			)
		));
		self::register("timepicker",array(
			"script" => array(
				"jquery-timepicker",
			),
			"style" => array(
				"jquery-timepicker",
			)
		));

		self::register("magnific-popup", array(
			"script" => array(
				"jquery-magnific-popup",
			),
			"style" => array(
				"jquery-magnific-popup",
			)
		));

		self::register("portamento", array(
			"script" => array(
				"jquery-portamento",
			)
		));

		self::register("swiper", array(
			"script" => array(
				"jquery-swiper",
			),
			"style" => array(
				"jquery-swiper",
			)
		));

		self::register("placeholder", array(
			"script" => array(
				"jquery-placeholder",
			)
		));
		

		self::register("alert",array(
			"script" => array(
				"jquery-vex",
				"sweet-alert",
				"jquery-modal",
			),
			"style" => array(
				"jquery-vex",
				"jquery-vex-theme",
				"sweet-alert",
				"jquery-modal",
			)
		));

		self::register("input-mask",array(
			"script" => array(
				"jquery-input-mask",
			)
		));
		self::register("pagination",array(
			"script" => array(
				"jquery-jqpagination",
			),
			"style" => array(
				"jquery-jqpagination",
			)
		));
		self::register("jtable",array(
			"script" => array(
				"jquery-ui",
				"jquery-jtable",
			),
			"style" => array(
				"jquery-ui",
				"jquery-jtable",
			)
		));
		self::register("tokeninput",array(
			"script" => array(
				"jquery-ui",
				"jquery-tokeninput",
			),
			"style" => array(
				"jquery-tokeninput",
				"jquery-tokeninput-facebook",
			)
		));
		self::register("waypoints",array(
			"script" => array(
				"jquery",
				"jquery-waypoints",
				"jquery-waypoints-inview",
			),
		));

		self::register("raty",array(
			"script" => array(
				"jquery",
				"jquery-raty",
			),
			"style" => array(
				"jquery-raty",
			),
		));

		self::register("main-basic",array(
			"library" => array("main","input-mask","pagination", "magnific-popup", "placeholder")
		));
		self::register("main-all",array(
			"library" => array("main","blockui","datepicker","timepicker","alert","input-mask","pagination", "magnific-popup", "placeholder")
		));
		self::register("admin-all",array(
			"library" => array("main","admin","blockui","datepicker","alert","input-mask","pagination")
		));
	}

	static function register($key_, $data_){
		global $kn_lib_map;
		$kn_lib_map[$key_] = $data_;
		return $kn_lib_map[$key_];
	}
	static function deregister($key_){
		global $kn_lib_map;
		unset($kn_lib_map[$key_]);
	}

	static function load($key_){
		global $kn_lib_map;
		$data_ = $kn_lib_map[$key_];
		if(!isset($data_)) return;

		$script_ = isset($data_["script"]) ? $data_["script"] : array();
		$style_ = isset($data_["style"]) ? $data_["style"] : array();
		$library_ = isset($data_["library"]) ? $data_["library"] : array();

		foreach($library_ as $lib_name_){
			self::load($lib_name_);
		}
		foreach($script_ as $script_name_){
			wp_enqueue_script($script_name_);
		}
		foreach($style_ as $style_name_){
			wp_enqueue_style($style_name_);
		}
	}
}

function kn_load_library($key_){
	return KN_library::load($key_);
}

add_action("init", array("KN_library","_initialize"));

?>