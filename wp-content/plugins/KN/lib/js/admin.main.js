(function($){

	KN = $.extend(KN,{
		select_media : function(options_){
			var element_ = options_.selector;

			options_ = $.extend({
				selector : null,
				title : null,
				multiple: false
			},options_);

			if(options_.selector === null || $(options_.selector).length == 0){
				if($("body div[data-kn-media-selector]").length == 0){
					$("body").append($("<div data-kn-media-selector></div>"));
				}
				
				options_.selector = "body div[data-kn-media-selector]";
			}
				
			var target_ = $(options_.selector);
			var media_frame_ = target_.data("kn-media-frame");

			if(media_frame_){
				media_frame_.open();
				return;
			}

			media_frame_ = wp.media({
				title: options_.title,
				multiple: options_.multiple
			});

			media_frame_.on('select', function(){
				var result_ = [];
				var selection_ = this.models[0].get('selection');
				selection_.map(function(attachment_){
					result_[result_.length] = attachment_.toJSON();
				});
				options_.callback(result_)
			});

			media_frame_.open();
		},error_popup : function(options_,callback_){
			swal($.extend({
				type : "error",
				confirmButtonColor: "#0B6EE4"
			},options_),callback_);
		},success_popup : function(options_,callback_){
			swal($.extend({
				type : "success",
				confirmButtonColor: "#0B6EE4"
			},options_),callback_);
		},warning_popup : function(options_,callback_){
			swal($.extend({
				type : "warning",
				confirmButtonColor: "#F8BB86",
				showCancelButton: true
			},options_),callback_);
		},info_popup :  function(options_,callback_){
			swal($.extend({
				type : "warning",
				confirmButtonColor: "#0B6EE4",
				showCancelButton: true
			},options_),callback_);
		},confirm_popup :  function(options_,callback_){
			KN.info_popup(options_, callback_);
		},alert_popup :  function(options_,callback_){
			swal($.extend({
				type : "warning",
				confirmButtonColor: "#0B6EE4"
			},options_),callback_);
		},prompt_popup : function(options_, callback_){
			options_ = $.extend({
				title : "",
				text : "",
				default : "",
				confirmButtonText : "",
				cancelButtonText : ""
			},options_);

			vex.dialog.prompt({
			message : ("<h3>"+options_.title+"</h3>"+"<p>"+options_.text+"</p>"),
			input : '<input name="vex" type="text" class="vex-dialog-prompt-input" placeholder="" value="'+options_.default+'"><script>jQuery("input.vex-dialog-prompt-input").select();</script>',
			buttons : [
				$.extend(vex.dialog.buttons.YES,{text : options_.confirmButtonText}),
				$.extend(vex.dialog.buttons.NO,{text : options_.cancelButtonText})
			],callback : callback_});
		},
		ajax : function(data_,callback_){
			$.post(ajaxurl,$.extend(true,{},data_),
				function(response_text_){
					console.log(response_text_);
					var result_ = true;
					var cause_ = null;
					try{
						response_text_ = JSON.parse(response_text_);
					}catch(ex_){
						result_ = false;
						cause_ = ex_;
					}
					callback_(result_,response_text_,cause_);
			});
		},
		post : function(message_,data_,callback_){
			$.blockUI({
				message : message_,
				onBlock : function(){
					KN.ajax(data_, function(result_,response_text_,cause_){
						$.unblockUI({
							onUnblock : function(){
								callback_(result_,response_text_,cause_);
							}
						});
					});
				}
			});
		}
	});

	return KN;

})(jQuery);