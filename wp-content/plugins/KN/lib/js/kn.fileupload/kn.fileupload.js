(function($){

	var _kn_fileupload_url_ = kn_fileupload_var["upload_url"];
	var _kn_upload_form_html_ = '<form role="form" id="upload" action="'+_kn_fileupload_url_+'" method="post" enctype="multipart/form-data"></form>';

	var kn_profile_uploader = (function(target_, options_){
		options_ = $.extend({},options_);
		
		this._target = target_;
		this._options = options_;

		var unique_id_ = KN.random_string(5, "0123456789");
		var default_img_url_ = target_.attr("data-default-url");
		if(default_img_url_ === undefined || default_img_url_ === null){
			default_img_url_ = (options_["default"] !== undefined ? options_["default"] : null);	
		}
		var html_class_ = (options_.class !== undefined ? options_.class : "");
		
		target_.wrap("<div class='kn-img-frame kn-profile "+html_class_+"'>");
		target_.data("kn-profile-id", unique_id_);
		target_.data("kn-profile-options",options_);
		target_.css({
			"display" : "block",
			"position" : "absolute",
			"zIndex" : -9999,
			"top" : -999999,
			"left" : -999999
		});

		target_.unbind("click", function(){
			this.click();
		});

		var wrap_ = target_.parent();
			wrap_.wrap(_kn_upload_form_html_);

		this._form = wrap_.parent();
		this._uploader = new Uploader({
			$form : this._form,
			postUrl : _kn_fileupload_url_,
			onErrorCallback : function(result_, file_, file_input_){
				var target_ = $(file_input_);
				var options_ = target_.data("kn-profile-options");
				wrap_.removeClass("disabled");
				wrap_.removeClass("uploading");

				if(options_.error !== undefined && options_.error !== null){
					options_.error();
				}else{
					alert(kn_fileupload_var["upload failed"]);
				}
			},onSuccessCallback : function(result_, file_, file_input_){
				var target_ = $(file_input_);
				var options_ = target_.data("kn-profile-options");
				wrap_.removeClass("disabled");
				wrap_.removeClass("uploading");

				target_.kn_profile_uploader("set_image",result_[0]["url"]);
				if(options_.uploaded !== undefined && options_.uploaded !== null){
					options_.uploaded(result_[0]["url"]);
				}
			},uploadFn : "UploaderWithIframe"
		});

		var profile_upload_html_ = '<a href="javascript:;" class="kn-profile-upload-btn">'+kn_fileupload_var["choose image"]+'</a>';
		var profile_delete_html_ = "<a class='kn-profile-delete-btn' href='javascript:;'>"+kn_fileupload_var["remove image"]+"</a>";
		var profile_display_html_ = "<span class='kn-profile-image'></span>";
		var upload_progress_bar_html_ = "<div class='kn-upload-progress kn-animate kn-animate-fadeloop'></div>";

		wrap_.append(profile_display_html_);
		wrap_.append(profile_upload_html_);
		wrap_.append(profile_delete_html_);
		wrap_.append(upload_progress_bar_html_);
		
		target_.change(function(){
			var target_ = $(this);
			if(target_.val() === ""){
				target_.kn_profile_uploader("remove_image");
				return;
			}

			var _temp_do_upload = function(target_){
				var wrap_ = target_.parent();
				wrap_.addClass("disabled");
				wrap_.addClass("uploading");
				target_.kn_profile_uploader("uploader").doUploadSingleInput(target_.kn_profile_uploader("form").find(":file"));
			}

			var options_ = target_.data("kn-profile-options");
			if(options_.upload_confirm !== undefined && options_.upload_confirm !== null){
				if(options_.upload_confirm(target_)){
					_temp_do_upload(target_);
				}else{ target_.val(""); }
				return;
			}

			_temp_do_upload(target_);
		});

		wrap_.children(".kn-profile-upload-btn").click(function(){
			$(this).parent().children("input").click();
		});
		wrap_.children(".kn-profile-delete-btn").click(function(){
			var delete_btn_ = $(this);
			var wrap_ = delete_btn_.parent();
			var target_ = wrap_.children("input");
			var options_ = target_.data("kn-profile-options");

			var _temp_do_delete = function(target_){
				target_.val("");
				target_.trigger("change");
			};

			if(options_.remove_confirm !== undefined && options_.remove_confirm !== null){
				if(options_.remove_confirm()){
					_temp_do_delete(target_);
				}

				return;
			}

			_temp_do_delete(target_);
		});

		if(default_img_url_ !== undefined && default_img_url_ !== null && default_img_url_ !== ""){
			//_tmp_set_image(target_, default_img_url_);
			this.set_image(default_img_url_);
		}
	});
	
	kn_profile_uploader.prototype.form = (function(){
		return this._form;
	});
	kn_profile_uploader.prototype.uploader = (function(){
		return this._uploader;
	});
	kn_profile_uploader.prototype.set_image = (function(image_url_){
		if(image_url_ === "" || image_url_ === null){
			this.remove_image();
			return;
		}
		this._image_url = image_url_;
		var display_ = this._target.parent().children(".kn-profile-image");
		display_.css({"backgroundImage" : "url("+image_url_+")"});
		this._target.parent().addClass("image-added");
	});
	kn_profile_uploader.prototype.remove_image = (function(image_url_){
		this._image_url = null;
		var display_ = this._target.parent().children(".kn-profile-image");
		display_.css({"backgroundImage" : ""});
		this._target.parent().removeClass("image-added");
		var options_ = this._target.data("kn-profile-options");
		if(options_.removed !== undefined && options_ !== null){
			options_.removed();
		}
	});
	kn_profile_uploader.prototype.image_url = (function(){
		return this._image_url;
	});

	$.fn.kn_profile_uploader = (function(){
		var arguments_ = arguments;
		var image_uploader_ = this.data("kn-profile-installed");

		if(image_uploader_ !== undefined && image_uploader_ !== null){
			return $.kn_selector(image_uploader_, arguments_);
		}

		image_uploader_ = new kn_profile_uploader(this, arguments_[0]);
		this.data("kn-profile-installed", image_uploader_);
		return image_uploader_;
	});

	var _kn_image_uploader = (function(target_, options_){
		this._target = target_;
		this._options = $.extend(true,{
			max_length : 1,
			uploaded : function(){},
			appended : function(){},
			removed : function(){}
		},options_);

		target_.addClass("kn-image-uploader");
		target_.append("<a href='javascript:;' class='kn-image-upload-btn'>+</a>");
		target_.append("<input type='file' name='files[]' class='temp-upload-input' accept='.jpg,.jpeg,.gif,.png,.bmp' multiple>");
		// target_.append("<div class='kn-upload-progress kn-animate kn-animate-fadeloop'></div>");

		this._upload_btn = target_.children(".kn-image-upload-btn");
		this._file_input = target_.children(".temp-upload-input");

		this._file_input.unbind("click",function(){
			this.click();
		});

		var wrap_ = target_.parent();
			wrap_.wrap(_kn_upload_form_html_);

		this._form = wrap_.parent();
		this._uploader = new Uploader({
			$form : this._form,
			postUrl : _kn_fileupload_url_,
			onErrorCallback : function(result_, file_, file_input_){
				alert(kn_fileupload_var["upload failed"]);
			},onSuccessCallback : function(result_, file_, file_input_){
				var target_ = file_input_;
				var parent_ = target_.parent();
				var options_ = parent_.kn_image_uploader("options");
				var max_length_ = options_.max_length;
				var current_length_ = parent_.kn_image_uploader("length");

				var target_length_ = max_length_ - current_length_;
				if(target_length_ > result_.length){
					target_length_ = result_.length;
				}

				for(var f_index_=0;f_index_< target_length_; ++f_index_){
					parent_.kn_image_uploader("append_item", result_[f_index_]["url"]);
					options_.uploaded.apply(this, [result_[f_index_]["url"]]);
				}

				target_.val("");
				parent_.removeClass("uploading");
				parent_.find(".kn-image-upload-btn").prop("disabled",false);
			},uploadFn : "UploaderWithIframe"
		});


		this._file_input.on("change", function(){
			var input_ = $(this);
			if(input_.val() === ""){
				return;
			}

			var files_ = input_.get(0).files;
			var parent_ = input_.parent();
			var options_ = parent_.kn_image_uploader("options");
			var max_length_ = options_.max_length;
			var target_length_ = files_.length;

			parent_.addClass("uploading");
			parent_.find(".kn-image-upload-btn").prop("disabled",true);
			parent_.kn_image_uploader("uploader").doUploadSingleInput(parent_.kn_image_uploader("form").find(":file"));
		});

		this._upload_btn.click(function(){
			var parent_ = $(this).parent();
			parent_.find(".temp-upload-input").click();
		});

		var that_ = this;
		var default_imgs_ = target_.children("img");
		default_imgs_.each(function(){
			that_.append_item($(this).attr("src"));
		});

		return this;
	});
	_kn_image_uploader.prototype.target = function(){
		return this._target;
	}
	_kn_image_uploader.prototype.form = (function(){
		return this._form;
	});
	_kn_image_uploader.prototype.uploader = (function(){
		return this._uploader;
	});

	_kn_image_uploader.prototype.options = function(){
		return this._options;
	};
	_kn_image_uploader.prototype.length = function(){
		return this._target.children(".kn-image-item").length;
	};
	_kn_image_uploader.prototype.remove_item = function(image_url_){
		var target_ = this.target().children(".kn-image-item");
		target_.each(function(){
			var item_ = $(this);
			if(item_.data("image-url") === image_url_){
				item_.remove();
			}
		});

		this._target.children(".kn-image-upload-btn").toggle((this._options.max_length > this.length()));
		this._options.removed.apply(this, [image_url_]);
	}
	_kn_image_uploader.prototype.append_item = function(image_url_){
		var unique_id_ = KN.random_string(5, "0123456789");
		var target_ = this.target();
		var length_ = this.length();
		var options_ = this.options();
		if(options_.max_length <= length_) return false;

		target_.prepend("<div class='kn-image-item' id='kn-image-item-"+unique_id_+"'><a class='kn-image-delete-btn' href='javascript:;'>"+kn_fileupload_var["remove image"]+"</a></div>");

		var item_ = target_.children("#kn-image-item-"+unique_id_);
		item_.css({"backgroundImage" : "url("+image_url_+")"});
		item_.data("image-url",image_url_);
		item_.toggle(false);
		item_.fadeIn(400);

		var delete_item_ = item_.children(".kn-image-delete-btn");
		delete_item_.click(function(){
			var delete_btn_ = $(this);
			var temp_item_ = delete_btn_.parent();
			var parent_ = temp_item_.parent();
			parent_.kn_image_uploader("remove_item",temp_item_.data("image-url"));
		});

		this._target.children(".kn-image-upload-btn").toggle((this._options.max_length > this.length()));
		this._options.appended.apply(this, [image_url_]);
	}
	_kn_image_uploader.prototype.toJSON = function(){
		var target_ = this.target();
		var results_ = [];
		var image_items_ = target_.children(".kn-image-item");
		image_items_.each(function(){
			results_.push($(this).data("image-url"));
		});

		return results_;
	}

	$.fn.kn_image_uploader = (function(){
		var arguments_ = arguments;
		var image_uploader_ = this.data("kn-image-uploader");

		if(image_uploader_ !== undefined && image_uploader_ !== null){
			return $.kn_selector(image_uploader_, arguments_);
		}

		image_uploader_ = new _kn_image_uploader(this, arguments_[0]);
		this.data("kn-image-uploader", image_uploader_);
		return image_uploader_;
	});

})(jQuery);