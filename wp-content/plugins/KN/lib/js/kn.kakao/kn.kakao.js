(function($){

	KN.kakao = {};
	KN.kakao = $.extend(KN.kakao,{
		_initialized : false,
		init : function(){
			if(this._initialized === true) return true;
			Kakao.init(kn_kakao_var["app_key"]);
			this._initialized = true;
		},create_link_button : function(options_){
			this.init();

			options_ = $.extend({
				image_width : 300,
				image_height : 200
			},options_);

			return Kakao.Link.createTalkLinkButton({
				container: options_["selector"],
				label: options_["label"],
				image: {
					src: options_["image_url"],
					width: options_["image_width"],
					height: options_["image_height"]
				},
				webButton: {
					text: options_["button_text"],
					url: options_["button_url"]
				}
			});
		}
	});

	return KN.kakao;

})(jQuery);