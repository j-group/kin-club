function _kn_list_table_initialize(unique_id_){
	$ = jQuery;
	var pagenav_ = $("[data-kn-listtable-pagenav-id='"+unique_id_+"']");
	var pagenav_buttons_ = pagenav_.find("a[data-page-index]");

	$.each(pagenav_buttons_, function(){
		var target_ = $(this);
		target_.data("kn-list-table-id",unique_id_);
		target_.click(function(){
			var that_ = $(this);
			var page_index_ = that_.data("page-index");
			var parent_form_ = $("[data-kn-listtable-id='"+that_.data("kn-list-table-id")+"']").parent("form");
			parent_form_.find("input[name='page_index']").val(page_index_);
			parent_form_.submit();
		});
	});
}