(function($){

	function _kn_maps_address_popup_initialize(){
		$ = jQuery;
		var address_el_ = $("#kn-maps-popup-address #kn-maps-popup-input-address");
		var popup_map_el_ = $("#kn-maps-popup-address #kn-address-map");
		var popup_map_ = popup_map_el_.data("map_module");
		var marker_ = KN.maps.marker(popup_map_.getCenter());

		marker_.setMap(popup_map_);
		popup_map_el_.data("map_marker",marker_);

		daum.maps.event.addListener(popup_map_, 'center_changed', function() {
			marker_.setPosition(popup_map_.getCenter());
		});

		daum.maps.event.addListener(popup_map_, 'center_changed', function() {
			marker_.setPosition(popup_map_.getCenter());
		});

		daum.maps.event.addListener(popup_map_, 'dragend', function() {
			_kn_maps_address_popup_load_list_by_map();
		});

		var current_geo_btn_ = $("#kn-maps-popup-address #kn-address-map-current-geo-btn");
		current_geo_btn_.click(function(){
			KN.maps.geolocation(function(result_){
				var current_latlng_ = KN.maps.latlng(result_.lat, result_.lng);
				popup_map_.setCenter(current_latlng_);
				_kn_maps_address_popup_load_list_by_map();
			});

		});

		address_el_.keypress(function(event_){
			if(event_.which == 13){
				_kn_maps_address_popup_search_address();
			}
		});
	}

	function _kn_maps_address_popup_search_address(){
		$ = jQuery;
		var results_frame_el_ = $("#kn-maps-popup-address .search-results-frame");
		var address_el_ = $("#kn-maps-popup-address #kn-maps-popup-input-address");

		_kn_maps_address_popup_init_list();
		results_frame_el_.addClass("loading");

		KN.maps.keywordSearch({
			keyword : address_el_.val(),
			callback : function(result_, data_){
				_kn_maps_address_popup_init_list();
				_kn_maps_address_popup_render_list(data_);
			}
		});
	}
	function _kn_maps_address_popup_load_list_by_map(){
		$ = jQuery;
		var address_el_ = $("#kn-maps-popup-address #kn-maps-popup-input-address");
		var popup_map_el_ = $("#kn-maps-popup-address #kn-address-map");
		var popup_map_ = popup_map_el_.data("map_module");
		var latlang_ = popup_map_.getCenter();
		
		KN.maps.coord2addr({
			lat : latlang_.getLat(),
			lng : latlang_.getLng(),
			callback : function(result_, data_){
				var addr_ = data_[0];
				_kn_maps_address_popup_render_list([{
					address : addr_["jibunAddress"]["name"],
					longitude : addr_["x"],
					latitude : addr_["y"]
				}]);
			}
		});
	}

	function _kn_maps_address_popup_init_list(){
		$ = jQuery;
		var results_frame_el_ = $("#kn-maps-popup-address .search-results-frame");
		var results_el_ = results_frame_el_.find(".search-results");
		results_el_.empty();
		results_frame_el_.removeClass("first");
		results_frame_el_.removeClass("norows");
		results_frame_el_.removeClass("loading");
	}

	function _kn_maps_address_popup_render_list(data_){
		$ = jQuery;
		var popup_el_ = $("#kn-maps-popup-address");
		var results_frame_el_ = $("#kn-maps-popup-address .search-results-frame");
		var results_el_ = results_frame_el_.find(".search-results");
		var popup_map_el_ = $("#kn-maps-popup-address #kn-address-map");
		var popup_map_ = popup_map_el_.data("map_module");

		_kn_maps_address_popup_init_list();

		if(data_.length == 0){
			results_frame_el_.addClass("norows");
		}

		for(var index_=0; index_<data_.length; index_++){
			var addr_ = data_[index_];

			var row_item_ = $('<li><a href="javascript:;">'+addr_.address+'</a></li>');
			results_el_.append(row_item_);
			var row_item_a_ = row_item_.find("a");

			if(!KN.is_mobile()){
				row_item_a_.on("hover",function(event_){
					var target_btn_ = $(event_.target);
					var latlng_ = target_btn_.data("latlng");
					popup_map_.setCenter(KN.maps.latlng(latlng_.lat, latlng_.lng));
				});
			}

			row_item_a_.on("click",function(event_){
				var target_btn_ = $(event_.target);
				var latlng_ = target_btn_.data("latlng");
				var options_ = popup_el_.data("options");

				KN.maps.coord2addr({
					lat : latlng_.lat,
					lng : latlng_.lng,
					callback : function(result_, data_){
						var selected_addr_ = data_[0];
						options_.callback({
							"address_info" : selected_addr_,
							"address" : selected_addr_["jibunAddress"]["name"],
							"lat" : selected_addr_["y"],
							"lng" : selected_addr_["x"]
						});
						$.magnificPopup.close();
					}
				});
			});
			row_item_a_.data("latlng",{lat : addr_.latitude, lng : addr_.longitude});
			
		}
	}
		
	KN.maps = $.extend(KN.maps,{
		address_picker : function(options_){
			$ = jQuery;

			var popup_el_ = $("#kn-maps-popup-address");
			var popup_map_el_ = $("#kn-maps-popup-address #kn-address-map");

			if(popup_map_el_.data("map_module") === undefined || popup_map_el_.data("map_module") === null){
				var popup_map_ = new daum.maps.Map(popup_map_el_[0], {
					center: new daum.maps.LatLng(33.450701, 126.570667)
				});

				popup_map_el_.data("map_module",popup_map_);
				_kn_maps_address_popup_initialize();
			}

			var callback_ = options_.callback;
			var show_secondary_btn_ = (options_.secondary_btn_title !== undefined);

			popup_el_.data("options",options_);

			var second_btn_ = $("#kn-maps-popup-address #kn-maps-popup-second-btn");
			second_btn_.unbind("click");
			second_btn_.toggle(show_secondary_btn_);

			if(show_secondary_btn_){
				second_btn_.text(options_.secondary_btn_title);
				second_btn_.click(options_.secondary_btn_callback);
			}

			$.magnificPopup.open({
				items: {
					src: '#kn-maps-popup-address',
					type: 'inline',
					preloader: false
				},
				closeOnBgClick : false,
				callbacks: {
					open: function() {
						var popup_map_el_ = $("#kn-maps-popup-address #kn-address-map");
						var popup_map_ = popup_map_el_.data("map_module");
						var marker_ = popup_map_el_.data("map_marker");
						popup_map_.relayout();
						KN.maps.geolocation(function(result_){
							var current_latlng_ = KN.maps.latlng(result_.lat, result_.lng);
							popup_map_.setCenter(current_latlng_);	
						});

						var results_frame_el_ = $("#kn-maps-popup-address .search-results-frame");
						var address_el_ = $("#kn-maps-popup-address #kn-maps-popup-input-address");

						_kn_maps_address_popup_init_list();
						results_frame_el_.addClass("first");

						setTimeout(function(){
							address_el_.val("");
							address_el_.focus();
						},100);
					}
				}
			});
		}
	});

	return KN.maps;

})(jQuery);