(function($){

	KN.maps = {};
	KN.maps = $.extend(KN.maps,{
		geolocation_ip : function(callback_){
			KN.post({
				action : "kn-maps-geolocation-ip"
			},function(result_, response_json_){
				if(response_json_.success !== true){
					callback_(false, response_json_);
					return;
				}

				var results_ = {
					"lat" : response_json_["data"]["lat"],
					"lng" : response_json_["data"]["lng"]
				};
				KN.maps.coord2addr({
					lat : results_.lat,
					lng : results_.lng,
					callback : function(status_, addr_){
						if(status_){
							results_["address"] = addr_[0];
						}
						callback_(results_);
					}
				});
			});
		},
		geolocation : function(callback_){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(function(position_){
					var results_ = {
						"lat" : position_.coords.latitude,
						"lng" : position_.coords.longitude
					};
					KN.maps.coord2addr({
						lat : results_.lat,
						lng : results_.lng,
						callback : function(status_, addr_){
							if(status_){
								results_["address"] = addr_[0];
							}
							callback_(results_);
						}
					});
				},function(){
					KN.maps.geolocation_ip(callback_);
				});
			}else{
				KN.maps.geolocation_ip(callback_);
			}
		},coord2addr : function(options_){
			var geocoder_ = new daum.maps.services.Geocoder();
			var coord_ = new daum.maps.LatLng(options_.lat, options_.lng);
			geocoder_.coord2detailaddr(coord_, function(status_, result_){
				options_.callback(status_ === daum.maps.services.Status.OK, result_);
			});
		},addr2coord : function(options_){
			var geocoder_ = new daum.maps.services.Geocoder();
			geocoder_.addr2coord(options_.address, function(status_, result_){
				options_.callback(status_ === daum.maps.services.Status.OK, result_);
			});
		},keywordSearch : function(options_){
			var places_ = new daum.maps.services.Places();
			places_.keywordSearch(options_.keyword, function(status_, data_){
				options_.callback((status === daum.maps.services.Status.OK), data_.places)
			});
		},
		latlng : function(lat_, lng_){
			return new daum.maps.LatLng(lat_, lng_);
		},marker : function(lat_, lng_, marker_umg_url_){
			if(marker_umg_url_ === undefined){
				marker_img_url_ = kn_maps_var["marker_url"];
			}

			return new daum.maps.Marker({
				position: this.latlng(lat_, lng_)
			});
		}
	});

	return KN.maps;

})(jQuery);