(function($){

	KN.siren24 = {};
	KN.siren24 = $.extend(KN.siren24,{
		_CBA_window : null,
		_auth_data : null,
		_open_siren24_popup : function(){
			this._CBA_window = window.open('', 'PCCWindow', 'width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200');
			//document.reqCBAForm.action = 'https://pcc.siren24.com/pcc_V3/jsp/pcc_V3_j10.jsp';
			document.reqCBAForm.target = 'PCCWindow';
			return true;
		},
		auth : function(data_, callback_, blocked_callback_){
			that_ = this;
			$ = jQuery;
			$("form[name='reqCBAForm']").remove();

			var ignore_popup_ = (data_["popup"] !== undefined && !data_["popup"]);

			console.log(data_);

			var form_html_ = '';
			form_html_ += '<form name="reqCBAForm" method="post" action = "https://pcc.siren24.com/pcc_V3/jsp/pcc_V3_j10.jsp" '+( ignore_popup_ ? "" : 'onsubmit="return KN.siren24._open_siren24_popup()"')+' style="display:none;">';
			form_html_ += '<input type="hidden" name="reqInfo" value = "'+data_["enc_data"]+'">';
			form_html_ += '<input type="hidden" name="retUrl" value = "32'+data_["return_url"]+'">';
			form_html_ += '</form>';
			
			var form_element_ = $(form_html_);
			$("body").append(form_element_);
			form_element_.submit();
			if(ignore_popup_) return;

			if(that_._CBA_window){

				var poll_timer_ = setInterval(function(){
					if(that_._CBA_window.closed !== false){
						clearInterval(poll_timer_);
						$("form[name='reqCBAForm']").remove();

						if(that_._auth_data !== null){
							var auth_data_ = $.extend(true,{},that_._auth_data);	
							that_._auth_data = null;
							callback_(true, auth_data_);
						}else{
							callback_(false);
						}
					}
				},200);
			}else{
				$("form[name='reqCBAForm']").remove();

				if(blocked_callback_ !== undefined){
					blocked_callback_();
				}else callback_(false);
			}
		}
	});

	return KN.siren24;

})(jQuery);