(function($){

	var kn_user_children_table = (function(target_, options_){
		this._target = target_;
		this._list_el = target_.children("tbody");
		this._options = options_;
		this._removed_data = [];

		this._target.wrap("<div class='kn-user-children-table-frame'></div>");
		$("#kn-user-children-table-popup .kn-child-img-file").kn_profile_uploader();

		var popup_ = $("#kn-user-children-table-popup");

		var that_ = this;
		popup_.find("a.modify-btn").click(function(){
			that_._apply_popup();
		});
		popup_.find("a.delete-btn").click(function(){
			that_._remove_popup();
		});

		if(options_.editable){
			var wrap_ = this._target.parent();
				wrap_.prepend("<input type='button' class='knbtn knbtn-primary add-btn' data-accessible='true' value='"+kn_user_children_table_var["button_add_child"]+"'>");
			
			var add_btn_ = wrap_.children("input.add-btn");
				add_btn_.click(function(){
					that_.open_popup();
				});
			add_btn_.wrap("<div class='button-area'></div>");
		}

		this.reload();

		return this;
	});

	kn_user_children_table.prototype.target = (function(){
		return this._target;
	});
	kn_user_children_table.prototype.options = (function(){
		return this._options;
	});

	kn_user_children_table.prototype.add_row = (function(data_){

		this._list_el.children("tr.norows").remove();

		var that_ = this;
		var temp_key_ = KN.random_string(5);
		var list_el_ = this._list_el;
		var row_html_ = '<tr data-unique-key="'+temp_key_+'"><td class="child-img-url"></td><td class="child-name"></td><td class="button-area"></td></tr>';
		list_el_.append(row_html_);
		
		return this.row_item(temp_key_, data_);
	});
	kn_user_children_table.prototype.remove_row = (function(key_){
		var row_item_ = this.row_item(key_);
		var child_data_ = row_item_.data("child-data");

		row_item_.remove();

		if(child_data_["is_new"] === "Y"){
			return;
		}

		this._removed_data.push(child_data_);
		if(this._list_el.children().length == 0){
			this._list_el.append("<tr class='norows'><th colspan='3'>"+kn_user_children_table_var["norows_message"]+"</th></tr>");
		}
	});

	kn_user_children_table.prototype.row_item = (function(key_, data_){
		var that_ = this;
		if(data_ !== undefined){
			var row_item_ = this.row_item(key_);
			var child_img_url_el_ = row_item_.children("td.child-img-url");
			var child_name_el_ = row_item_.children("td.child-name");
			var button_area_el_ = row_item_.children("td.button-area");

			row_item_.data("child-data",data_);
			child_img_url_el_.html("<span class='kn-img-frame kn-profile xsmall'><span style='background-image:url("+data_["child_img_url"]+");'></span><span>");
			child_name_el_.text(data_["child_name"]+"("+data_["child_nick"]+")");

			if(this._options.editable){
				button_area_el_.html("<a href='javascript:;' class='knbtn knbtn-secondary'>수정</a>");
				var edit_btn_ = button_area_el_.children("a");

				edit_btn_.click(function(){
					var target_btn_ = $(this);
					var row_item_ =  target_btn_.parent().parent();
					var child_data_ = target_btn_.parent().parent().data("child-data");

					that_.open_popup(row_item_.attr("data-unique-key"));
				});
			}
		}

		return this._list_el.children("tr[data-unique-key='"+key_+"']");
	});

	kn_user_children_table.prototype.json_string = (function(){
		var children_ = this._list_el.children("[data-unique-key]");
		var results_ = {};
		var data_ = [];
		children_.each(function(){
			data_.push($(this).data("child-data"));
		});

		results_["data"] = data_;
		results_["removed_data"] = this._removed_data;

		return results_;
	});

	kn_user_children_table.prototype.reload = (function(){
		var that_ = this;
		KN.post({
			action : "kn-user-children-table-load-list",
			user_id : this._options["user_id"]
		},function(result_, response_json_, cause_){
			if(response_json_.success !== true){
				KN.alert({
					title : "불러오기 실패",
					content : "아이정보를 불러오는데 실패했습니다.",
					OK : "확인"
				});
				return;
			}

			that_._list_el.children().remove();

			if(response_json_.data.length <= 0){
				that_._list_el.append("<tr class='norows'><th colspan='3'>"+kn_user_children_table_var["norows_message"]+"</th></tr>");
			}

			for(var index_=0;index_<response_json_.data.length; ++index_){
				that_.add_row(response_json_.data[index_]);
			}
		});
	});

	kn_user_children_table.prototype.open_popup = (function(key_){
		var that_ = this;

		var data_ = null;

		if(key_ === undefined || key_ === null){
			data_ = {
				seq : "",
				is_new : "Y",
				child_name : "",
				child_nick : "",
				child_img_url : "",
				age : "",
				sex : "",
			};
		}else{
			data_ = this.row_item(key_).data("child-data");
		}
		
		var popup_ = $("#kn-user-children-table-popup");
		var input_key_ = popup_.find("input[name='key']");
		var input_seq_ = popup_.find("input[name='seq']");
		var input_child_img_url_ = popup_.find("input.kn-child-img-file");
		var input_child_name_ = popup_.find("input[name='child_name']");
		var input_child_nick_ = popup_.find("input[name='child_nick']");
		var input_sex_ = popup_.find("select[name='sex']");
		var input_age_ = popup_.find("input[name='age']");

		input_key_.val(key_);
		input_seq_.val(data_["seq"]);
		input_child_img_url_.kn_profile_uploader("set_image",data_["child_img_url"]);
		input_child_name_.val(data_["child_name"]);
		input_child_nick_.val(data_["child_nick"]);
		input_age_.val(data_["age"]);
		input_sex_.val(data_["sex"]);

		var check_is_new_ = data_["is_new"] === "Y";
		var title_ = popup_.children("h3");
		title_.text((check_is_new_ ? kn_user_children_table_var["title_new"] : kn_user_children_table_var["title_modify"]));
		popup_.find("a.delete-btn").toggle(!check_is_new_);
		popup_.find("a.modify-btn").text((check_is_new_ ? kn_user_children_table_var["button_new"] : kn_user_children_table_var["button_modify"]));

		$.magnificPopup.open({
			items: {
				src: '#kn-user-children-table-popup',
				type: 'inline',
				preloader: false
			},
			closeOnBgClick : false,
			callbacks: {
				close : function(){
					// that_._on_close_popup();
				}
			}
		});
	});

	kn_user_children_table.prototype._popup_params = (function(){
		var popup_ = $("#kn-user-children-table-popup");
		var key_ = popup_.find("input[name='key']").val();
		var seq_ = popup_.find("input[name='seq']").val();
		var child_img_url_ = popup_.find("input.kn-child-img-file").kn_profile_uploader("image_url");
		var child_name_ = popup_.find("input[name='child_name']").val();
		var child_nick_ = popup_.find("input[name='child_nick']").val();
		var sex_ = popup_.find("select[name='sex']").selectVal();
		var age_ = popup_.find("input[name='age']").val();

		return {
			"key" : key_,
			"seq" : seq_,
			"child_img_url" : child_img_url_,
			"child_name" : child_name_,
			"child_nick" : child_nick_,
			"sex" : sex_,
			"age" : age_
		};
	});

	kn_user_children_table.prototype._apply_popup = (function(){
		var popup_ = $("#kn-user-children-table-popup");
		var key_ = popup_.find("input[name='key']").val();
		var seq_ = popup_.find("input[name='seq']").val();
		var child_img_url_ = popup_.find("input.kn-child-img-file").kn_profile_uploader("image_url");
		var child_name_ = popup_.find("input[name='child_name']").val();
		var child_nick_ = popup_.find("input[name='child_nick']").val();
		var sex_ = popup_.find("select[name='sex']").selectVal();
		var age_ = popup_.find("input[name='age']").val();

		if(child_name_ === ""){
			alert(kn_user_children_table_var["validate_child_name"]);
			return;
		}
		if(child_nick_ === ""){
			alert(kn_user_children_table_var["validate_child_name"]);
			return;
		}
		if(sex_ === ""){
			alert(kn_user_children_table_var["validate_sex"]);
			return;
		}
		if(age_ === ""){
			alert(kn_user_children_table_var["validate_age"]);
			return;
		}

		var popup_data_ = this._popup_params();

		if(key_ === ""){ //new
			this.add_row({
				"seq" : seq_,
				"child_img_url" : child_img_url_,
				"child_name" : child_name_,
				"child_nick" : child_nick_,
				"sex" : sex_,
				"age" : age_,
				"is_new" : "Y"
			});
		}else{
			var child_data_ = this.row_item(key_).data("child-data");

			child_data_["child_img_url"] = popup_data_["child_img_url"];
			child_data_["child_name"] = popup_data_["child_name"];
			child_data_["child_nick"] = popup_data_["child_nick"];
			child_data_["sex"] = popup_data_["sex"];
			child_data_["age"] = popup_data_["age"];

			this.row_item(key_, child_data_);
		}

		$.magnificPopup.close();
	});
	kn_user_children_table.prototype._remove_popup = (function(){
		var popup_ = $("#kn-user-children-table-popup");
		var key_ = popup_.find("input[name='key']").val();
		this.remove_row(key_);

		$.magnificPopup.close();
	});
	kn_user_children_table.prototype._on_close_popup = (function(){
		var popup_data_ = this._popup_params();
		var row_item_ = this.row_item(popup_data_["key"]);
	});

	$.fn.kn_user_children_table = (function(){
		var arguments_ = arguments;
		var children_table_ = this.data("kn-user-children-table");

		if(children_table_ !== undefined && children_table_ !== null){
			return $.kn_selector(children_table_, arguments_);
		}

		children_table_ = new kn_user_children_table(this, arguments_[0]);
		this.data("kn-user-children-table", children_table_);
		return children_table_;
	});

})(jQuery);