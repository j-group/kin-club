(function($){

	var popup_el_, start_date_el_, end_date_el_, pickup_btn_;

	KN.utils = $.extend(KN.utils,{
		date_picker : function(options_){

			if(popup_el_ === undefined || popup_el_ === null){
				popup_el_ = $("#kn-utils-popup-datepicker");
				start_date_el_ = popup_el_.find("#kn-utils-popup-datepicker-start-date");
				end_date_el_ = popup_el_.find("#kn-utils-popup-datepicker-end-date");
				pickup_btn_ = popup_el_.find("#kn-utils-popup-datepicker-pickup");

				console.log(start_date_el_);

				start_date_el_.Zebra_DatePicker({
					format: 'Y-m-d',
					show_icon : false,
					readonly_element: true,
					direction: true,
  					pair: end_date_el_,
  					show_icon : false,
  					default_position : "below"
				});

				end_date_el_.Zebra_DatePicker({
					format: 'Y-m-d',
					show_icon : false,
					readonly_element: true,
					direction: 1,
					show_icon : false,
					default_position : "below"
				});

				pickup_btn_.click(function(){
					_pickup_date();
				});
			}

			var callback_ = options_.callback;
			var default_start_date_ = options_.start_date;
			var default_end_date_ = options_.end_date;

			start_date_el_.val(null);
			end_date_el_.val(null);
			// start_date_el_.Zebra_DatePicker("update");
			// end_date_el_.Zebra_DatePicker("update");

			popup_el_.data("options",options_);

			$.magnificPopup.open({
				items: {
					src: '#kn-utils-popup-datepicker',
					type: 'inline',
					preloader: false
				},
				closeOnBgClick : false
			});
		}
	});

	function _pickup_date(){
		var options_ = popup_el_.data("options");
		var start_date_ = start_date_el_.val();
		var end_date_ = end_date_el_.val();

		if(start_date_ === undefined || start_date_ === null)
			start_date_ = "";
		if(end_date_ === undefined || end_date_ === null)
			end_date_ = "";

		options_.callback({
			start_date : (start_date_ !== "" ? new Date(start_date_) : null),
			end_date : (end_date_!== "" ? new Date(end_date_) : null)
		});
		$.magnificPopup.close();
	}


})(jQuery);