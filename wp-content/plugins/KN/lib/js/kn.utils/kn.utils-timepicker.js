(function($){

	var popup_el_, start_time_el_, end_time_el_, pickup_btn_;

	KN.utils = $.extend(KN.utils,{
		time_picker : function(options_){

			if(popup_el_ === undefined || popup_el_ === null){
				popup_el_ = $("#kn-utils-popup-timepicker");
				start_time_el_ = popup_el_.find("#kn-utils-popup-timepicker-start-time");
				end_time_el_ = popup_el_.find("#kn-utils-popup-timepicker-end-time");
				pickup_btn_ = popup_el_.find("#kn-utils-popup-timepicker-pickup");

				start_time_el_.timepicker({'scrollDefault': 'now', "timeFormat" : "H:i", "step" : 60});
				end_time_el_.timepicker({'scrollDefault': 'now', "timeFormat" : "H:i", "step" : 60});

				pickup_btn_.click(function(){
					_pickup_time();
				});
			}

			var callback_ = options_.callback;
			var default_start_time_ = options_.start_time;
			var default_end_time_ = options_.end_time;

			start_time_el_.val(null);
			end_time_el_.val(null);

			if(default_start_time_ === undefined){
				start_time_el_.timepicker("setTime", new Date());
			}

			popup_el_.data("options",options_);

			$.magnificPopup.open({
				items: {
					src: '#kn-utils-popup-timepicker',
					type: 'inline',
					preloader: false
				},
				closeOnBgClick : false
			});
		}
	});

	function _pickup_time(){
		var options_ = popup_el_.data("options");
		var start_time_ = start_time_el_.timepicker("getTime");
		var end_time_ = end_time_el_.timepicker("getTime");

		options_.callback({
			start_time : (start_time_ !== null ? start_time_.format("hh:mm") : null),
			end_time : (end_time_ !== null ? end_time_.format("hh:mm") : null)
		});
		$.magnificPopup.close();
	}


})(jQuery);