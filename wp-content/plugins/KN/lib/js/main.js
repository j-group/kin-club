(function($){

	var KN = window.KN = {
		indicator : function(bool_, callback_){
			var options_ = {
				"message" : "<div class='indicator'></div>",
				"css" : {"backgroundColor" : "transparent"}
			};

			if(bool_){
				options_.onBlock = callback_;
				$.blockUI(options_);
			}else{
				options_.onUnblock = callback_;
				$.unblockUI(options_);
			}
		},datetimediff : function(date1_, date2_){
			return (date1_.getTime() - date2_.getTime());
		},current_datetimediff : function(date_){
			return this.datetimediff(date_, new Date());
		},make_currency : function(value_){
			return String(value_).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
		},post : function(data_,callback_){
			$.post(ajaxurl,$.extend(true,{},data_),
				function(response_text_){
					console.log(response_text_);
					var result_ = true;
					var cause_ = null;
					try{
						response_text_ = JSON.parse(response_text_);
					}catch(ex_){
						result_ = false;
						cause_ = ex_;
					}
					callback_(result_,response_text_,cause_);
			});
		},make_url : function(url_, parameters_){
			var concat_char_ = "?";
			if(url_.indexOf(concat_char_) >= 0)
				concat_char_ = "&";
			return url_+concat_char_+$.param(parameters_);
		},is_mobile : function(){
			return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    				|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
		},random_string : function(length_, possible_){
			var text_ = "";
			var possible_ = (possible_ !== undefined ? possible_ : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

			for(var i=0; i < length_; i++)
				text_ += possible_.charAt(Math.floor(Math.random() * possible_.length));

			return text_;
		},make_options : function(array_, value_column_, title_column_){
			var html_ = '';
			for(var index_=0;index_<array_.length; ++index_){
				html_ += '<option value="'+array_[index_][value_column_]+'">'+array_[index_][title_column_]+'</option>';
			}

			return html_;
		},confirm : function(options_){
			options_ = $.extend({
				just_alert : false,
				callback : function(){}
			},options_);

			var html_ = '<div id="kn-common-confirm-popup" class="kn-mg-popup with-button kn-form">';
				html_ += '<h3>'+options_.title+'</h3>';
				html_ += '<div class="wrap kn-form">';
				html_ += '<div class="content-wrap">';
				html_ += options_.content;
				html_ += '</div>';
				html_ += '</div>';
				html_ += '<div class="button-area">';
				html_ += '<a class="knbtn knbtn-primary yes-btn" href="javascript:;">'+options_.OK+'</a>';
		 		if(!options_.just_alert) html_ += '<a class="knbtn knbtn-secondary no-btn" href="javascript:;">'+options_.cancel+'</a>';
				html_ += '</div>';
				html_ += '</div>';

			$.magnificPopup.open({
				items: {
					src: $(html_),
					type: 'inline',
					preloader: false
				},
				closeBtnInside : false,
				closeOnBgClick : false,
				showCloseBtn : false,
				enableEscapeKey : false
			});

			var popup_ = $("#kn-common-confirm-popup");

			popup_.find(".button-area .yes-btn").click(function(){
				$.magnificPopup.close();
				options_.callback(true);
				
			});
			popup_.find(".button-area .no-btn").click(function(){
				$.magnificPopup.close();
				options_.callback(false);
				
			});
		},alert : function(options_){
			options_.just_alert = true;
			this.confirm(options_);
		}
	}

	$.fn.serializeObject = (function(map_data_){
		var result_ = {};
		var pass_map_data_ = (map_data_ === undefined);
		var array_ = this.serializeArray();
		$.each(array_, function(){
			var converted_name_ = this.name;

			if(!pass_map_data_){
				if(map_data_[this.name] !== undefined && map_data_[this.name] !== null)
					converted_name_ = map_data_[this.name];
				else return true;
			}
			
			if(result_[converted_name_] !== undefined){
				if(!result_[converted_name_].push){
					result_[converted_name_] = [result_[converted_name_]];
				}

				result_[converted_name_].push(this.value || '');
			}else result_[converted_name_] = (this.value || '');
		});

		return result_;
	});

	$.fn.selectVal = (function(value_){
		if(value_ !== undefined){
			var options_ = $(this).children("option");
			options_.each(function(){
				if($(this).val() === value_) $(this).attr("selected",true);
				else $(this).removeAttr("selected");
			});
		}

		return $(this).find("option:selected").val();
	});

	$.fn.formWorking = (function(bool_){
		var inputs_ = this.find("[data-accessible='true']");
		if(bool_){
			this.attr("data-working","true");
			inputs_.attr("disabled","disabled");
		}else{
			this.removeAttr("data-working");
			inputs_.removeAttr("disabled");
		}
	});

	$.validator.addMethod("required", function(value, element){ 
		if(value === null || value === undefined) return false;
		return value.trim() !== "";
	});
	$.validator.addMethod("alphanum", function(value, element) {
		return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9]+$/.test(value);
	}); 

	$.validator.addMethod("notempty", function(value, element){
		return (value !== null && value.trim() !== "");
	});

	$.kn_selector = (function(target_, args_){
		if(args_.length === 0) return target_;
		
		var arguments_ = Array.prototype.slice.call(args_); 
		var entry_ = target_[arguments_[0]];
		if(entry_ === undefined){
			console.error(arguments_[0]+" is undefined");
			return undefined;
		}else if($.type(entry_) !== "function"){
			console.error(arguments_[0]+" is not function");
			return undefined;
		}
		return entry_.apply(target_, arguments_.slice(1));
	});

	Date.prototype.format = function(f) {
		if (!this.valueOf()) return " ";

		var d = this;

		return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
		
			switch ($1) {
				case "yyyy": return d.getFullYear();
				case "yy": return (d.getFullYear() % 1000).zf(2);
				case "MM": return (d.getMonth() + 1).zf(2);
				case "dd": return d.getDate().zf(2);
				// case "E": return weekName[d.getDay()];
				case "HH": return d.getHours().zf(2);
				case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
				case "mm": return d.getMinutes().zf(2);
				case "ss": return d.getSeconds().zf(2);
				case "a/p": return d.getHours() < 12 ? "AM" : "PM";
				default: return $1;
			}
		});
	};
 
	String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
	String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
	Number.prototype.zf = function(len){return this.toString().zf(len);};

	String.prototype.nl2br = (function(){
		return this.replace(/\n/g, "<br />");
	});

	return KN;

})(jQuery);