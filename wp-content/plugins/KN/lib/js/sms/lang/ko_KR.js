window.AWSys.sms.message = {
	"sender.loading.text" : "<h3>준비 중...</h3>",
	"sender.confirm.title" : "<h3>문자발송</h3>",
	"sender.confirm.target" : "수신자 : ",
	"sender.confirm.rCount" : "남은문자수 : ",
	"sender.confirm.unknown" : "알수없음",
	"sender.confirm.text" : "<p>아래 내용으로 문자를 발송합니다</p>",
	"sender.confirm.yes" : "발송",
	"sender.confirm.no" : "취소",
	"sender.confirm.without" : " 외",
	"sender.confirm.people" : "명",
	"sender.proccess.text" : "<h3>발송 중...</h3>",
	"sender.proccess.title" : "발송완료",
	"sender.proccess.ok" : "확인",
	"sender.error.title" : "발송실패",
	"sender.error.ok" : "확인"
};