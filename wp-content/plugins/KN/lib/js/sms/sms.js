(function($){

	window.JGM.sms = {
		send : function(options_){
			if($.type(options_.to) === "string"){
				options_.to = [options_.to];
			}

			$.post(ajaxurl,{
				action : "jgm_sms_send",
				numbers : options_.to,
				message : options_.message,
				sms_type : options_.sms_type
			},function(response_text_){
				try{
					response_text_ = JSON.parse(response_text_);
				}catch(ex_){
					options_.callback(false, response_text_);
					return;
				}

				options_.callback(true, response_text_);
			});
		},count : function(callback_){
			$.post(ajaxurl,{
				action : "jgm_sms_count"
			},function(response_text_){
				var result_ = true;
				var cause_ = response_text_;
				var count_ = -1;
				try{
					response_text_ = JSON.parse(response_text_);
				}catch(ex_){
					result_ = false;
				}

				if(response_text_.success === "success"){
					count_ = response_text_.count;
				}

				callback_(true,count_,cause_);
			});
		},
		_openSend : function(target_, count_, callback_){
			var receiver_;
			var more_count_ = 0;

			if($.type(target_) === "array"){
				receiver_ = target_[0];
				more_count_ = target_.length - 1;
			}else{
				receiver_ = target_;
			}

			if(receiver_.indexOf("|") > 0){
				receiver_ = receiver_.split("|")[1];
			}

			if(more_count_ > 0){
				receiver_ += JGM.sms.message["sender.confirm.without"]+" "+more_count_+JGM.sms.message["sender.confirm.people"];
			}

			var title_ = JGM.sms.message["sender.confirm.title"]+"<p>"+JGM.sms.message["sender.confirm.target"]+"<b>"+receiver_+"</b></p>";
			title_ += "<p>"+JGM.sms.message["sender.confirm.rCount"]+"<b>"+count_+"</b>"+"</p>"
			title_ += JGM.sms.message["sender.confirm.text"];
			title_ += ("<script type='text/javascript'>jQuery(document).ready(function($){$('.vex #jgm-sms-message').JGMSMSText('p'); });</script>");

			vex.dialog.prompt({
				message : title_,
				input : "<textarea name='message' id='jgm-sms-message'></textarea>",
				buttons : [
					$.extend(vex.dialog.buttons.YES,{text : JGM.sms.message["sender.confirm.yes"]}),
					$.extend(vex.dialog.buttons.NO,{text : JGM.sms.message["sender.confirm.no"]})
				],callback : function(data_){
					if(!data_ || data_.message === undefined || data_.message === null || data_.message.trim() === ""){
						if(callback_) callback_();
						return;
					}

					$.blockUI({
						message : JGM.sms.message["sender.proccess.text"],
						onBlock : function(){
							JGM.sms.send({
								to : target_,
								message : data_.message,
								sms_type : $('.vex #jgm-sms-message').data("sms_type"),
								callback : function(result_, response_text_, cause_){
									$.unblockUI({
										onUnblock : function(){

											if(!result_ || response_text_.result !== "success"){
												JGM.error_popup({
													title : JGM.sms.message["sender.error.title"],
													text : JSON.stringify(response_text_)+" "+cause_,
													confirmButtonText : JGM.sms.message["sender.error.ok"] 
												},function(){
													if(callback_) callback_(false);
												});
											}else{
												JGM.success_popup({
													title : JGM.sms.message["sender.proccess.title"],
													confirmButtonText : JGM.sms.message["sender.proccess.ok"]
												},function(){
													if(callback_) callback_(true);
												});
											}
										}
									});
								}
							});
						}
					});
					
				}
			});
		},
		openSender : function(number_, callback_){
			$.blockUI({
				message : JGM.sms.message["sender.loading.text"],
				onBlock : function(){
					JGM.sms.count(function(result_, count_){
						$.unblockUI({
							onUnblock : function(){
								count_ = (count_ === undefined || count_ === null ? JGM.sms.message["sender.confirm.unknown"] : JGM.make_money(count_));
								JGM.sms._openSend(number_, count_, callback_);
							}
						});
					});
				}
			});
		}
	}

	$(document).ready(function(){
		var list_ = $("*[data-jgm-sms]");
		list_.each(function(){
			var target_ = $(this);
			var number_ = target_.data("jgm-sms");
			var aTag_ = target_.wrap("<a href='javascript:JGM.sms.openSender(\""+number_+"\");'></a>");
		});
	});

	String.prototype.byteLength = function(){
		var contents = this;
		var str_character;
		var int_char_count;
		var int_contents_length;

		int_char_count = 0;
		int_contents_length = contents.length;

		for(k = 0; k < int_contents_length; k++){
			str_character = contents.charAt(k);
			if(escape(str_character).length > 4)
				int_char_count += 2;
			else int_char_count++;
		}
		return int_char_count;
	}

	jQuery.fn.JGMSMSText = (function(tag_name_){
		if(this._initialized !== undefined && this._initialized) return this;

		tag_name_ = (tag_name_ === undefined ? "span" : tag_name_);
		this._e_length_info = $("<"+tag_name_+" class='jgm-sms-length-info'></"+tag_name_+">");
		$(this).after(this._e_length_info);
		$(this).data("sms_type","S");

		var that_ = this;
		$.post(ajaxurl,{
			action : "jgm_sms_send_info"
		},function(response_text_){
			try{
				response_text_ = JSON.parse(response_text_);
			}catch(ex_){response_text_ = false;}

			if(response_text_.success !== true){
				that_._e_length_info.html("error");
				return;
			}

			that_._sms_send_info = response_text_.result;

			$(that_).on("change keyup",function(event_){
				var this_ = $(event_.target);
				var value_ = this_.val();
				var value_length_ = (value_ !== undefined && value_ !== null ? value_.byteLength() : 0);

				var sms_send_info_s_ = that_._sms_send_info["S"];
				var sms_send_info_l_ = that_._sms_send_info["L"];

				var is_long_ = value_length_ > sms_send_info_s_[1];

				var limit_ = (is_long_ ? sms_send_info_l_[1] : sms_send_info_s_[1]);
				var notice_message_ = (is_long_ ? sms_send_info_l_[3] : sms_send_info_s_[3]);

				that_._e_length_info.html(JGM.make_money(value_length_)+"/"+JGM.make_money(limit_)+" ("+notice_message_+")");
				$(that_).data("sms_type",(is_long_ ? "L" : "S"));
				
				if(is_long_) $(that_._e_length_info).addClass("long");
				else $(that_._e_length_info).removeClass("long");
			});
			$(that_).trigger("change");
		});
	});


	return window.JGM.sms;

})(jQuery);