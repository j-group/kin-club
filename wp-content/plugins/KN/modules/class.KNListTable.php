<?php

class KNListTable{

	static function _add_library(){
		wp_register_style("kn-listtable", (KN_PLUGIN_DIR_URL . 'lib/js/kn.listtable/kn.listtable.css'));
		wp_register_script("kn-listtable", (KN_PLUGIN_DIR_URL . 'lib/js/kn.listtable/kn.listtable.js'),array("jquery","kn-main"));

		KN_Library::register("kn-listtable",array(
			"script" => array(
				"kn-listtable",
			),
			"style" => array(
				"kn-listtable",
			),
			"library" => array(
				"main",
			)
		));
	}

	private $html_class = "";
	private $html_id = "";

	function __construct($id_ = "",$class_ = ""){
		$this->html_id = $id_;
		$this->html_class = $class_;
	}

	function offset($page_index_, $per_page_){
		return ($page_index_) * $per_page_;
	}

	function columns(){
		return array();
	}
	function column_value($item_, $column_name_){
		return $item_[$column_name_];
	}

	function row_attributes($items_){
		return array();
	}
	function before_row($items_, $row_index_){
		return "";
	}
	function after_row($items_, $row_index_){
		return "";
	}

	function norowdata(){
		return __("Data not found");
	}

	function prepare(){
		_e("must override KNListTable::prepare()");
	}
	
	function html(){
		$args_ = $this->prepare();
		
		$uniqueid_ = uniqid();
		$items_ = $args_["items"];
		$per_page_ = $args_["per_page"];
		$total_count_ = $args_["total_count"];
		$total_page_count_ = ceil($total_count_ / $per_page_);
		$page_index_ = $args_["page_index"];
		$pagenav_count_ = $args_["pagenav_count"];
		$hide_pagenav_ = (isset($args_["hide_pagenav"]) ? $args_["hide_pagenav"] : false);
		$hide_header_ = (isset($args_["hide_header"]) ? $args_["hide_header"] : false);
		$is_norowdata_ = (count($items_) == 0);

		$html_ = '';

		$html_ .= '<input type="hidden" name="page_index" value="'.$page_index_.'">';
		$html_ .= '<table class="kn-form-table kn-list-table '.($hide_header_ ? "hide-header" : "")." ".$this->html_class.'" '.(strlen($this->html_id) ? 'id="'.$this->html_id.'"' : '').' data-kn-listtable-id="'.$uniqueid_.'">';
		$html_ .= '<thead><tr>';

		$columns_ = $this->columns();
		foreach($columns_ as $key_ => $title_){
			$html_ .= '<th class="'.$key_.'">'.$title_.'</th>';
		}

		$html_ .= '</tr></thead>';
		$html_ .= '<tbody>';

		foreach($items_ as $row_index_ => $row_data_){
			$tr_attributes_ = $this->row_attributes($row_data_);
			$html_ .= $this->before_row($row_data_, $row_index_);
			$html_ .= '<tr ';
			foreach($tr_attributes_ as $key_ => $value_){
				$html_ .= $key_.'="'.$value_.'"';
			}
			$html_ .= ' >';

			foreach($columns_ as $key_ => $title_){
				$html_ .= '<td class="'.$key_.'">'.$this->column_value($row_data_, $key_).'</td>';
			}

			$html_ .= '</tr>'.$this->after_row($row_data_, $row_index_);
		}

		if($is_norowdata_){
			$html_ .= '<tr><td class="no-rowdata" colspan="'.(count($columns_)).'">'.$this->norowdata().'</td></tr>';
		}

		$html_ .= '</tbody>';
		$html_ .= '</table>';

		if(!$hide_pagenav_){
			$html_ .= '<div class="kn-list-pagenav '.$this->html_class.'" '.(strlen($this->html_id) ? 'id="'.$this->html_id.'-pagenav"' : '').' data-kn-listtable-pagenav-id="'.$uniqueid_.'">';

			$pagenav_offset_ = floor(($page_index_) / $pagenav_count_);
			$pagenav_start_index_ = ($pagenav_offset_ * $pagenav_count_);
			$pagenav_end_offset_ = ($pagenav_count_*($pagenav_offset_+1));

			if($total_page_count_ <= $pagenav_end_offset_){
				$pagenav_end_offset_= $total_page_count_;
			}

			if($pagenav_start_index_ > 0){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-left fa fa-angle-left" data-page-index="'.($pagenav_start_index_-1).'"></a>';
			}

			for($pagenav_index_ = $pagenav_start_index_; $pagenav_index_ < $pagenav_end_offset_; ++$pagenav_index_){
				$html_ .= '<a href="javascript:void(0);" data-page-index="'.($pagenav_index_).'" class="'.($pagenav_index_ == $page_index_ ? "current-index" : "").'">'.($pagenav_index_ + 1).'</a>';
			}

			if($total_page_count_ > $pagenav_end_offset_){
				$html_ .= '<a href="javascript:void(0);" class="pagenav-right fa fa-angle-right" data-page-index="'.($pagenav_end_offset_).'"></a>';
			}

			$html_ .= '</div>';	
		}
		
		$html_ .= '<script type="text/javascript">';
		$html_ .= 'jQuery(document).ready(function(){_kn_list_table_initialize("'.$uniqueid_.'");});';
		$html_ .= '</script>';

		return $html_;
	}
}

add_action("kn_library_init", array("KNListTable", "_add_library"));

?>