<?php

class KNShortcode{
	
	static function shortcode_name(){
		_e("must override KNShortcode::shortcode_name();");
		return null;
	}
	static function add_library(){_e("must override KNShortcode::add_library();");}

	static function filter(){
		global $post;

		if(has_shortcode($post->post_content, static::shortcode_name())){
			static::add_library();
		}
	}
	static function _filter(){
		do_action("kn_shortcode_filter");
	}
}

add_action("wp_enqueue_scripts", array("KNShortcode", "_filter"));

?>