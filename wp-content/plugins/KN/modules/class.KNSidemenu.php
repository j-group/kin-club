<?php

class KNSidemenu{

	static function _add_library(){
		wp_register_style("kn-sidemenu", (KN_PLUGIN_DIR_URL . 'lib/js/kn.sidemenu/kn.sidemenu.css'));
		wp_register_script("kn-sidemenu", (KN_PLUGIN_DIR_URL . 'lib/js/kn.sidemenu/kn.sidemenu.js'));

		KN_Library::register("kn-sidemenu",array(
			"script" => array(
				"kn-sidemenu",
			),
			"style" => array(
				"kn-sidemenu",
			),
			"library" => array(
				"main",
			)
		));
	}

	private $_id = "";
	private $_class = "";
	private $_title = "";
	private $_desc = "";
	private $_side_title = "";
	private $_base_url = "";

	private $sidemenu = array();
	
	function __construct($options_){
		$options_ = array_merge(array("id" => "",
			"class" => "",
			"title" => "",
			"desc" => "",
			"side_title" => "",
			"base_url" => "",
		),$options_);
		$this->_id = $options_["id"];
		$this->_class = $options_["class"];
		$this->_title = $options_["title"];
		$this->_desc = $options_["desc"];
		$this->_side_title = $options_["side_title"];
		$this->_base_url = $options_["base_url"];
	}

	function title(){
		return $this->_title;
	}
	function desc(){
		return $this->_desc;
	}
	function base_url(){
		return $this->_base_url;
	}

	function add_library(){
		kn_load_library("kn-sidemenu");
	
		$current_sideid_ = isset($_GET["sideid"]) ? $_GET["sideid"] : "";
		$sidemenu_ = $this->get_menu($current_sideid_);
		if(isset($sidemenu_)){
			call_user_func(array($sidemenu_["class"],"add_content_library"));
		}

		foreach($this->sidemenu as $row_index_ => $sidemenu_){
			call_user_func(array($sidemenu_["class"],"add_main_library"));
		}

	}

	function get_menu($sideid_){
		foreach($this->sidemenu as $row_index_ => $sidemenu_){
			if($sideid_ === $sidemenu_["sideid"]){
				return $sidemenu_;
			}

			foreach($sidemenu_["submenu"] as $row_sindex_ => $submenu_){
				if($sideid_ === $submenu_["sideid"]){
					return $submenu_;
				}
			}
		}

		return null;
	}

	function _do_sort_sidemenu($a_, $b_){
		$a_index_ = array_search($a_, $a_["_sidemenu"]);
		$b_index_ = array_search($b_, $b_["_sidemenu"]);

		if(isset($a_["index"]) && is_numeric($a_["index"]) && $a_["index"] >= 0){
			$a_index_ = $a_["index"];
		}
		if(isset($b_["index"]) && is_numeric($b_["index"]) && $b_["index"] >= 0){
			$b_index_ = $b_["index"];
		}

		return $a_index_ > $b_index_;
	}

	function _do_sort_submenu($a_, $b_){
		$a_index_ = array_search($a_, $a_["parent"]["submenu"]);
		$b_index_ = array_search($b_, $a_["parent"]["submenu"]);

		if(isset($a_["index"]) && is_numeric($a_["index"]) && $a_["index"] >= 0){
			$a_index_ = $a_["index"];
		}
		if(isset($b_["index"]) && is_numeric($b_["index"]) && $b_["index"] >= 0){
			$b_index_ = $b_["index"];
		}

		return $a_index_ > $b_index_;
	}

	function html(){
		$settings_ = KN_settings::get_settings();
		$current_sideid_ = isset($_GET["sideid"]) ? $_GET["sideid"] : "";
		$current_sidemenu_ = $this->get_menu($current_sideid_);

		usort($this->sidemenu, array($this, "_do_sort_sidemenu"));

		$html_ = '';

		$html_ .= '<div class="kn-form class="kn-sidemenu" id="'.$this->_id.'-frame">';
		$html_ .= '<div class="kn-wrap kn-sidemenu-title">';

		if(isset($current_sidemenu_)){
			$html_ .= '<h3 class="title-with-desc">'.call_user_func(array($current_sidemenu_["class"], "draw_title")).'</h3>';
			$html_ .= '<p class="desc alignleft">'.call_user_func(array($current_sidemenu_["class"], "draw_desc")).'</p>';
		}else{
			$html_ .= '<h3 class="title-with-desc">'.$this->_title.'</h3>';
			$html_ .= '<p class="desc alignleft">'.$this->_desc.'</p>';
		}
		
		$html_ .= '</div>';
		
		if(isset($current_sidemenu_)){
			$html_ .= '<div id="'.$this->_id.'-content" class="kn-sidemenu-content '.$current_sideid_.'">';
			$html_ .= call_user_func(array($current_sidemenu_["class"], "draw_content"));
			$html_ .= '</div>';
		}else{
			$html_ .= '<div id="'.$this->_id.'-main" class="kn-form-table kn-sidemenu-main">';

			foreach($this->sidemenu as $row_index_ => $sidemenu_){
				$side_class_ = $sidemenu_["class"];
				$side_main_content_ = call_user_func(array($side_class_, "draw_main"));
				if(!strlen($side_main_content_)) continue;

				$html_ .= '<div class="kn-sidemenu-main-item '.$sidemenu_["sideid"].'">';
				$html_ .= $side_main_content_;
				$html_ .= '</div>';
			}

			$html_ .= '</div>';
		}

		$html_ .= '<div id="'.$this->_id.'-menu-frame" class="kn-sidemenu-menu-frame">';
		$html_ .= $this->before_sidemenu();
		$html_ .= '<ul id="'.$this->_id.'-menu" class="kn-form-table kn-sidemenu-menu">';
		$html_ .= '<li><a href="'.$this->_base_url.'" '.(!strlen($current_sideid_) ? "class='current'" : "").'>'.$this->_side_title.'</a></li>';
		foreach($this->sidemenu as $row_index_ => $sidemenu_){
			$side_class_ = $sidemenu_["class"];
			$side_html_ = call_user_func(array($side_class_, "draw_sidemenu"));

			if(strlen($side_html_)){
				$html_ .= '<li '.($row_index_ >= 0 ? 'class="hr"' : '').'><a href="'.kn_make_url($this->_base_url,array(
					"sideid" => $sidemenu_["sideid"]
				)).'" '.($current_sideid_ === $sidemenu_["sideid"] ? "class='current'" : "").'>'.$side_html_.'</a>';

				//submenu
				$submenu_ = $sidemenu_["submenu"];
				usort($submenu_, array($this, "_do_sort_submenu"));
			
				$html_ .= '<ul class="kn-sidemenu-submenu">';

				foreach($submenu_ as $s_row_index_ => $submenu_){
					$sub_class_ = $submenu_["class"];
					$sub_html_ = call_user_func(array($sub_class_, "draw_sidemenu"));

					$html_ .= '<li><a href="'.kn_make_url($this->_base_url,array(
						"sideid" => $submenu_["sideid"]
					)).'" '.($current_sideid_ === $submenu_["sideid"] ? "class='current'" : "").'>'.$sub_html_.'</a>';
					$html_ .= '</li>';
				}

				$html_ .= '</ul>';

				$html_ .= '</li>';
			}
		}
		$html_ .= '</ul>';
		$html_ .= $this->after_sidemenu();
		$html_ .= '</div>';
		
		$html_ .= '</div>';

		return do_shortcode($html_);
	}

	function before_sidemenu(){

	}
	function after_sidemenu(){
		return null;
	}

	function add_sidemenu($sideid_, $class_, $index_ = null){
		$temp_array_ = array();
		$sidemenu_ = array(
			"sideid" => $sideid_,
			"class" => $class_,
			"index" => $index_,
			"_sidemenu" => &$this->sidemenu,
			"submenu" => &$temp_array_,
		);

		array_push($this->sidemenu, $sidemenu_);
		return $sidemenu_;
	}
	function add_submenu($parent_, $sideid_, $class_, $index_ = null){
		$parent_menu_ = $this->get_menu($parent_);
		if(!isset($parent_menu_) || !$parent_menu_){
			return false;
		}

		$submenu_ = array(
			"parent" => $parent_menu_,
			"sideid" => $sideid_,
			"class" => $class_,
		);

		array_push($parent_menu_["submenu"], $submenu_);
		return $submenu_;
	}
}

class KNSidemenu_template{

	static function add_main_library(){}
	static function add_content_library(){}

	static function draw_title(){
		_e("must override KNSidemenu_template::draw_title");
	}
	static function draw_desc(){
		_e("must override KNSidemenu_template::draw_desc");
	}
	static function draw_sidemenu(){
		_e("must override KNSidemenu_template::draw_sidemenu");
	}

	static function draw_main(){
		return null;
	}

	static function draw_content(){
		_e("must override KNSidemenu_template::draw_content");
	}
}

add_action("kn_library_init", array("KNSidemenu", "_add_library"));

?>