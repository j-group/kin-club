<?php

class KNAdmin_crontab_settings{
	static function _add_settings_form($settings_){

		$exists_cron_ = KN_crontab::available();
	?>	
	<br/>
	<hr/>
	<br/>
	<h3>스케쥴러설정</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="common_use_cron">스케쥴러사용</label></th>
				<td><p>
					<label for="common_use_cron"><input type="checkbox" name="common_use_cron" id="common_use_cron" <?php echo ($settings_["common_use_cron"] === true ? "checked" : ""); ?> <?= ($exists_cron_ === null ? "readonly" : ""); ?> >스케쥴러 사용</label>
					</p>
					<p class="hint">*Linux cron 환경에서만 가능합니다.</p>
				</td>
			</tr>
			<tr>
				<th><label for="common_cron_perform_page_id">스케쥴러 수행 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["common_cron_perform_page_id"],
						"name" => "common_cron_perform_page_id",
						"id" => "common_cron_perform_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>

		<?php if($exists_cron_){ ?>
			<tr>
				<th><label for="common_cron_cycle">주기설정</label></th>
				<td><select name="common_cron_cycle" id="common_cron_cycle" class="large-text" > 
					<?php for($c_index_ = 1; $c_index_ <= 24; ++$c_index_){
						?> <option value="<?=$c_index_;?>" <?=(((string)$c_index_) === $settings_["common_cron_cycle"] ? "selected" : "");?> ><?=$c_index_;?>시간마다</option><?php
					} ?>
				</td>
			</tr>
			
			<tr>
				<th>
					<label for="common_cron_perform_allow_access">접근허용</label>
				</th>
				<td>
					<select name="common_cron_perform_allow_access">
						<option value="admin" <?php echo ($settings_["common_cron_perform_allow_access"] === "admin" ? "selected" : ""); ?> >관리자만</option>
						<option value="all" <?php echo ($settings_["common_cron_perform_allow_access"] === "all" ? "selected" : ""); ?> >전체</option>
					</select>
					<p class="hint">*위 항목을 체크할 경우 관리자만 스케쥴러 수행 페이지에 접근할 수 있습니다.</p>
				</td>
			</tr>
			<tr>
				<th>
					<label for="common_cron_perform_ip_white_list">접근IP White List</label>
				</th>
				<td>
					<input type="text" name="common_cron_perform_ip_white_list" class="large-text" placeholder="접근IP" value="<?php echo $settings_["common_cron_perform_ip_white_list"]; ?>">
					<p class="hint">*위 항목을 체크할 경우 White List에 등록된 IP만 스케쥴러 수행 페이지에 접근할 수 있습니다.</p>
					<p class="hint">*다수의 접근IP를 설정할 경우 ',' 쉼표로 구분하여 입력하시길 바랍니다.</p>
				</td>
			</tr>
			<tr>
				<th>
					<label for="common_cron_perform_ip_white_list">cron 명령어 상태</label>
				</th>
				<td>
					<?php

					$cron_cycle_ = $settings_["common_cron_cycle"];
					if(KN_crontab::job_exists("0 */".$cron_cycle_." * * * curl ".get_permalink($settings_["common_cron_perform_page_id"]))){
						echo "<b style='color:blue;'>스케쥴러 명령어가 정상등록되었습니다.</b>";
					}else{
						echo "<b>스케쥴러 명령어가 등록되어 있지 않습니다.</b>";
					}
					?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php
	}

	static function _filter_settings($settings_){
		$settings_["common_use_cron"] = ((isset($settings_["common_use_cron"]) && $settings_["common_use_cron"] === "on") ? true : false);
		
		return $settings_;
	}

	static function _before_update_settings($settings_){
		if(KN_crontab::available() === null){
			return;
		}
		
		$use_cron_ = $settings_["common_use_cron"];
		
		if($use_cron_){
			$cron_cycle_ = $settings_["common_cron_cycle"];
			KN_crontab::remove_job("0 */".$cron_cycle_." * * * curl ".get_permalink($settings_["common_cron_perform_page_id"]));
		}

	}
	static function _after_update_settings($settings_){
		if(KN_crontab::available() === null){
			return;
		}

		$use_cron_ = $settings_["common_use_cron"];

		if($use_cron_){
			$cron_cycle_ = $settings_["common_cron_cycle"];
			KN_crontab::add_job("0 */".$cron_cycle_." * * * curl ".get_permalink($settings_["common_cron_perform_page_id"]));
		}
	}
}

add_action("knadmin_settings_form",array("KNAdmin_crontab_settings","_add_settings_form"));
add_filter("kn_update_settings", array("KNAdmin_crontab_settings", "_filter_settings"));
add_action("kn_before_update_settings", array("KNAdmin_crontab_settings", "_before_update_settings"));
add_action("kn_after_update_settings", array("KNAdmin_crontab_settings", "_after_update_settings"));

?>