<?php

class KNAdmin_fileupload_settings{
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>파일업로드관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="fileupload_receipt_page_id">파일업로드수신페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["fileupload_receipt_page_id"],
						"name" => "fileupload_receipt_page_id",
						"class" => "large-text",
					)); ?>
					<p class="hint">*설정하지 않을 경우 파일업로드기능이 정상작동하지 않습니다.</p>
				</td>
			</tr>
			
		</tbody>
	</table>

	<?php
	}
}

add_action("knadmin_settings_form",array("KNAdmin_fileupload_settings","_add_settings_form"));

?>