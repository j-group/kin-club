<?php

class KNAdmin_kakao_settings{
	
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>카카오관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="kakao_app_key">카카오 앱키(APP KEY)</label></th>
				<td><input type="text" name="kakao_app_key" class="large-text" placeholder="APP KEY" value="<?php echo $settings_["kakao_app_key"]; ?>"></td>
			</tr>
		</tbody>
	</table>

	<?php
	}
}

add_action("knadmin_settings_form",array("KNAdmin_kakao_settings","_add_settings_form"));

?>