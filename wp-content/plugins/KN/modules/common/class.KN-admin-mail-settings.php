<?php

class KNAdmin_mail_settings{
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>메일관련사항</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="email_sender">발신메일</label></th>
				<td><input type="text" name="email_sender" class="large-text" placeholder="발신메일주소" value="<?php echo $settings_["email_sender"]; ?>"></td>
			</tr>
			<tr>
				<th colspan="2"><label for="email_template">메일서식</label></th>
			</tr>
			<tr>
				<td colspan="2">
					<?php wp_editor(stripslashes($settings_["email_template"]), "kn-settings-email-template" , array(
						"textarea_name" => "email_template",
						"tinymce" => false,
					)); ?>
				</td>
			</tr>
		</tbody>
	</table>

	<?php
	}
}

add_action("knadmin_settings_form",array("KNAdmin_mail_settings","_add_settings_form"));

?>