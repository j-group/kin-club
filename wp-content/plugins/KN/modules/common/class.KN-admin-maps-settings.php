<?php

class KNAdmin_maps_settings{
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>지도관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="maps_daum_api_url">다음지도 API URL</label></th>
				<td><input type="text" name="maps_daum_api_url" class="large-text" placeholder="URL" value="<?php echo $settings_["maps_daum_api_url"]; ?>"></td>
			</tr>
			<tr>
				<th><label for="maps_daum_api_key">다음지도 API 키</label></th>
				<td><input type="text" name="maps_daum_api_key" class="large-text" placeholder="API키" value="<?php echo $settings_["maps_daum_api_key"]; ?>"></td>
			</tr>
			<tr>
				<th><label for="maps_daum_map_marker_img_url">다음지도 마커이미지 URL</label></th>
				<td>
					<input type="text" name="maps_daum_map_marker_img_url" class="large-text" placeholder="URL" value="<?php echo $settings_["maps_daum_map_marker_img_url"]; ?>">
					<a href="javascript:kn_settings_maps_select_marker();" class="button button-secondary">선택</a>
				</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
	function kn_settings_maps_select_marker(){
		KN.select_media({callback : function(result_){
			var img_url_ = result_[0]["url"];
			jQuery("input[name='maps_daum_map_marker_img_url']").val(img_url_);
		}});
	}
	</script>

	<?php
	}
}

add_action("knadmin_settings_form",array("KNAdmin_maps_settings","_add_settings_form"));

?>