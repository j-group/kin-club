<?php

define("KN_SLUG_ADMIN_SETTINGS","kn-admin-settings");

class KNAdmin_settings extends KNAction{
	
	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){

		add_action("admin_footer", array("KNAdmin_settings", "add_script"));

		$button_html_ = "<div class='kn-text-right'>";
		$button_html_ .= "<a class='button button-primary' href='javascript:_kn_save_settings();'>변경사항 저장</a>";
		$button_html_ .= "</div>";

		$settings_ = KN_settings::get_settings();
	?>

	<div class="wrap kn-limit-content form-wrap">
		<h2>JG시스템설정</h2>

	<form id="kn-settings-form">
	<?php echo $button_html_; ?>

	<?php do_action("knadmin_settings_form",$settings_); ?>

	<?php echo $button_html_; ?>
	</form>
	</div>

	<?php
	}

	static function add_script(){
	?>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});
	function _kn_save_settings(){
		$ = jQuery;

		var settings_form_ = $("#kn-settings-form");
		var settings_data_ = settings_form_.serializeObject();

		KN.post("<h3>저장 중...</h3>",{
			action : "kn_admin_save_settings",
			settings : settings_data_
		},function(result_, response_text_, cause_){
			if(!result_){
				KN.error_popup({
					title : "에러발생",
					text : cause_,
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "저장완료",
				confirmButtonText : "확인"
			},function(){
				document.location = document.location;
			});
		});
	}
	</script>
	<?php
	}

	static function _ajax_save_settings(){
		if(!KN_settings::is_admin()){
			echo "false";
			die();
		}

		$settings_ = $_POST["settings"];
		KN_settings::put_settings($settings_);
		echo json_encode(array("success" => true));
		die();
	}
}

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_SETTINGS,
	"title" => "설정",
	"class" => "KNAdmin_settings",
	"param" => array(),
	"eparam" => array(),
	"permission" => "manage_options",
));

kn_add_ajax("kn_admin_save_settings",array("KNAdmin_settings", "_ajax_save_settings"));

?>