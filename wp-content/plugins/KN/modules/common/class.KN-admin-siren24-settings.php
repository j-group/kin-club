<?php

class KNAdmin_siren24_settings{

	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>사이렌24설정(본인인증)</h3>
	<table class="form-table">
		<tr>
			<th>회원사ID</th>
			<td><input type="text" name="siren24_id" value="<?php echo $settings_["siren24_id"]; ?>" class="large-text"></td>
		</tr>
		<!-- <tr>
			<th>서비스ID</th>
			<td><input type="text" name="siren24_service_id" value="<?php echo $settings_["siren24_service_id"]; ?>" class="large-text"></td>
		</tr> -->
		<tr>
			<th>부/복호화 명령어경로</th>
			<td><input type="text" name="siren24_command_path" value="<?php echo $settings_["siren24_command_path"]; ?>" class="large-text"></td>
		</tr>
		<tr>
			<th>콜백 페이지</th>
			<td>
				<?php echo kn_make_select_with_pages(array(
					"default" => $settings_["siren24_callback_page_id"],
					"name" => "siren24_callback_page_id",
					"class" => "large-text",
				)); ?>
				<p>*콜백 페이지를 설정하지 않으면, 정상적으로 작동하지 않습니다.</p>
			</td>
		</tr>
	</table>


	<?php
	}//001001
}

add_action("knadmin_settings_form",array("KNAdmin_siren24_settings", "_add_settings_form"));

?>