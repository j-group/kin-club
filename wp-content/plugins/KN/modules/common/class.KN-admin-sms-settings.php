<?php

class KNAdmin_sms_settings{

	static function _add_settings_form($settings_){

		add_action("admin_footer",array("KNAdmin_sms_settings", "_add_settings_script"));
	?>
	<br/>
	<hr/>
	<br/>
	<h3>SMS설정</h3>
	<table class="form-table">
		<tr>
			<th>발송URL</th>
			<td><input type="text" name="sms_url" value="<?php echo $settings_["sms_url"]; ?>" class="large-text"></td>
		</tr>
		<tr>
			<th>잔여조회URL</th>
			<td><input type="text" name="sms_remaining_count_url" style="width:100%;" value="<?php echo $settings_["sms_remaining_count_url"]; ?>" class="large-text"></td>
		</tr>
		<tr>
			<th>사용자ID / Secure</th>
			<td>
				<div class="kn-column-frame kn-column-padding">
					<div class="kn-column l50"><input type="text" name="sms_user_id" value="<?php echo $settings_["sms_user_id"]; ?>" class="large-text" placeholder="User ID"></div>
					<div class="kn-column l50"><input type="text" name="sms_secure" value="<?php echo $settings_["sms_secure"]; ?>" class="large-text" placeholder="Secure"></div>
				</div>
			</td>
		</tr>
		<tr>
			<th>보내는 번호</th>
			<td>
				<div class="kn-column-frame">
					<div class="kn-column l30 fix-width"><input type="text" name="sms_sphone1" value="<?php echo $settings_["sms_sphone1"]; ?>" class="large-text"></div>
					<div class="kn-column l5 fix-width"><center>-</center></div>
					<div class="kn-column l30 fix-width"><input type="text" name="sms_sphone2" value="<?php echo $settings_["sms_sphone2"]; ?>" class="large-text"></div>
					<div class="kn-column l5 fix-width"><center>-</center></div>
					<div class="kn-column l30 fix-width"><input type="text" name="sms_sphone3" value="<?php echo $settings_["sms_sphone3"]; ?>" class="large-text"></div>
				</div>
			</td>
		</tr>
		<tr>
			<th>잔여수조회</th>
			<td>
				<input type="text" id="kn-admin-sms-test-count" readonly class="small-text">
				<a onclick="kn_sms_get_remain_count()" class="button button-primary" id="kn-admin-sms-test-count-btn">조회</a>
			</td>
		</tr>
		<tr>
			<th>테스트발송</th>
			<td>
				<input type="text" id="kn-admin-sms-test-number" placeholder="수신번호">
				<a onclick="kn_sms_send_test()" class="button button-primary" id="kn-admin-sms-test-number-btn">발송창열기</a>
			</td>
		</tr>
	</table>


	<?php
	}

	static function _add_settings_script(){
	?>
	<script type="text/javascript">
	// jQuery(document).ready(function($){
	// 	kn_sms_get_remain_count();
	// });	
	// function kn_sms_get_remain_count(){
	// 	$ = jQuery;
	// 	var counter_ = $("#kn-admin-sms-test-count");
	// 	var button_ = $("#kn-admin-sms-test-count-btn");
	// 	button_.attr("disabled","disabled");
	// 	KN.sms.count(function(success_, result_){
	// 		button_.removeAttr("disabled");
	// 		counter_.val(result_);
	// 	});
	// }
	// function kn_sms_send_test(){
	// 	$ = jQuery;
	// 	var number_ = $("#kn-admin-sms-test-number").val();
	// 	KN.sms.openSender(number_);
	// }
	</script>
	<?php
	}
}

add_action("knadmin_settings_form",array("KNAdmin_sms_settings", "_add_settings_form"));

?>