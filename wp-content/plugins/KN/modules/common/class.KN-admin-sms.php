<?php

class KNAdmin_sms{

	static function _ajax_count_sms(){
		if(!current_user_can('manage_options')){
			echo json_encode(array("result" => "permissions_denied"));
			die();
			return;
		}

		echo json_encode(self::r_count());
		die();
	}

	static function _ajax_send_sms(){
		if(!current_user_can('manage_options')){
			echo json_encode(array("result" => "permissions_denied"));
			die();
			return;
		}

		$result_ = self::send(array(
			"to" => $_POST["numbers"],
			"message" => $_POST["message"],
			"sms_type" => "S"
		));

		echo json_encode($result_);
		die();
	}

	static function _add_javascript(){
		wp_enqueue_script("kn-sms", (KN_PLUGIN_DIR_URL . 'lib/js/sms/sms.js'), array("kn-main"));
		wp_enqueue_script("kn-sms-lang", (KN_PLUGIN_DIR_URL . 'lib/js/sms/lang/ko_KR.js'), array("kn-sms"));
	}

}

include_once(KN_PLUGIN_DIR_PATH.'modules/common/class.KN-admin-sms-settings.php');

?>