<?php

include_once(KN_PLUGIN_DIR_PATH."/modules/common/lib/captcha/simple-php-captcha.php");

class KN_captcha{

	static function create($param_ = array()){
		$captcha_ = simple_php_captcha($param_);
		KN_session::put("kn-captcha-code", $captcha_["code"]);
		return $captcha_;
	}

	static function verfity($code_){
		$tcode_ = KN_session::get("kn-captcha-code");
		$result_ = (strlen($tcode_) && $tcode_ === $code_);
		return $result_;
	}

}

function kn_captcha_create($param_ = array()){
	return KN_captcha::create($param_);
}
function kn_captcha_verfity($code_){
	return KN_captcha::verfity($code_);
}

?>