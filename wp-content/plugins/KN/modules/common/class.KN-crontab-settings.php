<?php

class KN_crontab_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"common_use_cron" => false,
			"common_cron_cycle" => null,
			
			"common_cron_perform_page_id" => null,
			"common_cron_perform_allow_access" => "all",
			"common_cron_perform_ip_white_list" => "",
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_crontab_settings", "_add_default_settings"));

?>