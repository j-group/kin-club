<?php

class KN_crontab{

	static function available(){
		$return_ = shell_exec("which crontab");
		return (empty($return_) ? false : true);
	}

	static private function _string_to_array($jobs_ = ""){
		$array_ = explode("\r\n", trim($jobs_));
		foreach($array_ as $key_ => $item_){
			if($item_ === ""){
				unset($array_[$key_]);
			}
		}
		return $array_;
	}

	static private function _array_to_string($jobs_ = array()){
		return implode("\r\n", $jobs_);
	}

	static function jobs(){
		$output_ = shell_exec("crontab -l");
		return self::_string_to_array($output_);
	}

	static function save_jobs($jobs_ = array()){
		$output_ = shell_exec(('echo "'.self::_array_to_string($jobs_).'" | crontab -'));
		return $output_;
	}

	static function job_exists($job_ = ""){
		$jobs_ = self::jobs();
		if(in_array($job_, $jobs_)){
			return true;
		}else{
			return false;
		}
	}

	static function add_job($job_ = ""){
		if(self::job_exists($job_)){
			return false;
		}

		$jobs_ = self::jobs();
		array_push($jobs_, $job_);
		return self::save_jobs($jobs_);
	}

	static function remove_job($job_ = ""){
		if(!self::job_exists($job_)){
			return false;
		}

		$jobs_ = self::jobs();
		unset($jobs_[array_search($job_, $jobs_)]);
		return self::save_jobs($jobs_);
	}

	static function _prevent_page(){
		$current_page_id_ = (string)get_the_ID();
		$settings_ = KN_settings::get_settings();
		
		$common_cron_perform_page_id_ = (string)$settings_["common_cron_perform_page_id"];

		if(!strlen($common_cron_perform_page_id_) || $current_page_id_ !== $common_cron_perform_page_id_){
			return;
		}

		if($settings_["common_cron_perform_allow_access"] === "admin" && !kn_is_admin()){
			wp_die("잘못된 접근입니다.");
		}

		$white_list_ = $settings_["common_cron_perform_ip_white_list"];
		$client_ip_ = kn_client_ip();

		if($settings_["common_cron_perform_allow_access"] === "all"
			&& strlen(trim($white_list_)) > 0 && $_SERVER['SERVER_ADDR'] !== $client_ip_){
			if(strpos($white_list_, ",") === false){
				$white_list_ = array(trim($white_list_));	
			}else{
				$white_list_ = explode (",", $white_list_);
			}
			
			$can_access_ = false;
			foreach($white_list_ as $row_index_ => $ip_){
				if(strpos($client_ip_, $ip_) !== false){
					$can_access_ = true;
					break;
				}
			}

			if(!$can_access_){
				wp_die("잘못된 접근입니다.");
			}
		}

		do_action("kn_crontab_task");
		die();

	}
}

add_action("template_redirect", array("KN_crontab", "_prevent_page"));

include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-crontab-settings.php');

?>