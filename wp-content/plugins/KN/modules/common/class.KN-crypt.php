<?php

include_once(KN_PLUGIN_DIR_PATH . 'modules/common/lib/bcrypt/bcrypt.php');

function kn_crypt_hash($password_){
	return password_hash($password_, PASSWORD_DEFAULT); 
}
function kn_crypt_verify($password_, $hash_){
	return password_verify($password_, $hash_);
}

?>