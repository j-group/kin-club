<?php

class KN_fileupload_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"fileupload_receipt_page_id" => "",
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_fileupload_settings", "_add_default_settings"));

?>