<?php

class KN_fileupload{

	static function register_library(){
		$settings_ = KN_settings::get_settings();
		$fileupload_page_id_ = $settings_["fileupload_receipt_page_id"];

		wp_register_style("kn-fileupload", (KN_PLUGIN_DIR_URL. 'lib/js/kn.fileupload/kn.fileupload.css'));
		
		wp_register_script("jquery-fileupload", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.fileupload/Uploader.js'), array("jquery"));
		wp_register_script("jquery-fileupload-ajax", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.fileupload/UploaderWithAjax.js'), array("jquery-fileupload"));
		wp_register_script("jquery-fileupload-iframe", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.fileupload/UploaderWithIframe.js'), array("jquery-fileupload"));

		wp_register_script("jquery-iframe-transport", (KN_PLUGIN_DIR_URL. 'lib/js/jquery.iframe-transport.js'), array("jquery"));
		wp_register_script("kn-fileupload", (KN_PLUGIN_DIR_URL. 'lib/js/kn.fileupload/kn.fileupload.js'), array("jquery","kn-main", "jquery-form", "jquery-iframe-transport"));

		wp_localize_script("kn-fileupload", "kn_fileupload_var", array(
			"upload_url" => get_permalink($fileupload_page_id_),
			"choose image" => "이미지선택",
			"remove image" => "삭제",
			"upload failed" => "파일업로드에 실패하였습니다.",
			"max file size error" => "파일업로드에 실패하였습니다.",
		));

		KN_Library::register("kn-fileupload",array(
			"script" => array(
				"jquery",
				"main",
				"jquery-fileupload",
				"jquery-fileupload-ajax",
				"jquery-fileupload-iframe",
				"kn-fileupload",
			),"style" => array(
				"kn-fileupload",
			)
		));
	}

	static function _prevent_page(){
		$current_page_id_ = (string)get_the_ID();
		$settings_ = KN_settings::get_settings();
		$fileupload_page_id_ = $settings_["fileupload_receipt_page_id"];

		if(strlen($fileupload_page_id_) && ((string)$fileupload_page_id_) === ((string)$current_page_id_)){
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

			$results_ = array();

			$target_files_ = $_FILES["files"];
			foreach($target_files_["name"] as $key_ => $value_){
				if($target_files_['name'][$key_]){
					$target_file_ = array(
						'name'     => $target_files_['name'][$key_],
						'type'     => $target_files_['type'][$key_],
						'tmp_name' => $target_files_['tmp_name'][$key_],
						'error'    => $target_files_['error'][$key_],
						'size'     => $target_files_['size'][$key_]
					);
					$results_[] = wp_handle_upload($target_file_, array('test_form' => FALSE));
				}
			}

			echo json_encode($results_);
			exit();
		}
	}
}

add_action("template_redirect", array("KN_fileupload", "_prevent_page"));
add_action("kn_library_init", array("KN_fileupload","register_library"));

include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-fileupload-settings.php');

?>