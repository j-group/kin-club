<?php

class KN_kakao_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"kakao_app_key" => "",
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_kakao_settings", "_add_default_settings"));

?>