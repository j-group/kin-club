<?php

class KN_kakao{

	static function _initialize(){
		$settings_ = KN_settings::get_settings();
		wp_register_style("kn-kakao", (KN_PLUGIN_DIR_URL . 'lib/js/kn.kakao/kn.kakao.css'));
		wp_register_script("kakao", '//developers.kakao.com/sdk/js/kakao.min.js');
		wp_register_script("kn-kakao", (KN_PLUGIN_DIR_URL . 'lib/js/kn.kakao/kn.kakao.js'),array("jquery","kakao"));
		wp_localize_script("kn-kakao", "kn_kakao_var", array(
			"app_key" => $settings_["kakao_app_key"],
		));

		KN_library::register("kn-kakao",array(
			"style" => array(
				"kn-kakao",
			),
			"script" => array(
				"jquery","kakao","kn-kakao",
			),
		));
	}
}

add_action("init", array("KN_kakao","_initialize"));

include_once(KN_PLUGIN_DIR_PATH.'modules/common/class.KN-kakao-settings.php');

?>