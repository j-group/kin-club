<?php

class KN_mail_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"email_sender" => "",
			"email_template" => "",
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_mail_settings", "_add_default_settings"));

?>