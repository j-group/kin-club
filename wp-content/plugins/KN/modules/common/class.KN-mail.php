<?php

class KN_mail{
	
	static function template($data_ = array()){
		$settings_ = KN_settings::get_settings();
		$email_template_ = $settings_["email_template"];
		$email_template_ = kn_map_string($email_template_, $data_);
	
		return stripslashes($email_template_);
	}

	static function send($to_, $subject_, $data_ = array(), $headers_ = null, $attachments_ = null){
		return wp_mail($to_, $subject_, self::template($data_), $headers_, $attachments_);
	}
}

function kn_mail_template($data_ = array()){
	return KN_mail::template($data_);
}
function kn_mail_send($to_, $subject_, $data_ = array(), $headers_ = null, $attachments_ = null){
	return KN_mail::send($to_, $subject_, $data_, $headers_, $attachments_);
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-mail-settings.php');

?>