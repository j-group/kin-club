<?php

class KN_maps_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"maps_daum_api_url" => "",
			"maps_daum_api_key" => "",
			
			"maps_daum_map_marker_img_url" => "",
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_maps_settings", "_add_default_settings"));

?>