<?php

class KN_maps{

	static function register_map_api_library(){
		$settings_ = KN_settings::get_settings();
		$api_key_ = $settings_["maps_daum_api_key"];
		$api_url_ = $settings_["maps_daum_api_url"];
		$marker_url_ = $settings_["maps_daum_map_marker_img_url"];

		wp_register_style("kn-maps", (KN_PLUGIN_DIR_URL. 'lib/js/kn.maps/kn.maps.css'));
		wp_register_style("kn-maps-popup", (KN_PLUGIN_DIR_URL. 'lib/js/kn.maps/kn.maps-popup.css'));

		wp_register_script("kn-maps-daum-api", (kn_make_url($api_url_, array("apikey" => $api_key_, "libraries" => "services"))), array("jquery"));
		wp_localize_script("kn-maps-daum-api","kn_maps_var",array(
			"marker_url" => $marker_url_
		));

		wp_register_script("kn-maps", (KN_PLUGIN_DIR_URL. 'lib/js/kn.maps/kn.maps.js'), array("jquery", "kn-main","kn-maps-daum-api"));
		wp_register_script("kn-maps-popup", (KN_PLUGIN_DIR_URL. 'lib/js/kn.maps/kn.maps-popup.js'), array("jquery", "kn-maps"));
		
		KN_Library::register("kn-maps",array(
			"script" => array(
				"jquery",
				"main",
				"kn-maps-daum-api",
				"kn-maps",
				"kn-maps-popup"
			),"style" => array(
				"kn-maps",
				"kn-maps-popup"
			),
		));
	}

	static function draw_address_popup(){
		ob_start();

?>
<div id="kn-maps-popup-address" class="mfp-hide kn-maps-popup-address kn-mg-popup">
	<h3>검색위치선택<a href="javascript:;" class="knbtn knbtn-secondary" id="kn-maps-popup-second-btn"></a></h3>
	<div class="wrap kn-form">
		<div class="left-content">
			<div class="search-frame">
				<input type="search" name="keyword" placeholder="장소, 건물, 지번, 도로명" id="kn-maps-popup-input-address">
				<a class="fa fa-search fa-2" href="javascript:;"></a>
			</div>
			<div class="search-results-frame"><ul class="search-results">
				
			</ul>
			<div class="first">검색창이나 지도로 위치를 선택합니다.</div>
			<div class="loading">검색 중...</div>
			<div class="norows">검색결과가 없습니다.</div>
		</div>
		</div>
		<div class="right-content">
			<div id="kn-address-map"></div>
			<div class="btn-frame"><a href="javascript:;" id="kn-address-map-current-geo-btn" class="knbtn knbtn-primary knbtn-small">현재위치</a></div>
		</div>
	</div>
</div>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function geolocation_ip($ip_){
		$url_ = "http://ip-api.com/php/";
		$result_ = null;

		if(is_callable("curl_init")){
			$curl_ = curl_init();
			curl_setopt($curl_, CURLOPT_URL, $url_.$ip_);
			curl_setopt($curl_, CURLOPT_HEADER, false);
			curl_setopt($curl_, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl_, CURLOPT_RETURNTRANSFER, true);
			$result_ = unserialize(curl_exec($curl_));
			curl_close($curl_);
		}else{
			$result_ = unserialize(file_get_contents($url_.$ip_));
		}

		return array(
			"status" => $result_["status"],
			"lat" => isset($result_["lat"]) ? $result_["lat"] : "",
			"lng" => isset($result_["lon"]) ? $result_["lon"] : "",
		);
	}

	static function _ajax_load_geolocation_ip(){
		$result_ = self::geolocation_ip(kn_client_ip());

		echo json_encode(array(
			"success" => ($result_["status"] === "success"),
			"data" => $result_
		));
		die();
	}
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/common/class.KN-maps-settings.php');

add_action("kn_library_init", array("KN_maps","register_map_api_library"));
kn_add_ajax("kn-maps-geolocation-ip", array("KN_maps", "_ajax_load_geolocation_ip"), true);

?>