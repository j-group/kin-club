<?php

include_once(KN_PLUGIN_DIR_PATH."/modules/common/lib/mobile-detect/Mobile_Detect.php");

function kn_is_mobile(){
	$detect = new Mobile_Detect;
	return $detect->isMobile();
}
function kn_is_ios(){
	$detect = new Mobile_Detect;
	return $detect->isiOS();
}

function kn_is_tablet(){
	$detect = new Mobile_Detect;
	return $detect->isTablet();
}

?>