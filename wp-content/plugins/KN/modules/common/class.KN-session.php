<?php

class KN_session{

	static function _initialize(){
		if(!session_id()){
			session_start();
		}
	}

	static function put($key_, $value_){
		self::_initialize();
		$_SESSION[$key_] = $value_;
		return $_SESSION[$key_];
	}
	static function get($key_){
		self::_initialize();
		if(!isset($_SESSION[$key_])) return null;
		return $_SESSION[$key_];
	}
	static function remove($key_){
		self::_initialize();
		if(!isset($_SESSION[$key_])) return null;
		unset($_SESSION[$key_]);
		return $_SESSION;
	}
}

function kn_session_put($key_, $value_){
	return KN_session::put($key_, $value_);
}
function kn_session_get($key_){
	return KN_session::get($key_);
}
function kn_session_remove($key_){
	return KN_session::remove($key_);
}

add_action("init", array("KN_session", "_initialize"));

?>