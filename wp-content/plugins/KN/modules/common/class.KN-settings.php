<?php

define("KN_KEY_SETTINGS", "kn-settings");

class KN_settings{

	static function admin_permission(){
		return "manage_options";
	}

	static function is_admin(){
		return current_user_can(self::admin_permission());
	}

	protected static function _get_default_settings(){
		$settings_ = apply_filters("kn_default_settings", array(
			/*common settings*/
		));

		return $settings_;
	}

	static function get_settings(){
		$settings_ = get_option(KN_KEY_SETTINGS,array());
		if(gettype($settings_) !== "array")
			$settings_ = array();

		return array_merge(self::_get_default_settings(),$settings_);
	}

	static function put_settings($settings_){
		do_action("kn_before_update_settings",self::get_settings());
		update_option(KN_KEY_SETTINGS, array_merge(self::_get_default_settings(),apply_filters("kn_update_settings", $settings_)));
		do_action("kn_after_update_settings",self::get_settings());
		return self::get_settings();
	}
}

function kn_is_admin(){
	return KN_settings::is_admin();
}

?>