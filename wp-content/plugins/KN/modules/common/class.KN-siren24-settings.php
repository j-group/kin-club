<?php

class KN_siren24_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"siren24_id" => "",
			"siren24_command_path" => "",
			"siren24_callback_page_id" => null,
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_siren24_settings", "_add_default_settings"));

?>