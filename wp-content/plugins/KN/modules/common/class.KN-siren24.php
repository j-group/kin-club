<?php

define("KN_SIREN24_KEY_REQNUM", "kn-siren24-request-num");

class KN_siren24{

	static function _add_library(){
		wp_register_script("kn-siren24", (KN_PLUGIN_DIR_URL . 'lib/js/kn.siren24/kn.siren24.js'),array("jquery","kn-main"));

		KN_Library::register("kn-siren24",array(
			"script" => array(
				"kn-siren24",
			),
		));
	}

	static function is_error($dec_data_){
		return (is_numeric($dec_data_) && $dec_data_ < 0);
	}

	static function _prevent_page(){
		$settings_ = KN_settings::get_settings();
		$callback_page_id_ = (string)$settings_["siren24_callback_page_id"];
		$current_page_id_ = (string)get_the_ID();

		if($callback_page_id_ !== $current_page_id_) return;

		if(!isset($_REQUEST["retInfo"])){
			wp_die("잘못된 접근입니다.");
		}

		$dec_data_ = self::decode_enc_data();

		?>
		<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo get_bloginfo("language"); ?>"><head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo get_bloginfo("charset"); ?>">
	<?php if(self::is_error($dec_data_)){ ?>
		<script type="text/javascript">
		//error : "<?php echo $dec_data_; ?>";
		opener.KN.siren24._auth_data = null;
		window.close();
		</script>
	<?php }else{ ?>
		<script type="text/javascript">
		opener.KN.siren24._auth_data = <?= self::_convert_javascript_json_data($dec_data_); ?>;
		window.close();
		</script>
	<?php } ?>
		</head></html>
		<?php

		die();
	}

	static function _convert_javascript_json_data($dec_data_){
		$result_ = '';
		$result_ .= '{
			"name" : "'.$dec_data_["name"].'",
			"birth_ymd" : "'.$dec_data_["birth_ymd"].'",
			"sex" : "'.$dec_data_["sex"].'",
			"di" : "'.$dec_data_["di"].'",
			"ci1" : "'.$dec_data_["ci1"].'",
			"ci2" : "'.$dec_data_["ci2"].'",
			"ci_version" : "'.$dec_data_["ci_version"].'",
			"request_num" : "'.$dec_data_["request_num"].'",
			"result" : "'.$dec_data_["result"].'",
			"cert_gb" : "'.$dec_data_["cert_gb"].'",
			"cell_num" : "'.$dec_data_["cell_num"].'",
			"cell_corp" : "'.$dec_data_["cell_corp"].'",
			"cert_date" : "'.$dec_data_["cert_date"].'",
			"add_var" : "'.$dec_data_["add_var"].'",

			"ext1" : "'.$dec_data_["ext1"].'",
			"ext2" : "'.$dec_data_["ext2"].'",
			"ext3" : "'.$dec_data_["ext3"].'",
			"ext4" : "'.$dec_data_["ext4"].'"
		}';
		return $result_;
	}

	static function gen_request_num(){
		self::remove_request_num();
		date_default_timezone_set("Asia/Seoul");
		$request_number_ = date("YmdHis")."".rand(100000, 999999);
		kn_session_put(KN_SIREN24_KEY_REQNUM, $request_number_);
		return $request_number_;
	}
	static function request_num(){
		$request_number_ = kn_session_get(KN_SIREN24_KEY_REQNUM);
		if($request_number_ === null) return false;
		return $request_number_;
	}

	static function remove_request_num(){
		kn_session_remove(KN_SIREN24_KEY_REQNUM);
	}

	static function enc_request_info($service_id_ = "", $cert_gb_ = "H", $add_var_ = null){
		$settings_ = KN_settings::get_settings();
		$siren24_id_ = $settings_["siren24_id"];
		$siren24_command_path_ = $settings_["siren24_command_path"];
		$siren24_return_url_ = get_permalink($settings_["siren24_callback_page_id"]);

		if(kn_is_https()){
			$siren24_return_url_ = kn_make_https($siren24_return_url_);			
		}

		$request_number_ = self::gen_request_num();
		$ex_var_ = "0000000000000000";
		date_default_timezone_set("Asia/Seoul");
		$request_info_ = $siren24_id_ . "^" . $service_id_ . "^" . $request_number_ . "^" . date("YmdHis") . "^" . $cert_gb_ . "^" . $add_var_ . "^" . $ex_var_;

		if(preg_match('~[^0-9a-zA-Z+/=^]~', $request_info_, $matches_)){
			wp_die(new WP_Error(-1, "잘못된 접근입니다."));
		}
		$enc_request_info_ = exec("$siren24_command_path_ SEED 1 1 $request_info_ ");
		$hmac_str = exec("$siren24_command_path_ HMAC 1 1 $enc_request_info_ ");
		$enc_request_info_ = $enc_request_info_. "^" .$hmac_str. "^" .$ex_var_;
		$enc_request_info_ = exec("$siren24_command_path_ SEED 1 1 $enc_request_info_ ");

		if(preg_match('~[^0-9a-zA-Z+/=^]~', $request_info_, $matches_)){
			wp_die(new WP_Error(-1, "잘못된 접근입니다."));
			return false;
		}

		return array(
			"enc_data" => $enc_request_info_,
			"return_url" => $siren24_return_url_,
			"request_num" => self::request_num(),
		);
	}

	static function has_enc_data(){
		return (isset($_REQUEST["retInfo"]) && strlen(isset($_REQUEST["retInfo"])));
	}

	static function decode_enc_data(){
		$enc_data_ = $_REQUEST["retInfo"];
		$request_num_ = self::request_num();
		if($request_num_ === false) return -1;

		self::remove_request_num();

		if(preg_match('~[^0-9a-zA-Z+/=^]~', $request_num_, $matches_) || preg_match('~[^0-9a-zA-Z+/=^]~', $enc_data_, $matches_)){
			return -3;
		}

		$settings_ = KN_settings::get_settings();
		$siren24_command_path_ = $settings_["siren24_command_path"];
		
		$dec_data_ = exec("$siren24_command_path_ SEED 2 0 $request_num_ $enc_data_ ");

		$total_info_ = split("\\^", $dec_data_);
		$enc_params_  = $total_info_[0];		//본인확인1차암호화값
		$enc_msg_   = $total_info_[1];		//암호화된 통합 파라미터의 위변조검증값

		if(preg_match('~[^0-9a-zA-Z+/=^]~', $enc_params_, $matches_)){
			return -5;
		}
		$hmac_str_ = exec("$siren24_command_path_ HMAC 1 0 $enc_params_ ");

		if($hmac_str_ !== $enc_msg_){
			return -7;
		}

		if(preg_match('~[^0-9a-zA-Z+/=^]~', $request_num_, $matches_)||preg_match('~[^0-9a-zA-Z+/=^]~', $enc_params_, $matches_)){
			return -9;
		}

		$dec_params_ = exec("$siren24_command_path_ SEED 2 0 $request_num_ $enc_params_ ");

		$split_dec_ret_info_ = split("\\^", $dec_params_);

		return array(
			"name" => $split_dec_ret_info_[0],
			"birth_ymd" => $split_dec_ret_info_[1],
			"sex" => $split_dec_ret_info_[2],
			"foreigner_gbn" => $split_dec_ret_info_[3],
			"di" => $split_dec_ret_info_[4],
			"ci1" => $split_dec_ret_info_[5],
			"ci2" => $split_dec_ret_info_[6],
			"ci_version" => $split_dec_ret_info_[7],
			"request_num" => $split_dec_ret_info_[8],
			"result" => $split_dec_ret_info_[9],
			"cert_gb" => $split_dec_ret_info_[10],
			"cell_num" => $split_dec_ret_info_[11],
			"cell_corp" => $split_dec_ret_info_[12],
			"cert_date" => $split_dec_ret_info_[13],
			"add_var" => $split_dec_ret_info_[14],

			"ext1" => $split_dec_ret_info_[15],
			"ext2" => $split_dec_ret_info_[16],
			"ext3" => $split_dec_ret_info_[17],
			"ext4" => $split_dec_ret_info_[18],
			"ext5" => $split_dec_ret_info_[19],
		);
	}
}

include_once(KN_PLUGIN_DIR_PATH.'modules/common/class.KN-siren24-settings.php');

kn_add_ajax("kn-siren24-enc-request-info", array("KN_siren24", "_ajax_enc_request_info"), true);
add_action("template_redirect", array("KN_siren24", "_prevent_page"));
add_action("kn_library_init", array("KN_siren24", "_add_library"));

?>