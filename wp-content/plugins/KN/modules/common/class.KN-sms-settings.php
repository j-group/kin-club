<?php

class KN_sms_settings{

	static function get_send_options(){
		$settings_ = KN_settings::get_settings();
		return array(
			"S" => array((int)$settings_["sms_send_options_s_min"],
				(int)$settings_["sms_send_options_s_max"],
				(int)$settings_["sms_send_options_s_use"],
				$settings_["sms_send_options_s_msg"]),
			"L" => array((int)$settings_["sms_send_options_l_min"],
				(int)$settings_["sms_send_options_l_max"],
				(int)$settings_["sms_send_options_l_use"],
				$settings_["sms_send_options_l_msg"]),
		);
	}

	static function _add_default_settings($settings_){
		return array_merge($settings_,array(
			"sms_url" => "http://sslsms.cafe24.com/sms_sender.php",
			"sms_remaining_count_url" => "http://sslsms.cafe24.com/sms_remain.php",
			"sms_user_id" => "",
			"sms_secure" => "",
			"sms_sphone1" => "",
			"sms_sphone2" => "",
			"sms_sphone3" => "",
			"sms_send_options_s_min" => 0,
			"sms_send_options_s_max" => 90,
			"sms_send_options_s_use" => 1,
			"sms_send_options_s_msg" => "단문, 1개당 1건차감",
			"sms_send_options_l_min" => 91,
			"sms_send_options_l_max" => 2000,
			"sms_send_options_l_use" => 3,
			"sms_send_options_l_msg" => "장문, 1개당 3건차감",
		));
	}
}

add_filter("kn_default_settings", array("KN_sms_settings", "_add_default_settings"));

?>