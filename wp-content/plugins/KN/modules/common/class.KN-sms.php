<?php

define("KNSMS_ERROR_CODE_CONNECTION_FAILED", "connection_faeild");
define("KNSMS_ERROR_CODE_SENDING_FAILED", "sending_faeild");

class KN_sms{

	static function r_count(){
		$settings_ = KN_settings::get_settings();

		$sms_url_ = $settings_["sms_remaining_count_url"];

		$sms_ = array();

		$sms_["user_id"] = base64_encode($settings_["sms_user_id"]);
		$sms_["secure"] = base64_encode($settings_["sms_secure"]);
		$sms_['mode'] = base64_encode("1");

		$host_info_ = explode("/", $sms_url_);
		$host_ = $host_info_[2];
		$path_ = $host_info_[3];
		srand((double)microtime()*1000000);
		$boundary_ = "---------------------".substr(md5(rand(0,32000)),0,10);

		$header_ = "POST /".$path_ ." HTTP/1.0\r\n";
		$header_ .= "Host: ".$host_."\r\n";
		$header_ .= "Content-type: multipart/form-data, boundary=".$boundary_."\r\n";

		$data_ = "";
		foreach($sms_ AS $index_ => $value_){
			$data_ .="--$boundary_\r\n";
			$data_ .= "Content-Disposition: form-data; name=\"".$index_."\"\r\n";
			$data_ .= "\r\n".$value_."\r\n";
			$data_ .="--$boundary_\r\n";
		}
		$header_ .= "Content-length: " . strlen($data_) . "\r\n\r\n";

		$fp_ = fsockopen($host_, 80);

		if($fp_){
			fputs($fp_, $header_.$data_);
			$rsp_ = '';
			while(!feof($fp_)){
				$rsp_ .= fgets($fp_,8192);
			}
			fclose($fp_);
			$msg_ = explode("\r\n\r\n",trim($rsp_));
			
			return array(
				"result" => "success",
				"count" => $msg_[1]
			);
		}
		else{
			return new WP_Error(KNSMS_ERROR_CODE_CONNECTION_FAILED, $msg_[1], KNDOMAIN);
		}
	}

	static function send($t_numbers_, $message_, $sms_type_ = "S"){
		$t_numbers_count_ = count($t_numbers_);
		$numbers_ = "";
		for($index_=0;$index_<$t_numbers_count_;++$index_){
			if($index_ > 0) $numbers_ .= ",";
			$numbers_ .= $t_numbers_[$index_];
		}

		$settings_ = KN_settings::get_settings();

		$sms_url_ = $settings_["sms_url"];

		$sms_ = array();

		$sms_["user_id"] = base64_encode($settings_["sms_user_id"]);
		$sms_["secure"] = base64_encode($settings_["sms_secure"]);
		$sms_["sphone1"] = base64_encode($settings_["sms_sphone1"]);
		$sms_["sphone2"] = base64_encode($settings_["sms_sphone2"]);
		$sms_["sphone3"] = base64_encode($settings_["sms_sphone3"]);
		$sms_['mode'] = base64_encode("1") ;
		$sms_['smsType'] = base64_encode($sms_type_);

		$sms_["destination"] = base64_encode($numbers_);
		$sms_['msg'] = base64_encode(stripslashes($message_));

		$host_info_ = explode("/", $sms_url_);
		$host_ = $host_info_[2];
		$path_ = $host_info_[3];

		srand((double)microtime()*1000000);
		$boundary_ = "---------------------".substr(md5(rand(0,32000)),0,10);

		$header_ = "POST /".$path_ ." HTTP/1.0\r\n";
		$header_ .= "Host: ".$host_."\r\n";
		$header_ .= "Content-type: multipart/form-data, boundary=".$boundary_."\r\n";

		$data_ = "";
		foreach($sms_ AS $index_ => $value_){
			$data_ .="--$boundary_\r\n";
			$data_ .= "Content-Disposition: form-data; name=\"".$index_."\"\r\n";
			$data_ .= "\r\n".$value_."\r\n";
			$data_ .="--$boundary_\r\n";
		}
		$header_ .= "Content-length: " . strlen($data_) . "\r\n\r\n";

		$fp_ = fsockopen($host_, 80);

		if($fp_){
			fputs($fp_, $header_.$data_);
			$rsp_ = '';
			while(!feof($fp_)) { 
				$rsp_ .= fgets($fp_,8192); 
			}
			fclose($fp_);
			$msg_ = explode("\r\n\r\n",trim($rsp_));
			$t_result_ = explode(",", $msg_[1]);
			$result_ = ($t_result_[0] === "reserved" ? "success" : $t_result_[0]); //result
			$remaining_count_ = $t_result_[1];

			if($result_ === "success"){
				return array(
					"result" => ($result_ === "reserved" ? "success" : $result_),
					"count" => $remaining_count_
				);
			}else{
				return new WP_Error(KNSMS_ERROR_CODE_SENDING_FAILED, "SMS sending failed", KNDOMAIN);
			}
		}else{
			return new WP_Error(KNSMS_ERROR_CODE_CONNECTION_FAILED, "connection failed to SMS Server", KNDOMAIN);
		}
	}
}

function kn_sms_send($t_numbers_, $message_, $sms_type_ = "S"){
	return KN_sms::send($t_numbers_, $message_, $sms_type_);
}
function kn_sms_count(){
	return KN_sms::r_count();
}

include_once(KN_PLUGIN_DIR_PATH.'modules/common/class.KN-sms-settings.php');

?>