<?php

class KN_utils{

	static function register_utils_api_library(){
		wp_register_style("kn-utils", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils.css'));
		wp_register_style("kn-utils-timepicker", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils-timepicker.css'));
		wp_register_style("kn-utils-datepicker", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils-datepicker.css'));

		wp_register_script("kn-utils", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils.js'), array("jquery","kn-main"));
		wp_register_script("kn-utils-timepicker", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils-timepicker.js'), array("jquery", "kn-utils"));
		wp_register_script("kn-utils-datepicker", (KN_PLUGIN_DIR_URL. 'lib/js/kn.utils/kn.utils-datepicker.js'), array("jquery", "kn-utils"));
		
		KN_Library::register("kn-utils",array(
			"script" => array(
				"jquery",
				"main",
				"kn-utils",
				"kn-utils-timepicker",
				"kn-utils-datepicker",
			),"style" => array(
				"kn-utils",
				"kn-utils-timepicker",
				"kn-utils-datepicker",
			),"library" => array(
				"timepicker"
			)
		));
	}

	static function draw_timepicker_popup(){
		ob_start();

?>
	<div id="kn-utils-popup-timepicker" class="mfp-hide kn-utils-popup-timepicker kn-mg-popup">
	<h3>시간선택</h3>
	<div class="wrap kn-form">
		<div class="kn-column-frame kn-column-padding">
			<div class="kn-column l30 fix-width"><span class="input-for-label">시작시간</span></div>
			<div class="kn-column l70 fix-width">
				<input id="kn-utils-popup-timepicker-start-time" type="text" autocomplete="off" placeholder="시작시간선택">
			</div>
		</div>
		<div class="kn-column-frame kn-column-padding">
			<div class="kn-column l30 fix-width"><span class="input-for-label">종료시간</span></div>
			<div class="kn-column l70 fix-width">
				<input id="kn-utils-popup-timepicker-end-time" type="text" autocomplete="off" placeholder="종료시간선택">
			</div>
		</div>
		
		
	</div>
	<div class="button-area">
		<a class="knbtn knbtn-primary" id="kn-utils-popup-timepicker-pickup">선택하기</a>
	</div>
</div>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function draw_datepicker_popup(){
		ob_start();

?>
	<div id="kn-utils-popup-datepicker" class="mfp-hide kn-utils-popup-datepicker kn-mg-popup">
	<h3>시간선택</h3>
	<div class="wrap kn-form">
		<div class="kn-column-frame kn-column-padding">
			<div class="kn-column l30 fix-width"><span class="input-for-label">시작일자</span></div>
			<div class="kn-column l70 fix-width">
				<input id="kn-utils-popup-datepicker-start-date" type="text" autocomplete="off" placeholder="시작일자선택">
				<div id="kn-utils-popup-datepicker-start-date-cal"></div>
			</div>
		</div>
		<div class="kn-column-frame kn-column-padding">
			<div class="kn-column l30 fix-width"><span class="input-for-label">종료일자</span></div>
			<div class="kn-column l70 fix-width">
				<input id="kn-utils-popup-datepicker-end-date" type="text" autocomplete="off" placeholder="종료일자선택">
				<div id="kn-utils-popup-datepicker-end-date-cal"></div>
			</div>
		</div>
		
		
	</div>
	<div class="button-area">
		<a class="knbtn knbtn-primary" id="kn-utils-popup-datepicker-pickup">선택하기</a>
	</div>
</div>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

/**
워드프레스 AJAX 요청을 추가합니다.
*/
function kn_add_ajax($action_, $class_, $add_nopriv_ = false){
	add_action("wp_ajax_".$action_, $class_);
	if($add_nopriv_) add_action("wp_ajax_nopriv_".$action_, $class_);
}

/**
현재 관리자메뉴의 화면옵션을 반환합니다.
*/
function kn_get_screen_option($key_){
	$user_id_ = get_current_user_id();
	$screen_ = get_current_screen();
	$option_ = $screen_->get_option($key_, "option");

	$value_ = get_user_meta($user_id_, $option_, true);
	if(!strlen($value_)){
		$value_ = $screen_->get_option($key_, "default");		
	}

	return $value_;
}

/**
줄바꿈을 <br>태그로 변환합니다.
*/
function kn_nl2br($value_){
	return str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br/>", $value_);
}

/**
램덤 문자열을 반환합니다.
*/
function kn_random_string($length_ = 20, $characters_ = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"){
	$characters_length_ = strlen($characters_);
	$random_string_ = "";
	for($i_ = 0; $i_ < $length_; $i_++){
		$random_string_ .= $characters_[rand(0, $characters_length_ - 1)];
	}
	return $random_string_;
}

/**
URL을 생성합니다.
*/
function kn_make_url($base_url_, $params_, $make_ssl_ = false){
	$concat_char_ = "?";

	foreach($params_ as $key_ => $value_){
		if(strpos($base_url_, "?") > 0)
			$concat_char_ = "&";

		$base_url_ .= $concat_char_.$key_."=".$value_;			
	}

	return $base_url_;
}

/**
URL의 ssl 여부를 체크합니다.
*/
function kn_is_https($url_ = null){
	if(!strlen($url_)){
		$url_ = kn_current_url();
	}

	return (strpos($url_, "https") === 0);
}

/**
URL을 ssl로 변환하여 반환합니다.
*/
function kn_make_https($url_ = null){
	if(!strlen($url_)){
		$url_ = kn_current_url();
	}

	if(kn_is_https($url_)){
		return $url_;
	}

	return preg_replace('|^http://|', 'https://', $url_);
}

/**
현재 URL을 반환합니다.
*/
function kn_current_url(){
	$page_url_ = "http";
	if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === "on")
		$page_url_ .= "s";

	$page_url_ .= "://";
	$page_url_ .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	
	return $page_url_;
}

/**
option 태그를 만듭니다.
*/
function kn_make_options($data_, $default_ = null){
	$html_ = "";
	foreach($data_ as $value_ => $name_){
		$html_ .= '<option value="'.$value_.'" '.((isset($default_) && ($default_) === $value_) ? "selected" : "").'>'.$name_.'</option>';
	}

	return $html_;
}

/**
워드프레스 페이지를 select 태그로 반환합니다.
*/
function kn_make_select_with_pages($args_ = array()){
	$page_list_ = $pages = get_pages($args_);

	$html_ = '<select '.(isset($args_["name"]) ? 'name="'.$args_["name"].'"' : "").' '.(isset($args_["class"]) ? 'class="'.$args_["class"].'"' : "").'>';

	$html_ .= '<option value="">-페이지선택-</option>';
	foreach($page_list_ as $row_index_ => $page_){
		$html_ .= '<option value="'.$page_->ID.'" '.((isset($args_["default"]) && ((string)$args_["default"]) === (string)$page_->ID) ? "selected" : "").'>'.'('.$page_->ID.') '.$page_->post_title.'</option>';
	}

	$html_ .= '</select>';

	return $html_;
}

/**
클라이언트IP를 반환합니다.
*/
function kn_client_ip(){
	$ipaddress_ = false;
	
	if(isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress_ = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress_ = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress_ = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress_ = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress_ = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress_ = $_SERVER['REMOTE_ADDR'];

	return $ipaddress_;
}

// function kn_latlng_near($lat_, $lng_, $kilometers_ = 0){
// 	$difference_ = 0.07;
// 	$distance_ = $kilometers_ != 0 ? ceil($kilometers_ / 111) : 0;

// 	return array(
// 		"lat_from" => (floatval($lat_) - $difference_ - $distance_),
// 		"lat_to" => (floatval($lat_) + $difference_ + $distance_),
// 		"lng_from" => (floatval($lng_) - $difference_ - $distance_),
// 		"lng_to" => (floatval($lng_) + $difference_ + $distance_),
// 	);
// }

function _KN_MAP($maps_, $key_, $default_ = null){
	$value_ = isset($maps_[$key_]) ? $maps_[$key_] : $default_;

	if(gettype($value_) === "string")
		return (strlen($value_) ? $value_ : $default_);
	
	return $value_;
}
function KN_REQUEST($key_, $default_ = null){
	return _KN_MAP($_REQUEST, $key_, $default_);
}
function KN_GET($key_, $default_ = null){
	return _KN_MAP($_GET, $key_, $default_);
}
function KN_POST($key_, $default_ = null){
	return _KN_MAP($_POST, $key_, $default_);
}

function kn_map_string($string_, $map_){
	$result_ = $string_;
	foreach($map_ as $key_ => $value_){
		$result_ = str_replace("{".$key_."}",$value_,$result_);
	}

	return $result_;
}
function kn_pretty_number($number_){
	if(ABS($number_) > 1000000){
		return number_format(floor($number_/1000000))."m";
	}else if(ABS($number_) > 1000){
		return number_format(floor($number_/1000))."k";
	}else return number_format($number_);
}

function kn_query_latlng_distance($olat_, $olng_, $tlat_, $tlng_){
	$olat_ = strlen($olat_) ? $olat_ : "NULL";
	$olng_ = strlen($olng_) ? $olng_ : "NULL";
	$tlat_ = strlen($tlat_) ? $tlat_ : "NULL";
	$tlng_ = strlen($tlng_) ? $tlng_ : "NULL";

	return "(1000 * 3956 * 2 * ASIN(SQRT( POWER(SIN(($olat_ - $tlat_) * pi()/180 / 2), 2) +COS($olat_ * pi()/180) * COS($tlat_ * pi()/180) *POWER(SIN(($olng_ - $tlng_) * pi()/180 / 2), 2) )))";
}

define("KN_QUERY_NULL","##__KN_NULL__##");
define("KN_QUERY_NOW","##__KN_NOW__##");
function KN_QUERY_FUNC($func_){
	return "##__KN_FUNC:".$func_.":KN_FUNC__##";
}

function _kn_filter_query_null_mapping($query_){
	$query_ = str_replace(("'".KN_QUERY_NULL."'"), "NULL", $query_);
	$query_ = str_replace(("'".KN_QUERY_NOW."'"), "NOW()", $query_);
	$query_ = str_replace(("'##__KN_FUNC:"), "", $query_);
	$query_ = str_replace((":KN_FUNC__##'"), "", $query_);

	return $query_;
}
add_filter("query", "_kn_filter_query_null_mapping");


function kn_query_insert($table_name_, $data_, $data_format_ = null, $replace_ = false){
	global $wpdb;

	$values_ = array();
	$query_data_column_ = "";
	$query_data_format_ = "";
	$current_index_ = 0;
	foreach($data_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_data_column_ .= ",";
			$query_data_format_ .= ",";
		}

		$query_data_column_ .= $column_name_;
		$query_data_format_ .= isset($data_format_[$current_index_]) ? $data_format_[$current_index_] : "%s";
		$values_[] = $column_value_;

		++$current_index_;
	}

	$query_ = ($replace_ ? "REPLACE" : "INSERT")." INTO {$table_name_} ($query_data_column_) VALUES ($query_data_format_)";


	return $wpdb->prepare($query_, $values_);
}

function kn_query_update($table_name_, $data_, $where_, $data_format_ = null, $where_format_ = null){
	global $wpdb;

	$values_ = array();
	$query_data_column_ = "";
	$query_where_column_ = "";

	$current_index_ = 0;
	foreach($data_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_data_column_ .= ",";
		}

		$query_data_column_ .= $column_name_." = ".(isset($data_format_[$current_index_]) ? $data_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	$current_index_ = 0;
	foreach($where_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_where_column_ .= " AND ";
		}

		$query_where_column_ .= $column_name_." = ".(isset($where_format_[$current_index_]) ? $where_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	return $wpdb->prepare("UPDATE {$table_name_} SET $query_data_column_ WHERE $query_where_column_", $values_);
}

function kn_query_delete($table_name_, $where_, $where_format_ = null){
	global $wpdb;

	$values_ = array();
	$query_where_column_ = "";

	$current_index_ = 0;
	foreach($where_ as $column_name_ => $column_value_){
		if($current_index_ > 0){
			$query_where_column_ .= " AND ";
		}

		$query_where_column_ .= $column_name_." = ".(isset($where_format_[$current_index_]) ? $where_format_[$current_index_] : "%s");
		$values_[] = $column_value_;

		++$current_index_;
	}

	return $wpdb->prepare("DELETE FROM {$table_name_} WHERE $query_where_column_", $values_);
}

function kn_query_day_kor($date_column_){
	return "SUBSTR( _UTF8'일월화수목금토', DAYOFWEEK($date_column_), 1)";
}

add_action("kn_library_init", array("KN_utils","register_utils_api_library"));

?>