<?php

define("KN_SLUG_ADMIN_GCODE_LIST","kn-gcode-list");

class KNAdmin_gcode_list extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
		kn_load_library("jtable");
		add_action("admin_head", array("KNAdmin_gcode_list", "add_script"));
	}

	static function _draw_view(){
		
	?>

	<div class="wrap">
	<h2>공통코드</h2>
	<br/>
	<div id="kn-gcode-list"></div>
	
	</div><!-- wrap-->

	<?php
	}

	static function add_script(){
	?>

	<script type="text/javascript">
	jQuery(document).ready(function($){
		var gcode_list_ = $("#kn-gcode-list");

		var url_concat_char_ = "?";
		if(ajaxurl.indexOf(url_concat_char_) >= 0){
			url_concat_char_ = "&";
		}

		var gcode_list_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-list"
		});
		var gcode_create_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-create"
		});
		var gcode_update_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-update"
		});
		var gcode_delete_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-delete"
		});

		var gcode_dlist_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-dtl-list"
		});
		var gcode_dcreate_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-dtl-create"
		});
		var gcode_dupdate_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-dtl-update"
		});
		var gcode_ddelete_action_ = ajaxurl+url_concat_char_+$.param({
			action : "kn-admin-gcode-dtl-delete"
		});

		gcode_list_.jtable({
			title : "공통코드내역",
			paging: true,
			animationsEnabled : false,
			actions : {
				listAction : gcode_list_action_,
				createAction : gcode_create_action_,
				updateAction : gcode_update_action_,
				deleteAction : gcode_delete_action_
			},
			fields : {
				R_CODE_ID : {
					key : true,
					list : false
				},
				CODE_ID : {
					title : "코드ID",
					width : "5%",
					input : function(data_){
						var value_ = (data_.record === undefined ? "" : data_.record["CODE_ID"]);
						return "<input type='text' maxlength='5' name='CODE_ID' value='"+value_+"'>";
					},
					display: function (data_) {
						return '<b>'+data_.record["CODE_ID"]+'</b>';
					}
				},
				CODE_NM : {
					title : "코드명"
				},
				CODE_DESC : {
					title : "코드설명"
				},
				USE_YN : {
					title : "사용여부",
					width : "5%",
					options : {"Y" : "사용", "N" : "미사용"}
				},
				GCODE_DTL : {
					title : "",
					width : "1%",
					edit : false,
					create : false,
					display : function(gcode_){
						var button_icon_ = $("<span class='fa fa-bars'></span>");
						button_icon_.click(function(){
							gcode_list_.jtable('openChildTable', button_icon_.closest("tr"),{
								title : gcode_.record["CODE_NM"]+" - 상세코드",
								paging: true,
								animationsEnabled : false,
								recordUpdated : function(event_, data_){
									$(event_.target).jtable("reload");
								},
								recordAdded : function(event_, data_){
									$(event_.target).jtable("reload");
								},
								actions : {
									listAction : gcode_dlist_action_+"&code_id="+gcode_.record["CODE_ID"],
									createAction : gcode_dcreate_action_,
									updateAction : gcode_dupdate_action_,
									deleteAction : gcode_ddelete_action_+"&code_id="+gcode_.record["CODE_ID"]
								},
								fields : {
									CODE_ID : {
										type : "hidden",
										defaultValue : gcode_.record["CODE_ID"]
									},
									R_CODE_DID : {
										key : true,
										list : false
									},
									CODE_DID : {
										title : "코드상세ID",
										width : "5%",
										input : function(data_){
											var value_ = (data_.record === undefined ? "" : data_.record["CODE_DID"]);
											return "<input type='text' maxlength='5' name='CODE_DID' value='"+value_+"'>";
										}
									},
									CODE_DNM : {
										title : "코드상세명"
									},
									CODE_DDESC : {
										title : "코드상세설명"
									},
									USE_YN : {
										title : "사용여부",
										width : "5%",
										options : {"Y" : "사용", "N" : "미사용"}
									},
									SORT_CHAR : {
										title : "정령순서",
										width : "5%",
										type : "number"
									}
								}
							},function (data) { //opened handler
								data.childTable.jtable('load');
							});
						});
						return button_icon_;
					}
				}
			}
		});
		gcode_list_.jtable('load');

	});

	function kn_gcode_load_dlist(code_id_){
		var gcode_dlist_ = $("#kn-gcode-dlist");


	}
	</script>
	<?php
	}

	static function _ajax_get_gcode_list(){
		$page_index_ = isset($_GET["jtStartIndex"]) ? (int)$_GET["jtStartIndex"] : 0;
		$page_size_ = isset($_GET["jtPageSize"]) ? (int)$_GET["jtPageSize"] : 0;
		
		global $wpdb;

		$total_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_gcode")->CNT;

		$query_ = "SELECT
			CODE_ID R_CODE_ID, 
			CODE_ID,
			CODE_NM,
			CODE_DESC,
			USE_YN
		FROM {$wpdb->base_prefix}kn_gcode gcd
		WHERE 1=1
		ORDER BY CODE_ID ASC
		LIMIT $page_index_, $page_size_";

		$result_ = $wpdb->get_results($query_,ARRAY_A);
		print json_encode(array(
			"Result" => "OK",
			"Records" => $result_,
			"TotalRecordCount" => $total_count_,
		));
		die();
	}

	static function _ajax_create_gcode(){
		$code_id_ = $_POST["CODE_ID"];

		if(kn_gcode($code_id_) === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "이미 존재하는 코드ID"));
			die();	
			return;
		}

		$insert_data_ = array(
			"CODE_ID" => $code_id_,
			"CODE_NM" => $_POST["CODE_NM"],
			"CODE_DESC" => $_POST["CODE_DESC"],
			"USE_YN" => $_POST["USE_YN"],
		);
		global $wpdb;
		$result_ = $wpdb->insert("{$wpdb->base_prefix}kn_gcode",$insert_data_);

		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 추가실패"));
			die();	
			return;
		}

		$insert_data_["R_CODE_ID"] = $code_id_;

		print json_encode(array(
			"Result" => "OK",
			"Record" => $insert_data_,
		));
		die();
	}
	static function _ajax_update_gcode(){
		$r_code_id_ = $_POST["R_CODE_ID"];
		$code_id_ = $_POST["CODE_ID"];
		$code_nm_ = $_POST["CODE_NM"];
		$code_desc_ = $_POST["CODE_DESC"];
		$use_yn_ = $_POST["USE_YN"];

		$update_data_ = array(
			"CODE_NM" => $_POST["CODE_NM"],
			"CODE_DESC" => $_POST["CODE_DESC"],
			"USE_YN" => $_POST["USE_YN"],
		);

		if($r_code_id_ !== $code_id_){
			if(kn_gcode($code_id_) === false){
				print json_encode(array(
					"Result" => "ERROR",
					"Message" => "이미 존재하는 코드ID"));
				die();	
				return;
			}else{
				$update_data_["CODE_ID"] = $code_id_;
			}
		}
		global $wpdb;
		$result_ = $wpdb->update("{$wpdb->base_prefix}kn_gcode",$update_data_,
		array(
			"CODE_ID" => $r_code_id_
		));
		
		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 수정실패"));
			die();	
			return;
		}

		print json_encode(array(
			"Result" => "OK"
		));
		die();
	}

	static function _ajax_delete_gcode(){
		$code_id_ = $_POST["R_CODE_ID"];

		global $wpdb;
		$result_ = $wpdb->delete("{$wpdb->base_prefix}kn_gcode",array(
			"CODE_ID" => $code_id_
		));

		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 삭제실패"));
			die();	
			return;
		}

		print json_encode(array(
			"Result" => "OK"
		));
		die();
	}

	static function _ajax_get_gcode_dtl_list(){
		$code_id_ = $_GET["code_id"];
		$page_index_ = isset($_GET["jtStartIndex"]) ? (int)$_GET["jtStartIndex"] : 0;
		$page_size_ = isset($_GET["jtPageSize"]) ? (int)$_GET["jtPageSize"] : 0;
		
		global $wpdb;

		$total_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_gcode_dtl WHERE CODE_ID = '$code_id_'")->CNT;

		$query_ = "SELECT
			CODE_ID,
			CODE_DID R_CODE_DID, 
			CODE_DID,
			CODE_DNM,
			CODE_DDESC,
			USE_YN,
			SORT_CHAR
		FROM {$wpdb->base_prefix}kn_gcode_dtl gcd
		WHERE 1=1
		AND   CODE_ID = '$code_id_'
		ORDER BY CAST(SORT_CHAR AS UNSIGNED) ASC
		LIMIT $page_index_, $page_size_";

		$result_ = $wpdb->get_results($query_,ARRAY_A);
		print json_encode(array(
			"Result" => "OK",
			"Records" => $result_,
			"TotalRecordCount" => $total_count_,
		));
		die();
	}

	static function _ajax_create_gcode_dtl(){
		$code_id_ = $_POST["CODE_ID"];
		$code_did_ = $_POST["CODE_DID"];

		if(kn_gcode_dtl($code_id_, $code_did_) === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "이미 존재하는 코드상세ID"));
			die();
			return;
		}

		$insert_data_ = array(
			"CODE_ID" => $code_id_,
			"CODE_DID" => $code_did_,
			"CODE_DNM" => $_POST["CODE_DNM"],
			"CODE_DDESC" => $_POST["CODE_DDESC"],
			"USE_YN" => $_POST["USE_YN"],
			"SORT_CHAR" => $_POST["SORT_CHAR"],
		);

		global $wpdb;
		$result_ = $wpdb->insert("{$wpdb->base_prefix}kn_gcode_dtl",$insert_data_);

		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 추가실패"));
			die();	
			return;
		}

		$insert_data_["R_CODE_DID"] = $code_did_;

		print json_encode(array(
			"Result" => "OK",
			"Record" => $insert_data_,
		));
		die();
	}
	static function _ajax_update_gcode_dtl(){
		$code_id_ = $_POST["CODE_ID"];
		$r_code_did_ = $_POST["R_CODE_DID"];
		$code_did_ = $_POST["CODE_DID"];

		$update_data_ = array(
			"CODE_DNM" => $_POST["CODE_DNM"],
			"CODE_DDESC" => $_POST["CODE_DDESC"],
			"USE_YN" => $_POST["USE_YN"],
			"SORT_CHAR" => $_POST["SORT_CHAR"],
		);

		if($r_code_did_ !== $code_did_){
			if(kn_gcode_dtl($code_id_, $code_did_) === false){
				print json_encode(array(
					"Result" => "ERROR",
					"Message" => "이미 존재하는 코드상세ID"));
				die();	
				return;
			}else{
				$update_data_["CODE_DID"] = $code_did_;
			}
		}
		global $wpdb;
		$result_ = $wpdb->update("{$wpdb->base_prefix}kn_gcode_dtl",$update_data_,
		array(
			"CODE_ID" => $code_id_,
			"CODE_DID" => $r_code_did_
		));
		
		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 수정실패"));
			die();	
			return;
		}

		print json_encode(array(
			"Result" => "OK"
		));
		die();
	}

	static function _ajax_delete_gcode_dtl(){
		$code_id_ = $_GET["code_id"];
		$code_did_ = $_POST["R_CODE_DID"];

		global $wpdb;
		$result_ = $wpdb->delete("{$wpdb->base_prefix}kn_gcode_dtl",array(
			"CODE_ID" => $code_id_,
			"CODE_DID" => $code_did_
		));

		if($result_ === false){
			print json_encode(array(
				"Result" => "ERROR",
				"Message" => "공통코드 삭제실패"));
			die();	
			return;
		}

		print json_encode(array(
			"Result" => "OK"
		));
		die();
	}
}

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_GCODE_LIST,
	"title" => "공통코드",
	"sub_title" => "공통코드",
	"class" => "KNAdmin_gcode_list",
	"param" => array(),
	"eparam" => array(),
	"permission" => "manage_options",
	"screen_options" => array(
		"per_page" => array(
			"label" => "공통코드",
			"default" => 20,
		)
	)
));

kn_add_ajax("kn-admin-gcode-list", array("KNAdmin_gcode_list", "_ajax_get_gcode_list"));
kn_add_ajax("kn-admin-gcode-create", array("KNAdmin_gcode_list", "_ajax_create_gcode"));
kn_add_ajax("kn-admin-gcode-update", array("KNAdmin_gcode_list", "_ajax_update_gcode"));
kn_add_ajax("kn-admin-gcode-delete", array("KNAdmin_gcode_list", "_ajax_delete_gcode"));

kn_add_ajax("kn-admin-gcode-dtl-list", array("KNAdmin_gcode_list", "_ajax_get_gcode_dtl_list"));
kn_add_ajax("kn-admin-gcode-dtl-create", array("KNAdmin_gcode_list", "_ajax_create_gcode_dtl"));
kn_add_ajax("kn-admin-gcode-dtl-update", array("KNAdmin_gcode_list", "_ajax_update_gcode_dtl"));
kn_add_ajax("kn-admin-gcode-dtl-delete", array("KNAdmin_gcode_list", "_ajax_delete_gcode_dtl"));

?>