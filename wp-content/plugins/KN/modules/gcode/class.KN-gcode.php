<?php

/**
공통코드 클래스
*/
class KN_gcode{
	
	static function gcode($code_id_){
		global $wpdb;
		$query_ = "SELECT code_id,
						code_nm,
						code_desc,
						use_yn,
						reg_date,
						mod_date
		FROM {$wpdb->base_prefix}kn_gcode GCD
		WHERE 1=1
		AND   CODE_ID = %s";

		return $wpdb->get_row($wpdb->prepare($query_,$code_id_),ARRAY_A);
	}

	static function gcode_nm($code_id_){
		$gcode_ = self::gcode($code_id_);
		if(!isset($gcode_)) return $gcode_;
		return $gcode_["code_nm"];
	}

	static function gcode_dtl($code_id_, $code_did_){
		global $wpdb;
		$query_ = "SELECT code_did,
						code_dnm,
						code_ddesc,
						use_yn,
						sort_char,
						reg_date,
						mod_date
		FROM {$wpdb->base_prefix}kn_gcode_dtl GCD
		WHERE 1=1
		AND   CODE_ID = %s
		AND   CODE_DID = %s";

		return $wpdb->get_row($wpdb->prepare($query_,$code_id_,$code_did_),ARRAY_A);
	}

	static function gcode_dtl_list($code_id_, $use_yn_ = true, $excludes_ = null){
		global $wpdb;

		$query_use_yn_ = ($use_yn_ ? "AND GCD.USE_YN = 'Y' " : "");
		$query_excludes_ = "";

		if(isset($excludes_) && count(isset($excludes_)) > 0){
			$query_excludes_ = "AND GCD.CODE_DID IN (''";
			foreach($excludes_ as $row_index_ => $exclude_){
				$query_excludes_ .= ",'".$exclude_."'";
			}

			$query_excludes_ .= ")";
		}

		$query_ = "SELECT code_did,
						code_dnm,
						code_ddesc,
						use_yn,
						sort_char,
						reg_date,
						mod_date
		FROM {$wpdb->base_prefix}kn_gcode_dtl GCD
		WHERE 1=1
		$query_use_yn_
		$query_excludes_
		AND   CODE_ID = %s
		ORDER BY CAST(SORT_CHAR AS UNSIGNED) ASC";

		return $wpdb->get_results($wpdb->prepare($query_,$code_id_), ARRAY_A);
	}

	static function make_select($code_id_, $options_ = array()){
		$excludes_ = isset($options_["excludes"]) ? $options_["excludes"] : null;
		$use_yn_ = isset($options_["use_yn"]) ? $options_["use_yn"] : true;
		$select_id_ = isset($options_["id"]) ? $options_["id"] : "";
		$select_name_ = isset($options_["name"]) ? $options_["name"] : "";
		$select_class_ = isset($options_["class"]) ? $options_["class"] : "";
		$select_attrs_ = isset($options_["attrs"]) ? $options_["attrs"] : array();
		$default_ = isset($options_["default"]) ? $options_["default"] : "";
		$add_title_ = isset($options_["add_title"]) ? $options_["add_title"] : true;

		$dtl_list_ = self::gcode_dtl_list($code_id_, $use_yn_, $excludes_);

		$html_ = '<select '.(strlen($select_id_) ? 'id="'.$select_id_.'"' : '').' '.(strlen($select_class_) ? 'class="'.$select_class_.'"' : '').' '.(strlen($select_name_) ? 'name="'.$select_name_.'"' : '');

		foreach($select_attrs_ as $attr_name_ => $attr_value_){
			$html_ .= ' '.$attr_name_.'="'.$attr_value_.'" ';
		}

		$html_ .= '>';
		
		if($add_title_){
			$gcode_ = self::gcode($code_id_);
			$title_ = isset($options_["title"]) ? $options_["title"] : ("--".$gcode_["code_nm"]."--");
			$html_ .= '<option value="">'.$title_.'</option>';
		}

		foreach($dtl_list_ as $row_index_ => $row_data_){
			$html_ .= '<option value="'.$row_data_["code_did"].'" '.($default_ === $row_data_["code_did"] ? "selected" : "").'>'.$row_data_["code_dnm"].'</option>';	
		}
		$html_ .= '</select>';
		return $html_;
	}

	static function gcode_dtl_nm($code_id_, $code_did_){
		$gcode_dtl_ = self::gcode_dtl($code_id_,$code_did_);
		if(!isset($gcode_dtl_)) return $gcode_dtl_;
		return $gcode_dtl_["code_dnm"];
	}

	static function query_gcode_dtl_nm($code_id_, $column_){
		global $wpdb;
		return "(SELECT KN_GCD_TMP.CODE_DNM FROM {$wpdb->base_prefix}kn_gcode_dtl KN_GCD_TMP WHERE KN_GCD_TMP.CODE_ID = '$code_id_' AND KN_GCD_TMP.CODE_DID = {$column_})";
	}
}

function kn_gcode($code_id_){
	return KN_gcode::gcode($code_id_);
}
function kn_gcode_nm($code_id_){
	return KN_gcode::gcode_nm($code_id_);
}

function kn_gcode_dtl($code_id_, $code_did_){
	return KN_gcode::gcode_dtl($code_id_, $code_did_);
}
function kn_gcode_dtl_nm($code_id_, $code_did_){
	return KN_gcode::gcode_dtl_nm($code_id_, $code_did_);
}
function kn_gcode_make_select($code_id_, $options_ = array()){
	return KN_gcode::make_select($code_id_, $options_);
}

function kn_gcode_dtl_lst($code_id_, $use_yn_ = true, $excludes_ = null){
	return KN_gcode::gcode_dtl_list($code_id, $use_yn_, $excludes_);
}

function kn_query_gcode_dtl_nm($code_id_, $column_){
	return KN_gcode::query_gcode_dtl_nm($code_id_, $column_);
}

?>