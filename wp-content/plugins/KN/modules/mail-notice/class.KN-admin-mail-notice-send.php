<?php

define("KN_SLUG_ADMIN_MAIL_NOTICE_SEND","kn-mail-notice-send");

class KNAdmin_mail_notice_send extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
		kn_load_library("tokeninput");
		add_action("admin_footer",array("KNAdmin_mail_notice_send", "add_script"));
	}
	static function _draw_view(){

		$button_html_ = '<div class="kn-text-right">';
		$button_html_ .= '<input type="button" class="button button-primary" value="발송" onClick="kn_mail_notice_send();"> ';
		$button_html_ .= '</div>';
	?>
	<div class="wrap kn-limit-content">
	<h2>공지메일발송</h2>
	<p>등록된 회원 또는 특정 회원에게 공지메일을 발송합니다.</p>

	<?php echo $button_html_; ?>
	<table class="form-table kn-limit-content">
	<tr>
		<th>발송대상</th>
		<td><input type="text" id="kn-target-list" /></td>
	</tr>
	<tr>
		<th colspan="2"><input type="text" id="kn-mail-notice-title" class="large-text" placeholder="메일제목" /></th>
	</tr>
	<tr>
		<td colspan="2"><?php wp_editor("", "kn-mail-notice-html" , array(
			"textarea_name" => "mail_html",
		)); ?></td>
	</tr>
	</table>
	<?php echo $button_html_; ?>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$("#kn-target-list").tokenInput(KN.make_url(ajaxurl,{action : "kn-admin-mail-notice-target-list"}),{
			theme: "facebook",
			hintText: "회원검색",
			noResultsText: "검색된 회원이 없습니다.",
			searchingText: "회원 검색 중...",
			preventDuplicates : true,
			onAdd : function(item_){},
			onDelete : function(item_){}
		});

		$("#kn-target-list").tokenInput("add", {id: -1, name: "모든 회원에게"});
	});


	function kn_mail_notice_send(){
		$ = jQuery;
		var target_list_ = $("#kn-target-list").tokenInput("get");
		
		if(target_list_.length == 0){
			KN.error_popup({
				title : "발송대상없음",
				text : "발송대상을 입력해주세요!",
				confirmButtonText : "확인"
			});
			return;
		}

		KN.confirm_popup({
			title : "메일발송",
			text : "작성한 내용으로 공지메일을 발송합니다.",
			confirmButtonText : "발송",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			kn_mail_notice_send_p1();
		});
	}
	function kn_mail_notice_send_p1(){
		$ = jQuery;
		var target_list_ = $("#kn-target-list").tokenInput("get");
		var mail_title_ = $("#kn-mail-notice-title").val();
		var mail_html_ = $("#kn-mail-notice-html").val();

		KN.post("<h3>발송 중...</h3>",{
			action : "kn-admin-mail-notice-target-send",
			target_list : target_list_,
			mail_title : mail_title_,
			mail_html : mail_html_
		},function(result_, response_json_, cause_){
			if(!result_){
				KN.error_popup({
					title : "에러발생",
					text : response_json_,
					confirmButtonText : "확인"
				});
				return;
			}

			if(response_json_.success === false){
				KN.error_popup({
					title : "발송실패",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "발송완료",
				text : "총 "+response_json_.results.length+"통의 메일이 발송되었습니다.",
				confirmButtonText : "확인"
			},function(){document.location = document.location;});
		});
	}
	</script>
	<?php
	}

	static function _ajax_target_list(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}
		$search_text_ = isset($_GET["q"]) ? $_GET["q"] : "";
		$target_list_ = KN_user::user_list(array("verified" => true, "search_text" => $search_text_));
		$result_ = array();

		array_push($result_, array(
			"id" => -1,
			"name" => "모든 회원에게",
		));

		foreach($target_list_ as $row_index_ => $target_){
			array_push($result_, array(
				"id" => $target_["ID"],
				"name" => $target_["user_name"]."(".$target_["user_login"].")",
			));
		}

		echo json_encode($result_);
		die();
	}

	static function _ajax_send(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$target_list_ = isset($_POST["target_list"]) ? $_POST["target_list"] : array();
		$mail_title_ = $_POST["mail_title"];
		$mail_html_ = $_POST["mail_html"];
		$results_ = array();

		$mail_header_ = "Content-Type: text/html; charset=UTF-8";

		foreach($target_list_ as $row_index_ => $target_){
			if($target_["id"] == -1){ // to all

				$user_list_ = KN_user::user_list(array("verified" => true));
				foreach($user_list_ as $user_index_ => $user_){
					$result_ = kn_mail_send($user_["user_email"], kn_map_string($mail_title_, $user_),
						array(
							"message" => kn_map_string($mail_html_, $user_),
						), array($mail_header_));

					array_push($results_,array(
						"user_id" => $user_["ID"],
						"result" => $result_,
					));
				}

			}else{ //to someone
				$user_ = kn_user($target_["id"]);
				$result_ = kn_mail_send($user_["user_email"], kn_map_string($mail_title_, $user_),
					array(
						"message" => kn_map_string($mail_html_, $user_),
					), array($mail_header_));

				array_push($results_,array(
					"user_id" => $user_["ID"],
					"result" => $result_,
				));

			}
		}

		echo json_encode(array(
			"success" => true,
			"results" => $results_
		));
		die();
	}
}

kn_add_ajax("kn-admin-mail-notice-target-list", array("KNAdmin_mail_notice_send","_ajax_target_list"));
kn_add_ajax("kn-admin-mail-notice-target-send", array("KNAdmin_mail_notice_send","_ajax_send"));

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_MAIL_NOTICE_SEND,
	"title" => "공지메일발송",
	"subject" => "공지메일발송",
	"class" => "KNAdmin_mail_notice_send",
	"param" => array(),
	"eparam" => array(),
	"permission" => "manage_options",
));


?>