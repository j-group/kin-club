<?php

define("KN_SLUG_ADMIN_PLAY_LIST","kn-play-list");

class KNAdmin_play_list_table extends KNWPTable{

	function __construct(){
		parent::__construct(array(
			'singular' => 'play',
			'plural' => 'play-list',
			'ajax' => false
		));
	}

	function no_items(){
		_e('조회된 놀이활동이 없습니다.');
	}

	function column_default($item_, $column_name_){
		switch($column_name_){
		case "status_nm" :
		case "reg_date_txt" :
			return $item_[$column_name_];
			break;
		default : 
			return "--unknown column--";
		}
	}

	function get_columns(){
		return array(
			"play_title"	=> "놀이활동명",
			"status_nm"	=> "상태",
			"reg_date_txt"	=> "등록일",
		);
	}

	function column_play_title($item_){
		$actions = array(
			"edit" => sprintf('<a href="?page=%s&play_id=%s">%s</a>',$_REQUEST["page"],$item_["ID"],__("상세정보")),
		);

	  return "<b>[".$item_["ID"]."] ".$item_["play_title"]."</b>".$this->row_actions($actions);
	}

	function extra_tablenav($which_){
		$tablenav_ = "";
		$tablenav_ .= '<div class="alignleft actions bulkactions">';
		if($which_ === "top"){

		}
		$tablenav_ .= '</div>';

		echo $tablenav_;
	}

	function prepare_items(){
		$columns_ = $this->get_columns();
		$this->_column_headers = array($columns_, array(), $this->get_sortable_columns());
		
		$search_text_ = KN_GET("s");
		
		global $wpdb;

		$conditions_ = array(
			"table_alias" => "PLY",
			"search_text" => $search_text_,
		);

		$total_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}KN_PLAY PLY WHERE 1=1 ".KN_play::query_make_conditions($conditions_))->CNT;
		$per_page_ = kn_get_screen_option("per_page");
		$paged_ = $this->paged();
		$total_page_count_ = $this->total_page_count($per_page_, $total_count_);
		$offset_ = $this->offset($paged_, $per_page_);

		$conditions_["limit"] = array(
			"offset" => $offset_,
			"length" => $per_page_,
		);
		//$conditions_["sort"] = "";

		$result_ = KN_play::play_list($conditions_);

		$this->set_pagination_args(array(
			"total_items" => $total_count_,
			"total_pages" => $total_page_count_,
			"per_page" => $per_page_
		));

		$this->items = $result_;
	}
}

class KNAdmin_play_list extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){
		add_action("admin_footer", array("KNAdmin_play_list", "add_script"));

		$button_html_ = "<div class='kn-text-right'>";
		//$button_html_ .= "<a class='button button-primary' href='javascript:_kn_save_settings();'>변경사항 저장</a>";
		$button_html_ .= "</div>";

		//$settings_ = KN_settings::get_settings();

	?>

	<div class="wrap">
	<h2>놀이활동내역</h2>
	<form method="get" id="kn-play-list" action>
		<input type="hidden" name="page" value="<?php echo $_GET['page'] ?>" />
		<?php

		$table_ = new KNAdmin_play_list_table();
		$table_->prepare_items();
		$table_->search_box('검색', 'kn-play-list-searchbox');
		$table_->display();

		?>
	</form>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<style type="text/css">
	.wp-list-table.play-list thead tr th.column-play_title{
		width: 50%;
	}
	.wp-list-table.play-list thead tr th.column-status_nm{
		width: 27%;
	}
	.wp-list-table.play-list thead tr th.column-reg_date_txt{
		width: 23%;
	}

	.wp-list-table.play-list thead tr th.column-status_nm,
	.wp-list-table.play-list thead tr th.column-reg_date_txt,
	.wp-list-table.play-list tfoot tr th.column-status_nm,
	.wp-list-table.play-list tfoot tr th.column-reg_date_txt,
	.wp-list-table.play-list tbody tr td.column-status_nm,
	.wp-list-table.play-list tbody tr td.column-reg_date_txt{
		text-align: center;
	}
	.wp-list-table.play-list tbody tr td.column-reg_date_txt{
		font-size: 12px;
	}
	
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});
	</script>
	<?php
	}
}

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_PLAY_LIST,
	"title" => "놀이활동관리",
	"class" => "KNAdmin_play_list",
	"param" => array(),
	"eparam" => array(),
	"permission" => "manage_options",
	"screen_options" => array(
		"per_page" => array(
			"label" => "페이지당 놀이활동 수",
			"default" => 10,
		)
	)
));


?>