<?php

class KNAdmin_play_settings{
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>놀이활동관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="play_find_page_id">놀이활동검색 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["play_find_page_id"],
						"name" => "play_find_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>

			<tr>
				<th><label for="play_view_page_id">놀이활동보기 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["play_view_page_id"],
						"name" => "play_view_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>
			
			<tr>
				<th><label for="play_register_page_id">놀이활동등록 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["play_register_page_id"],
						"name" => "play_register_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>

			<tr>
				<th><label for="play_edit_page_id">놀이활동수정 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["play_edit_page_id"],
						"name" => "play_edit_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>

			<tr>
				<th colspan="2">
					<label for="play_register_agreement_content">약관2 내용</label>
				</th>	
			</tr>
			<tr>
				<td colspan="2">
					<?php wp_editor(stripslashes($settings_["play_register_agreement_content"]), "play_register_agreement_content" , array(
						"textarea_name" => "play_register_agreement_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>

			<tr>
				<th><label for="play_pro_switch_total_time">일반 > 전문가 전환 필요 총시간</label></th>
				<td>
					<input type="number" class="large-text" name="play_pro_switch_total_time" value="<?=$settings_["play_pro_switch_total_time"]?>">
				</td>
			</tr>

		</tbody>
	</table>

	<?php
	}

	static function _filter_settings($settings_){
		// $settings_["user_login_need_ssl"] = ((isset($settings_["user_login_need_ssl"]) && $settings_["user_login_need_ssl"] === "on") ? true : false);

		return $settings_;
	}
}

add_action("knadmin_settings_form",array("KNAdmin_play_settings","_add_settings_form"));
add_filter("kn_update_settings", array("KNAdmin_play_settings", "_filter_settings"));

?>