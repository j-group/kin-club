<?php

class KNAdmin_play_view extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){
		add_action("admin_footer", array("KNAdmin_play_view", "add_script"));

		$play_data_ = KN_play::play($_GET["play_id"]);

		$button_html_ = "<div class='kn-text-right'>";
		$button_html_ .= "<a class='button button-delete' href='javascript:_kn_remove_play();'>놀이활동삭제</a>";	
		$button_html_ .= "</div>";
	?>

	<div class="wrap kn-limit-content">
	<h2></h2>
	<h2>놀이활동상세정보 <a class="add-new-h2" href="?page=<?php echo KN_SLUG_ADMIN_PLAY_LIST; ?>">돌아가기</a></h2>
	<form method="post" id="kn-play-view" action>
		<input type="hidden" name="ID" value="<?php echo $_GET['play_id'] ?>" />
		<?php echo $button_html_; ?>
		<table class="form-table">
			<tr>
				<th>놀이활동명</th>
				<td><b><?=$play_data_["play_title"]; ?></b></td>
			</tr>
			<tr>
				<th>날짜</th>
				<td><?=$play_data_["play_date_txt"]?></td>
			</tr>
			<tr>
				<th>상태</th>
				<td><?=$play_data_["display_status_nm"]?></td>
			</tr>
		</table>
		<?php echo $button_html_; ?>
	</form>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<style type="text/css">
	
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});

	function _kn_remove_play(){
		KN.warning_popup({
			title : "놀이활동삭제",
			text : "해당 놀이활동을 삭제합니다. 삭제된 놀이활동은 복구할 수 없습니다.",
			confirmButtonText : "삭제하기",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_kn_remove_play_p1();
		});
	}
	
	function _kn_remove_play_p1(block_reason_){
		var play_data_ = jQuery("#kn-play-view").serializeObject();
		KN.post("<h3>삭제중</h3>",{
			action : "kn-admin-play-remove",
			play_id : play_data_["ID"]
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				KN.error_popup({
					title : "에러발생",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "놀이활동삭제완료",
				confirmButtonText : "확인"
			},function(){
				document.location = "?page=<?php echo KN_SLUG_ADMIN_PLAY_LIST; ?>";
			});
		});
	}
	
	</script>
	<?php
	}
	
	static function _ajax_do_remove(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$play_id_ = $_POST["play_id"];
		$result_ = KN_play::delete($play_id_);

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}
}

kn_add_ajax("kn-admin-play-remove", array("KNAdmin_play_view", "_ajax_do_remove"));

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_PLAY_LIST,
	"title" => "놀이활동정보",
	"class" => "KNAdmin_play_view",
	"param" => array("play_id"),
	"eparam" => array(),
	"permission" => "manage_options",
));

?>