<?php

class KN_play_edit extends KNShortcode{

	static function shortcode_name(){
		return "kn-play-edit";
	}

	static function add_library(){
		kn_load_library("main");

		wp_enqueue_style("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play.css"));
		wp_enqueue_style("kn-play-register", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-register.css"));
		
		wp_enqueue_script("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play.js"), array("jquery-validate"));
		wp_register_script("kn-play-register", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-register.js"), array("kn-play"));

		wp_enqueue_script("kn-play-register");

		
		KN_play_editor::add_library();
	}

	static function _do_shortcode($attrs_){
		$play_id_ = isset($_GET["play_id"]) ? $_GET["play_id"] : null;
		
		ob_start();
?>

	<?=KN_play_editor::draw_editor($play_id_)?>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$login_page_id_ = $settings_["user_login_page_id"]; 
		$play_edit_page_id_ = $settings_["play_edit_page_id"]; 

		if(strlen($play_edit_page_id_) && $current_page_id_ === $play_edit_page_id_){
			if(!kn_is_user_logged_in()){
				$login_url_ = kn_make_url(get_permalink($login_page_id_), array("redirect" => urlencode(get_permalink($play_edit_page_id_))));
				wp_redirect($login_url_);
				exit();
			}
		}
	}
}

add_shortcode((KN_play_edit::shortcode_name()), array("KN_play_edit","_do_shortcode"));
add_action("kn_shortcode_filter", array("KN_play_edit","filter"));
add_action("kn_play_prevent_page",array("KN_play_edit","_prevent_page"));

?>