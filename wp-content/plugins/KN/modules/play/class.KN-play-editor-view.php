<?php

class KN_play_editor_view{

	static function add_library(){
		kn_load_library("main-all");
		kn_load_library("kn-maps");
		kn_load_library("kn-utils");
	}

	static function draw_view($play_id_, $options_btn_html_ = null){
		$settings_ = KN_settings::get_settings();
		$current_user_ = kn_current_user();
		$is_new_ = !strlen($play_id_);

		$play_data_ = KN_play::play($play_id_);

		if($play_data_ === false || $current_user_["ID"] !== $play_data_["host_id"]){
			wp_die("잘못된 접근입니다.");		
		}

		$button_html_ = '<div class="kn-button-area">';
		$button_html_ .= '<span class="kn-indicator"></span>';
		$button_html_ .= $options_btn_html_;
		$button_html_ .= '<a href="'.get_permalink($settings_["play_view_page_id"]).$play_id_.'" class="knbtn knbtn-primary">신청화면보기</a> ';

		$button_html_ .= '</div>';

		ob_start();
?>
<form class="kn-form" id="kn-play-editor-form">
	
	<div class="kn-column-frame kn-column-padding">
		<div class="kn-column l50">
			<h1>놀이활동정보 - <span><?=$play_data_["display_status_nm"]?></span></h1>
		</div>
		<div class="kn-column l50">
			<?=$button_html_?>
		</div>
	</div>

	<h3>놀이활동기본정보</h3>
	<table class="kn-form-table">
		<tr>
			<th >놀이활동명</th>
			<td>
				<?=$play_data_["play_title"]?>
			</td>
		</tr>
		<tr>
			<th >장소</th>
			<td>
				<div><?=$play_data_["address"]?></div>
				<div><?=$play_data_["address_dtl"]?></div>
			</td>
		</tr>
		<tr>
			<th >날짜</th>
			<td>
				<?=$play_data_["play_date"]?>
			</td>
		</tr>
		<tr>
			<th >시간</th>
			<td>
				<?=$play_data_["play_srt_time"]?> - <?=$play_data_["play_end_time"]?>
			</td>
		</tr>
		<tr>
			<th >상세설명</th>
			<td>
				<?=$play_data_["desc_html"]?>
			</td>
		</tr>

		<tr>
			<th >인원</th>
			<td>
				<?=$play_data_["av_min_cnt"]?> ~ <?=$play_data_["av_max_cnt"]?> 명
			</td>
		</tr>

		<!-- <tr>
			<th >가격</th> 	
			<td>
				<span class="kn-input-inset first-child l10-l l20-s kn-input-inset-button" readonly>&#8361;</span><input
					 type="text" name="need_cost" data-accessible="true" class="kn-input-inset l30-l l80-s last-child">
				<p class="hint">*참여하는 1인당 단가입니다.</p>
			</td>
		</tr> -->

		<tr>
			<th >수확할 킨체리</th> 	
			<td class="column-need-point">
				<?=$play_data_["need_point"]?>
			</td>
		</tr>

		<tr>
			<th >놀이분류</th> 	
			<td>
				<?=$play_data_["main_ctg_nm"]?> / <?=$play_data_["sub_ctg_nm"]?>
			</td>
		</tr>
		<tr>
			<th>관련링크</th>
			<td>
				<?=$play_data_["ref_url"]?>
			</td>
		</tr>
	</table>
<?= $button_html_ ?>
</form>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>