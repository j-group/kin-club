<?php

class KN_play_editor{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("main-all");
		kn_load_library("kn-maps");
		kn_load_library("kn-utils");
		kn_load_library("kn-fileupload");

		wp_enqueue_style("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play.css"));
		wp_enqueue_style("kn-play-editor", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-editor.css"));
		
		wp_enqueue_script("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play.js"), array("jquery-validate"));
		wp_register_script("kn-play-editor", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-editor.js"), array("kn-play"));

		wp_localize_script("kn-play-editor", "kn_play_editor_var", array(
			"play_title_required" => "놀이활동명을 입력해주세요",
			"address_required" => "주소를 입력해주세요",
			"play_date_required" => "날짜를 입력해주세요",
			"play_srt_time_required" => "시작날짜을 입력해주세요",
			"play_end_time_required" => "종료시간을 입력해주세요",
			"desc_html_required" => "상세설명 입력해주세요",
			"av_min_cnt_required" => "참여가능 최소인원 입력해주세요",
			"av_max_cnt_required" => "참여가능 최대인원 입력해주세요",
			"main_ctg_required" => "대분류를 선택해주세요",
			"sub_ctg_required" => "소분류를 선택해주세요",
			"need_cost_required" => "가격을 입력해주세요",
			"need_point_required" => "수확할 킨체리을 입력해주세요",
			"img_attach_bool_required" => "이미지를 등록해주세요",

			"notexsits_home_address" => "저장된 집주소가 없습니다.",

			"play_edit_url" => get_permalink($settings_["play_edit_page_id"]),
			"play_view_url" => get_permalink($settings_["play_view_page_id"]),

			"save_success" => "정상적으로 저장되었습니다.",
			"save_error" => "저장 중 에러가 발생했습니다. 관리자에게 문의해주시길 바랍니다.",
			"submit_success" => "놀이활동이 등록되었습니다.",
			"submit_error" => "등록 중 에러가 발생했습니다. 관리자에게 문의해주시길 바랍니다.",

			"confirm_delete" => "삭제한 놀이활동은 복구할 수 없습니다. 삭제하시겠습니까?",
			"delete_error" => "삭제중 에러가 발생했습니다. 관리자에게 문의해주시길 바랍니다.",
			"delete_success" => "삭제되었습니다. 놀이활동내역으로 이동합니다.",
			"myplay_url" => kn_make_url(get_permalink($settings_["user_myinfo_page_id"]), array("sideid" => "myplay")),

			"preview_url" => get_permalink($settings_["play_view_page_id"]),
			
		));
		wp_enqueue_script("kn-play-editor");
		
	}

	static function draw_editor($play_id_ = null, $options_btn_html_ = null){
		if(!kn_is_user_logged_in()){
			wp_die("잘못된 접근입니다.");
		}

		$current_user_ = kn_current_user();
		$is_new_ = !strlen($play_id_);

		$play_data_ = null;

		if(!$is_new_){
			//놀이활동 존재여부 check
			$play_data_ = KN_play::play($play_id_);

			if($play_data_ === false || $current_user_["ID"] !== $play_data_["host_id"]){
				wp_die("잘못된 접근입니다.");		
			}
			
			if($play_data_["end_yn"] === "Y"){
				return KN_play_editor_view::draw_view($play_id_, $options_btn_html_);
			}

		}else{
			$play_data_ = array(
				"addr_lat" => null,
				"addr_lng" => null,
				"play_title" => null,
				"address" => null,
				"address_dtl" => null,
				"play_date" => null,
				"play_srt_time" => null,
				"play_end_time" => null,
				"desc_html" => null,
				"av_min_cnt" => null,
				"av_max_cnt" => null,
				"need_cost" => null,
				"need_point" => null,
				"main_ctg" => null,
				"sub_ctg" => null,
				"ref_url" => null,
				"main_img_urls" => array(),
				"status" => "00001",
			);
		}

		$button_html_ = '<div class="kn-button-area">';
		$button_html_ .= '<span class="kn-indicator"></span>';
		$button_html_ .= $options_btn_html_;

		if(strlen($play_id_)){
			$button_html_ .= '<a href="javascript:kn_play_editor_delete();" class="knbtn knbtn-delete">삭제하기</a> ';
			// $button_html_ .= '<input type="button" onClick="javascript:;" class="knbtn knbtn-secondary" data-accessible="true" value="미리보기"> ';
		}
		
		if($play_data_["status"] === "00001"){
			$button_html_ .= '<input type="button" onClick="kn_play_editor_save();" class="knbtn knbtn-secondary" data-accessible="true" value="임시저장"> ';
			$button_html_ .= '<input type="submit" onClick="javascript:;" class="knbtn knbtn-primary" value="등록하기" data-accessible="true">';
		}else{
			$button_html_ .= '<input type="button" onClick="kn_play_editor_goto_view();" class="knbtn knbtn-secondary" data-accessible="true" value="신청화면보기"> ';

			if($play_data_["display_status"] === "00003" || $play_data_["display_status"] === "00007"){
				$button_html_ .= '<input type="button" onClick="kn_play_editor_save();" class="knbtn knbtn-primary" data-accessible="true" value="저장하기"> ';
			}
		}
		$button_html_ .= '</div>';

		$settings_ = KN_settings::get_settings();

		$addinfo_page_url_ = kn_make_url(get_permalink($settings_["user_myinfo_page_id"]), array("sideid" => "addinfo"));

		ob_start();
?>
<form class="kn-form" id="kn-play-editor-form">
<input type="hidden" name="ID" value="<?=$play_id_?>">
<input type="hidden" name="addr_lat" value="<?=$play_data_["addr_lat"]?>">
<input type="hidden" name="addr_lng" value="<?=$play_data_["addr_lng"]?>"> 
	
	<div class="kn-column-frame kn-column-padding">
		<div class="kn-column l50">
			<h1><?=$is_new_ ? "놀이활동등록" : "놀이활동수정"?><?=$is_new_ ? "" : '- <span>'.$play_data_["display_status_nm"].'</span>' ?></h1>
		</div>
		<div class="kn-column l50">
			<?=$button_html_?>
		</div>
	</div>

	<h3>놀이활동기본정보</h3>
	<table class="kn-form-table">
		<tr>
			<th class="required">놀이활동명</th>
			<td>
				<input type="text" name="play_title" class="full-width" placeholder="놀이활동명" data-accessible="true" value="<?=$play_data_["play_title"]?>">
				<p class="hint">*놀이활동검색에 노출됩니다.</p>
			</td>
		</tr>
		<tr>
			<th class="required">장소</th>
			<td>
				<input type="text" name="address" class="full-width" readonly placeholder="여기를 눌러 주소검색" onClick="kn_play_editor_open_address_popup();" data-accessible="true" value="<?=$play_data_["address"]?>">
				<input type="text" name="address_dtl" class="full-width" placeholder="상세주소" data-accessible="true" value="<?=$play_data_["address_dtl"]?>">
				<label id="address-error" class="error" for="address" style="display:none;"></label>

				<div class="kn-play-editor-ourhome-btn-frame">
				<?php if(strlen($current_user_["address"])){ ?>
				<a class="kn-alink"
				 data-addr-lat="<?=$current_user_["addr_lat"]?>"
				 data-addr-lng="<?=$current_user_["addr_lng"]?>"
				 data-address="<?=$current_user_["address"]?>"
				 data-address-dtl="<?=$current_user_["address_dtl"]?>"
				 href="javascript:;"
				 id="kn-play-editor-ourhome-btn">우리집으로 설정</a>
				 <?php }else{ ?>
				 	<span>설정된 집주소가 없습니다. <a href="<?=$addinfo_page_url_?>" class="kn-alink" target="_blank">집주소 설정하기</a></span>
				 <?php } ?>
				 </div>
			</td>
		</tr>
		<tr>
			<th class="required">날짜</th>
			<td>
				<input type="text" name="play_date" data-accessible="true" class="kn-input-inset l33-l l100-s first-child last-child" data-accessible="true" value="<?=$play_data_["play_date"]?>">
			</td>
		</tr>
		<tr>
			<th class="required">시간</th>
			<td>
				<span type="button" class="kn-input-inset l10-l l20-s first-child kn-input-inset-button" readonly>시작</span><input 
					type="text" name="play_srt_time" data-accessible="true" class="kn-input-inset l25-l l30-s" data-accessible="true" value="<?=$play_data_["play_srt_time"]?>"><span 
					type="button" class="kn-input-inset l10-l l20-s kn-input-inset-button" readonly>종료</span><input 
					type="text" name="play_end_time" data-accessible="true" class="kn-input-inset l25-l l30-s last-child" data-accessible="true" value="<?=$play_data_["play_end_time"]?>">

				<label id="play_srt_time-error" class="error" for="play_srt_time" style="display:none;"></label>
				<label id="play_end_time-error" class="error" for="play_end_time" style="display:none;"></label>
			</td>
		</tr>
		<tr>
			<th class="required">상세설명</th>
			<td>
				<textarea name="desc_html" placeholder="놀이에 대한 설명을 자세히 적어주세요" class="full-width" data-accessible="true"><?=$play_data_["desc_html"]?></textarea>
				<p class="hint">*위 설명은 회원들에게 노출됩니다. 비속어 및 저속한 단어사용을 금지합니다.</p>
			</td>
		</tr>

		<tr>
			<th class="required">인원</th>
			<td>
				<span type="button" class="kn-input-inset l10-l l20-s first-child kn-input-inset-button" readonly>최소</span><input 
					type="text" name="av_min_cnt" data-accessible="true" class="kn-input-inset l25-l l30-s" value="<?=$play_data_["av_min_cnt"]?>"><span 
					type="button" class="kn-input-inset l10-l l20-s kn-input-inset-button" readonly>최대</span><input 
					type="text" name="av_max_cnt" data-accessible="true" class="kn-input-inset l25-l l30-s last-child" value="<?=$play_data_["av_max_cnt"]?>">

				<label id="av_min_cnt-error" class="error" for="av_min_cnt" style="display:none;"></label>
				<label id="av_max_cnt-error" class="error" for="av_max_cnt" style="display:none;"></label>
			</td>
		</tr>

		<!-- <tr>
			<th class="required">가격</th> 	
			<td>
				<span class="kn-input-inset first-child l10-l l20-s kn-input-inset-button" readonly>&#8361;</span><input
					 type="text" name="need_cost" data-accessible="true" class="kn-input-inset l30-l l80-s last-child">
				<p class="hint">*참여하는 1인당 단가입니다.</p>
			</td>
		</tr> -->

		<tr>
			<th class="required">수확할 킨체리</th> 	
			<td class="column-need-point">
				<span class="kn-input-inset first-child l10-l l20-s kn-input-inset-button" readonly><span class="knicon knicon-point-xsmall"></span></span><input
					 type="text" name="need_point" data-accessible="true" class="kn-input-inset l25-l l80-s last-child" value="<?=$play_data_["need_point"]?>">
				<p class="hint">*참여하는 1인당 수확할 킨체리 수 입니다.</p>
			</td>
		</tr>

		<tr>
			<th class="required">놀이분류</th> 	
			<td><div class="kn-column-frame kn-column-padding">
				<div class="kn-column l40 fix-width">
					<select class="full-width" name="main_ctg">
						<option value="">--대분류--</option>
						<?=KN_play::main_categories_options($play_data_["main_ctg"]);?>
					</select>
				</div>
				<div class="kn-column l60 fix-width">
					<select class="full-width" name="sub_ctg">
						<option value="">--소분류--</option>
						<?=KN_play::sub_categories_options($play_data_["main_ctg"], $play_data_["sub_ctg"]);?>
					</select>
				</div>
			</div>
			<label id="main_ctg-error" class="error" for="main_ctg" style="display:none;"></label>
			<label id="sub_ctg-error" class="error" for="sub_ctg" style="display:none;"></label>
			<p class="hint">*해당분류가 없을 경우 기타/기타 로 선택하세요.</p>
		</td>
		</tr>
		<tr>
			<th>관련링크</th>
			<td>
				<input type="text" name="ref_url" placeholder="관련링크(URL) 입력" data-accessible="true" class="full-width" value="<?=$play_data_["ref_url"]?>">
				<p class="hint">*SNS페이지, 관련링크, 또는 웹사이트가 있을 경우</p>
			</td>
		</tr>
	</table>

<?= $button_html_ ?>
</form>

<h3>놀이활동사진등록</h3>
<table class="kn-form-table kn-form">
	<tr>
		<th class="required">이미지등록</th>
		<td>
			<div id="kn-main-image-uploader">
				<!-- 기존이미지 설정 -->
			<?php

			$main_img_urls_ = $play_data_["main_img_urls"];
			foreach($main_img_urls_ as $img_url_){
				echo "<img style='display:none;' src='".$img_url_."'>";
			}
			
			?>
			</div>
			<input type="hidden" name="img_attach_bool"> 
			<p class="hint">*이미지는 최대 5장까지 설정 가능합니다.</p>
		</td>
	</tr>
</table>

<div id="kn-play-editor-agreement" class="mfp-hide kn-mg-popup with-button kn-form">
	<h3>놀이활동등록약관동의</h3>
	<div class="wrap kn-form">			
		<div class="kn-term-table">
			<div class="term-content kn-form-table"><?=$settings_["play_register_agreement_content"]?></div>
			<p class="hint">*안전하고 건강한 놀이활동을 위해 약관동의가 필요합니다.</p>
		</div>
	</div>
	<div class="button-area">
		<a class="knbtn knbtn-primary" id="kn-play-editor-agree-btn" data-accessible="true">위 약관에 동의하고 등록합니다.</a>
		 <a class="knbtn knbtn-secondary" id="kn-play-editor-cancel-agree-btn" data-accessible="true">취소</a>
	</div>
</div>
<?=KN_maps::draw_address_popup()?>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_load_sub_categories(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$main_ctg_ = $_POST["main_ctg"];
		$sub_categories_ = KN_play::sub_categories($main_ctg_);

		echo json_encode($sub_categories_);
		die();
	}

	static function _ajax_save(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$play_data_ = $_POST["play_data"];
		// $play_data_["status"] = '00001';

		if(!strlen($play_data_["ID"])){ //insert
			$play_data_["host_id"] = kn_current_user_id();
			$play_id_ = KN_play::create($play_data_);
			$play_data_["ID"] = $play_id_;
		}

		$org_play_data_ = KN_play::play($play_data_["ID"]);
		if(!kn_is_admin() && $org_play_data_["host_id"] !== kn_current_user_id()){
			echo json_encode(array("reject" => true));
			die();
		}

		KN_play::update($play_data_["ID"], $play_data_);
		
		echo json_encode(array(
			"success" => true,
			"play_id" => $play_data_["ID"],
		));
		die();
	}

	static function _ajax_delete(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$play_id_ = $_POST["play_id"];
		$play_data_ = KN_play::play($play_id_);

		if(!kn_is_admin() && $play_data_["host_id"] !== kn_current_user_id()){
			echo json_encode(array("reject" => true));
			die();
		}
		
		KN_play::delete($play_id_);
		
		echo json_encode(array(
			"success" => true
		));
		die();
	}

	static function _ajax_submit(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$play_id_ = $_POST["play_id"];
		$play_data_ = KN_play::play($play_id_);

		if(!kn_is_admin() && $play_data_["host_id"] !== kn_current_user_id()){
			echo json_encode(array("reject" => true));
			die();
		}

		KN_play::update($play_id_, array("status" => "00003"));

		echo json_encode(array(
			"success" => true,
			"play_id" => $play_id_
		));
		die();
	}

	static function _prevent_page($current_page_id_){
		
	}
}

kn_add_ajax("kn-play-editor-load-sub-categories", array("KN_play_editor","_ajax_load_sub_categories"), true);
kn_add_ajax("kn-play-editor-save", array("KN_play_editor","_ajax_save"), true);
kn_add_ajax("kn-play-editor-submit", array("KN_play_editor","_ajax_submit"), true);
kn_add_ajax("kn-play-editor-delete", array("KN_play_editor","_ajax_delete"), true);

include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-editor-view.php');

?>