<?php

class KN_play_find extends KNShortcode{

	static function shortcode_name(){
		return "kn-play-find";
	}

	static function add_library(){
		$settings_ = KN_settings::get_settings();
		kn_load_library("main-all");
		kn_load_library("kn-maps");
		kn_load_library("kn-utils");
		kn_load_library("portamento");
		kn_load_library("swiper");
		kn_load_library("waypoints");
		
		kn_load_library("kn-play");
		kn_load_library("kn-play-finder");
		
		wp_register_script("kn-play-find", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-find.js"), array("kn-play"));
		wp_localize_script("kn-play-find","kn_play_find_var",array(
			"indicator_img_url" => (KN_PLUGIN_DIR_URL."lib/img/ajax-loader.gif"),
			"norows_message" => "검색된 놀이활동이 없습니다.",
			"play_view_url" => get_permalink($settings_["play_view_page_id"]),
		));
		wp_enqueue_script("kn-play-find");


		if(!kn_is_mobile()){
			wp_enqueue_script("kn-play-find-pc", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-find-pc.js"), array("kn-play-find"));
		}

		wp_enqueue_style("kn-play-find", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-find.css"));
	}

	static function _do_shortcode($attrs_){
		$settings_ = KN_settings::get_settings();
		$current_user_ = kn_current_user();

		$play_register_url_ = get_permalink($settings_["play_register_page_id"]);

		ob_start();
		?>
		


<div class="kn-play-find-frame kn-form">
	<div class="header-frame">
		<div class="header-top">
			<div class="logo-frame">
				<a href="<?=home_url()?>"><?=get_bloginfo("name")?></a>
			</div>
			<div class="search-bar-frame">
				<a class="address-btn" data-cond-target-address name="target_address" href="javascript:kn_play_finder_open_address_popup();">
					<i class="fa fa-map-marker"></i><span></span>
				</a>
			</div>
			<div class="button-area">
				<ul class="button-list">

					<?php
						do_action("kn_user_before_login_header");
						if(kn_is_user_logged_in()){
							$current_user_ = kn_current_user();

							do_action("kn_user_before_login_header_logged_in");
					?>
					<li><a href="<?php echo get_permalink($settings_["play_register_page_id"]); ?>" class="knbtn knbtn-secondary">놀이활동등록</a></li>
					<li><span data-kn-user-dropdown><?=KN_user::user_profile_tag(kn_current_user_id(), "xxsmall")?> <?=$current_user_["display_name"]?>
						<ul class="submenu">
							<li><a href="<?php echo get_permalink($settings_["user_myinfo_page_id"]); ?>">내정보</a></li>
							<li><a href="<?php echo get_permalink($settings_["user_logout_page_id"]); ?>">로그아웃</a></li>
						</ul>
					</span></li>
					<!-- <li><a href="<?php echo get_permalink($settings_["user_logout_page_id"]); ?>">로그아웃</a></li> -->
					<?php
						do_action("kn_user_after_login_header_logged_in");
						}else{ 

						do_action("kn_user_before_login_header_logged_out");
					?>
					<li><a href="<?php echo get_permalink($settings_["user_login_page_id"]); ?>" class="">로그인</a></li>
					<li><a href="<?php echo get_permalink($settings_["user_register_page_id"]); ?>" class="">회원가입</a></li>
					<?php
						do_action("kn_user_after_login_header_logged_out");
						
						}

						do_action("kn_user_after_login_header");
					?>
				</ul>
			</div>
		</div>
		
	</div>
	<div class="content-frame">
		<?=KN_play::draw_finder("kn_play_search_play", 2, false, $_REQUEST)?>
		<form class="kn-form" id="kn-play-find-list-form">
			<div id="kn-play-find-list-frame" class="first">
				<input type="hidden" name="page_index" value="0">
				<input type="hidden" name="feed_length" value="8">
				<div class="kn-play-list"></div>
				<div class="loading">검색중...</div>
				<div class="norows">검색된 놀이활동이 없습니다.</div>
			</div>
		</form>
		<div class="kn-play-find-maps-frame"><div id="kn-play-find-maps"></div></div>
	</div>
</div>

<?=KN_maps::draw_address_popup()?>
<?=KN_utils::draw_timepicker_popup()?>
<?=KN_utils::draw_datepicker_popup()?>

		<?php

		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_load_play_list(){
		$params_ = $_POST["params"];

		$play_list_ = KN_play::play_list(array(
			"current_latlng" => array(
				"lat" => $params_["lat"],
				"lng" => $params_["lng"]
			),"latlng_distance" => $params_["distance"],
			"start_date" => $params_["start_date"],
			"end_date" => $params_["end_date"],
			"play_srt_time" => $params_["start_time"],
			"play_end_time" => $params_["end_time"],
			"av_min_cnt" => $params_["av_min_cnt"],
			"av_max_cnt" => $params_["av_max_cnt"],
			"search_text" => (isset($params_["keyword"]) ? $params_["keyword"] : null),
			"sort" => "find_sort asc, addr_distance asc",
			"limit" => array(
				"offset" => $params_["page_index"],
				"length" => $params_["feed_length"],
			),"status" => array("00003")
		));

		echo json_encode($play_list_);
		die();
	}
}

add_shortcode((KN_play_find::shortcode_name()), array("KN_play_find","_do_shortcode"));
add_action("kn_shortcode_filter", array("KN_play_find","filter"));
kn_add_ajax("kn-play-find-load-list", array("KN_play_find", "_ajax_load_play_list"), true);

?>