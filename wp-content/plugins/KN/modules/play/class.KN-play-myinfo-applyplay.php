<?php

class KN_play_myinfo_applyplay_table extends KNListTable{

	function prepare(){
		$user_id_ = kn_current_user_id();
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 10;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		global $wpdb;

		$query_conditions_ = array("table_alias" => "PLY", "mem_id" => $user_id_);
		$query_defaults_ = KN_play::query_make_conditions($query_conditions_);

		$query_ = "SELECT COUNT(*) CNT
			FROM {$wpdb->base_prefix}kn_play PLY
			WHERE 1=1
			$query_defaults_ ";

		$total_count_ = $wpdb->get_row($query_)->CNT;
		$items_ = KN_play::play_list(array_merge($query_conditions_, array(
			"sort" => "reg_date desc", 
			"limit" => array(
				"offset" => $offset_,
				"length" => $per_page_,
			)
		)));
		
		return array(
			"items" => $items_,
			"page_index" => $page_index_,
			"total_count" => $total_count_,
			"per_page" => $per_page_,
			"pagenav_count" => 3,
			"hide_header" => true,
		);
	}

	function columns(){
		return array(
			"single_row" => "",
		);
	}
	function column_value($item_, $column_name_){
		$settings_ = KN_settings::get_settings();

		$play_view_url_ = get_permalink($settings_["play_view_page_id"]);
		// $play_edit_url_ = get_permalink($settings_["play_edit_page_id"]);

		$play_status_ = $item_["status"];
		$close_yn_ = $item_["close_yn"];
		$end_yn_ = $item_["end_yn"];

		$forward_url_ = $play_view_url_.$item_["ID"];

		ob_start();
?>
<div class="wrap"><div class="content-area">
	<div class="main-img-frame kn-img-frame noimg" href="<?=$forward_url_?>">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php
					foreach($item_["main_img_urls"] as $img_url_){
						echo '<div class="swiper-slide" style="background-image:url('.$img_url_.');"></div>';
					}
				?>
			</div>
			<div class="swiper-pagination"></div>
		</div>
	</div>
	<div class="desc-frame">
		<h4><a href="<?=$forward_url_?>"><?=strlen($item_["play_title"]) ? $item_["play_title"] : "(놀이활동명없음)"?></a><span class="play-date"><?=strlen($item_["play_date_txt"]) ? $item_["play_date_txt"] : "(놀이날짜없음)"?></span></h4>
		<p class="address"><?=strlen($item_["address"]) ? $item_["address"] : "(놀이활동주소없음)"?></p>
		<p class="mem-count">참여자 <?=$item_["mem_count"]?>/<?=$item_["av_max_cnt"]?> 명</p>
	</div>
</div>
<div class="status-area">
	<?php if($item_["display_status"] === "00003"){ ?>
		<span class="kn-play-status-<?=$item_["display_status"]?>">참여신청완료</span>
	<?php }else{ ?>
		<span class="kn-play-status-<?=$item_["display_status"]?>"><?=$item_["display_status_nm"]?></span>
	<?php } ?>
</div>
<div class="review-area">
<?php

if($end_yn_ === "Y"){

	if(!KN_play::did_review($item_["ID"])){ ?>
		<a href="<?=$forward_url_?>" class="knbtn knbtn-primary">후기작성</a>
	<?php }else{ ?>
		<a href="<?=$forward_url_?>" class="knbtn knbtn-secondary">후기작성함</a>
	<?php } 

}

?>
</div>
</div>

<?php

		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
		
	}

	function norowdata(){
		return "참가한 놀이활동이 없습니다.";
	}
}

class KN_play_myinfo_applyplay extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("applyplay", "KN_play_myinfo_applyplay", 310);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("kn-listtable");
		kn_load_library("main-all");
		kn_load_library("kn-utils");
		kn_load_library("kn-fileupload");
		kn_load_library("kn-play");
		kn_load_library("swiper");

		wp_enqueue_style("kn-play-myinfo-applyplay", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-myinfo-applyplay.css"));
		wp_register_script("kn-play-myinfo-applyplay", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-myinfo-applyplay.js"), array("jquery", "kn-play"));

		wp_localize_script("kn-play-myinfo-applyplay", "kn_applyplay_var", array(
			
		));

		wp_enqueue_script("kn-play-myinfo-applyplay");
	}

	static function draw_sidemenu(){
		$html_ = '참가한 놀이활동';
		return $html_;
	}

	static function draw_title(){
		$html_ = '참가한 놀이활동';
		return $html_;
	}
	static function draw_desc(){
		$html_ = '내가 신청,참가한 놀이활동을 열람할 수 있습니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';

		//$user_data_ = kn_current_user();
		//$html_ .= '<h4>'.$user_data_["user_name"].'회원님 환영합니다!</h4>';

		return $html_;
	}

	static function draw_content(){
		$user_data_ = kn_current_user();

		$play_list_table_ = new KN_play_myinfo_applyplay_table("kn-applyplay-list-table", "kn-applyplay-list-table fix-width");

		ob_start();
?>

<form class="kn-play-applyplay-frame">
<input type="hidden" name="sideid" value="applyplay">
	<?=$play_list_table_->html()?>
	
</form>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

add_action("kn_user_myinfo_sidemenu", array("KN_play_myinfo_applyplay","_add_sidemenu"));

?>