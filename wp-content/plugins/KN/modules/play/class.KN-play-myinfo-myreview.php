<?php

class KN_play_myinfo_myreview_table extends KNListTable{

	function prepare(){
		$user_id_ = kn_current_user_id();
		$page_index_ = isset($_GET["page_index"]) ? (int)$_GET["page_index"] : 0;
		$per_page_ = 10;
		$offset_ = $this->offset($page_index_, $per_page_);

		$keyword_ = isset($_GET["keyword"]) ? $_GET["keyword"] : "";

		global $wpdb;

		$query_conditions_ = array("table_alias" => "REV", "host_id" => $user_id_, "column_play_info" => true);
		$query_defaults_ = KN_play::query_make_review_conditions($query_conditions_);

		$query_ = "SELECT COUNT(*) CNT
			FROM {$wpdb->base_prefix}kn_play_rev REV
			LEFT OUTER JOIN {$wpdb->base_prefix}kn_users REV1
			ON REV.WRITER_ID = REV1.ID
			LEFT OUTER JOIN {$wpdb->base_prefix}kn_play REV2
			ON REV.ID = REV2.ID
			WHERE 1=1
			$query_defaults_ ";

		$total_count_ = $wpdb->get_row($query_)->CNT;
		$items_ = KN_play::review_list(array_merge($query_conditions_, array(
			"sort" => "reg_date desc", 
			"limit" => array(
				"offset" => $offset_,
				"length" => $per_page_,
			)
		)));
		
		return array(
			"items" => $items_,
			"page_index" => $page_index_,
			"total_count" => $total_count_,
			"per_page" => $per_page_,
			"pagenav_count" => 3,
			"hide_header" => true,
		);
	}

	function columns(){
		return array(
			"single_row" => "",
		);
	}
	function column_value($item_, $column_name_){
		$settings_ = KN_settings::get_settings();

		$play_view_url_ = get_permalink($settings_["play_view_page_id"]);
		// $play_edit_url_ = get_permalink($settings_["play_edit_page_id"]);


		$forward_url_ = $play_view_url_.$item_["ID"];

		ob_start();
?>
<div class="review-item" data-writer-id="3">
	<div class="play-info">
		<a href="<?=$forward_url_?>"><?=$item_["play_title"]?></a>
	</div>
	<div class="writer-info">
		<div class="kn-img-frame kn-profile xxsmall"><span style="background-image:url(http://localhost/kinclub/wp-content/uploads/2015/10/boys-838221_192012.jpg);"></span></div>
		<h5>홍길동(TEST)</h5>
		<div class="review-info">2015.10.25 14:24</div>
	</div>
	<div class="kn-score"><div style="width:60%"></div></div>
	<div class="review-content"></div>
</div>

<?php

		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
		
	}

	function norowdata(){
		return "등록된 후기가 없습니다.";
	}
}

class KN_play_myinfo_myreview extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("myreview", "KN_play_myinfo_myreview", 320);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("kn-listtable");
		kn_load_library("main-all");
		kn_load_library("kn-utils");
		kn_load_library("kn-fileupload");
		kn_load_library("kn-play");
		// kn_load_library("swiper");

		wp_enqueue_style("kn-play-myinfo-myreview", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-myinfo-myreview.css"));
		wp_register_script("kn-play-myinfo-myreview", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-myinfo-myreview.js"), array("jquery", "kn-play"));

		wp_localize_script("kn-play-myinfo-myreview", "kn_myreview_var", array(
			
		));

		wp_enqueue_script("kn-play-myinfo-myreview");
	}

	static function draw_sidemenu(){
		$html_ = '내 놀이활동 후기';
		return $html_;
	}

	static function draw_title(){
		$html_ = '내 놀이활동 후기';
		return $html_;
	}
	static function draw_desc(){
		$html_ = '내 놀이활동의 후기를 확인할 수 있습니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';

		//$user_data_ = kn_current_user();
		//$html_ .= '<h4>'.$user_data_["user_name"].'회원님 환영합니다!</h4>';

		return $html_;
	}

	static function draw_content(){
		$user_data_ = kn_current_user();
		$play_list_table_ = new KN_play_myinfo_myreview_table("kn-myreview-list-table", "kn-myreview-list-table fix-width kn-play-review-list");

		ob_start();
?>

<form class="kn-play-myreview-frame">
<input type="hidden" name="sideid" value="myreview">

	<?=$play_list_table_->html()?>
	
</form>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

add_action("kn_user_myinfo_sidemenu", array("KN_play_myinfo_myreview","_add_sidemenu"));

?>