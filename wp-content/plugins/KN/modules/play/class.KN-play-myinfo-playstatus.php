<?php

class KN_play_myinfo_playstatus extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("playstatus", "KN_play_myinfo_playstatus", 300);
	}

	static function add_main_library(){
		wp_enqueue_style("kn-play-myinfo-playstatus", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-myinfo-playstatus.css"));
	}

	static function draw_sidemenu(){return null;}
	static function draw_title(){return null;}
	static function draw_desc(){return null;}

	static function draw_main(){
		$settings_ = KN_settings::get_settings();
		global $wpdb;
		$register_conditions_array_ = array("host_id" => kn_current_user_id(), "only_registered" => true, "table_alias" => "PLY");
		$apply_conditions_array_ = array("mem_id" => kn_current_user_id(), "only_registered" => true, "table_alias" => "PLY");
		$review_host_conditions_array_ = array("host_id" => kn_current_user_id(), "table_alias" => "PLY");
		$review_write_conditions_array_ = array("writer_id" => kn_current_user_id(), "table_alias" => "PLY");
	
		$register_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_play PLY WHERE 1=1 ".KN_play::query_make_conditions($register_conditions_array_))->CNT;
		$apply_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_play PLY WHERE 1=1 ".KN_play::query_make_conditions($apply_conditions_array_))->CNT;
		$review_host_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_play_rev PLY WHERE 1=1 ".KN_play::query_make_review_conditions($review_host_conditions_array_))->CNT;
		$review_write_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_play_rev PLY WHERE 1=1 ".KN_play::query_make_review_conditions($review_write_conditions_array_))->CNT;

		$current_user_ = kn_current_user();
		$total_play_time_ = KN_play::total_play_time($current_user_["ID"]);
		$need_total_play_time_ = (int)$settings_["play_pro_switch_total_time"];
		$is_pro_ = KN_user::is_pro($current_user_["ID"]);

		ob_start();
?>

<h3 class="title-with-content">놀이활동현황</h3>
<div class="kn-form kn-form-table kn-playstatus-frame">
	<div class="kn-column-frame kn-column-padding">
		<div class="kn-column l33">
			<h4>등록</h4>
			<div class="point"><a href="<?=kn_make_url(get_permalink($settings_["user_myinfo_page_id"]),array("sideid"=>"myplay"))?>"><?=kn_pretty_number($register_count_)?> 건</a></div>
		</div>
		<div class="kn-column l33">
			<h4>참여</h4>
			<div class="point"><a href="<?=kn_make_url(get_permalink($settings_["user_myinfo_page_id"]),array("sideid"=>"applyplay"))?>"><?=kn_pretty_number($apply_count_)?> 건</a></div>
		</div>
		<div class="kn-column l33">
			<h4>후기(작성/받음)</h4>
			<div class="point">
				<?=kn_pretty_number($review_host_count_)?> 건 / <a href="<?=kn_make_url(get_permalink($settings_["user_myinfo_page_id"]),array("sideid"=>"myreview"))?>"><?=kn_pretty_number($review_write_count_)?> 건</a>
			</div>
		</div>
	</div>
</div>
<div class="kn-playstatus-playtime">
	<h4 class="title"><?=$current_user_["display_name"]?>님은 총 <?=number_format(KN_play::total_play_time(kn_current_user_id()))?>시간의 놀이활동을 진행하였습니다.</h4>
<?php if($is_pro_){ ?>
	<div class="content">현재 <?=get_bloginfo("name")?>의 <b>전문가</b>로 등록되어 있습니다.</div>
<?php }else{ ?>
	<div class="content">전문가전환까지 <b><?=$need_total_play_time_-$total_play_time_?></b>시간의 놀이활동진행이 필요합니다.</div>
<?php } ?>
</div>


<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function draw_content(){return null;}
}

add_action("kn_user_myinfo_sidemenu", array("KN_play_myinfo_playstatus","_add_sidemenu"));

?>