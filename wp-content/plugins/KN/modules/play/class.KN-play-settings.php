<?php

class KN_play_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			
			"play_find_page_id" => "",
			"play_view_page_id" => "",

			"play_register_page_id" => "",
			"play_edit_page_id" => "",

			"play_register_agreement_content" => "",

			"play_pro_switch_total_time" => "",
			
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_play_settings", "_add_default_settings"));

?>