<?php

class KN_play_view extends KNShortcode{

	static function shortcode_name(){
		return "kn-play-view";
	}

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		$user_addinfo_page_url_ = kn_make_url(get_permalink($settings_["user_myinfo_page_id"]), array("sideid" => "addinfo"));

		kn_load_library("main-all");
		// kn_load_library("blockui");
		kn_load_library("kn-listtable");
		kn_load_library("kn-utils");
		kn_load_library("kn-maps");
		kn_load_library("swiper");
		kn_load_library("raty");

		wp_enqueue_style("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play.css"));
		wp_enqueue_style("kn-play-view", (KN_PLUGIN_DIR_URL."modules/play/lib/css/class.KN-play-view.css"));
		
		wp_enqueue_script("kn-play", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play.js"), array("jquery-validate"));
		wp_register_script("kn-play-view", (KN_PLUGIN_DIR_URL."modules/play/lib/js/class.KN-play-view.js"), array("kn-play"));	

		wp_localize_script("kn-play-view", "kn_play_view_var",array(
			"has_no_children" => "놀이활동에 참여할 아이가 없습니다. 아이정보를 등록하시겠습니까?",
			"user_myinfo_addinfo_url" => $user_addinfo_page_url_,
			"user_myinfo_applyplay_url" => kn_make_url(get_permalink($settings_["user_myinfo_page_id"]), array("sideid" => "applyplay")),
			
			"noselect_child_title" => "참여인원이 없습니다",
			"noselect_child" => "<p>놀이활동에 참여할 아이를 선택하세요!</p>",
			
			"no_enough_remin_title" => "참여인원이 너무 많습니다.",
			"no_enough_remin" => "신청하신 인원이 모집정원보다 많습니다!",

			"apply_error_already" => "선택한 아이정보로 이미 신청하셨습니다.",
			"apply_error_reject" => "신청할 수 없는 놀이활동입니다.",
			"apply_error_no_enough_point" => "킨체리가 부족합니다.",
			"apply_error_no_enough_point_content" => "<p>현재 보유하신 킨체리가 부족합니다.</p><p>킨체리를 충전하시겠습니까?</p>",
			"button_confirm" => "확인",
			"button_recharge" => "충전하기",
			"button_cancel" => "취소",
			
			"apply_success" => "놀이활동참가신청이 완료되었습니다!",
			"cancel_success" => "놀이활동신청이 취소되었습니다.",

			"cancel_popup_title" => "참가신청취소",
			"cancel_popup_content" => "<p>놀이활동참가신청을 취소합니다</p><p>킨체리는 반환됩니다.</p>",
			"cancel_popup_OK" => "신청취소",

			"star-off-url" => (KN_PLUGIN_DIR_URL."lib/img/icon/score-back.png"),
			"star-on-url" => (KN_PLUGIN_DIR_URL."lib/img/icon/score-active.png"),
		));

		wp_enqueue_script("kn-play-view");
		
	}

	static function _do_shortcode($attrs_){
		global $wp_query;
		if(!isset($_GET["play_id"]) && !isset($wp_query->query["play_id"])){
			wp_die("잘못된 접근입니다.");
		}

		$play_id_ = isset($_GET["play_id"]) ? $_GET["play_id"] : $wp_query->query["play_id"];

		$current_user_ = kn_current_user();
		$play_data_ = KN_play::play($play_id_);
		$is_mine_ = ($play_data_["host_id"] === $current_user_["ID"]);
		$did_apply_ = KN_play::did_apply($play_id_, $current_user_["ID"]);


		if(!isset($play_data_) || $play_data_ === false){
			wp_die("잘못된 접근입니다.");
		}


		if($play_data_["host_id"] !== $current_user_["ID"]){
			if($play_data_["display_status"] === "00001" || $play_data_["display_status"] === "00009"){
				wp_die("잘못된 접근입니다.");
			}
		}

		$settings_ = KN_settings::get_settings();

		$user_addinfo_page_url_ = kn_make_url(get_permalink($settings_["user_myinfo_page_id"]), array("sideid" => "addinfo"));
		$play_edit_page_url_ = kn_make_url(get_permalink($settings_["play_edit_page_id"]), array("play_id" => $play_id_));

		ob_start();
?>
<form class="kn-play-view-form kn-form" id="kn-play-view-form">
	
	<input type="hidden" name="play_id" value="<?=$play_data_["ID"]?>">
	<input type="hidden" name="av_min_count" value="<?=$play_data_["av_min_cnt"]?>">
	<input type="hidden" name="av_max_count" value="<?=$play_data_["av_max_cnt"]?>">
	<input type="hidden" name="need_point" value="<?=$play_data_["need_point"]?>">

<div class="top-frame">
	<div class="main-img-frame swiper-container">
		<div class="swiper-wrapper">
<?php
	foreach($play_data_["main_img_urls"] as $main_img_url_){
		echo '<div class="swiper-slide" style="background-image:url('.$main_img_url_.');"></div>';
	}
?>

		</div>
	<div class="swiper-pagination"></div>
	</div>
	<div class="maps-frame" data-lat="<?=$play_data_["addr_lat"]?>" data-lng="<?=$play_data_["addr_lng"]?>" data-play-title="<?=$play_data_["play_title"]?>">

	</div>
	<div class="target-date-frame"><?=$play_data_["play_date_txt"]?></div>
</div>

<div class="title-frame"><div class="wrap">

	<div class="play-title-frame">
		<div class="play-title">
			<h3><?=$play_data_["play_title"]?></h3>
			<p class="address"><?=$play_data_["address"]?></p>
			<p class="play-ctg"><?=$play_data_["main_ctg_nm"]?> &gt; <?=$play_data_["sub_ctg_nm"]?></p>
		</div>
		<div class="play-point kn-color kn-color-point">
			<span class="point-icon knicon knicon-point-small"></span><span class="point-text"><?=$play_data_["need_point"]?></span>
		</div>
	</div>
	<div class="play-content">
		<div class="kn-host-review">
			<div class="kn-img-frame kn-profile xsmall"><span style="background-image:url(<?=$play_data_["host_profile_url"]?>)"></span></div>
			<div class="host-desc">
				<h5 class="host-name"><?=$play_data_["host_name"]?></h5>
				<div class="kn-score"><div style="width:<?=$play_data_["host_review_rate"]?>%"></div></div>
			</div>
		</div>
		<div class="button-frame">
			<?php if(!$is_mine_){

					if($play_data_["display_status"] === "00003" || $play_data_["display_status"] === "00005"){
						if($did_apply_){
							?><a href="javascript:;" class="knbtn knbtn-secondary kn-cancel-apply-play-btn">신청취소</a><?php
						}else{
							?><a href="javascript:;" class="knbtn knbtn-primary kn-apply-play-btn">참여하기</a><?php
						}
					}else{
						?><a href="javascript:;" class="knbtn knbtn-secondary disabled"><?=$play_data_["display_status_nm"]?></a><?php
					}
			?>
			<?php }else{
				?><a href="<?=$play_edit_page_url_?>" class="knbtn knbtn-primary">관리하기</a><?php
			} ?>
		</div>
	</div>
</div></div>
<div class="mem-list-frame"><div class="wrap">
	<h4><?=($play_data_["mem_count"] > 0 ? $play_data_["mem_count"]."명이 참여했습니다." : "참여자가 없습니다.")?></h4>
	<div class="swiper-container">
		<div class="swiper-wrapper">
<?php
		global $wpdb;

		$children_ = KN_play::member_list(array("play_id" => $play_data_["ID"]));
	
		$query_ = "SELECT MEM.MEM_ID mem_id
					  ,IF(USR.NICK_NAME IS NULL OR USR.NICK_NAME = '', USR.USER_NAME, USR.NICK_NAME) disply_name
				      ,CAST(DATE_FORMAT(NOW(),'%Y') AS UNSIGNED) - CAST(USR.BIRTH_YYYY AS UNSIGNED) +1 age
				      ,profile_url profile_url
				FROM (
					SELECT MEM.MEM_ID
				    FROM {$wpdb->base_prefix}KN_PLAY_MEM MEM
				    WHERE 1=1
				    AND   MEM.ID = ".$play_data_["ID"]."
				    AND   MEM.STATUS = '00001'
				    GROUP BY MEM.MEM_ID
				) MEM
				LEFT OUTER JOIN {$wpdb->base_prefix}KN_USERS USR
				ON   MEM.MEM_ID = USR.ID
				WHERE 1=1 ";

		$vistors_ = $wpdb->get_results($query_, ARRAY_A);

		foreach($vistors_ as $vistor_){

			$etc_mem_count_ = KN_play::etc_member_count($play_data_["ID"], $vistor_["mem_id"]);
?>
	<div class="swiper-slide">
		<div class="kn-img-frame kn-profile xsmall"><span style="background-image:url(<?=$vistor_["profile_url"]?>);"></span></div>
		<h5><?=$vistor_["disply_name"]?></h5>
		<p><?=$vistor_["age"]?>세</p>
		<div style="clear:both;"></div>
	</div>
<?php 
			foreach($children_ as $child_){
				if($child_["mem_id"] === $vistor_["mem_id"]){
?>
	<div class="swiper-slide">
		<div class="kn-img-frame kn-profile xsmall child"><span style="background-image:url(<?=$child_["child_img_url"]?>);"></span></div>
		<h5><?=$child_["child_name"]?></h5>
		<p><?=$child_["age"]?>세</p>
		<div style="clear:both;"></div>
	</div>
					
<?php	
				}
			}

			if($etc_mem_count_ > 0){ 
	?>
	<div class="swiper-slide">
		<div class="kn-img-frame kn-profile xsmall etc"><span><?=$etc_mem_count_?></span></div>
		<h5>기타인원</h5>
		<div style="clear:both;"></div>
	</div>

	<?php } ?>

	<div class="swiper-slide div"></div>

	<?php } ?>
		</div>
	</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="content-frame">
	<h4>놀이활동내용</h4>
	<p><?=$play_data_["desc_html"]?></p>
</div>

<div class="mobile-button-frame">

	<?php if(!$is_mine_){

			if($play_data_["display_status"] === "00003" || $play_data_["display_status"] === "00005"){
				if($did_apply_){
					?><a href="javascript:;" class="knbtn knbtn-secondary kn-cancel-apply-play-btn">신청취소</a><?php
				}else{
					?><a href="javascript:;" class="knbtn knbtn-primary kn-apply-play-btn">참여하기</a><?php
				}
			}else{
				?><a href="javascript:;" class="knbtn knbtn-secondary disabled"><?=$play_data_["display_status_nm"]?></a><?php
			}

	?>
	<?php }else{
		?><a href="<?=$play_edit_page_url_?>" class="knbtn knbtn-primary">관리하기</a><?php
	} ?>
</div>

</form>

<?php 
	
	if($play_data_["end_yn"] === "Y"){ 
	
?>

<div class="kn-play-review-frame loading" id="kn-play-view-review-frame" data-play-id="<?=$play_id_?>"><div class="wrap">
	<h4>놀이활동후기</h4>

<?php 
	
$did_review_ = KN_play::did_review($play_id_);
if(!$is_mine_ && $did_apply_){ ?>

<form id="kn-play-view-review-write-form" class="kn-form" style="<?=($did_review_ ? "display:none;" : "")?>">
	<input type="hidden" name="ID" value="<?=$play_id_?>">

	<div class="score-frame">
		<div class="score"></div>
	</div>
	<div class="txt-frame kn-column-frame kn-column-padding">
		<div class="kn-column l80">
			<textarea name="rev_txt" class="full-width" placeholder="후기를 남겨주세요!"></textarea>
		</div>
		<div class="kn-column l20">
			<a href="javascript:kn_play_view_write_review();" class="knbtn knbtn-primary">후기작성</a>
		</div>
	</div>
</form>

<?php } ?>

	<div class="kn-play-review-list"></div>
	<div class="more-btn"><a class="knbtn knbtn-secondary">더보기</a></div>
	<div class="nomore">후기를 모두 읽었습니다!</div>
	<div class="loading">불러오는 중...</div>
	<div class="norows">등록된 후기가 없습니다.</div>
</div></div>

<?php } ?>

<?php
	
	$children_ = KN_user::children(array("user_id" => $current_user_["ID"]));

?>
<div id="kn-play-apply-popup" class="mfp-hide kn-mg-popup with-button kn-form" data-user-current-point="<?=KN_point::point_count($current_user_["ID"])?>">
	<h3><?=$play_data_["play_title"]?> 참가</h3>
	<div class="wrap kn-form">
		<div class="content-wrap">
			<h4>놀이활동정보</h4>
			<p class="address"><?=$play_data_["address"]?> <?=$play_data_["address_dtl"]?></p>
			<p class="play-date-txt"><?=$play_data_["play_date_txt"]?></p>
			<p class="av-count">최소 <?=$play_data_["av_min_cnt"]?>명, 최대 <?=$play_data_["av_max_cnt"]?>명 참여가능</p>
		</div>

		<div class="content-wrap">
			<h4>놀이활동에 참여할 아이</h4>
	<?php
		if(count($children_) <= 0){
			?>
			<p class="hint">아이정보가 없습니다. <a href="<?=$user_addinfo_page_url_?>" class="kn-alink" target="_blank" id="kn-play-view-add-new-child">여기를 클릭</a>하여 아이정보를 추가합니다.</p>
			<?php
		}else{
			?>

			
			<table class="kn-list-table kn-form-table fix-width" id="kn-play-view-child-table">
				<tbody>
					<?php

						foreach($children_ as $child_){
						?>
						<tr>
						<td class="checkbox"><input type="checkbox" name="child_select" data-child-seq="<?=$child_["seq"]?>" data-accessible="true"></td>
						<td class="child-image-url"><span class="kn-img-frame kn-profile xxsmall"><span style='background-image:url();'></span></span></td>
						<td class="child-name"><?=$child_["display_name"]?></td>
						</tr>

						<?php
						}

					?>
				</tbody>
			</table>

			<?php
		}
	
	?>
		<div class="etc-mem-count-frame">
			<label for="etc_mem_count">기타인원 <input type="number" name="etc_mem_count" id="etc_mem_count" data-accessible="true"> 명</label>
		</div>

		<div class="total-info-frame">
			<span class="remin-count">
				<i class="fa fa-users"></i>
				<span class="remin-count-txt"></span>
			</span>
			<span class="need-point">
				<i class="knicon knicon-point-small"></i>
				<span class="need-point-txt"></span>
			</span>
		</div>
		
		</div>
		<div class="content-wrap">
			<h4>놀이활동이용약관</h4>
			<div class="kn-term-table">
				<div class="term-content kn-form-table"></div>
				<p class="hint">*놀이활동신청과 함께 약관에 동의한 것으로 간주합니다.</p>
			</div>
		</div>
	</div>
	<div class="button-area">
		<a class="knbtn knbtn-primary" id="kn-play-editor-agree-btn" data-accessible="true">참가신청</a>
		 <a class="knbtn knbtn-secondary" id="kn-play-editor-cancel-agree-btn" data-accessible="true">취소</a>
	</div>
</div>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_check_my_point(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}
	}

	static function _ajax_apply_play(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}

		$current_user_ = kn_current_user();
		$play_id_ = $_POST["play_id"];
		$play_data_ = KN_play::play($play_id_);
		$mem_id_ = $current_user_["ID"];
		$selected_children_ = isset($_POST["selected_children"]) ? $_POST["selected_children"] : array();
		$etc_mem_count_ = $_POST["etc_mem_count"];

		$total_mem_count_ = count($selected_children_) + $etc_mem_count_;
		$current_point_ = KN_point::point_count($current_user_["ID"]);
		$need_point_ = ($play_data_["need_point"]) * ($total_mem_count_);

		if($play_data_["av_min_cnt"] > $total_mem_count_ || $play_data_["av_max_cnt"] < $total_mem_count_){
			echo json_encode(array(
				"reject" => true,
			));
			die();
		}

		if($need_point_ > $current_point_){
			echo json_encode(array(
				"no_enough_point" => true,
			));
			die();
		}

		KN_point::add_point($mem_id_, -$need_point_, array("play_id" => $play_id_ ,"RMK" => $play_data_["play_title"]." ".$total_mem_count_."명 참가"));

		foreach($selected_children_ as $child_seq_){
			KN_play::add_member($play_id_, $mem_id_, $child_seq_);
		}

		KN_play::update_etc_member_count($play_id_, $mem_id_, $etc_mem_count_);

		echo json_encode(array(
			"success" => true
		));
		die();
	}

	static function _ajax_cancel_apply_play(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}

		$current_user_ = kn_current_user();
		$play_id_ = $_POST["play_id"];
		$play_data_ = KN_play::play($play_id_);

		if($play_data_["display_status"] !== "00003" && $play_data_["display_status"] === "00007"){
			echo json_encode(array(
				"reject" => true,
			));
			die();
		}

		$target_point_ = KN_point::point_count($current_user_["ID"], array("only_registered" => true, "play_id" => $play_id_));
		
		KN_point::add_point($current_user_["ID"], -$target_point_, array("play_id" => $play_id_,"RMK" => $play_data_["play_title"]." 참가취소"));
		KN_play::cancel_member($play_id_, $current_user_["ID"]);

		echo json_encode(array(
			"success" => true
		));
		die();
	}

	static function _ajax_load_review_list(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}

		$play_id_ = $_POST["play_id"];
		$result_ = KN_play::review_list(array("play_id" => $play_id_, "column_is_mine" => true));

		echo json_encode(array(
			"success" => true,
			"result" => $result_,
		));
		die();
	}

	static function _ajax_write_review(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}

		$review_data_ = $_POST["review_data"];
		$review_data_["writer_id"] = kn_current_user_id();

		$result_ = KN_play::write_review($review_data_);
		echo json_encode(array(
			"success" => ($result_ > 0)
		));
		die();
	}
	static function _ajax_remove_review(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array("reject" => true));
			die();
		}

		$play_id_ = $_POST["play_id"];
		$writer_id_ = kn_current_user_id();

		$result_ = KN_play::remove_review($play_id_, $writer_id_);
		echo json_encode(array(
			"success" => ($result_ > 0)
		));
		die();
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();
		$user_login_page_id_ = $settings_["user_login_page_id"];
		$play_view_page_id_ = $settings_["play_view_page_id"];

		if(strlen($play_view_page_id_) && ((string)$current_page_id_) === ((string)$play_view_page_id_) ){
			if(!kn_is_user_logged_in()){
				global $wp_query;
				$play_id_ = kn_current_play_id();
				$login_url_ = kn_make_url(get_permalink($user_login_page_id_), array("redirect" => urlencode(get_permalink($play_view_page_id_).$play_id_)));
				wp_redirect($login_url_);
				exit();
			}
		}
	}
}

add_shortcode((KN_play_view::shortcode_name()), array("KN_play_view","_do_shortcode"));
add_action("kn_shortcode_filter", array("KN_play_view","filter"));
add_action("kn_play_prevent_page", array("KN_play_view", "_prevent_page"));

kn_add_ajax("kn-play-view-check-my-point", array("KN_play_view", "_ajax_check_my_point"), true);
kn_add_ajax("kn-play-view-apply-child", array("KN_play_view", "_ajax_apply_play"), true);
kn_add_ajax("kn-play-view-cancel-apply", array("KN_play_view", "_ajax_cancel_apply_play"), true);
kn_add_ajax("kn-play-view-load-review-list", array("KN_play_view", "_ajax_load_review_list"), true);
kn_add_ajax("kn-play-view-write-review", array("KN_play_view", "_ajax_write_review"), true);
kn_add_ajax("kn-play-view-remove-review", array("KN_play_view", "_ajax_remove_review"), true);

?>