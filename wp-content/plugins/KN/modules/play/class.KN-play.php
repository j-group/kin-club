<?php

class KN_play{
	/**
	놀이활동 - 기본
	*/
	static function _filter_encode_play_columns($data_){
		if(isset($data_["main_img_urls"])){
			$data_["main_img_urls"] = json_encode($data_["main_img_urls"]);
		}

		if(isset($data_["play_date"])){
			$data_["play_date"] = strlen($data_["play_date"]) ? $data_["play_date"] : KN_QUERY_NULL;
		}
		if(isset($data_["play_srt_time"])){
			$data_["play_srt_time"] = strlen($data_["play_srt_time"]) ? $data_["play_srt_time"] : KN_QUERY_NULL;
		}
		if(isset($data_["play_end_time"])){
			$data_["play_end_time"] = strlen($data_["play_end_time"]) ? $data_["play_end_time"] : KN_QUERY_NULL;
		}

		return $data_;
	}

	static function _filter_decode_play_columns($data_){
		if(isset($data_["main_img_urls"])){
			$data_["main_img_urls"] = json_decode($data_["main_img_urls"]);
		}else{
			$data_["main_img_urls"] = array();
		}

		return $data_;
	}

	static function create($data_){
		global $wpdb;

		$data_ = array_merge(array(
			"play_title" => KN_QUERY_NULL,
			"desc_html" => KN_QUERY_NULL,
			
			"status" => "00001",

			"main_ctg" => KN_QUERY_NULL,
			"sub_ctg" => KN_QUERY_NULL,

			"host_id" => KN_QUERY_NULL,
			
		),$data_);

		$data_ = apply_filters("kn_play_encode_columns",$data_);

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_play", array(
			"play_title" => $data_["play_title"],
			"desc_html" 	=> $data_["desc_html"],

			"status" => $data_["status"],
			
			"main_ctg" => $data_["main_ctg"],
			"sub_ctg" => $data_["sub_ctg"],

			"host_id" => $data_["host_id"],
		),array(
			"%s",
			"%s",

			"%s",
			
			"%s",
			"%s",

			"%d",
		));

		if(!$wpdb->query($query_)) return false;

		$id_ = $wpdb->insert_id;
		do_action("kn_play_created",$id_);

		return $id_;
	}

	static function update($id_, $data_){
		global $wpdb;

		unset($data_["id"]);

		$query_ = kn_query_update("{$wpdb->base_prefix}kn_play", apply_filters("kn_play_encode_columns", $data_),array(
				"ID" => $id_
			),null, array("%d"));

		$result_ = $wpdb->query($query_);

		if($result_ === false) return false;

		do_action("kn_play_updated",$id_);
		return $id_;
	}

	static function delete($id_){
		global $wpdb;

		$result_ = $wpdb->delete("{$wpdb->base_prefix}kn_play",array(
			"ID" => $id_
		), array("%d"));

		if($result_ === false) return false;

		do_action("kn_play_deleted",$id_);
		return $id_;	
	}

	static function query_make_conditions($conditions_ = array()){
		global $wpdb;

		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_PLY_TMP";

		if(isset($conditions_["status"])){
			if(gettype($conditions_["status"]) === "string")
				$conditions_["status"] = array($conditions_["status"]);

			$query_ .= " AND {$table_alias_}.status IN (-999 ";
			foreach($conditions_["status"] as $row_index_ => $status_){
				$query_ .= ",'".$status_."' ";
			}
			$query_ .= ") ";
		}

		if(isset($conditions_["play_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["play_id"]." ";
		}

		if(isset($conditions_["host_id"])){
			$query_ .= " AND {$table_alias_}.host_id = ".$conditions_["host_id"]." ";
		}

		if(isset($conditions_["mem_id"])){
			$query_ .= " AND ".$conditions_["mem_id"]." IN (SELECT {$table_alias_}3.MEM_ID FROM {$wpdb->base_prefix}KN_PLAY_MEM {$table_alias_}3 WHERE {$table_alias_}3.ID = {$table_alias_}.ID) ";
		}

		if(isset($conditions_["main_ctg"])){
			$query_ .= " AND {$table_alias_}.main_ctg = '".($conditions_["main_ctg"])."' ";
		}
		if(isset($conditions_["sub_ctg"])){
			$query_ .= " AND {$table_alias_}.sub_ctg = '".($conditions_["sub_ctg"])."' ";
		}

		if(isset($conditions_["start_date"]) && strlen($conditions_["start_date"])){
			$query_ .= " AND {$table_alias_}.play_date >= '".($conditions_["start_date"])."' ";
		}
		if(isset($conditions_["end_date"]) && strlen($conditions_["end_date"])){
			$query_ .= " AND {$table_alias_}.play_date <= '".($conditions_["end_date"])."' ";
		}

		if(isset($conditions_["play_srt_time"]) && strlen($conditions_["play_srt_time"])){
			$query_ .= " AND {$table_alias_}.play_srt_time >= '".($conditions_["play_srt_time"])."' ";
		}
		if(isset($conditions_["play_end_time"]) && strlen($conditions_["play_end_time"])){
			$query_ .= " AND {$table_alias_}.play_srt_time >= '".($conditions_["play_end_time"])."' ";
		}


		if(isset($conditions_["av_min_cnt"]) && strlen($conditions_["av_min_cnt"])){
			$query_ .= " AND {$table_alias_}.av_min_cnt <= '".($conditions_["av_min_cnt"])."' ";
		}
		if(isset($conditions_["av_max_cnt"]) && strlen($conditions_["av_max_cnt"])){
			$query_ .= " AND {$table_alias_}.av_max_cnt >= '".($conditions_["av_max_cnt"])."' ";
		}

		if(isset($conditions_["av_age"])){
			if(gettype($conditions_["av_age"]) !== "array"){
				$tmp_age_ = $conditions_["av_age"];
				$conditions_["av_age"] = array(
					"from" => $tmp_age_,
					"to" => $tmp_age_,
				);
			}

			if(isset($conditions_["av_age"]["from"]))
				$query_ .= " AND {$table_alias_}.av_srt_age <= ".$conditions_["av_age"]["from"]." ";

			if(isset($conditions_["av_age"]["to"]))
				$query_ .= " AND {$table_alias_}.av_end_age >= ".$conditions_["av_age"]["to"]." ";
		}

		if(isset($conditions_["current_latlng"]) && isset($conditions_["latlng_distance"])){
			$query_ .= " AND ".kn_query_latlng_distance($conditions_["current_latlng"]["lat"], $conditions_["current_latlng"]["lng"], "{$table_alias_}.addr_lat", "{$table_alias_}.addr_lng")." <= ".$conditions_["latlng_distance"]." ";
		}

		if(isset($conditions_["search_text"]) && strlen($conditions_["search_text"])){
			$query_ .= " AND CONCAT({$table_alias_}.play_title,' ',(SELECT TMP.user_name FROM {$wpdb->base_prefix}kn_users TMP WHERE TMP.ID = host_id)) LIKE '%".$conditions_["search_text"]."%' ";
		}

		return $query_;
	}

	static function play_list($conditions_ = array()){
		global $wpdb;

		$conditions_["table_alias"] = "KN_PLY_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY ID DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		$query_mem_conditions_ = self::query_make_member_conditions(array("status" => "00001", "table_alias" => "{$table_alias_}1"));

		$query_display_status_column_ = "CASE
					WHEN {$table_alias_}.end_yn = 'Y' THEN '00007'
					WHEN {$table_alias_}.close_yn = 'Y' THEN '00005'
					ELSE {$table_alias_}.STATUS
				END";

		$query_ = "SELECT 
				{$table_alias_}.ID ID,
				{$table_alias_}.play_title play_title,
				{$table_alias_}.desc_html desc_html,
				
				{$table_alias_}.status status,
				".kn_query_gcode_dtl_nm("P0001","{$table_alias_}.status")." status_nm,
				
				main_ctg main_ctg,
				".kn_query_gcode_dtl_nm("P0003","{$table_alias_}.main_ctg")." main_ctg_nm,

				sub_ctg sub_ctg,
				".kn_query_gcode_dtl_nm("P0004","{$table_alias_}.sub_ctg")." sub_ctg_nm,
				
				play_date play_date,
				play_srt_time play_srt_time,
				play_end_time play_end_time,
				CONCAT(DATE_FORMAT(play_date, '%c/%e'),'(',".kn_query_day_kor("play_date").",')',IFNULL(DATE_FORMAT(play_srt_time,' %l:%e %p'),'')) play_date_txt,

				host_id host_id,
				(SELECT TMP.user_name FROM {$wpdb->base_prefix}kn_users TMP WHERE TMP.ID = host_id) host_name,
				(SELECT TMP.profile_url FROM {$wpdb->base_prefix}kn_users TMP WHERE TMP.ID = host_id) host_profile_url,
				IF((SELECT TMP.ID FROM {$wpdb->base_prefix}kn_users_pro TMP WHERE TMP.ID = host_id AND TMP.STATUS ='00003') IS NULL, 'N', 'Y') pro_yn,
				{$table_alias_}.mem_count mem_count,

				{$table_alias_}.close_yn close_yn,
				{$table_alias_}.end_yn end_yn,
				IF({$table_alias_}.close_yn = 'N' AND {$table_alias_}.END_YN = 'N' AND {$table_alias_}.STATUS = '00003', 'Y', 'N') can_apply_yn,

				$query_display_status_column_ display_status,
				".kn_query_gcode_dtl_nm("P0001","($query_display_status_column_)")." display_status_nm,
				

				IF(status = '00003' AND END_YN = 'N' AND CLOSE_YN = 'N', 1, 0) find_sort,

				".self::query_column_review_score("{$table_alias_}.ID")." review_score,
				".self::query_column_review_rate("{$table_alias_}.ID")." review_rate,

				".self::query_column_host_review_score("{$table_alias_}.HOST_ID")." host_review_score,
				".self::query_column_host_review_rate("{$table_alias_}.HOST_ID")." host_review_rate,

				address address,
				address_dtl address_dtl,
				addr_lat addr_lat,
				addr_lng addr_lng,

				need_point need_point,
				need_cost need_cost,

				av_min_cnt av_min_cnt,
				av_max_cnt av_max_cnt,

				av_srt_age av_srt_age,
				av_end_age av_end_age,

				main_img_urls main_img_urls,
				ref_url ref_url,";

		if(isset($conditions_["current_latlng"])){
			$query_ .= kn_query_latlng_distance($conditions_["current_latlng"]["lat"], $conditions_["current_latlng"]["lng"], "{$table_alias_}.addr_lat", "{$table_alias_}.addr_lng")." addr_distance,";
		}

		$query_ .= "reg_date reg_date,
				mod_date mod_date,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";
		
		$query_ .= " FROM (
					SELECT {$table_alias_}.*,";

		//$query_ .= "IF((IFNULL({$table_alias_}1.mem_count,0) >= {$table_alias_}.av_max_cnt) AND {$table_alias_}.STATUS = '00003', 'Y', 'N') close_yn,";
		$query_ .= "'N' close_yn,";

		$query_ .= "	IF(TIMESTAMPDIFF(MINUTE,now(),concat(play_date, ' ',play_srt_time)) <= 0 AND {$table_alias_}.STATUS = '00003' , 'Y', 'N') end_yn,
						IFNULL({$table_alias_}1.mem_count,0) mem_count

					FROM {$wpdb->base_prefix}KN_PLAY {$table_alias_}";
		$query_ .= " LEFT OUTER JOIN (
				SELECT {$table_alias_}1.ID,
						count(CHILD_SEQ)+SUM(IFNULL(ETC_MEM_CNT,0)) mem_count
				FROM {$wpdb->base_prefix}KN_PLAY_MEM {$table_alias_}1
				WHERE 1=1
				$query_mem_conditions_
				GROUP BY {$table_alias_}1.ID
			) {$table_alias_}1 ";
		$query_ .= " ON   {$table_alias_}.ID = {$table_alias_}1.ID ";
		$query_ .= " ){$table_alias_} ";
		$query_ .= " WHERE 1=1 ";
		$query_ .= self::query_make_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		$results_ = $wpdb->get_results($query_,ARRAY_A);
		foreach($results_ as $row_index_ => $result_){
			$results_[$row_index_] = apply_filters("kn_play_decode_columns", $result_);
		}

		return $results_;
	}

	static function play($play_id_){
		$play_ = self::play_list(array("play_id" => $play_id_));
		if(!isset($play_) || count($play_) <= 0) return false;
		return $play_[0];
	}

	static function total_play_time($user_id_){
		$table_alias_ = "KN_PLY_TMP";

		global $wpdb;

		$query_mem_conditions_ = self::query_make_member_conditions(array("status" => "00001", "table_alias" => "{$table_alias_}1"));

		$query_ = "";
		$query_ .= "SELECT SUM(IFNULL(CAST((DATE_FORMAT(TIMEDIFF({$table_alias_}.PLAY_END_TIME, {$table_alias_}.PLAY_SRT_TIME), '%H')) AS UNSIGNED), 0)) total_time";
		$query_ .= " FROM (
					SELECT {$table_alias_}.*,
						IF(TIMESTAMPDIFF(MINUTE,now(),concat(play_date, ' ',play_srt_time)) <= 0 AND {$table_alias_}.STATUS = '00003' , 'Y', 'N') end_yn,
						IFNULL({$table_alias_}1.mem_count,0) mem_count

					FROM {$wpdb->base_prefix}KN_PLAY {$table_alias_}";
		$query_ .= " LEFT OUTER JOIN (
				SELECT {$table_alias_}1.ID,
						count(CHILD_SEQ)+SUM(IFNULL(ETC_MEM_CNT,0)) mem_count
				FROM {$wpdb->base_prefix}KN_PLAY_MEM {$table_alias_}1
				WHERE 1=1
				$query_mem_conditions_
				GROUP BY {$table_alias_}1.ID
			) {$table_alias_}1 ";
		$query_ .= " ON   {$table_alias_}.ID = {$table_alias_}1.ID ";
		$query_ .= " ){$table_alias_} ";
		$query_ .= " WHERE 1=1 ";
		$query_ .= " AND   IFNULL({$table_alias_}.mem_count,0) > 0 ";
		$query_ .= " AND   {$table_alias_}.END_YN = 'Y' AND {$table_alias_}.HOST_ID = $user_id_ ";

		$total_time_ = $wpdb->get_row($query_)->total_time;

		if(!strlen($total_time_)){
			$total_time_ = 0;
		}

		return $total_time_;
	}

	/**
	놀이활동 - 참여자 
	*/
	static function _get_new_member_seq($play_id_, $mem_id_){
		global $wpdb;
		$query_ = "SELECT IFNULL((SELECT MAX(TMP.SEQ) FROM {$wpdb->base_prefix}KN_PLAY_MEM TMP WHERE TMP.ID = $play_id_ AND TMP.MEM_ID = $mem_id_),0)+1 SEQ FROM DUAL";
		return $wpdb->get_row($query_)->SEQ;
	}

	static function add_member($play_id_, $mem_id_, $child_seq_){
		if(self::member($play_id_, $mem_id_, $child_seq_) !== false){
			return -2;
		}

		global $wpdb;
		$new_seq_ = self::_get_new_member_seq($play_id_, $mem_id_);

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_play_mem", array(
			"id" => $play_id_,
			"seq" => $new_seq_,
			"mem_id" => $mem_id_,
			"child_seq" => $child_seq_,
			"status" => "00001",
		));


		$result_ = $wpdb->query($query_);
		if($result_ === false) return $result_;

		return $new_seq_;
	}
	static function cancel_member($play_id_, $mem_id_, $child_seq_ = null){
		$data_ = array(
			"id" => $play_id_,
			"mem_id" => $mem_id_,
		);

		if(strlen($child_seq_)){
			$data_["child_seq"] = $child_seq_;
		}

		global $wpdb;
		$query_ = kn_query_delete("{$wpdb->base_prefix}kn_play_mem", $data_);
		return $wpdb->query($query_);
	}
	static function query_make_member_conditions($conditions_){
		global $wpdb;

		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_PLY_TMP";

		if(isset($conditions_["status"])){
			if(gettype($conditions_["status"]) === "string")
				$conditions_["status"] = array($conditions_["status"]);

			$query_ .= " AND {$table_alias_}.status IN (-999 ";
			foreach($conditions_["status"] as $row_index_ => $status_){
				$query_ .= ",'".$status_."' ";
			}
			$query_ .= ") ";
		}

		if(isset($conditions_["play_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["play_id"]." ";
		}
		if(isset($conditions_["mem_id"])){
			$query_ .= " AND {$table_alias_}.mem_id = ".$conditions_["mem_id"]." ";
		}
		if(isset($conditions_["seq"])){
			$query_ .= " AND {$table_alias_}.seq = ".$conditions_["seq"]." ";
		}

		if(isset($conditions_["child_seq"])){
			$query_ .= " AND {$table_alias_}.child_seq = ".$conditions_["child_seq"]." ";
		}

		return $query_;
	}
	static function member_list($conditions_ = array()){
		global $wpdb;

		$conditions_["table_alias"] = "KN_PLY_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY MEM_ID DESC, CHILD_SEQ DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		$query_ = "SELECT 
				{$table_alias_}.ID ID,
				{$table_alias_}.mem_id mem_id,
				{$table_alias_}.seq seq,

				{$table_alias_}.child_seq child_seq,

				{$table_alias_}1.child_name,
				{$table_alias_}1.age,
				{$table_alias_}1.sex,
				
				{$table_alias_}.status status,
				".kn_query_gcode_dtl_nm("P0007","{$table_alias_}.status")." status_nm,

				child_img_url child_img_url,
				
				{$table_alias_}.reg_date reg_date,
				{$table_alias_}.mod_date mod_date,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";
		
		$query_ .= " FROM {$wpdb->base_prefix}KN_PLAY_MEM {$table_alias_} ";
		$query_ .= " LEFT OUTER JOIN  {$wpdb->base_prefix}KN_USERS_CHD {$table_alias_}1 
					ON  {$table_alias_}.MEM_ID = {$table_alias_}1.ID 
					AND {$table_alias_}.CHILD_SEQ = {$table_alias_}1.SEQ ";


		$query_ .= " WHERE {$table_alias_}.CHILD_SEQ IS NOT NULL ";
		$query_ .= self::query_make_member_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		return $wpdb->get_results($query_,ARRAY_A);
	}

	static function member($play_id_, $mem_id_, $child_seq_){
		$member_ = self::member_list(array("play_id" => $play_id_, "mem_id" => $mem_id_, "child_seq" => $child_seq_));
		if(!isset($member_) || count($member_) <= 0) return false;
		return $member_[0];
	}

	static function did_apply($play_id_, $mem_id_){
		$etc_member_count_ = self::etc_member_count($play_id_, $mem_id_);
		if($etc_member_count_ > 0) return true;

		$member_list_ = self::member_list(array("play_id" => $play_id_, "mem_id" => $mem_id_));
		return (isset($member_list_) && count($member_list_) > 0);
	}

	static function etc_member_count($play_id_, $mem_id_){
		global $wpdb;

		$query_ = "SELECT SUM(TMP.ETC_MEM_CNT) ETC_MEM_CNT FROM {$wpdb->base_prefix}KN_PLAY_MEM TMP
				WHERE 1=1
				AND   TMP.STATUS = '00001'
				AND   TMP.ID = $play_id_
				AND   TMP.MEM_ID = $mem_id_ ";

		return $wpdb->get_row($query_)->ETC_MEM_CNT;
	}

	static function update_etc_member_count($play_id_, $mem_id_, $count_){
		global $wpdb;

		$result_ = false;

		if($count_ > 0){
			$new_seq_ = self::_get_new_member_seq($play_id_, $mem_id_);
			$query_ = kn_query_insert("{$wpdb->base_prefix}KN_PLAY_MEM", array(
				"id" => $play_id_,
				"mem_id" => $mem_id_,
				"seq" => $new_seq_,
				"status" => "00001",
				"etc_mem_cnt" => $count_,
			), null, true);

			$result_ = $wpdb->query($query_);
		}else{
			$query_ = "SELECT TMP.SEQ 
					FROM {$wpdb->base_prefix}KN_PLAY_MEM TMP
					WHERE 1=1
					AND   TMP.ID = $play_id_
					AND   TMP.MEM_ID = $mem_id_
					AND   TMP.ETC_MEM_CNT IS NOT NULL";

			$exists_ = $wpdb->get_row($query_);
			if(!isset($exists_) || !$exists_){
				return true;
			}

			$query_ = kn_query_delete("{$wpdb->base_prefix}KN_PLAY_MEM", array(
				"id" => $play_id_,
				"mem_id" => $mem_id_,
				"seq" => $exists_->SEQ,
			), null, true);

			$result_ = $wpdb->query($query_);
		}

		if($result_ === false) return $result_;
		return $new_seq_;
	}


	/**
	사용자리뷰
	*/
	static function query_column_review_rate($play_id_column_){
		global $wpdb;
		return "(SELECT (AVG(KN_TMP_PLAY_REV_ALIAS.REV_SCORE)/5)*100 FROM {$wpdb->base_prefix}kn_play_rev KN_TMP_PLAY_REV_ALIAS WHERE KN_TMP_PLAY_REV_ALIAS.ID = $play_id_column_)";
	}
	static function query_column_review_score($play_id_column_){
		global $wpdb;
		return "(SELECT SUM(KN_TMP_PLAY_REV_ALIAS.REV_SCORE) FROM {$wpdb->base_prefix}kn_play_rev KN_TMP_PLAY_REV_ALIAS WHERE KN_TMP_PLAY_REV_ALIAS.ID = $play_id_column_)";	
	}
	static function query_column_host_review_rate($user_id_column_){
		global $wpdb;
		return "(SELECT (AVG(KN_TMP_PLAY_REV_ALIAS.REV_SCORE)/5)*100 FROM {$wpdb->base_prefix}kn_play_rev KN_TMP_PLAY_REV_ALIAS WHERE KN_TMP_PLAY_REV_ALIAS.ID IN (SELECT KN_TMP_PLAY_REV_ALIAS1.ID FROM {$wpdb->base_prefix}kn_play KN_TMP_PLAY_REV_ALIAS1 WHERE KN_TMP_PLAY_REV_ALIAS1.HOST_ID = $user_id_column_))";
	}
	static function query_column_host_review_score($user_id_column_){
		global $wpdb;
		return "(SELECT SUM(KN_TMP_PLAY_REV_ALIAS.REV_SCORE) FROM {$wpdb->base_prefix}kn_play_rev KN_TMP_PLAY_REV_ALIAS WHERE KN_TMP_PLAY_REV_ALIAS.ID IN (SELECT KN_TMP_PLAY_REV_ALIAS1.ID FROM {$wpdb->base_prefix}kn_play KN_TMP_PLAY_REV_ALIAS1 WHERE KN_TMP_PLAY_REV_ALIAS1.HOST_ID = $user_id_column_))";
	}


	static function review_rate($play_id_){
		global $wpdb;
		$query_ = "SELECT ".self::query_column_review_rate($play_id_)." RATE FROM DUAL DUAL";
		return $wpdb->get_row($query_)->RATE;
	}
	static function review_score($play_id_){
		global $wpdb;
		$query_ = "SELECT ".self::query_column_review_score($play_id_)." SCORE FROM DUAL DUAL";
		return $wpdb->get_row($query_)->SCORE;
	}

	static function host_review_rate($play_id_){
		global $wpdb;
		$query_ = "SELECT ".self::query_column_host_review_rate($play_id_)." RATE FROM DUAL DUAL";
		return $wpdb->get_row($query_)->RATE;
	}
	static function host_play_rate($play_id_){
		global $wpdb;
		$query_ = "SELECT ".self::query_column_host_review_score($play_id_)." SCORE FROM DUAL DUAL";
		return $wpdb->get_row($query_)->SCORE;
	}

	static function write_review($data_){
		if(!isset($data_["ID"]) || !isset($data_["writer_id"])) return false;

		if(self::did_review($data_["ID"], $data_["writer_id"])){
			return -2;
		}

		global $wpdb;
		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_play_rev", $data_);

		$result_ = $wpdb->query($query_);
		if($result_ === false) return $result_;

		return true;
	}

	static function remove_review($play_id_, $writer_id_){
		$data_ = array(
			"id" => $play_id_,
			"writer_id" => $writer_id_,
		);

		global $wpdb;
		$query_ = kn_query_delete("{$wpdb->base_prefix}kn_play_rev", $data_);
		return $wpdb->query($query_);
	}

	static function query_make_review_conditions($conditions_){
		global $wpdb;
		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_REV_TMP";

		if(isset($conditions_["play_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["play_id"]." ";
		}

		if(isset($conditions_["writer_id"])){
			$query_ .= " AND {$table_alias_}.writer_id = ".$conditions_["writer_id"]." ";
		}

		if(isset($conditions_["host_id"])){
			$query_ .= " AND {$table_alias_}.ID IN (SELECT TMP.ID FROM {$wpdb->base_prefix}kn_play TMP WHERE TMP.HOST_ID = ".$conditions_["host_id"].") ";
		}

		if(isset($conditions_["column_play_info"]) && $conditions_["column_play_info"] === true){
			if(isset($conditions_["host_id"])){
				$query_ .= " AND {$table_alias_}2.host_id = ".$conditions_["host_id"]." ";		
			}
			
		}

		return $query_;
	}

	static function review_list($conditions_ = array()){
		$conditions_["table_alias"] = "KN_REV_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY REG_DATE DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		global $wpdb;

		$query_ = "SELECT 
				{$table_alias_}.ID ID,
				{$table_alias_}.writer_id writer_id,
				{$table_alias_}1.user_name writer_name,
				{$table_alias_}1.profile_url writer_img_url,

				rev_txt rev_txt,
				rev_score rev_score,
				(rev_score/5)*100 rev_score_rate,";

		if(isset($conditions_["column_is_mine"]) && $conditions_["column_is_mine"] === true){
			$query_ .= " IF({$table_alias_}.writer_id = ".kn_current_user_id().", 'Y', 'N') is_mine, ";
		}

		if(isset($conditions_["column_play_info"]) && $conditions_["column_play_info"] === true){
			$query_ .= " {$table_alias_}2.play_title play_title,
						 {$table_alias_}2.play_date play_date,
						 CONCAT(DATE_FORMAT({$table_alias_}2.play_date, '%c/%e'),'(',".kn_query_day_kor("{$table_alias_}2.play_date").",')',IFNULL(DATE_FORMAT({$table_alias_}2.play_srt_time,' %l:%e %p'),'')) play_date_txt,
						 {$table_alias_}2.host_id,";
		}

		$query_ .= "
				{$table_alias_}.reg_date reg_date,
				{$table_alias_}.mod_date mod_date,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";

		$query_ .= " FROM {$wpdb->base_prefix}KN_PLAY_REV {$table_alias_}";
		$query_ .= " LEFT OUTER JOIN {$wpdb->base_prefix}KN_USERS {$table_alias_}1 ";
		$query_ .= " ON {$table_alias_}.writer_id = {$table_alias_}1.ID ";
		if(isset($conditions_["column_play_info"]) && $conditions_["column_play_info"] === true){
			$query_ .= " LEFT OUTER JOIN {$wpdb->base_prefix}KN_PLAY {$table_alias_}2 ";
			$query_ .= " ON {$table_alias_}.ID = {$table_alias_}2.ID ";
		}

		$query_ .= " WHERE 1=1 ";
		$query_ .= self::query_make_review_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		return $wpdb->get_results($query_,ARRAY_A);
	}
	static function review($play_id_, $writer_id_){
		$review_ = self::review_list(array("play_id" => $play_id_, "writer_id" => $writer_id_));
		if(!isset($review_) || count($review_) <= 0) return false;
		return $review_[0];
	}

	static function did_review($play_id_, $writer_id_ = null){
		if(!strlen($writer_id_)){
			$writer_id_ = kn_current_user_id();
		}

		return (self::review($play_id_, $writer_id_) !== false);
	}

	/**
	시스템
	*/
	static function register_library(){

		wp_register_style("kn-play", (KN_PLUGIN_DIR_URL. 'modules/play/lib/css/class.KN-play.css'));
		wp_register_style("kn-play-finder", (KN_PLUGIN_DIR_URL. 'modules/play/lib/css/class.KN-play-finder.css'));

		wp_register_script("kn-play", (KN_PLUGIN_DIR_URL. 'modules/play/lib/js/class.KN-play.js'), array("jquery","kn-main"));
		wp_localize_script("kn-play", "kn_play_var", array(
			"closed" => "마감됨",
			"ended" => "종료됨",
			"cannot_use_geolocation" => "위치정보를 확인할 수 없습니다.",
		));

		wp_register_script("kn-play-finder", (KN_PLUGIN_DIR_URL. 'modules/play/lib/js/class.KN-play-finder.js'), array("jquery", "kn-main","kn-play"));

		KN_Library::register("kn-play",array(
			"script" => array(
				"jquery",
				"main",
				"kn-play",
			),"style" => array(
				"kn-play",
			),
		));
		KN_Library::register("kn-play-finder",array(
			"script" => array(
				"jquery",
				"kn-play",
				"kn-play-finder",
			),"style" => array(
				"kn-play",
				"kn-play-finder",
			),"library" => array(
				"kn-play",
			)
		));
	}

	static function main_categories($filter_ = array()){
		$main_ctg_list_ = KN_gcode::gcode_dtl_list("P0003");
		return $main_ctg_list_;
	}
	static function sub_categories($main_category_, $filter_ = array()){
		$temp_list_ = KN_gcode::gcode_dtl_list("P0004");
		$sub_ctg_list_ = array();

		$check_char_ = substr($main_category_, 0,2);
		foreach($temp_list_ as $sub_ctg_){
			if(substr($sub_ctg_["code_did"], 0,2) === $check_char_){
				$sub_ctg_list_[] = $sub_ctg_;
			}
		}

		return $sub_ctg_list_;
	}

	static function main_categories_options($default_, $filter_ = array()){
		$main_ctg_list_ = self::main_categories($filter_);
		$html_ = "";
		foreach($main_ctg_list_ as $main_ctg_){
			$html_ .= '<option value="'.$main_ctg_["code_did"].'" '.($default_ === $main_ctg_["code_did"] ? "selected" : "").'>'.$main_ctg_["code_dnm"].'</option>';
		}

		return $html_;
	}
	static function sub_categories_options($main_category_, $default_, $filter_ = array()){
		$sub_ctg_list_ = self::sub_categories($main_category_, $filter_);
		$html_ = "";
		foreach($sub_ctg_list_ as $sub_ctg_){
			$html_ .= '<option value="'.$sub_ctg_["code_did"].'" '.($default_ === $sub_ctg_["code_did"] ? "selected" : "").'>'.$sub_ctg_["code_dnm"].'</option>';
		}

		return $html_;
	}

	static function _prevent_page(){
		do_action("kn_play_prevent_page",((string)get_the_ID()));
	}
	static function _rewrite_page(){
		$settings_ = KN_settings::get_settings();
		$play_view_page_id_ = $settings_["play_view_page_id"];

		if(!strlen($play_view_page_id_)) return;

		add_rewrite_tag('%play_id%', '([^&]+)');

		$play_view_page_ = get_page($play_view_page_id_);
		add_rewrite_rule('^'.$play_view_page_->post_name.'/(.+)/?$', 'index.php?page_id='.$play_view_page_id_.'&play_id=$matches[1]', 'top');
		global $wp_rewrite;
		$wp_rewrite->flush_rules(false);
	}
	static function _rewrite_page_query_vars($query_vars_){
		$query_vars_[] = 'play_id';
		return $query_vars_;
	}

	static function draw_finder($callback_ = null, $layout_lvl_ = 1, $show_search_btn_ = false, $default_ = array()){

		$open_detail_filter_ = (isset($default_["keyword"]) && strlen($default_["keyword"])) || (isset($default_["mem_count"]) && strlen($default_["mem_count"]));

		$mem_count_array_ = array(
			"1,3" => "1~3명",
			"4,6" => "4~6명",
			"7,10" => "7~10명",
			"10," => "10명 이상",
		);

		$html_ = "";

		ob_start();
?>
<form id="kn-play-finder" class="kn-form <?=($show_search_btn_ ? "with-button" : "")?>">
<input type="hidden" name="lat" value="<?=(isset($default_["lat"]) ? $default_["lat"] : "")?>">
<input type="hidden" name="lng" value="<?=(isset($default_["lng"]) ? $default_["lng"] : "")?>">
<input type="hidden" name="address" value="<?=(isset($default_["address"]) ? $default_["address"] : "")?>">
<input type="hidden" name="distance" value="<?=(isset($default_["distance"]) ? $default_["distance"] : "5000")?>">
<input type="hidden" name="start_date" value="<?=(isset($default_["start_date"]) ? $default_["start_date"] : "")?>">
<input type="hidden" name="end_date" value="<?=(isset($default_["end_date"]) ? $default_["end_date"] : "")?>">
<input type="hidden" name="start_time" value="<?=(isset($default_["start_time"]) ? $default_["start_time"] : "")?>">
<input type="hidden" name="end_time" value="<?=(isset($default_["end_time"]) ? $default_["end_time"] : "")?>">
<input type="hidden" name="mem_count" value="<?=(isset($default_["mem_count"]) ? $default_["mem_count"] : "")?>">

<?php 

	switch($layout_lvl_){
	case 0 : { // no detail search
?>

<div class="kn-finder-frame pc"><div class="wrap">
<div class="content-wrap">
	<a class="kn-input-inset l40 button" data-cond-target-address name="target_address" href="javascript:kn_play_finder_open_address_popup();">
		<i class="fa fa-map-marker"></i><span></span>
	</a><input type="button" class="kn-input-inset l40" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l20 button last-child" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a>
</div>
	<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div></div>
<div class="kn-finder-frame mobile"><div class="wrap">
	<a class="kn-input-inset l100 button last-child" data-cond-target-address name="target_address" href="javascript:kn_play_finder_open_address_popup();" >
		<i class="fa fa-map-marker"></i><span></span>
	</a><p class="cond-item-div"></p>
	<div class="content-wrap">
	<input type="button" class="kn-input-inset l50 first-child" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l50 button" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a>
</div>
<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div></div>

<?php

	break;
	}
	case 2 : { // without address bar

?>

<div class="kn-finder-frame pc"><div class="wrap">
<div class="content-wrap">
	<input type="button" class="kn-input-inset l33" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l33 button" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a><select
	 class="kn-input-inset l33 last-child" data-cond-target-mem-count name="mem_count">
		<option value="">인원선택</option>
		<?= kn_make_options($mem_count_array_, (isset($default_["mem_count"]) ? $default_["mem_count"] : "")) ?>
	</select>
</div>
	<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div></div>
<div class="kn-finder-frame mobile"><div class="wrap">
	<input type="button" class="kn-input-inset l33" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l33 button" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a><select
	 class="kn-input-inset l33 last-child" data-cond-target-mem-count name="mem_count">
		<option value="">인원선택</option>
		<?= kn_make_options($mem_count_array_, (isset($default_["mem_count"]) ? $default_["mem_count"] : "")) ?>
	</select>
</div>
<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div>

<?php

		break;
	}
	default : // default
?>

<div class="kn-finder-frame pc"><div class="wrap">
<div class="content-wrap">
	<a class="kn-input-inset l40 button" data-cond-target-address name="target_address" href="javascript:kn_play_finder_open_address_popup();">
		<i class="fa fa-map-marker"></i><span></span>
	</a><input type="button" class="kn-input-inset l40" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l20 button last-child" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a>
</div>
	<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div></div>
<div class="kn-finder-frame mobile"><div class="wrap">
	<a class="kn-input-inset l100 button last-child" data-cond-target-address name="target_address" href="javascript:kn_play_finder_open_address_popup();" >
		<i class="fa fa-map-marker"></i><span></span>
	</a><p class="cond-item-div"></p>
	<div class="content-wrap">
	<input type="button" class="kn-input-inset l50 first-child" value="모든날짜" name="target_date_display" onClick="javascript:kn_play_finder_open_cal();" data-cond-target-date><a 
		class="kn-input-inset l50 button" data-cond-target-time name="target_time" href="javascript:kn_play_finder_open_time_popup();">
		<span>모든시간</span>
	</a>
</div>
<?php if($show_search_btn_) {?>
<div class="search-btn-wrap">
	<a class="search-btn" href="javascript:_kn_play_finder_search_btn();"><span class="fa fa-search fa-2"></span></a>
</div>
<?php }?>
</div></div>
<div class="kn-play-finer-detail-frame"><div class="wrap">
	<input type="search" name="keyword" placeholder="키워드로 검색 예) 영어" class="kn-input-inset l60" value="<?=(isset($default_["keyword"]) ? $default_["keyword"] : "")?>"><select
	 class="kn-input-inset l40" data-cond-target-mem-count name="mem_count">
		<option value="">인원선택</option>
		<?= kn_make_options($mem_count_array_, (isset($default_["mem_count"]) ? $default_["mem_count"] : "")) ?>
	</select>
</div>
<a class="padding-btn" href="javascript:;"><span class="fa fa-chevron-down fa-2"></span></a>
</div>

<?php

	break;
	}


?>


</form>
<script type="text/javascript">
jQuery(document).ready(function($){
	var callback_ = <?=(strlen($callback_) ? "'".$callback_."'" : "null")?>;

	var finder_ = $("#kn-play-finder");
	finder_.data("callback",callback_);
	var detail_form_ = finder_.find(".kn-play-finer-detail-frame");
	var address_el_ = $("[data-cond-target-address]");
	var target_date_el_ = finder_.find("[data-cond-target-date]");
	var target_time_el_ = finder_.find("[data-cond-target-time]");
	var target_mem_count_el_ = finder_.find("[data-cond-target-mem-count]");
	var target_keyword_ = finder_.find(".kn-play-finer-detail-frame input[name='keyword']");

	target_mem_count_el_.change(function(event_){
		var target_ = $(event_.target);
		var target_val_ = target_.val();
		$(target_mem_count_el_).selectVal(target_val_);
		kn_play_finder_set_target_mem_count(target_val_);
	});
	detail_form_.find("a.padding-btn").click(function(){
		detail_form_.toggleClass("opened");
	});

	target_keyword_.on("keyup",function(event_){
		if(event_.keyCode == 13) {
			event_.preventDefault();
			_kn_play_finder_apply();
			return false;
		}
	});
});
</script>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _check_user_to_pro($user_id_){
		$settings_ = KN_settings::get_settings();
		if(!strlen($settings_["play_pro_switch_total_time"]) || KN_user::did_apply_pro($user_id_)){
			return;
		}

		$total_time_ = self::total_play_time($user_id_);

		if(isset($total_time_) && $total_time_ >= (int)$settings_["play_pro_switch_total_time"]){
			KN_user::insert_pro(array(
				"ID" => $user_id_,
				"status" => "00003",
				"accept_date" => KN_QUERY_FUNC("NOW()"),
			));
		}
	}
}

add_action("template_redirect", array("KN_play", "_prevent_page"));
add_filter("init", array("KN_play","_rewrite_page"), 10, 0);
// add_filter("query_vars", array("KN_play","_rewrite_page_query_vars"));

add_filter("kn_play_encode_columns", array("KN_play", "_filter_encode_play_columns"));
add_filter("kn_play_decode_columns", array("KN_play", "_filter_decode_play_columns"));
add_action("kn_library_init", array("KN_play","register_library"));

include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-settings.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-editor.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-register.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-edit.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-find.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-view.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-myinfo-playstatus.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-myinfo-myplay.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-myinfo-applyplay.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-myinfo-myreview.php');

function _kn_layers_widgets_init(){
	include_once(KN_PLUGIN_DIR_PATH . 'modules/play/class.KN-play-layerwp-widget.php');	
}

add_action('widgets_init','_kn_layers_widgets_init', 22);
add_action("kn_user_signin", array("KN_play", "_check_user_to_pro"));

function kn_current_play_id(){
	global $wp_query;
	return isset($_GET["play_id"]) ? $_GET["play_id"] : (isset($wp_query->query["play_id"]) ? $wp_query->query["play_id"] : null);
}
function kn_play_did_apply($play_id_, $mem_id_){
	return KN_play::did_apply($play_id_, $mem_id_);
}

?>