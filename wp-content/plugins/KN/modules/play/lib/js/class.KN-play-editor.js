jQuery(document).ready(function($){
	var editor_form_ = $("#kn-play-editor-form");
	var address_el_ = editor_form_.find("input[name='address']");
	var play_date_el_ = editor_form_.find("input[name='play_date']");
	var start_time_el_ = editor_form_.find("input[name='play_srt_time']");
	var end_time_el_ = editor_form_.find("input[name='play_end_time']");
	var main_ctg_el_ = editor_form_.find("select[name='main_ctg']");
	var sub_ctg_el_ = editor_form_.find("select[name='sub_ctg']");
	var image_uploader_ = $("#kn-main-image-uploader");
	var ourhome_btn_ = editor_form_.find("#kn-play-editor-ourhome-btn");

	main_ctg_el_.on("change",function(){
		var target_ = $(this);
		var main_ctg_ = target_.children(":selected").val();
		sub_ctg_el_.empty();
		sub_ctg_el_.append(KN.make_options([{
			"value" : "",
			"title" : "--소분류--"
		}],"value", "title"));

		KN.post({
			action : "kn-play-editor-load-sub-categories",
			main_ctg : main_ctg_
		},function(result_, response_json_){
			sub_ctg_el_.append(KN.make_options(response_json_,"code_did", "code_dnm"));
		});

	});

	play_date_el_.Zebra_DatePicker({
		format: 'Y-m-d',
		show_icon : false,
		readonly_element: true,
		direction: true,
		show_icon : false,
		default_position : "below"
	});

	start_time_el_.timepicker({"timeFormat" : "H:i", "step" : 60});
	end_time_el_.timepicker({"timeFormat" : "H:i", "step" : 60});

	image_uploader_.kn_image_uploader({
		max_length : 5,
		uploaded : function(){
			editor_form_.find("input[name='img_attach_bool']").val("Y");
		},removed : function(){
			editor_form_.find("input[name='img_attach_bool']").val((image_uploader_.kn_image_uploader("length") > 0 ? "Y" : ""));
			
		}
	});

	$("#kn-play-editor-agree-btn").click(function(){
		_kn_play_editor_submit();
	});

	$("#kn-play-editor-cancel-agree-btn").click(function(){
		$.magnificPopup.close();
	});

	editor_form_.validate({
		ignore : "",
		rules : {
			"play_title" : {
				"required" : true
			},
			"address" : {
				"required" : true
			},
			"play_date" : {
				"required" : true
			},
			"play_srt_time" : {
				"required" : true
			},
			"play_end_time" : {
				"required" : true
			},
			"desc_html" : {
				"required" : true
			},
			"av_min_cnt" : {
				"required" : true
			},
			"av_max_cnt" : {
				"required" : true
			},
			"need_cost" : {
				"required" : true
			},
			"need_point" : {
				"required" : true
			},
			"main_ctg" : {
				"required" : true
			},
			"sub_ctg" : {
				"required" : true
			},
			"img_attach_bool" : {
				"required" : true
			}
		},messages : {
			"play_title" : {
				"required" : kn_play_editor_var["play_title_required"]
			},
			"address" : {
				"required" : kn_play_editor_var["address_required"]
			},
			"play_date" : {
				"required" : kn_play_editor_var["play_date_required"]
			},
			"play_srt_time" : {
				"required" : kn_play_editor_var["play_srt_time_required"]
			},
			"play_end_time" : {
				"required" : kn_play_editor_var["play_end_time_required"]
			},
			"desc_html" : {
				"required" : kn_play_editor_var["desc_html_required"]
			},
			"av_min_cnt" : {
				"required" : kn_play_editor_var["av_min_cnt_required"]
			},
			"av_max_cnt" : {
				"required" : kn_play_editor_var["av_max_cnt_required"]
			},
			"need_cost" : {
				"required" : kn_play_editor_var["need_cost_required"]
			},
			"need_point" : {
				"required" : kn_play_editor_var["need_point_required"]
			},
			"main_ctg" : {
				"required" : kn_play_editor_var["main_ctg_required"]
			},
			"sub_ctg" : {
				"required" : kn_play_editor_var["sub_ctg_required"]
			},
			"img_attach_bool" : {
				"required" : kn_play_editor_var["img_attach_bool_required"]
			}
		},submitHandler : function(){
			var play_date_ = editor_form_.find("[name='play_date']").val();
			var play_srt_time_ = editor_form_.find("[name='play_srt_time']").val();

			if(KN.current_datetimediff(new Date(play_date_+' '+play_srt_time_)) < 0){
				KN.alert({
					title : "놀이날짜와 시간을 확인하세요!",
					content : "놀이날짜와 시간이 현재시간보다 이전입니다.",
					OK : "확인"
				});
				return;
			}
			
			kn_play_editor_submit();
		}
	});

	ourhome_btn_.click(function(){
		btn_ = $(this);

		var addr_lat_ = btn_.data("addr-lat");
		var addr_lng_ = btn_.data("addr-lng");
		var address_ = btn_.data("address");
		var address_dtl_ = btn_.data("address-dtl");

		editor_form_.find("input[name='address']").val(address_);
		editor_form_.find("input[name='address_dtl']").val(address_dtl_);
		editor_form_.find("input[name='addr_lat']").val(addr_lat_);
		editor_form_.find("input[name='addr_lng']").val(addr_lng_);
	});
});

function kn_play_editor_open_address_popup(){
	$ = jQuery;
	var editor_form_ = $("#kn-play-editor-form");
	var address_el_ = editor_form_.find("input[name='address']");
	var addr_lat_el_ = editor_form_.find("input[name='addr_lat']");
	var addr_lng_el_ = editor_form_.find("input[name='addr_lng']");
	var address_dtl_el_ = editor_form_.find("input[name='address_dtl']");
	
	KN.maps.address_picker({
		callback : function(address_){
			address_el_.val(address_["address"]);
			addr_lat_el_.val(address_["lat"]);
			addr_lng_el_.val(address_["lng"]);
			address_dtl_el_.focus();
		},
		secondary_btn_title : "현재위치선택",
		secondary_btn_callback : function(){
			$ = jQuery;
			$.magnificPopup.close();
			KN.maps.geolocation(function(result_){
				address_el_.val(result_["address"]["jibunAddress"]["name"]);
				addr_lat_el_.val(result_["lat"]);
				addr_lng_el_.val(result_["lng"]);
				address_dtl_el_.focus();
			});
		}
	});
}

function _kn_play_editor_save(callback_){
	$ = jQuery;
	var editor_form_ = $("#kn-play-editor-form");
	var image_uploader_ = $("#kn-main-image-uploader");
	var play_data_ = editor_form_.serializeObject();

	delete play_data_["img_attach_bool"];

	play_data_["main_img_urls"] = image_uploader_.kn_image_uploader("toJSON");
	editor_form_.formWorking(true);

	KN.post({
		action : "kn-play-editor-save",
		play_data : play_data_
	},function(result_, response_json_){
		editor_form_.formWorking(false);
		callback_(result_, response_json_);
	});
}

function kn_play_editor_save(){
	$ = jQuery;
	_kn_play_editor_save(function(result_, response_json_){
		if(response_json_.success !== true){
			KN.alert({
				title : "저장실패",
				content : kn_play_editor_var["save_error"],
				OK : "확인"
			});
			return;
		}
		document.location = KN.make_url(kn_play_editor_var["play_edit_url"], {
			"play_id" : response_json_["play_id"]
		});
	});
}

function kn_play_editor_delete(){
	$ = jQuery;
	if(!confirm(kn_play_editor_var["confirm_delete"])){
		return;
	}
	
	$ = jQuery;
	var editor_form_ = $("#kn-play-editor-form");
	var play_data_ = editor_form_.serializeObject();
	editor_form_.formWorking(true);

	KN.post({
		action : "kn-play-editor-delete",
		play_id : play_data_["ID"]
	},function(result_, response_json_){
		editor_form_.formWorking(false);
		if(response_json_.success !== true){
			KN.alert({
				title : "삭제에러",
				content : kn_play_editor_var["delete_error"],
				OK : "확인"
			});
			return;
		}

		KN.alert({
			title : "삭제완료",
			content : kn_play_editor_var["delete_success"],
			OK : "확인",
			callback : function(){
				document.location = kn_play_editor_var["myplay_url"];		
			}
		});
	});
}


function kn_play_editor_submit(){
	$ = jQuery;
	$.magnificPopup.open({
		items: {
			src: '#kn-play-editor-agreement',
			type: 'inline',
			preloader: false
		},
		closeOnBgClick : false,
	});
}

function kn_play_editor_goto_view(){
	$ = jQuery;
	var editor_form_ = $("#kn-play-editor-form");
	var play_data_ = editor_form_.serializeObject();

	document.location = kn_play_editor_var["preview_url"]+play_data_["ID"];
}

function _kn_play_editor_submit(){
	$ = jQuery;
	$.magnificPopup.close();
	_kn_play_editor_save(function(result_, response_json_){
		var editor_form_ = $("#kn-play-editor-form");
		var agreement_form_ = $("#kn-play-editor-agreement");
		var image_uploader_ = editor_form_.find("#kn-main-image-uploader");
		var play_id_ = response_json_["play_id"];

		editor_form_.formWorking(true);
		agreement_form_.formWorking(true);
		KN.post({
			action : "kn-play-editor-submit",
			play_id : play_id_
		},function(result_, response_json_){
			editor_form_.formWorking(false);
			agreement_form_.formWorking(false);

			if(response_json_.success !== true){
				KN.alert({
					title : "등록에러",
					content : kn_play_editor_var["submit_error"],
					OK : "확인"
				});
				return;
			}

			KN.alert({
				title : "등록완료",
				content : kn_play_editor_var["submit_success"],
				OK : "확인",
				callback : function(){
					document.location = KN.make_url(kn_play_editor_var["play_view_url"], {
						"play_id" : response_json_["play_id"]
					});
				}
			});
			
		});
	});
}