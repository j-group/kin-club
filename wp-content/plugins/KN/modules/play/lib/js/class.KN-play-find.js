jQuery(document).ready(function($){
	var finder_ = $("#kn-play-finder");

	var play_list_form_ = $("#kn-play-find-list-form");
	var play_maps_ = $("#kn-play-find-maps");
	
	var play_maps_module_ = new daum.maps.Map(play_maps_[0], {
		center: new daum.maps.LatLng(33.450701, 126.570667)
	});

	play_maps_.data("map_module",play_maps_module_);

	daum.maps.event.addListener(play_maps_module_, "zoom_changed", function() {
		kn_play_finder_set_distance(play_maps_module_.getLevel() * 900);
	});

	var default_check_ = finder_.find("input[name='address']").val();

	play_list_form_.waypoint({
		handler : function(direction_){
			if(direction_ === "down"){
				_kn_play_search_play();
			}
		}
	});

	play_list_form_.find("[name='page_index']").val(0);

	if(default_check_ === "")
		kn_play_finder_set_current_address();
	else kn_play_search_play(kn_play_finder_get_params());
});

kn_play_make_map_marker = (function(title_, lat_, lng_, unique_id_, disabled_){
	var play_list_form_ = $("#kn-play-find-list-form");
	var play_maps_ = $("#kn-play-find-maps");
	var map_module_ = play_maps_.data("map_module");

	var content_ = '<div class="kn-play-item-marker '+(disabled_ ? "disabled" : "")+'" data-float-marker-id="'+unique_id_+'">'+title_+'</div>';

	var custom_overlay_ = new daum.maps.CustomOverlay({
		position: KN.maps.latlng(lat_, lng_),
		content: content_
	});

	custom_overlay_.setMap(map_module_);

	return custom_overlay_;
});


kn_play_move_center_float_map = (function(lat_,lng_){
	$ = jQuery
	var play_maps_ = $("#kn-play-find-maps");
	var map_module_ = play_maps_.data("map_module");
	map_module_.setCenter(KN.maps.latlng(lat_,lng_));
});

function kn_play_get_marker(unique_id_){
	return jQuery("#kn-play-find-maps").find("[data-float-marker-id='"+unique_id_+"']")
}

function kn_play_search_play(param_){
	_kn_play_remove_play_items();
	kn_play_move_center_float_map(param_.lat, param_.lng);
	$("#kn-play-find-list-form [name='page_index']").val(0);
	_kn_play_search_play(param_);
}
function _kn_play_search_play(param_){
	$ = jQuery;

	var play_list_frame_ = $("#kn-play-find-list-frame");
	var play_list_ = play_list_frame_.find(".kn-play-list");

	if(play_list_frame_.hasClass("loading")){
		return;
	}

	if(param_ === undefined){
		param_ = kn_play_finder_get_params();
	}

	param_["page_index"] = parseInt(play_list_frame_.find("[name='page_index']").val());
	param_["feed_length"] = parseInt(play_list_frame_.find("[name='feed_length']").val());

	play_list_frame_.addClass("loading");
	play_list_frame_.removeClass("norows");
	play_list_frame_.removeClass("first");

	KN.post({
		action : "kn-play-find-load-list",
		params : param_
	},function(result_, response_json_){
		play_list_frame_.removeClass("loading");

		if(response_json_.length == 0){
			play_list_frame_.addClass("norows");
			return;
		}

		play_list_frame_.find("[name='page_index']").val(param_["page_index"]+param_["feed_length"]);

		for(var index_ = 0; index_ < response_json_.length; ++index_){
			_kn_play_add_play_item(response_json_[index_]);
		}
	});
}

function _kn_play_remove_play_items(){
	$ = jQuery;
	var cond_form_ = $("#kn-play-find-cond-form");
	var play_list_frame_ = $("#kn-play-find-list-frame");
	var play_list_ = play_list_frame_.find(".kn-play-list");

	var play_items_ = play_list_.children();

	play_items_.each(function(){
		var play_item_ = $(this);
		var map_overlay_ = play_item_.data("map-overlay");
		map_overlay_.setMap(null);
	});

	play_items_.remove();
}

function _kn_play_add_play_item(data_){
	$ = jQuery;
	var play_list_frame_ = $("#kn-play-find-list-frame");
	var play_list_ = play_list_frame_.find(".kn-play-list");
	data_["link_url"] = kn_play_find_var["play_view_url"]+data_["ID"];
	var play_item_ = KN.play.append_play_item(play_list_, data_);

	var unique_id_ = KN.random_string(5,"1234567890abcdef");

	var map_overlay_ = kn_play_make_map_marker(data_["play_title"], data_["addr_lat"], data_["addr_lng"], unique_id_, (data_["status"] === "0003"));
	play_item_.data("play-title", data_["play_title"]);
	play_item_.data("map-overlay", map_overlay_);
	play_item_.data("map-overlay-id",unique_id_);

	play_item_.on("mouseenter", function(event_){
		var target_ = $(this);
		var map_overlay_id_ = target_.data("map-overlay-id");
		var map_overlay_ = target_.data("map-overlay");
		var overlay_pos_ = map_overlay_.getPosition();
		kn_play_move_center_float_map(overlay_pos_.getLat(), overlay_pos_.getLng());

		var map_overlay_element_ = kn_play_get_marker(map_overlay_id_);
		map_overlay_element_.addClass("hover");
	});
	play_item_.on("mouseleave", function(event_){
		var target_ = $(this);
		var map_overlay_id_ = target_.data("map-overlay-id");
		var map_overlay_element_ = kn_play_get_marker(map_overlay_id_);
		map_overlay_element_.removeClass("hover");
	});

	return play_item_;
}

