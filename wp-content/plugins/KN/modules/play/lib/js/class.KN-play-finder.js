jQuery(document).ready(function($){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");

	var mem_count_ = finder_.find("input[name='mem_count']").val();
	var lat_ = finder_.find("input[name='lat']").val();
	var lng_ = finder_.find("input[name='lng']").val();
	var address_ = finder_.find("input[name='address']").val();
	var start_date_ = finder_.find("input[name='start_date']").val();
	var end_date_ = finder_.find("input[name='end_date']").val();
	var start_time_ = finder_.find("input[name='start_time']").val();
	var end_time_ = finder_.find("input[name='end_time']").val();

	kn_play_finder_set_address({
		address : address_,
		lat : lat_,
		lng : lng_
	}, false);	
	
	kn_play_finder_set_target_date({
		start_date : (start_date_ !== "" ? new Date(start_date_) : null),
		end_date : (end_date_ !== "" ? new Date(end_date_) : null),
	}, false);

	kn_play_finder_set_target_time({
		start_time : (start_time_ !== "" ? start_time_ : null),
		end_time : (end_time_ !== "" ? end_time_ : null)
	}, false);
});

function kn_play_finder_open_address_popup(){
	KN.maps.address_picker({
		callback : function(address_){
			kn_play_finder_set_address(address_);
		},
		secondary_btn_title : "현재위치선택",
		secondary_btn_callback : kn_play_finder_set_current_address
	});
}

function kn_play_finder_set_current_address(){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");
	$.magnificPopup.close();
	$("[data-cond-target-address] span").text("");
	var icon_ = $("[data-cond-target-address] .fa");
	if(icon_.hasClass("loadding")) return;
	icon_.addClass("loadding");
	KN.maps.geolocation(function(result_){
		icon_.removeClass("loadding");;
		if(result_ === false){
			alert(kn_play_var["cannot_use_geolocation"]);
			kn_play_finder_set_address({
				address : "",
				lat : "",
				lng : ""
			});
			return;
		}

		kn_play_finder_set_address({
			address : result_["address"]["jibunAddress"]["name"],
			lat : result_["lat"],
			lng : result_["lng"]
		});
	});
}

function kn_play_finder_set_address(data_, fire_event_){
	$ = jQuery;
	fire_event_ = fire_event_ === undefined ? true : fire_event_;
	var finder_ = $("#kn-play-finder");
	var input_lat_ = finder_.find("[name='lat']");
	var input_lng_ = finder_.find("[name='lng']");
	var input_address_ = finder_.find("[name='address']");

	input_lat_.val(data_.lat);
	input_lng_.val(data_.lng);
	input_address_.val(data_.address);

	$("[data-cond-target-address] span").text(data_.address);
	if(fire_event_) _kn_play_finder_apply();
}

function kn_play_finder_set_distance(distance_, fire_event_){
	$ = jQuery;
	fire_event_ = fire_event_ === undefined ? true : fire_event_;
	var finder_ = $("#kn-play-finder");
	var input_distance_ = finder_.find("[name='distance']");
	
	input_distance_.val(distance_);
	if(fire_event_) _kn_play_finder_apply();
}

function kn_play_finder_open_cal(){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");
	var target_date_el_ = finder_.find("[data-cond-target-date]");
	var start_date_input_ = finder_.find("input[name='start_date']");
	var end_date_input_ = finder_.find("input[name='end_date']");

	KN.utils.date_picker({
		callback : function(result_){
			kn_play_finder_set_target_date(result_);
		}
	});
}

function kn_play_finder_open_time_popup(){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");
	var time_display_ = finder_.find("[data-cond-target-time] span");
	var start_time_input_ = finder_.find("input[name='start_time']");
	var end_time_input_ = finder_.find("input[name='end_time']");

	KN.utils.time_picker({
		callback : function(result_){
			kn_play_finder_set_target_time(result_);
		}
	});
}

function kn_play_finder_set_target_time(data_, fire_event_){
	$ = jQuery;
	fire_event_ = fire_event_ === undefined ? true : fire_event_;
	var finder_ = $("#kn-play-finder");
	var time_display_ = finder_.find("[data-cond-target-time] span");
	var start_time_input_ = finder_.find("input[name='start_time']");
	var end_time_input_ = finder_.find("input[name='end_time']");

	start_time_input_.val(data_.start_time);
	end_time_input_.val(data_.end_time);

	var display_text_ = "모든시간";
	var add_display_text_  = "";
	if(data_.start_time !== null){
		display_text_ = "";
		add_display_text_ += data_.start_time;

		if(data_.end_time !== null){
			add_display_text_ += " ~ ";
		}else add_display_text_ += " 부터";
	}

	if(data_.end_time !== null){
		display_text_ = "";
		add_display_text_ += data_.end_time+"";
		if(data_.start_time === null){
			add_display_text_ += " 까지";
		}
	}

	time_display_.text(display_text_ + add_display_text_);
	if(fire_event_) _kn_play_finder_apply();
}

function kn_play_finder_set_target_date(data_, fire_event_){
	$ = jQuery;
	fire_event_ = fire_event_ === undefined ? true : fire_event_;
	var finder_ = $("#kn-play-finder");
	var target_date_el_ = finder_.find("[data-cond-target-date]");
	var start_date_input_ = finder_.find("input[name='start_date']");
	var end_date_input_ = finder_.find("input[name='end_date']");

	start_date_input_.val(data_.start_date);
	end_date_input_.val(data_.end_date);
	target_date_el_.css({"fontSize" : ""});

	var display_text_ = "모든날짜";
	if(data_.start_date !== null){
		var start_month_ = data_.start_date.getMonth()+1;
		var start_day_ = data_.start_date.getDate();
		display_text_ = start_month_+"월 "+start_day_+"일";

		if(data_.end_date !== null){
			if(data_.start_date.getTime() != data_.end_date.getTime()){
				var end_month_ = data_.end_date.getMonth()+1;
				var end_day_ = data_.end_date.getDate();

				display_text_ += " ~ "+end_month_+"월 "+end_day_+"일";
			}
		}
	}else if(data_.end_date !== null){
		var end_month_ = data_.end_date.getMonth()+1;
		var end_day_ = data_.end_date.getDate();
		display_text_ = "~ "+end_month_+"월 "+end_day_+"일";
	}
	
	target_date_el_.val(display_text_);
	if(fire_event_) _kn_play_finder_apply();
}


function kn_play_finder_set_target_mem_count(value_, fire_event_){
	$ = jQuery;
	fire_event_ = fire_event_ === undefined ? true : fire_event_;
	var finder_ = $("#kn-play-finder");
	var input_ = finder_.find("input[name='mem_count']");
	input_.val(value_);
	if(fire_event_) _kn_play_finder_apply();
}

function kn_play_finder_get_params(){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");

	var mem_count_ = finder_.find("input[name='mem_count']").val();
	mem_count_ = mem_count_.split(",");

	var min_count_ = mem_count_[0];
	var max_count_ = mem_count_[1];

	var start_date_ = finder_.find("input[name='start_date']").val();
	var end_date_ = finder_.find("input[name='end_date']").val();

	start_date_ = (start_date_ === "" ? null : new Date(start_date_).format("yyyy-MM-dd"));
	end_date_ = (end_date_ === "" ? null : new Date(end_date_).format("yyyy-MM-dd"));
	
	var param_ = {
		lat : finder_.find("input[name='lat']").val(),
		lng : finder_.find("input[name='lng']").val(),
		address : finder_.find("input[name='address']").val(),
		distance : finder_.find("input[name='distance']").val(),
		start_date : start_date_,
		end_date : end_date_,
		start_time : finder_.find("input[name='start_time']").val(),
		end_time : finder_.find("input[name='end_time']").val(),
		av_min_cnt : (min_count_ === undefined ? null : min_count_),
		av_max_cnt : (max_count_ === undefined ? null : max_count_),
		keyword : finder_.find("input[name='keyword']").val()
	};

	return param_;
}

function _kn_play_finder_apply(){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");
	if(finder_.hasClass("with-button")) return;

	var param_ = kn_play_finder_get_params();

	var callback_ = finder_.data("callback");
	if(callback_ !== null){
		var callback_func_ = window[callback_];
		callback_func_(param_);
	}
}

function _kn_play_finder_search_btn(page_url_){
	$ = jQuery;
	var finder_ = $("#kn-play-finder");
	var param_ = kn_play_finder_get_params();

	var callback_ = finder_.data("callback");
	if(callback_ !== null){
		var callback_func_ = window[callback_];
		callback_func_(param_);
	}
}
