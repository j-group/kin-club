jQuery(document).ready(function($){
	var main_img_frame_ = $("#kn-applyplay-list-table .main-img-frame");
	var sliders_ = main_img_frame_.children(".swiper-container");

	main_img_frame_.click(function(){
		document.location = $(this).attr("href");
	});

	if(sliders_.length > 0){
		sliders_.each(function(){
			new Swiper(this, {
				loop: true,
				autoplay : 3000
		    });
		});
	}
});

