jQuery(document).ready(function($){
	var main_slider_ = $("#kn-play-view-form .main-img-frame");
	var maps_el_ = $("#kn-play-view-form .top-frame .maps-frame");
	var mem_list_el_ = $("#kn-play-view-form .mem-list-frame .swiper-container");
	var addr_lat_ = maps_el_.data("lat");
	var addr_lng_ = maps_el_.data("lng");
	var play_title_ = maps_el_.data("play-title");
	var apply_btn_ = $("#kn-play-view-form .kn-apply-play-btn");
	var cancel_btn_ = $("#kn-play-view-form .kn-cancel-apply-play-btn");
	var maps_module_ = new daum.maps.Map(maps_el_[0], {
		center: new daum.maps.LatLng(addr_lat_, addr_lng_)
	});

	maps_el_.data("maps-module",maps_module_);

	var content_ = '<div class="kn-play-item-marker hover">'+play_title_+'</div>';
	var custom_overlay_ = new daum.maps.CustomOverlay({
		position: KN.maps.latlng(addr_lat_, addr_lng_),
		content: content_
	});

	custom_overlay_.setMap(maps_module_);

	var main_img_slider_ = new Swiper(main_slider_[0], {
		loop: true,
		autoplay : 3000
	});

	var swiper = new Swiper(mem_list_el_[0], {
		slidesPerView: 'auto',
		// centeredSlides: true,
		// spaceBetween: 30,
		freeMode: true,
		autoplay: 2500,
		autoplayDisableOnInteraction: false
	});

	apply_btn_.click(function(){
		kn_play_view_confirm_apply();
	});
	cancel_btn_.click(function(){
		kn_play_view_cancel_apply();
	});

	//popup
	var popup_el_ = $("#kn-play-apply-popup");
	var popup_child_table_ = popup_el_.find("#kn-play-view-child-table");
	var popup_apply_btn_ = popup_el_.find("#kn-play-editor-agree-btn");
	var popup_cancel_btn_ = popup_el_.find("#kn-play-editor-cancel-agree-btn");

	var child_select_checkbox_ = popup_child_table_.find("[name='child_select']");
	child_select_checkbox_.change(function(){
		kn_play_view_update_total_info();
	});
	var popup_etc_mem_count_ = popup_el_.find("[name='etc_mem_count']")
	popup_etc_mem_count_.change(function(){
		kn_play_view_update_total_info();
	});

	popup_apply_btn_.click(function(){
		var btn_ = $(this);
		kn_play_view_do_apply();
	});

	$("#kn-play-editor-cancel-agree-btn").click(function(){
		$.magnificPopup.close();
	});

	$("#kn-play-point-recharge-popup #kn-play-recharge-cancel-btn").click(function(){
		$.magnificPopup.close();
	});

	//review
	var review_list_frame_ = $("#kn-play-view-review-frame");
	review_list_frame_.data("page-index", 0);
	kn_play_view_load_review_list();

	var review_score_ = $("#kn-play-view-review-write-form .score");
	review_score_.raty({
		starOff : kn_play_view_var["star-off-url"],
		starOn : kn_play_view_var["star-on-url"]
	});
});

function kn_play_view_confirm_apply(){
	$ = jQuery;
	$.magnificPopup.open({
		items: {
			src: '#kn-play-apply-popup',
			type: 'inline',
			preloader: false
		},
		closeOnBgClick : false,
		callbacks : {
			open : function(){
				var popup_el_ = $("#kn-play-apply-popup");
				var child_select_el_ = popup_el_.find("select[name='child']");
				kn_play_view_update_total_info();
			}
		}
	});
}

function kn_play_view_get_total_info(){
	$ = jQuery;
	var view_form_ = $("#kn-play-view-form");
	var popup_el_ = $("#kn-play-apply-popup");
	var popup_child_table_ = popup_el_.find("#kn-play-view-child-table");
	var child_select_checkbox_ = popup_child_table_.find("[name='child_select']");
	var etc_mem_count_ = parseInt(popup_el_.find("[name='etc_mem_count']").val());

	var user_current_point_ = parseInt(popup_el_.data("user-current-point"));
	var need_point_ = parseInt(view_form_.find("[name='need_point']").val());
	var av_min_count_ = parseInt(view_form_.find("[name='av_min_count']").val());
	var av_max_count_ = parseInt(view_form_.find("[name='av_max_count']").val());

	var apply_count_ = 0;
	if(child_select_checkbox_.length > 0){
		child_select_checkbox_.each(function(){
			if($(this).prop("checked")){
				++apply_count_;
			}
		});
	}

	apply_count_ += (isNaN(etc_mem_count_) ? 0 : etc_mem_count_);
	user_current_point_ = isNaN(user_current_point_) ? 0 : user_current_point_;
	need_point_ = isNaN(need_point_) ? 0 : need_point_;
	av_min_count_ = isNaN(av_min_count_) ? 0 : av_min_count_;
	av_max_count_ = isNaN(av_max_count_) ? 0 : av_max_count_;

	return {
		apply_count : apply_count_,
		av_min_count : av_min_count_,
		av_max_count : av_max_count_,
		need_point : need_point_,
		user_current_point : user_current_point_,
		total_point : need_point_*(apply_count_),
	};
}

function kn_play_view_update_total_info(){
	$ = jQuery;
	var total_info_ = kn_play_view_get_total_info();
	var popup_el_ = $("#kn-play-apply-popup");
	var total_info_frame_ = popup_el_.find(".total-info-frame");
	var remain_count_txt_ = total_info_frame_.find(".remin-count .remin-count-txt");
	var need_point_txt_ = total_info_frame_.find(".need-point .need-point-txt");

	remain_count_txt_.text(KN.make_currency(total_info_.apply_count)+"/"+KN.make_currency(total_info_.av_max_count));
	need_point_txt_.text(KN.make_currency(total_info_.total_point)+"/"+KN.make_currency(total_info_.user_current_point));

	remain_count_txt_.toggleClass("overed", (total_info_.apply_count == 0 || total_info_.apply_count > total_info_.av_max_count || total_info_.apply_count < total_info_.av_min_count));
	need_point_txt_.toggleClass("overed", (total_info_.total_point > total_info_.user_current_point));
}

function kn_play_view_do_apply(){
	$ = jQuery;
	var total_info_ = kn_play_view_get_total_info();
	var view_form_ = $("#kn-play-view-form");

	if(total_info_.apply_count <= 0){
		KN.alert({
			title : kn_play_view_var["noselect_child_title"],
			content : kn_play_view_var["noselect_child"],
			OK : kn_play_view_var["button_confirm"],
			callback : function(){
				kn_play_view_confirm_apply();
			}
		});
		return;
	}

	if(total_info_.av_min_count > total_info_.apply_count){
		KN.alert({
			title : "최소인원확인",
			content : "신청 최소인원은 "+total_info_.av_min_count+"명입니다.",
			OK : kn_play_view_var["button_confirm"],
			callback : function(){
				kn_play_view_confirm_apply();
			}
		});
		return;	
	}
	if(total_info_.av_max_count < total_info_.apply_count){
		KN.alert({
			title : "최대인원확인",
			content : "신청 최대인원은 "+total_info_.av_max_count+"명입니다.",
			OK : kn_play_view_var["button_confirm"],
			callback : function(){
				kn_play_view_confirm_apply();
			}
		});
		return;	
	}

	if(total_info_.user_current_point < total_info_.total_point){
		KN.confirm({
			title : kn_play_view_var["apply_error_no_enough_point"],
			content : kn_play_view_var["apply_error_no_enough_point_content"],
			OK : kn_play_view_var["button_recharge"],
			cancel : kn_play_view_var["button_cancel"],
			callback : function(confirmed_){
				if(!confirmed_){
					kn_play_view_confirm_apply();
					return;
				}
			}
		});
		return;		
	}

	_kn_play_view_do_apply();
}

function _kn_play_view_do_apply(){
	$ = jQuery;
	var play_view_form_ = $("#kn-play-view-form");
	var play_id_ = play_view_form_.find("[name='play_id']").val();
	var popup_el_ = $("#kn-play-apply-popup");
	var popup_child_table_ = popup_el_.find("#kn-play-view-child-table");
	var child_select_checkbox_ = popup_child_table_.find("[name='child_select']");
	var etc_mem_count_ = parseInt(popup_el_.find("[name='etc_mem_count']").val());

	var selected_children_ = [];
	child_select_checkbox_.each(function(){
		if($(this).prop("checked")){
			selected_children_.push($(this).data("child-seq"));
		}
	});

	popup_el_.formWorking(true);
	$.magnificPopup.close();
	KN.indicator(true, function(){
		KN.post({
			action : "kn-play-view-apply-child",
			play_id : play_id_,
			selected_children : selected_children_,
			etc_mem_count : etc_mem_count_
		},function(result_, response_json_, cause_){
			popup_el_.formWorking(false);
			KN.indicator(false);

			if(response_json_.already === true){
				KN.alert({
					title : "이미신청한 놀이활동",
					content : kn_play_view_var["apply_error_already"],
					OK : "확인"
				});
				return;
			}

			if(response_json_.no_enough_point === true){
				KN.alert({
					title : "킨체리가 부족합니다!",
					content : kn_play_view_var["apply_error_no_enough_point"],
					OK : "확인"
				});
				return;
			}

			if(response_json_.reject === true){
				KN.alert({
					title : "잘못된 신청정보",
					content : kn_play_view_var["apply_error_reject"],
					OK : "확인"
				});
				return;
			}

			if(response_json_.success === true){
				KN.confirm({
					title : "신청완료",
					content : kn_play_view_var["apply_success"],
					OK : "확인",
					cancel : "마이페이지",
					callback : function(confirmed_){
						if(confirmed_){
							document.location = document.location;
						}else{
							document.location = kn_play_view_var["user_myinfo_applyplay_url"];
						}
						
					}
				});
				
				return;
			}
		});
	});
}

function kn_play_view_cancel_apply(){
	KN.confirm({
		title : kn_play_view_var["cancel_popup_title"],
		content : kn_play_view_var["cancel_popup_content"],
		OK : kn_play_view_var["cancel_popup_OK"],
		cancel : kn_play_view_var["button_cancel"],
		callback : function(confirmed_){
			if(!confirmed_) return;

			_kn_play_view_cancel_apply();
		}
	});
}

function _kn_play_view_cancel_apply(){
	$ = jQuery;
	var play_view_form_ = $("#kn-play-view-form");
	var play_id_ = play_view_form_.find("[name='play_id']").val();

	KN.indicator(true, function(){
		KN.post({
			action : "kn-play-view-cancel-apply",
			play_id : play_id_
		},function(result_, response_json_){
			KN.indicator(false);

			if(response_json_.reject === true){
				alert(kn_play_view_var["apply_error_reject"]);
				return;
			}

			if(response_json_.success === true){
				alert(kn_play_view_var["cancel_success"]);
				document.location = document.location;
				return;
			}

		});
	});
}

function kn_play_view_load_review_list(renew_){
	$ = jQuery;
	//review
	var review_list_frame_ = $("#kn-play-view-review-frame");
	var review_list_ = review_list_frame_.find(".kn-play-review-list");

	if(review_list_frame_.length <= 0) return;

	if(renew_ !== undefined && renew_ === true){
		review_list_frame_.data("page-index", 0);	
		review_list_.empty();
	}
	
	var play_id_ = review_list_frame_.data("play-id");
	var page_index_ = review_list_frame_.data("page-index");
	var feed_length_ = 5;

	review_list_frame_.removeClass("nomore");
	review_list_frame_.removeClass("norows");
	review_list_frame_.addClass("loading");

	KN.post({
		action : "kn-play-view-load-review-list",
		play_id : play_id_
	},function(result_, response_json_){
		review_list_frame_.removeClass("loading");
		if(!result_ || response_json_.success !== true){
			review_list_frame_.addClass("norows");
			alert("error!");
			return;
		}

		var review_data_ = response_json_.result;

		if(review_data_.length <= 0){
			if(page_index_ <= 0)
				review_list_frame_.addClass("norows");
			else review_list_frame_.addClass("nomore");
			return;
		}

		for(var index_=0; index_ < review_data_.length; ++index_){
			var review_item_ = KN.play.append_review_item(review_list_, review_data_[index_], (review_data_[index_].is_mine === "Y"), kn_play_view_remove_review );
		}
		
		review_list_frame_.data("page-index", (page_index_+feed_length_));
		review_list_frame_.removeClass("loading");
		if(feed_length_ > review_data_.length){
			review_list_frame_.addClass("nomore");
		}

	});
}

function kn_play_view_write_review(){
	$ = jQuery;
	var write_form_ = $("#kn-play-view-review-write-form");
	var score_ = write_form_.find(".score").raty("score");
		score_ = (score_ === undefined ? 0 : score_);
	var review_data_ = write_form_.serializeObject();

	if(score_ <= 0){
		KN.alert({
			title : "후기점수를 선택하세요!",
			content : "<p>후기를 남기기 위해서는 후기점수를 선택해야 합니다.</p>",
			OK : "확인",
			callback : function(){
				write_form_.find("[name='rev_txt']").focus();
			}
		});
		return;
	}

	if(review_data_.rev_txt === null || review_data_.rev_txt.trim().length <= 0){
		KN.alert({
			title : "후기내용를 작성하세요!",
			content : "<p>후기를 남기기 위해서는 후기내용을 입력해주세요!</p>",
			OK : "확인",
			callback : function(){
				write_form_.find("[name='rev_txt']").focus();
			}
		});
		return;
	}

	delete review_data_["score"];
	review_data_["rev_score"] = score_;

	KN.indicator(true, function(){
		KN.post({
			action : "kn-play-view-write-review",
			review_data : review_data_
		},function(result_, response_json_){
			KN.indicator(false);
			if(!result_ || response_json_.success !== true){
				alert("error!");
				return;
			}

			KN.alert({
				title : "후기작성완료",
				content : "<p>후기가 정상적으로 등록되었습니다.</p>",
				OK : "확인",
				callback : function(){
					write_form_.fadeOut();
					kn_play_view_load_review_list(true);
				}
			});
		});
	});
}

function kn_play_view_remove_review(writer_id_){
	KN.confirm({
		title : "후기삭제",
		content : "<p>삭제한 후기는 복구할 수 없습니다.</p>",
		OK : "삭제하기",
		cancel : "취소",
		callback : function(confirmed_){
			if(!confirmed_) return;
			_kn_play_view_remove_review(writer_id_);
		}
	});
}
function _kn_play_view_remove_review(writer_id_){
	$ = jQuery;
	var review_list_frame_ = $("#kn-play-view-review-frame");
	var play_id_ = review_list_frame_.data("play-id");
	
	KN.indicator(true, function(){
		KN.post({
			action : "kn-play-view-remove-review",
			play_id : play_id_
		},function(result_, response_json_){
			KN.indicator(false);
			if(!result_ || response_json_.success !== true){
				alert("error!");
				return;
			}

			KN.alert({
				title : "후기삭제완료",
				content : "<p>작성하신 후기가 정상적으로 삭제되었습니다.</p>",
				OK : "확인",
				callback : function(){
					$("#kn-play-view-review-write-form").fadeIn();
					kn_play_view_load_review_list(true);
				}
			});
		});
	});
}