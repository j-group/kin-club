(function($){


	KN.play = $.extend(KN.play,{
		append_play_item : function(target_, data_){
			var html_ = '';

			var link_url_ = data_["link_url"];
			if(link_url_ === undefined || link_url_ === null){
				link_url_ = "javascript:;";
			}

		html_ += '<div class="play-item '+(data_["display_status"] !== "00003" ? "closed" : "")+'" id="play-item-'+data_["ID"]+'">';
		html_ += '	<div class="top-flag">';
		html_ += 		data_["play_date_txt"];
		html_ += '	</div>';
		html_ += '	<div class="main-img swiper-container" href="'+link_url_+'">';

		html_ += '		<div class="swiper-wrapper">';

			var main_img_urls_ = data_["main_img_urls"];
			for(var index_=0;index_<main_img_urls_.length;++index_){
				html_ += '<div class="swiper-slide" style="background-image:url('+main_img_urls_[index_]+');"></div>';
			}

		html_ += '		</div>';
		html_ += '		<div class="swiper-pagination"></div>';

		html_ += '	</div>';
		html_ += '	<div class="play-title-frame">';
		html_ += '		<div class="play-title">';
		html_ += '			<h4>'+data_["play_title"];
		if(data_["pro_yn"] === "Y"){
			html_ += '					<span class="pro">전문가진행</span>';	
		}
		html_ += '</h4>';
		html_ += '			<p>'+data_["address"]+' <span class="distance">('+KN.make_currency(parseInt(data_["addr_distance"]))+'M)<span></p>';
		
		html_ += '		</div>';
		html_ += '		<div class="play-point kn-color kn-color-point">';
		html_ += '			<span class="point-icon knicon knicon-point-small"></span><span class="point-text">'+data_["need_point"]+'</span>';
		html_ += '		</div>';
		html_ += '	</div>';
		html_ += '	<div class="play-content">';
		html_ += '		<div class="kn-host-review">';
		html_ += '			<a href="javascript:;" class="kn-profile-link"><span class="kn-img-frame kn-profile xsmall"><span style="background-image:url('+data_["host_profile_url"]+')"><span></span></a>';
		html_ += '			<div class="host-desc">';
		html_ += '				<h5 class="host-name">'+data_["host_name"];
		html_ += '				</h5>';
		html_ += '				<div class="kn-score"><div style="width:'+data_["host_review_rate"]+'%"></div></div>';
		html_ += '			</div>';
		html_ += '		</div>';
		html_ += '		<div class="button-frame">';
		if(data_["display_status"] === "00003") html_ += '			<a href="'+link_url_+'" class="knbtn knbtn-primary">참여하기</a>';
		html_ += '		</div>';
		html_ += '	</div>';
		if(data_["display_status"] !== "00003"){
			html_ += '	<div class="cannot-join" href="'+link_url_+'"><span>'+data_["display_status_nm"]+'</span></div>';
		}
		html_ += '</div>';

			target_.append($(html_));
			var play_item_ = target_.find("#play-item-"+data_["ID"]);
			var main_slider_ = play_item_.find(".main-img");
			var mySwiper = new Swiper(main_slider_[0], {
				loop: true,
				autoplay : 3000
		    });

		    main_slider_.click(function(){
		    	document.location = $(this).attr("href");
		    });

		    var block_div_ = play_item_.find(".cannot-join");
		    block_div_.click(function(){
		    	document.location = $(this).attr("href");
		    });

			play_item_.toggle(false);
		    play_item_.fadeIn(400);

			return play_item_;
		},
		append_review_item : function(target_, data_, deletable_, delete_func_){
			var html_ = '';
			deletable_ = (deletable_ === undefined ? false : deletable_);

			html_ += '<div class="review-item" data-writer-id="'+data_.writer_id+'">';
			if(deletable_) html_ += 	'<a class="remove-btn" href="javascript:;"><i class="fa fa-close"></i></a>';
			html_ += 	'<div class="writer-info">';
			html_ += 		'<div class="kn-img-frame kn-profile xxsmall"><span style="background-image:url('+data_.writer_img_url+');"></span></div>';
			html_ += 		'<h5>'+data_.writer_name+'</h5>';
			html_ += 		'<div class="review-info">'+data_.reg_date_txt+'</div>';
			html_ += 	'</div>';
			html_ += 	'<div class="kn-score"><div style="width:'+((data_.rev_score/5)*100)+'%"></div></div>';
			html_ += 	'<div class="review-content">';
			html_ += 		data_.rev_txt.nl2br();
			html_ += 	'</div>';
			html_ += '</div>';

			target_.append($(html_));

			var review_item_ = target_.find("[data-writer-id='"+data_.writer_id+"']");

			if(deletable_){
				var remove_btn_ = review_item_.find(".remove-btn");
				remove_btn_.data("writer-id", data_.writer_id);
				review_item_.find(".remove-btn").click(function(){
					delete_func_($(this).data("writer-id"));
				});
			}

			review_item_.toggle(false);
			review_item_.fadeIn(400);

			return review_item_;
		}
	});


})(jQuery);