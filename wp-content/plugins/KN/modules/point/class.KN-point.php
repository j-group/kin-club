<?php

class KN_point{

	static function query_make_conditions($conditions_){
		global $wpdb;

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_PNT_TMP";

		$query_ = "";

		if(isset($conditions_["point_id"]) && strlen($conditions_["point_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["point_id"]." ";
		}

		if(isset($conditions_["user_id"]) && strlen($conditions_["user_id"])){
			$query_ .= " AND {$table_alias_}.user_id = ".$conditions_["user_id"]." ";
		}

		if(isset($conditions_["play_id"]) && strlen($conditions_["play_id"])){
			$query_ .= " AND {$table_alias_}.play_id = ".$conditions_["play_id"]." ";
		}

		if(isset($conditions_["point_ctg"]) && strlen($conditions_["point_ctg"])){
			$query_ .= " AND {$table_alias_}.point_ctg = '".$conditions_["point_ctg"]."' ";
		}

		if(isset($conditions_["only_used"]) && $conditions_["only_used"] === true){
			$query_ .= " AND {$table_alias_}.point_ctg IN ('00001') ";
		}else if(isset($conditions_["only_income"]) && $conditions_["only_income"] === true){
			$query_ .= " AND {$table_alias_}.point_ctg IN ('00003') ";
		}

		if(isset($conditions_["only_registered"]) && $conditions_["only_registered"] === true){
			$query_ .= " AND {$table_alias_}.status IN ('00001') ";
		}

		return $query_;

	}
	
	static function point_list($conditions_ = array()){
		global $wpdb;

		$conditions_["table_alias"] = "KN_PNT_TMP";
		$table_alias_ = $conditions_["table_alias"];

		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY ID DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		$query_conditions_ = self::query_make_conditions($conditions_);

		$query_ = "SELECT id ID,

						POINT_CTG point_ctg,
						".kn_query_gcode_dtl_nm("P0021","{$table_alias_}.point_ctg")." point_ctg_nm,

						STATUS status,
						".kn_query_gcode_dtl_nm("P0023","{$table_alias_}.status")." status_nm,

						USER_ID user_id,
						(SELECT IFNULL(TMP.NICK_NAME, TMP.USER_NAME) FROM {$wpdb->base_prefix}kn_users TMP WHERE TMP.ID = {$table_alias_}.USER_ID) user_display_name,

						PLAY_ID play_id,
						(SELECT TMP.PLAY_TITLE FROM {$wpdb->base_prefix}kn_play TMP WHERE TMP.ID = {$table_alias_}.play_id) play_title,

						POINT point,
						CONCAT(IF(POINT < 0, '-', IF(POINT > 0, '+', '')), FORMAT(POINT)) point_txt,

						RMK rmk,
						{$table_alias_}.reg_date reg_date,
						{$table_alias_}.mod_date mod_date,
						DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
						DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt

				FROM {$wpdb->base_prefix}KN_point {$table_alias_}
				WHERE 1=1 
				$query_conditions_ 
				$query_sort_ 
				$query_limit_ ";

		$results_ = $wpdb->get_results($query_, ARRAY_A);

		return $results_;
	}

	static function point($id_){
		$point_ = self::point_list(array("ID" => $id_));
		if(!isset($point_) || count($point_) <= 0) return false;
		return $point_[0];
	}

	static function add_point($user_id_, $point_, $data_ = array()){
		global $wpdb;

		$data_ = array_merge(array(
			"status" => "00001",
			"point_ctg" => ($point_ > 0 ? "00001" : "00003"),
		),$data_);

		$data_["user_id"] = $user_id_;
		$data_["point"] = $point_;

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_point", $data_);
		$result_ = $wpdb->query($query_);

		if($result_ === false) return $result_;
		
		return $wpdb->insert_id;
	}
	static function remove_point($id_){
		global $wpdb;
		$query_ = kn_query_delete("{$wpdb->base_prefix}kn_point",array("ID" => $id_));
		return $wpdb->query($query_);
	}

	static function point_count($user_id_, $conditions_ = array("only_registered" => true)){
		global $wpdb;

		$conditions_["table_alias"] = "KN_PNT_TMP";
		$table_alias_ = $conditions_["table_alias"];
		$query_conditions_ = self::query_make_conditions($conditions_);

		$query_ = "SELECT SUM({$table_alias_}.POINT) CNT
					FROM {$wpdb->base_prefix}KN_POINT {$table_alias_}
					WHERE 1=1
					$query_conditions_ ";

		return $wpdb->get_row($query_)->CNT;
	}
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/point/class.KN-point-settings.php');

?>