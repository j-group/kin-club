<?php

define("KN_SLUG_ADMIN_USER_PRO_LIST","kn-user-pro-list");

class KNAdmin_user_pro_list_table extends KNWPTable{

	function __construct(){
		parent::__construct(array(
			'singular' => 'pro',
			'plural' => 'pro-list',
			'ajax' => false
		));
	}

	function no_items(){
		_e('조회된 전문가가 없습니다.');
	}

	function column_default($item_, $column_name_){
		switch($column_name_){
		case "status_nm" :
		case "reg_date" :
			return $item_[$column_name_];
			break;
		case "profile_url" :
			return "<img src='".$item_[$column_name_]."'>";
			break;
		default : 
			return "--unknown column--";
		}
	}

	function get_columns(){
		return array(
			"user_name"	=> "회원명(아이디)",
			"status_nm"	=> "상태",
			"reg_date"	=> "신청일",
		);
	}

	function column_user_name($item_){
		$actions = array(
			"edit" => sprintf('<a href="?page=%s&user_id=%s">%s</a>',$_REQUEST["page"],$item_["ID"],__("신청서보기")),
		);

	  return "<img src='".$item_["profile_url"]."' width='40px' height='40px'>"."<b>".$item_["user_name"]."</b>(".$item_["user_login"].")".$this->row_actions($actions);
	}

	function extra_tablenav($which_){
		$tablenav_ = "";
		$tablenav_ .= '<div class="alignleft actions bulkactions">';
		if($which_ === "top"){

		}
		$tablenav_ .= '</div>';

		echo $tablenav_;
	}

	function prepare_items(){
		$columns_ = $this->get_columns();
		$this->_column_headers = array($columns_, array(), $this->get_sortable_columns());
		
		$search_text_ = KN_GET("s");
		
		global $wpdb;

		$conditions_ = array(
			"table_alias" => "PRO",
			"search_text" => $search_text_,
		);

		$total_count_ = $wpdb->get_row("SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}KN_USERS_PRO PRO WHERE 1=1 ".KN_user::query_make_conditions($conditions_))->CNT;
		$per_page_ = kn_get_screen_option("per_page");
		$paged_ = $this->paged();
		$total_page_count_ = $this->total_page_count($per_page_, $total_count_);
		$offset_ = $this->offset($paged_, $per_page_);

		$conditions_["limit"] = array(
			"offset" => $offset_,
			"length" => $per_page_,
		);
		$conditions_["sort"] = "REG_DATE DESC";

		$result_ = KN_user::pro_list($conditions_);

		$this->set_pagination_args(array(
			"total_items" => $total_count_,
			"total_pages" => $total_page_count_,
			"per_page" => $per_page_
		));

		$this->items = $result_;
	}
}

class KNAdmin_user_pro_list extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){
		add_action("admin_footer", array("KNAdmin_user_pro_list", "add_script"));

		$button_html_ = "<div class='kn-text-right'>";
		//$button_html_ .= "<a class='button button-primary' href='javascript:_kn_save_settings();'>변경사항 저장</a>";
		$button_html_ .= "</div>";

		//$settings_ = KN_settings::get_settings();

	?>

	<div class="wrap">
	<h2>회원내역</h2>
	<form method="get" id="kn-pro-list" action>
		<input type="hidden" name="page" value="<?php echo $_GET['page'] ?>" />
		<?php

		$table_ = new KNAdmin_user_pro_list_table();
		$table_->prepare_items();
		$table_->search_box('검색', 'kn-pro-list-searchbox');
		$table_->display();

		?>
	</form>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<style type="text/css">
	
	.wp-list-table.pro-list thead tr th.column-profile_url{
		width: 15%;
	}
	.wp-list-table.pro-list thead tr th.column-user_name{
		width: 30%;
	}
	.wp-list-table.pro-list thead tr th.column-user_ctg_nm{
		width: 20%;
	}
	.wp-list-table.pro-list thead tr th.column-status_nm{
		width: 20%;
	}
	.wp-list-table.pro-list thead tr th.column-reg_date_txt{
		width: 15%;
	}

	.wp-list-table.pro-list thead tr th.column-profile_url,
	.wp-list-table.pro-list thead tr th.column-status_nm,
	.wp-list-table.pro-list thead tr th.column-reg_date_txt,
	.wp-list-table.pro-list thead tr th.column-user_ctg_nm,
	.wp-list-table.pro-list tfoot tr th.column-profile_url,
	.wp-list-table.pro-list tfoot tr th.column-status_nm,
	.wp-list-table.pro-list tfoot tr th.column-reg_date_txt,
	.wp-list-table.pro-list tfoot tr th.column-user_ctg_nm,
	.wp-list-table.pro-list tbody tr td.column-profile_url,
	.wp-list-table.pro-list tbody tr td.column-status_nm,
	.wp-list-table.pro-list tbody tr td.column-reg_date_txt,
	.wp-list-table.pro-list tbody tr td.column-user_ctg_nm{
		text-align: center;
	}

	.wp-list-table.pro-list tbody tr td.column-user_name *{
		vertical-align: top;
	}
	.wp-list-table.pro-list tbody tr td.column-user_name img{
		margin-right: 7px;
	}
	.wp-list-table.pro-list tbody tr td.column-reg_date_txt{
		font-size: 12px;
	}
	
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});
	</script>
	<?php
	}
}

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_USER_PRO_LIST,
	"title" => "전문가관리",
	"class" => "KNAdmin_user_pro_list",
	"param" => array(),
	"eparam" => array(),
	"permission" => "manage_options",
	"screen_options" => array(
		"per_page" => array(
			"label" => "페이지 당 보여줄 전문가",
			"default" => 10,
		)
	)
));


?>