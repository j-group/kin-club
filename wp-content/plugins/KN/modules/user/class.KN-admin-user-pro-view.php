<?php

class KNAdmin_user_pro_view extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){
		add_action("admin_footer", array("KNAdmin_user_pro_view", "add_script"));

		$user_data_ = KN_user::pro($_GET["user_id"]);
		$site_name_ = get_bloginfo('name');

		$button_html_ = "<div class='kn-text-right'>";
		if($user_data_["status"] === "00001"){
			$button_html_ .= "<a class='button button-primary' href='javascript:_kn_accept_pro();'>승인처리</a>";	
		}
		$button_html_ .= " <a class='button button-delete' href='javascript:_kn_delete_pro();'>요청삭제</a>";	
		
		$button_html_ .= "</div>";
	?>

	<div class="wrap kn-limit-content">
	<h2>전문가상세정보 <a class="add-new-h2" href="?page=<?php echo KN_SLUG_ADMIN_USER_PRO_LIST; ?>">돌아가기</a></h2>
	<form method="post" id="kn-user-pro-view" action>
		<input type="hidden" name="user_id" value="<?php echo $_GET['user_id'] ?>" />
		<?php echo $button_html_; ?>
		<h3>기본정보</h3>
		<table class="form-table">
			<tr>
				<th>회원명</th>
				<td><b><a href="?page=<?=KN_SLUG_ADMIN_USER_LIST?>&user_id=<?=$user_data_["ID"]?>"><?php echo $user_data_["user_name"]; ?></a></b></td>
			</tr>
			<tr>
				<th>생년월일</th>
				<td><?php echo $user_data_["birth_yyyy"].".".$user_data_["birth_mm"].".".$user_data_["birth_dd"]; ?></td>
			</tr>
			<tr>
				<th>성별</th>
				<td><?php echo ($user_data_["sex"] === "M" ? "남자" : "여자"); ?></td>
			</tr>
			<tr>
				<th>상태</th>
				<td><b><?php echo $user_data_["status_nm"]; ?></b></td>
			</tr>
			<tr>
				<th>신청일자</th>
				<td><?php echo $user_data_["reg_date_txt"]; ?></td>
			</tr>

		<?php if($user_data_["status"] === "00003"){ ?>
			<tr>
				<th class="kn-point">승인일자</th>
				<td class="kn-point"><b><?php echo $user_data_["accept_date_txt"]; ?></b></td>
			</tr>

		<?php } ?>
			
		</table>

		<hr/>
		<h3>질문답변</h3>
<?php if(strlen($user_data_["qna01"])){ ?>
		<b class="qq"><?=$site_name_?>을 알게된 경로</b>
		<p class="qa"><?=kn_gcode_dtl_nm("U0007", $user_data_["qna01"])?></p>

		<b class="qq">왜 놀이활동 전문가가 되고 싶으신가요?</b>
		
			<?php
				$qna02_ = $user_data_["qna02"];
				$div_bool_ = false;
				foreach($qna02_ as $value_){
					
					echo "<p class='qa'>".KN_user_pro_apply::convert_var_parameter(kn_gcode_dtl_nm("U0009", $value_))."</p>";
					$div_bool_ = true;
				}
			?>
		

		<b class="qq">본인의 교육관은?</b>
		<p class="qa"><?=nl2br($user_data_["qna03"])?></p>

		<b class="qq">어떠한 분야에 놀이활동 전문가가 되고 싶으세요?</b>
		<p class="qa">
			<?php
				$qna04_ = $user_data_["qna04"];
				$div_bool_ = false;
				foreach($qna04_ as $value_){
					if($div_bool_) echo ", ";
					echo KN_user_pro_apply::convert_var_parameter(kn_gcode_dtl_nm("U0011", $value_));
					$div_bool_ = true;
				}
			?>
		</p>

		<b class="qq">놀이활동 진행 경험이 얼마나 있으십니까?</b>
		<p class="qa">
			<?=KN_user_pro_apply::convert_var_parameter(kn_gcode_dtl_nm("U0013", $user_data_["qna05"]))?>
		</p>

		<b class="qq">놀이활동 전문가 활동이 가능한 시간은?</b>
		<p class="qa">
			<?=$user_data_["qna06"]->from?> ~ <?=$user_data_["qna06"]->to?>
		</p>

		<b class="qq">내 놀이활동에 참여가능한 아이의 연령대는?</b>
		<p class="qa">
			<?=$user_data_["qna07"]->from?> ~ <?=$user_data_["qna07"]->to?>세 <?=strlen($user_data_["qna07"]->month) ? "(".$user_data_["qna07"]->month."개월)" : ""?>
		</p>

		<b class="qq">놀이활동 전문가 활동 형태를 선택해 주세요.</b>
		<p class="qa">
			<?=KN_user_pro_apply::convert_var_parameter(kn_gcode_dtl_nm("U0015", $user_data_["qna08"]))?>
		</p>

		<?php if($user_data_["qna08"] === "00001"){ ?>
		<b class="qq"><?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</b>
		<p class="qa">
			<?=nl2br($user_data_["qna09"])?>
		</p>
		<?php } ?>

		<?php if($user_data_["qna08"] === "00003"){ ?>
		<b class="qq">현재 운영하시는 사업과 <?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</b>
		<p class="qa">
			<?=nl2br($user_data_["qna10"])?>
		</p>
		<?php } ?>

		<?php if($user_data_["qna08"] === "00005"){ ?>
		<b class="qq">현재 활동하시는 분야와 <?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</b>
		<p class="qa">
			<?=nl2br($user_data_["qna11"])?>
		</p>
		<?php } ?>

		<b class="qq">놀이활동을 진행한 사진이 있을경우 업로드 하여 주세요.</b>
		<p class="qa img-grid">
			<?php
				$qna12_ = $user_data_["qna12"];
				foreach($qna12_ as $value_){
					?>
					<a href="<?=$value_?>" target="_blank"><img src="<?=$value_?>"></a>
					<?php
				}
			?>
		</p>

		<b class="qq">나와 나의 재능을 보여줄 수 있는 링크가 있을경우 입력해주세요.</b>
		<p class="qa">
			<?=$user_data_["qna13"]?>
		</p>

		<b class="qq">놀이활동 진행 장소를 선택해 주세요</b>
		<p class="qa">
			<?php
				$qna14_ = $user_data_["qna14"];
				$div_bool_ = false;
				foreach($qna14_ as $value_){
					if($div_bool_) echo ", ";
					echo KN_user_pro_apply::convert_var_parameter(kn_gcode_dtl_nm("U0017", $value_));
					$div_bool_ = true;
				}
			?>
		</p>
<?php } ?>
		<?php echo $button_html_; ?>
	</form>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<style type="text/css">
	#kn-user-pro-view .qq{
		font-size: 17px;
		margin-top: 40px;
		margin-bottom: 5px;
		display: block;
		color: #464646;
	}
	#kn-user-pro-view .qq:before{
		color: #a2a2a2;
		content: "Q ";
		font-size: 17px;
	}
	#kn-user-pro-view .qa{
		margin-top: 0px;
		margin-bottom: 0px;
		font-size: 15px;
	}
	#kn-user-pro-view .qa:before{
		color: #a2a2a2;
		content: "A ";
		font-size: 17px;
	}

	#kn-user-pro-view .img-grid img{
		display: inline-block;
		height: 60px;
		width: auto;
	}
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});

	function _kn_accept_pro(){
		KN.warning_popup({
			title : "전문가승인처리",
			text : "회원을 전문가로 승인처리합니다.",
			confirmButtonText : "승인처리",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_kn_accept_pro_p1();
		});
	}
	function _kn_accept_pro_p1(){
		var user_data_ = jQuery("#kn-user-pro-view").serializeObject();
		KN.post("<h3>승인처리중</h3>",{
			action : "kn-admin-user-pro-accept",
			user_id : user_data_["user_id"]
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				KN.error_popup({
					title : "에러발생",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "승인처리완료",
				confirmButtonText : "확인"
			},function(){
				document.location = document.location;
			});
		});
	}
	function _kn_delete_pro(){
		KN.warning_popup({
			title : "전문가요청삭제",
			text : "회원의 전문가 요청을 삭제합니다.",
			confirmButtonText : "삭제하기",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_kn_delete_pro_p1();
		});
	}
	function _kn_delete_pro_p1(){
		var user_data_ = jQuery("#kn-user-pro-view").serializeObject();
		KN.post("<h3>삭제 중</h3>",{
			action : "kn-admin-user-pro-delete",
			user_id : user_data_["user_id"]
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				KN.error_popup({
					title : "에러발생",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "삭제완료",
				confirmButtonText : "확인"
			},function(){
				document.location = "?page=<?=KN_SLUG_ADMIN_USER_PRO_LIST?>";
			});
		});
	}
	</script>
	<?php
	}
	
	static function _ajax_accept(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$user_id_ = $_POST["user_id"];

		$result_ = KN_user::update_pro($user_id_, array(
			"status" => "00003",
			"accept_date" => KN_QUERY_FUNC("NOW()"),
		));

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}

	static function _ajax_delete(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$user_id_ = $_POST["user_id"];
		$result_ = KN_user::delete_pro($user_id_);

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}
}

kn_add_ajax("kn-admin-user-pro-accept", array("KNAdmin_user_pro_view", "_ajax_accept"));
kn_add_ajax("kn-admin-user-pro-delete", array("KNAdmin_user_pro_view", "_ajax_delete"));

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_USER_PRO_LIST,
	"title" => "전문가상세정보",
	"class" => "KNAdmin_user_pro_view",
	"param" => array("user_id"),
	"eparam" => array(),
	"permission" => "manage_options",
));

?>