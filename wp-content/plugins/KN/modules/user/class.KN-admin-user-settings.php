<?php

class KNAdmin_user_settings{
	static function _add_settings_form($settings_){
	?>
	<br/>
	<hr/>
	<br/>
	<h3>회원약관사항</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th>
					<label for="user_term001_title">약관1 명칭/내용</label>
				</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="user_term001_title" class="large-text" placeholder="약관명칭" value="<?php echo $settings_["user_term001_title"]; ?>"><br/><br/>
					<?php wp_editor(stripslashes($settings_["user_term001_content"]), "user_term001_content" , array(
						"textarea_name" => "user_term001_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>

			<tr>
				<th>
					<label for="user_term002_title">약관2 명칭/내용</label>
				</th>	
			</tr>
			<tr>
				<td>
					<input type="text" name="user_term002_title" class="large-text" placeholder="약관명칭" value="<?php echo $settings_["user_term002_title"]; ?>"><br/><br/>
					<?php wp_editor(stripslashes($settings_["user_term002_content"]), "user_term002_content" , array(
						"textarea_name" => "user_term002_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>

			<tr>
				<th>
					<label for="user_term003_title">약관3 명칭/내용</label>
				</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="user_term003_title" class="large-text" placeholder="약관명칭" value="<?php echo $settings_["user_term003_title"]; ?>"><br/><br/>
					<?php wp_editor(stripslashes($settings_["user_term003_content"]), "user_term003_content" , array(
						"textarea_name" => "user_term003_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>
			<tr>
				<th>
					<label for="user_term004_title">약관4 명칭/내용</label>
				</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="user_term004_title" class="large-text" placeholder="약관명칭" value="<?php echo $settings_["user_term004_title"]; ?>"><br/><br/>
					<?php wp_editor(stripslashes($settings_["user_term004_content"]), "user_term004_content" , array(
						"textarea_name" => "user_term004_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>
			<tr>
				<th>
					<label for="user_term004_title">회원탈퇴 동의사항</label>
				</th>
			</tr>
			<tr>
				<td>
					<?php wp_editor(stripslashes($settings_["user_leave_term001_content"]), "user_leave_term001_content" , array(
						"textarea_name" => "user_leave_term001_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>

			
		</tbody>
	</table>
	<br/>
	<hr/>
	<br/>
	<h3>회원등록관련사항</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="user_login_page_id">로그인 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_login_page_id"],
						"name" => "user_login_page_id",
						"class" => "large-text",
					)); ?>
					<p><label for="user_login_need_ssl"><input type="checkbox" name="user_login_need_ssl" id="user_login_need_ssl" <?php echo ($settings_["user_login_need_ssl"] === true ? "checked" : ""); ?> >SSL필요</label></p>
				</td>
			</tr>
			<tr>
				<th><label for="user_login_page_back_img_url">로그인 배경화면</label></th>
				<td>
					<input type="text" name="user_login_page_back_img_url" class="large-text" placeholder="URL" value="<?php echo $settings_["user_login_page_back_img_url"]; ?>">
					<a href="javascript:kn_settings_user_select_back('user_login_page_back_img_url');" class="button button-secondary">선택</a>
				</td>
			</tr>
			<tr>
				<th><label for="user_login_redirect_url">로그인 리다이렉트 URL</label></th>
				<td>
					<input type="text" name="user_login_redirect_url" class="large-text" placeholder="로그인 리다이렉트 URL" value="<?php echo $settings_["user_login_redirect_url"]; ?>">
					<p class="hint">*설정하지 않을 경우, 홈페이지로 리다이렉트합니다.</p>
				</td>
			</tr>
			<tr>
				<th><label for="user_login_captcha_count">Captcha 요구 로그인횟수</label></th>
				<td>
					<input type="text" name="user_login_captcha_count" class="large-text" placeholder="Captcha 요구 로그인횟수" value="<?php echo $settings_["user_login_captcha_count"]; ?>">
					<p class="hint">*로그인 실패 횟수에 따라, Captcha를 요구합니다.</p>
					<p class="hint">*설정하지 않을 경우, Captcha를 요구하지 않습니다.</p>
				</td>
			</tr>
			<tr>
				<th><label for="user_logout_page_id">로그아웃 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_logout_page_id"],
						"name" => "user_logout_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>
			<tr>
				<th><label for="user_logout_redirect_url">로그아웃 리다이렉트 URL</label></th>
				<td>
					<input type="text" name="user_logout_redirect_url" class="large-text" placeholder="로그아웃 리다이렉트 URL" value="<?php echo $settings_["user_logout_redirect_url"]; ?>">
					<p class="hint">*설정하지 않을 경우, 홈페이지로 리다이렉트합니다.</p>
				</td>
			</tr>
			<tr>
				<th><label for="user_register_page_id">회원등록 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_register_page_id"],
						"name" => "user_register_page_id",
						"class" => "large-text",
					)); ?>
					<p><label for="user_register_need_ssl"><input type="checkbox" name="user_register_need_ssl" id="user_register_need_ssl" <?php echo ($settings_["user_register_need_ssl"] === true ? "checked" : ""); ?> >SSL필요</label></p>
					<p><label for="user_register_test_mode"><input type="checkbox" name="user_register_test_mode" id="user_register_test_mode" <?php echo ($settings_["user_register_test_mode"] === true ? "checked" : ""); ?> >테스트모드</label></p>
				</td>
			</tr>
			<tr>
				<th><label for="user_register_page_back_img_url">회원등록 배경화면</label></th>
				<td>
					<input type="text" name="user_register_page_back_img_url" class="large-text" placeholder="URL" value="<?php echo $settings_["user_register_page_back_img_url"]; ?>">
					<a href="javascript:kn_settings_user_select_back('user_register_page_back_img_url');" class="button button-secondary">선택</a>
				</td>
			</tr>
			<tr>
				<th><label for="user_forgotpassword_page_id">암호찾기 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_forgotpassword_page_id"],
						"name" => "user_forgotpassword_page_id",
						"class" => "large-text",
					)); ?>
					<p><label for="user_forgotpassword_need_ssl"><input type="checkbox" name="user_forgotpassword_need_ssl" id="user_forgotpassword_need_ssl" <?php echo ($settings_["user_forgotpassword_need_ssl"] === true ? "checked" : ""); ?> >SSL필요</label></p>
				</td>
			</tr>
			<tr>
				<th><label for="user_changepassword_page_id">암호변경 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_changepassword_page_id"],
						"name" => "user_changepassword_page_id",
						"class" => "large-text",
					)); ?>
					<p><label for="user_changepassword_need_ssl"><input type="checkbox" name="user_changepassword_need_ssl" id="user_changepassword_need_ssl" <?php echo ($settings_["user_changepassword_need_ssl"] === true ? "checked" : ""); ?> >SSL필요</label></p>
				</td>
			</tr>
			<tr>
				<th><label for="user_myinfo_page_id">내정보 URL</label></th>
				<td>
						<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_myinfo_page_id"],
						"name" => "user_myinfo_page_id",
						"class" => "large-text",
					)); ?>
					<p><label for="user_myinfo_need_ssl"><input type="checkbox" name="user_myinfo_need_ssl" id="user_myinfo_need_ssl" <?php echo ($settings_["user_myinfo_need_ssl"] === true ? "checked" : ""); ?> >SSL필요</label></p>
				</td>
			</tr>
			
			<tr>
				<th><label for="user_login_header_action">로그인해더표시 Action</label></th>
				<td>
					<input type="text" name="user_login_header_action" class="large-text" placeholder="로그인해더표시 Action" value="<?php echo $settings_["user_login_header_action"]; ?>">
					<p class="hint">*설정하지 않을 경우 로그인해더를 표시하지 않습니다.</p>
				</td>
			</tr>
			
		</tbody>
	</table>
	<p class="hint">*URL를 설정하지 않을 경우, AW시스템의 회원가입 및 로그인 기능의 정상적인 사용이 불가능합니다.</p>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="user_find_id_email_title">아이디찾기 이메일 발송제목</label></th>
				<td>
					<input type="text" name="user_find_id_email_title" class="large-text" placeholder="아이디찾기 이메일 발송제목" value="<?php echo $settings_["user_find_id_email_title"]; ?>">
				</td>
			</tr>
			<tr>
				<th colspan="2"><label for="user_find_id_email_content">아이디찾기 메일 발송내용</label></th>
			</tr>
			<tr>
				<td colspan="2"><?php wp_editor(stripslashes($settings_["user_find_id_email_content"]), "kn-settings-user-find-id-email-content" , array(
					"textarea_name" => "user_find_id_email_content",
					"tinymce" => false,
				)); ?>
				<p class="hint">*아이디찾기 이메일 발송내용 서식에는 반드시 {user_login}이 포함되어야 합니다.</p>
			</td>
			</tr>

			<tr>
				<th><label for="user_forgotpassword_email_title">암호찾기 이메일 발송제목</label></th>
				<td>
					<input type="text" name="user_forgotpassword_email_title" class="large-text" placeholder="암호찾기 이메일 발송제목" value="<?php echo $settings_["user_forgotpassword_email_title"]; ?>">
				</td>
			</tr>
			<tr>
				<th colspan="2"><label for="user_forgotpassword_email_content">암호찾기 이메일 발송내용</label></th>
			</tr>
			<tr>
				<td colspan="2"><?php wp_editor(stripslashes($settings_["user_forgotpassword_email_content"]), "kn-settings-user-forgotpassword-email-content" , array(
					"textarea_name" => "user_forgotpassword_email_content",
					"tinymce" => false,
				)); ?>
				<p class="hint">*암호찾기 이메일 발송내용 서식에는 반드시 {changepassword_url}이 포함되어야 암호변경URL에 접근할 수 있습니다.</p>
			</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<hr/>
	<br/>
	<h3>전문가관련</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="user_pro_apply_page_id">전문가 신청 페이지</label></th>
				<td>
					<?php echo kn_make_select_with_pages(array(
						"default" => $settings_["user_pro_apply_page_id"],
						"name" => "user_pro_apply_page_id",
						"class" => "large-text",
					)); ?>
				</td>
			</tr>
			<tr>
				<th><label for="user_pro_apply_page_back_img_url">전문가 신청 페이지 배경화면</label></th>
				<td>
					<input type="text" name="user_pro_apply_page_back_img_url" class="large-text" placeholder="URL" value="<?php echo $settings_["user_pro_apply_page_back_img_url"]; ?>">
					<a href="javascript:kn_settings_user_select_back('user_pro_apply_page_back_img_url');" class="button button-secondary">선택</a>
				</td>
			</tr>
			<tr>
				<th colspan="2">
					<label for="user_pro_apply_term_title">약관 명칭/내용</label>
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<input type="text" name="user_pro_apply_term_title" class="large-text" placeholder="약관명칭" value="<?php echo $settings_["user_pro_apply_term_title"]; ?>"><br/><br/>
					<?php wp_editor(stripslashes($settings_["user_pro_apply_term_content"]), "user_pro_apply_term_content" , array(
						"textarea_name" => "user_pro_apply_term_content",
						"tinymce" => false,)); ?>
				</td>
			</tr>
		</tbody>
	</table>
			
	<script type="text/javascript">
	function kn_settings_user_select_back(target_){
		KN.select_media({callback : function(result_){
			var img_url_ = result_[0]["url"];
			jQuery("input[name='"+target_+"']").val(img_url_);
		}});
	}
	</script>

	<?php
	}

	static function _filter_settings($settings_){
		$settings_["user_login_need_ssl"] = ((isset($settings_["user_login_need_ssl"]) && $settings_["user_login_need_ssl"] === "on") ? true : false);
		$settings_["user_register_need_ssl"] = ((isset($settings_["user_register_need_ssl"]) && $settings_["user_register_need_ssl"] === "on") ? true : false);
		$settings_["user_forgotpassword_need_ssl"] = ((isset($settings_["user_forgotpassword_need_ssl"]) && $settings_["user_forgotpassword_need_ssl"] === "on") ? true : false);
		$settings_["user_changepassword_need_ssl"] = ((isset($settings_["user_changepassword_need_ssl"]) && $settings_["user_changepassword_need_ssl"] === "on") ? true : false);
		$settings_["user_myinfo_need_ssl"] = ((isset($settings_["user_myinfo_need_ssl"]) && $settings_["user_myinfo_need_ssl"] === "on") ? true : false);
		$settings_["user_register_test_mode"] = ((isset($settings_["user_register_test_mode"]) && $settings_["user_register_test_mode"] === "on") ? true : false);

		return $settings_;
	}
}

add_action("knadmin_settings_form",array("KNAdmin_user_settings","_add_settings_form"));
add_filter("kn_update_settings", array("KNAdmin_user_settings", "_filter_settings"));

?>