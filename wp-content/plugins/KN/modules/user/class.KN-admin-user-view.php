<?php

class KNAdmin_user_view extends KNAction{

	static function _initialize(){
		kn_load_library("admin-all");
	}
	static function _draw_view(){
		add_action("admin_footer", array("KNAdmin_user_view", "add_script"));

		$user_data_ = kn_user($_GET["user_id"]);

		$button_html_ = "<div class='kn-text-right'>";
		if($user_data_["status"] === "00001"){
			$button_html_ .= "<a class='button button-delete' href='javascript:_kn_block_account();'>정지처리</a>";	
		}else if($user_data_["status"] === "90001"){
			$button_html_ .= "<a class='button button-delete' href='javascript:_kn_unblock_account();'>정지취소</a>";	
		}
		
		$button_html_ .= "</div>";
	?>

	<div class="wrap kn-limit-content">
	<h2></h2>
	<h2>회원상세정보 <a class="add-new-h2" href="?page=<?php echo KN_SLUG_ADMIN_USER_LIST; ?>">돌아가기</a></h2>
	<form method="post" id="kn-user-view" action>
		<input type="hidden" name="user_id" value="<?php echo $_GET['user_id'] ?>" />
		<?php echo $button_html_; ?>
		<table class="form-table">
			<tr>
				<th>회원명</th>
				<td><b><?php echo $user_data_["user_name"]; ?></b></td>
			</tr>
			<tr>
				<th>생년월일</th>
				<td><?php echo $user_data_["birth_yyyy"].".".$user_data_["birth_mm"].".".$user_data_["birth_dd"]; ?></td>
			</tr>
			<tr>
				<th>아이디/이메일</th>
				<td><?php echo "<b>".$user_data_["user_login"]."</b> / ".$user_data_["user_email"]; ?></td>
			</tr>
			<tr>
				<th>이메일수신여부</th>
				<td><?php echo $user_data_["email_rev_yn"] === "Y" ? "수신함" : "수신안함"; ?></td>
			</tr>
			<tr>
				<th>휴대폰번호</th>
				<td><?php echo $user_data_["phone1"]."-".$user_data_["phone2"]."-".$user_data_["phone3"]; ?></td>
			</tr>
			<tr>
				<th>SMS수신여부</th>
				<td><?php echo $user_data_["sms_rev_yn"] === "Y" ? "수신함" : "수신안함"; ?></td>
			</tr>
			<tr>
				<th>회원상태</th>
				<td><?php echo $user_data_["status_nm"]; ?></td>
			</tr>
			<tr>
				<th>가입일자</th>
				<td><?php echo $user_data_["reg_date"]; ?></td>
			</tr>
		<?php if($user_data_["status"] === "00009"){ ?>

			<tr>
				<th>탈퇴일자</th>
				<td class="kn-color kn-point"><?php echo $user_data_["lev_date"]; ?></td>
			</tr>

		<?php }else if($user_data_["status"] === "90001"){ ?>

			<tr>
				<th>정지일자</th>
				<td class="kn-color kn-error"><?php echo $user_data_["blk_date"]; ?></td>
			</tr>
			<tr>
				<th>정지사유</th>
				<td><?php echo nl2br($user_data_["blk_rsn"]); ?></td>
			</tr>

		<?php } ?>
			
		</table>
		<?php echo $button_html_; ?>
	</form>

	</div>
	<?php
	}

	static function add_script(){
	?>
	<style type="text/css">
	
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($){

	});

	function _kn_block_account(){
		KN.warning_popup({
			title : "계정정지처리",
			text : "계정을 정지처리를 진행합니다",
			confirmButtonText : "정지처리",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_kn_block_account_p1();
		});
	}
	function _kn_block_account_p1(){
		KN.prompt_popup({
			title : "계정정지사유입력",
			text : "계정을 정지사유를 입력하세요",
			confirmButtonText : "정지처리",
			cancelButtonText : "취소"
		},function(data_){
			if(data_ === false){
				return;
			}

			if(data_ === null || data_.trim().length == 0){
				KN.error_popup({
					title : "정지사유를 입력하세요!",
					text : "계정정지사유는 반드시 입력해야 합니다.",
					confirmButtonText : "확인"
				},function(){
					_kn_block_account_p1();
				});
				return;
			}

			_kn_block_account_p2(data_);
		});
	}
	function _kn_block_account_p2(block_reason_){
		var user_data_ = jQuery("#kn-user-view").serializeObject();
		KN.post("<h3>정지처리중</h3>",{
			action : "kn-admin-user-block",
			user_id : user_data_["user_id"],
			block_reason : block_reason_
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				KN.error_popup({
					title : "에러발생",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "정지처리완료",
				confirmButtonText : "확인"
			},function(){
				document.location = document.location;
			});
		});
	}
	function _kn_unblock_account(){
		KN.warning_popup({
			title : "계정정지취소",
			text : "계정정지처리를 취소합니다",
			confirmButtonText : "정지취소",
			cancelButtonText : "취소"
		},function(confirmed_){
			if(!confirmed_) return;
			_kn_unblock_account_p1();
		});
	}
	function _kn_unblock_account_p1(){
		var user_data_ = jQuery("#kn-user-view").serializeObject();
		KN.post("<h3>정지취소중</h3>",{
			action : "kn-admin-user-unblock",
			user_id : user_data_["user_id"]
		},function(result_, response_json_){
			if(!result_ || response_json_.success !== true){
				KN.error_popup({
					title : "에러발생",
					confirmButtonText : "확인"
				});
				return;
			}

			KN.success_popup({
				title : "정지취소완료",
				confirmButtonText : "확인"
			},function(){
				document.location = document.location;
			});
		});
	}
	</script>
	<?php
	}
	
	static function _ajax_do_block(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$user_id_ = $_POST["user_id"];
		$block_reason_ = $_POST["block_reason"];

		$result_ = KN_user::block($user_id_, $block_reason_);

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}

	static function _ajax_do_unblock(){
		if(!kn_is_admin()){
			echo json_encode(false);
			die();return;
		}

		$user_id_ = $_POST["user_id"];

		$result_ = KN_user::unblock($user_id_);

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}
}

kn_add_ajax("kn-admin-user-block", array("KNAdmin_user_view", "_ajax_do_block"));
kn_add_ajax("kn-admin-user-unblock", array("KNAdmin_user_view", "_ajax_do_unblock"));

KN::add_plugin_menu(array(
	"page" => KN_SLUG_ADMIN_USER_LIST,
	"title" => "회원상세정보",
	"class" => "KNAdmin_user_view",
	"param" => array("user_id"),
	"eparam" => array(),
	"permission" => "manage_options",
));

?>