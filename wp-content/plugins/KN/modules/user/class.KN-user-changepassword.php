<?php

class KN_user_changepassword extends KNShortcode{

	static function shortcode_name(){
		return "kn-user-changepassword";
	}
	static function add_library(){
		$settings_ = KN_settings::get_settings();
		kn_load_library("main");
		
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));

		wp_register_script("kn-user-changepassword", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-changepassword.js"), array("kn-user"));

		wp_localize_script("kn-user-changepassword", "kn_user_cp_msg", array(
			"formval_user_pass_required" => "암호를 입력하여 주세요.",
			"formval_user_pass_minlength" => "암호는 {0}자 이상으로 입력하여 주세요.",
			"formval_user_pass_maxlength" => "암호는 {0}자 이상 입력하여 주세요.",
			"formval_user_pass_c_equalsto" => "암호가 일치하지 않습니다.",
			"changepassword_ajax_error" => "비밀번호 변경 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changepassword_ajax_failed" => "비밀번호 변경에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changepassword_ajax_success" => "정상적으로 변경되었습니다. 로그인화면으로 이동합니다.",
			"changepassword_ajax_redirect_url" => kn_make_url(get_permalink($settings_["user_login_page_id"]),array("user_login" => $_GET["user_login"])),
		));

		wp_enqueue_script("kn-user-changepassword");
	}

	static function _do_shortcode($attrs_){
		$settings_ = KN_settings::get_settings();
		$attrs_ = shortcode_atts(array(),$attrs_);

		$user_login_ = isset($_GET["user_login"]) ? $_GET["user_login"] : "";
		$change_key_ = isset($_GET["key"]) ? $_GET["key"] : "";

		$html_ = '';
		$html_ .= '<form class="kn-form" id="kn-changepassword-form">';
		$html_ .= '<input type="hidden" name="user_login" value="'.$user_login_.'">';
		$html_ .= '<input type="hidden" name="change_key" value="'.$change_key_.'">';

		$html_ .= '<h3 class="title-with-desc">새로운 암호을 설정합니다.</h3>';
		$html_ .= '<p class="desc alignleft"><b>'.$user_login_.'</b> 고객님의 암호를 새롭게 설정합니다.</p>';

		$html_ .= '<table class="kn-form-table">';
		$html_ .= '<tr>';
		$html_ .= '<th>새로운 암호</th>';
		$html_ .= '<td><div class="kn-column-frame kn-column-padding">';

		$html_ .= '<div class="kn-column l50"><input type="password" name="user_pass" placeholder="암호" class="full-width" title="암호는 8~25자 사용이 가능합니다."></div>';
		$html_ .= '<div class="kn-column l50"><input type="password" name="user_pass_c" placeholder="암호확인" class="full-width" title="암호가 정확하지 않습니다." data-accessible="true"></div>';

		$html_ .= '</div>';
		$html_ .= '<label id="user_pass-error" class="error" for="user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label><p class="hint">*암호는 8~25자 사용이 가능합니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '</table>';
		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="변경하기" data-accessible="true">';
		$html_ .= '</div>';

		$html_ .= '</form>';

		return do_shortcode($html_);
	}

	static function _ajax_change_password(){
		$user_data_ = $_POST["user_data"];

		global $wpdb;

		$id_ = kn_user_id($user_data_["user_login"]);

		$result_ = kn_update_user($id_, array(
			"change_key" => null,
		));

		$result_ = KN_user::change_password($id_, $user_data_["user_pass"]);

		echo json_encode(array(
			"success" => ($result_ !== false)
		));
		die();
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$home_url_ = home_url();
		$changepassword_page_id_ = $settings_["user_changepassword_page_id"];

		if(strlen($changepassword_page_id_) && ((string)$changepassword_page_id_) === ((string)$current_page_id_) ){

			if($settings_["user_changepassword_need_ssl"] === true && !kn_is_https()){
				wp_redirect(kn_make_https());
				exit;
			}

			$user_login_ = isset($_GET["user_login"]) ? $_GET["user_login"] : "";
			$change_key_ = isset($_GET["key"]) ? $_GET["key"] : "";

			global $wpdb;
			$query_ = "SELECT ID
				FROM {$wpdb->base_prefix}kn_USERS
				WHERE 1=1
				AND   USER_LOGIN = LOWER(%s)
				AND   CHANGE_KEY = %s";

			$check_data_ = $wpdb->get_row($wpdb->prepare($query_, $user_login_, $change_key_));

			if(!isset($check_data_)){
				wp_die("잘못된 접근입니다.");
			}
		}
	}
  
}

add_shortcode((KN_user_changepassword::shortcode_name()), array("KN_user_changepassword","_do_shortcode"));

add_action("kn_shortcode_filter", array("KN_user_changepassword","filter"));
kn_add_ajax("kn-user-changepassword", array("KN_user_changepassword", "_ajax_change_password"), true);

add_action("kn_user_prevent_page",array("KN_user_changepassword", "_prevent_page"));

?>