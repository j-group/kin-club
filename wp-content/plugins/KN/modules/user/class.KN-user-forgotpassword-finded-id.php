<?php

class KN_user_forgotpassword_finded_id{

	static function _do_shortcode($attrs_){
		$settings_ = KN_settings::get_settings();

		$id_ = $_POST["user_id"];
		//$id_ = "4";
		$user_data_ = kn_user($id_);	

		$login_url_ = kn_make_url($settings_["user_login_url"], array(
			"user_login" => $user_data_["user_login"]
		));

		$forgotpassward_url_ = kn_make_url($settings_["user_forgotpassword_url"], array(
			"user_login" => $user_data_["user_login"]
		));

		$html_ = '';

		$html_ .= '<div class="kn-form-table kn-user-findedid">';
		$html_ .= '<center><span class="logo awsymbol"></span></center>';		
		$html_ .= '<h3 class="title">가입하신 아이디는 <b>'.$user_data_["user_login"].'</b>입니다.</h3>';
		$html_ .= '<p class="desc">기타문의사항은 '.$settings_["email_sender"].'로 문의해주세요.</p>';
		$html_ .= '<div class="button-area">';
		$html_ .= '<a href="'.$login_url_.'" class="kn-alink">로그인하기</a>';
		$html_ .= '<a href="'.$forgotpassward_url_.'" class="kn-alink">비밀번호찾기</a>';
		$html_ .= '</div>';

		$html_ .= '</div>';
		
		return $html_;
	}

  
}

?>