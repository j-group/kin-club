<?php

class KN_user_forgotpassword_main{

	static function _do_shortcode($attrs_){
		$settings_ = KN_settings::get_settings();

		$user_login_ = isset($_REQUEST["user_login"]) ? $_REQUEST["user_login"] : "";

		wp_register_script("kn-user-forgotpassword", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-forgotpassword.js"), array("kn-user"));
		wp_localize_script("kn-user-forgotpassword", "kn_forgotpassword_msg"
		,array(
			"formval_pass_user_login_required" => "아이디를 입력해주세요.",
			"send_sms_failed" => "SMS 발송에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"cannot_finded_id" => "입력하신 휴대폰으로 가입한 이력이 없습니다.",
			"sms_send_completed" => "가입하신 휴대폰번호로 아이디를 발송했습니다.",
			"send_email_failed" => "이메일 발송에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"cannot_finded_email" => "입력하신 아이디로 가입한 이력이 없습니다.",
			"email_send_completed" => "가입하신 이메일로 암호를 변경할 수 있는 안내를 발송했습니다.",

			"auth_failed" => "본인인증에 실패하였습니다.",
			"cannot_finded_user" => "가입한 이력을 찾을 수 없습니다.",
			"findid_email_send_completed" => "가입하신 이메일로 회원님의 아이디를 발송했습니다.",
		));

		wp_enqueue_script("kn-user-forgotpassword");

		$html_  = '';
		$html_ .= '<div class="kn-column-frame kn-column-padding">';

		//find ID
		$html_ .= '<div class="kn-column l50">';
		$html_ .= '<form class="kn-form" id="kn-forgotid-form">';
		$html_ .= '<h3 class="title-with-desc">아이디를 잃어버렸나요?</h3>';
		$html_ .= '<p class="desc alignleft">본인인증을 통해 고객님의 아이디를 찾으실 수 있습니다.</p>';
		$html_ .= '<table class="kn-form-table with-bottom-area">';
		$html_ .= '<tr>';
		$html_ .= '<th>인증방식선택</th>';
		$html_ .= '<td>';
		$html_ .= '<input type="button" value="휴대폰" class="knbtn knbtn-medium knbtn-primary" data-accessible="true" onclick="kn_user_find_id(\'H\');">';
		//$html_ .= ' <input type="button" value="아이핀" class="knbtn knbtn-medium knbtn-primary">';
		$html_ .= '<span class="kn-indicator"></span>';
		$html_ .= '<p class="hint">*고객님의 개인정보를 소중하게 보호하겠습니다.</p>';
		$html_ .= '<p class="hint">*본인인증을 통해 아이디를 찾을 수 있습니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '</table>';

		$html_ .= '<div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l60 fix-width">';
		$html_ .= '<div class="button-area alignleft other-handle">';
		$html_ .= '<a href="'.get_permalink($settings_["user_register_page_id"]).'" class="kn-alink">회원이 아니신가요?</a>';
		$html_ .= '</div>';
		$html_ .= '</div>';
		$html_ .= '<div class="kn-column l40 fix-width">';
		$html_ .= '</div>';
		$html_ .= '</div>';

		$html_ .= '</form>';
		
		$html_ .= '<form id="kn-forgotid-form-fined" style="display:none;">';
		$html_ .= '<input type="hidden" name="user_id" value="">';
		$html_ .= '<input type="hidden" name="finded_id" value="true">';
		$html_ .= '</form>';

		$html_ .= '</div>';

		$html_ .= '<div class="kn-column l5"></div>';

		//find PASWORD
		$html_ .= '<div class="kn-column l45">';
		$html_ .= '<form class="kn-form" id="kn-forgotpass-form">';
		$html_ .= '<h3 class="title-with-desc">암호를 잃어버렸나요?</h3>';
		$html_ .= '<p class="desc alignleft">가입하신 아이디나 이메일로 비밀번호를 찾습니다.</p>';
		$html_ .= '<table class="kn-form-table with-bottom-area">';
		$html_ .= '<tr>';
		$html_ .= '<th>아이디</th>';
		$html_ .= '<td>';
		$html_ .= '<input type="text" name="user_login" placeholder="아이디 입력" class="full-width" data-accessible="true" value="'.$user_login_.'" tabindex="10">';
		$html_ .= '<p class="hint">*가입 시, 기입한 이메일로 안내문을 발송합니다.</p>';
		$html_ .= '<p class="hint">*발송된 안내문을 통해 암호를 변경할 수 있습니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '</table>';

		$html_ .= '<div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l60 fix-width">';
		$html_ .= '<div class="button-area alignleft other-handle">';
		$html_ .= '<a href="'.get_permalink($settings_["user_login_page_id"]).'" class="kn-alink" tabindex="12">로그인하기</a>';
		$html_ .= '</div>';
		$html_ .= '</div>';
		$html_ .= '<div class="kn-column l40 fix-width">';

		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="이메일 발송" data-accessible="true" tabindex="11">';
		$html_ .= '</div>';

		$html_ .= '</div>';
		$html_ .= '</div>';

		$html_ .= '</form>';
		$html_ .= '</div>';

		$html_ .= '</div>';

		$auth_req_data_ = KN_siren24::enc_request_info("001007");

		$html_ .= '<form id="kn-user-auth-req-form">';
		$html_ .= '<input type="hidden" name="enc_data" value="'.$auth_req_data_["enc_data"].'">';
		$html_ .= '<input type="hidden" name="return_url" value="'.$auth_req_data_["return_url"].'">';
		$html_ .= '<input type="hidden" name="request_num" value="'.$auth_req_data_["request_num"].'">';
		$html_ .= '</form>';
		
		return $html_;
	}

	static function _ajax_senjd_email_for_findid(){
		$auth_di_ = $_POST["auth_di"];
		$user_data_ = KN_user::user_list(array("auth_di" => $auth_di_));
		
		if(!isset($user_data_) || count($user_data_) <= 0){
			echo json_encode(array(
				"success" => false,
				"not_found" => true,
			));
			die();
		}

		$user_data_ = $user_data_[0];

		$result_ = KN_user::send_email_for_find_id($user_data_["user_login"]);
		$send_error_ = is_wp_error($result_);

		echo json_encode(array(
			"success" => !$send_error_,
			"result" => ($send_error_ ? false : $result_),
		));
		die();
	}

	static function _ajax_send_email_for_password(){
		$user_login_ = $_POST["user_login"];
		$result_ = KN_user::send_email_for_change_password($user_login_);
		$send_error_ = is_wp_error($result_);

		echo json_encode(array(
			"success" => !$send_error_,
			"result" => ($send_error_ ? false : $result_),
		));
		die();
	}  
}

kn_add_ajax("kn-user-findid-send-email", array("KN_user_forgotpassword_main","_ajax_senjd_email_for_findid"), true);
kn_add_ajax("kn-user-forgotpass-send-email", array("KN_user_forgotpassword_main","_ajax_send_email_for_password"), true);

?>