<?php

class KN_user_forgotpassword extends KNShortcode{

	static function shortcode_name(){
		return "kn-user-forgotpassword";
	}

	static function _current_class(){
		$class_ = "KN_user_forgotpassword_main";
		if(isset($_POST["finded_id"]) && $_POST["finded_id"] === "true"){
			$class_ = "KN_user_forgotpassword_finded_id";
		}

		return $class_;
	}

	static function add_library(){
		$settings_ = KN_settings::get_settings();
		kn_load_library("main");
		kn_load_library("kn-siren24");
		
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));
	}

	static function _do_shortcode($attrs_){
		$settings_ = KN_settings::get_settings();
		
		$attrs_ = shortcode_atts(array(),$attrs_);
		$class_ = self::_current_class();

		return do_shortcode(call_user_func_array(array($class_, "_do_shortcode"),array($attrs_)));
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$home_url_ = home_url();
		$user_forgotpassword_page_id_ = $settings_["user_forgotpassword_page_id"]; 

		if(strlen($user_forgotpassword_page_id_) && ((string)$user_forgotpassword_page_id_) === ((string)$current_page_id_) ){
			if(kn_is_user_logged_in()){
				wp_redirect($home_url_);
			}

			if($settings_["user_forgotpassword_need_ssl"] === true && !kn_is_https()){
				wp_redirect(kn_make_https());
				exit;
			}
		}
	}
  
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-forgotpassword-main.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-forgotpassword-finded-id.php');

add_shortcode((KN_user_forgotpassword::shortcode_name()), array("KN_user_forgotpassword","_do_shortcode"));

add_action("kn_shortcode_filter", array("KN_user_forgotpassword","filter"));
add_action("kn_user_prevent_page",array("KN_user_forgotpassword","_prevent_page"));

?>