<?php

class KN_user_login extends KNShortcode{

	static function shortcode_name(){
		return "kn-user-login";
	}
	static function add_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("main");
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));
		wp_enqueue_style("kn-user-login", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-login.css"));
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));

		wp_register_script("kn-user-login", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-login.js"), array("kn-user"));

		wp_localize_script("kn-user-login", "kn_user_login_msg"
		,array(
			"formval_user_login_required" => "아이디를 입력하여 주세요.",
			"formval_user_login_exists" => "가입되지 않은 아이디입니다.",
			"formval_user_pass_required" => "암호를 입력하여 주세요.",
			"formval_captcha_required" => "보안문자를 입력하여 주세요.",
			"formval_captcha_remote" => "보안문자가 일치하지 않습니다.",
			"login_error" => "로그인 중 에러가 발생했습니다. ".$settings_["email_sender"]."로 문의바랍니다. 불편을 드려 죄송합니다.",
			"login_failed" => "아이디나 암호가 정확하지 않습니다. 확인 후 다시 시도해주세요.",
			"login_url" => get_permalink($settings_["user_login_page_id"]),
			"redirect_url" => (isset($_GET["redirect"]) ? $_GET["redirect"] : get_permalink($settings_["user_login_page_id"])),
		));

		wp_enqueue_script("kn-user-login");
	}

	static function _do_shortcode($attrs_){
		$attrs_ = shortcode_atts(array(),$attrs_);
		$settings_ = KN_settings::get_settings();
		$redirect_url_ = $settings_["user_login_redirect_url"];

		$user_login_ = isset($_COOKIE["kn-user-login"]) ? $_COOKIE["kn-user-login"] : "";
		$user_login_ = isset($_REQUEST["user_login"]) ? $_REQUEST["user_login"] : $user_login_;

		$site_name_ = get_bloginfo("name");
		$register_url_ = get_permalink($settings_["user_register_page_id"]);
		$forgotpassword_url_ = get_permalink($settings_["user_forgotpassword_page_id"]);

		$background_url_ = $settings_["user_login_page_back_img_url"];

		$html_ = '<form class="kn-form" id="kn-user-login-form" style="background-image:url('.$background_url_.');"><div class="wrap">';
		$html_ .= '<div class="kn-user-login-frame">';	
	
		$html_ .= '<h3 class="title-with-desc">'.$site_name_.'에 오신걸 환영합니다!</h3>';
		$html_ .= '<p class="desc alignleft">가입하신 아이디와 암호를 입력하세요.</p>';
		$html_ .= '<table class="kn-form-table with-bottom-area">';
		$html_ .= '<tr>';
		$html_ .= '<th>아이디</th>';
		$html_ .= '<td><input type="text" class="full-width" name="user_login" placeholder="아이디 또는 이메일" data-accessible="true" value="'.$user_login_.'"></td>';
		$html_ .= '</tr>';
		$html_ .= '<tr>';
		$html_ .= '<th>암호</th>';
		$html_ .= '<td><input type="password" class="full-width" name="user_pass" placeholder="암호입력" data-accessible="true">';
		$html_ .= '<label for="kn-id-remember-yn" class="for-input"><input type="checkbox" id="kn-id-remember-yn" name="id_remember_yn" data-accessible="true" '.(isset($_COOKIE["kn-user-login"]) ? "checked" : "").'>아이디 기억하기</label>';
		$html_ .= '<p class="hint">*공공장소에서는 아이디 기억하기를 해제하여 소중한 개인정보를 지킬 수 있습니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';

		$captcha_count_ = KN_session::get("kn-user-signin-count");
		if(!isset($count_)){
			$count_ = 0;
		}
		$tcaptcha_count_ = $settings_["user_login_captcha_count"];
	
		if($captcha_count_ >= $tcaptcha_count_){
			$captcha_ = kn_captcha_create(array(
				'min_length' => 7,
				'max_length' => 7,
				"min_font_size" => 24,
				"max_font_size" => 24,
			));

			$html_ .= '<tr class="captcha warning">';
			$html_ .= '<th>보안문자입력</th>';
			$html_ .= '<td><div class="kn-column-frame kn-column-padding">';
			$html_ .= '<div class="kn-column l30"><img src="//'.$_SERVER['SERVER_NAME'].$captcha_["image_src"].'" width="100%"></div>';
			$html_ .= '<div class="kn-column l70"><input type="text" class="full-width" name="captcha" placeholder="보안문자입력" data-accessible="true"></div>';
			$html_ .= '</div>';
			$html_ .= '<p class="hint">*개인정보보호를 위해 <span class="error">로그인 '.$tcaptcha_count_.'회 실패 시, 보안문자를 함께 입력</span>해야 합니다.</p>';
			$html_ .= '<p class="hint">*보안문자는 대소문자를 구분합니다.</p>';
			$html_ .= '</td>';
			$html_ .= '</tr>';	
		}
		

		$html_ .= '</table>';

		$html_ .= '<div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l60 fix-width">';
		$html_ .= '<div class="button-area alignleft other-handle">';
		$html_ .= '<a href="'.$register_url_.'" class="kn-alink">회원가입하기</a>';
		$html_ .= '<a href="'.$forgotpassword_url_.'" class="kn-alink">아이디/암호 찾기 </a>';
		$html_ .= '</div>';
		$html_ .= '</div>';
		$html_ .= '<div class="kn-column l40 fix-width">';

		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="로그인" data-accessible="true">';
		$html_ .= '</div>';

		$html_ .= '</div>';
		$html_ .= '</div>';

		$html_ .= '</div>';
		$html_ .= '</div></form>';

		return do_shortcode($html_);
	}

	static function _ajax_signin(){
		$user_data_ = $_POST["user_data"];
		$result_ = kn_signin($user_data_["user_login"], $user_data_["user_pass"]);
		$show_captcha_ = false;

		if(!$result_){
			$count_ = KN_session::get("kn-user-signin-count");
			if(!isset($count_)){
				$count_ = 0;
			};

			++$count_;
			KN_session::put("kn-user-signin-count", $count_);

			$settings_ = KN_settings::get_settings();
			$show_captcha_ = ($count_ >= (int)$settings_["user_login_captcha_count"]);
		}else{
			KN_session::remove("kn-user-signin-count");
		}

		echo json_encode(array(
			"result" => $result_,
			"show_captcha" => $show_captcha_
		));
		die();
	}

	static function _ajax_check_captcha(){
		$count_ = KN_session::get("kn-user-signin-count");
		if(!isset($count_)){
			$count_ = 0;
		}

		$settings_ = KN_settings::get_settings();
		echo json_encode(($count_ < (int)$settings_["user_login_captcha_count"] || kn_captcha_verfity($_GET["captcha"])));
		die();
	}

	static function _remove_user_login_cookie(){
		if(!isset($_COOKIE["kn-user-login"])) return false;
		unset($_COOKIE["kn-user-login"]);
		setcookie("kn-user-login", null, -1);
		return true;
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$home_url_ = home_url();
		$login_page_id_ = $settings_["user_login_page_id"];
		$login_redirect_url_ = $settings_["user_login_redirect_url"];

		if((strlen($login_page_id_) && ((string)$login_page_id_) === ((string)$current_page_id_))){
			if(kn_is_user_logged_in()){
				wp_redirect((strlen($login_redirect_url_) ? $login_redirect_url_ : $home_url_));
				exit;
			}

			if($settings_["user_login_need_ssl"] === true && !kn_is_https()){
				wp_redirect(kn_make_https());
				exit;
			}
		}
	}
}

add_shortcode((KN_user_login::shortcode_name()), array("KN_user_login","_do_shortcode"));
add_action("kn_user_signout", array('KN_user_login', "_remove_user_login_cookie"));

kn_add_ajax("kn_user_signin", array('KN_user_login', "_ajax_signin"), true);
kn_add_ajax("kn_user_signin_check_captcha", array('KN_user_login', "_ajax_check_captcha"), true);

add_action("kn_shortcode_filter", array('KN_user_login', "filter"));
add_action("kn_user_prevent_page",array("KN_user_login","_prevent_page"));

?>