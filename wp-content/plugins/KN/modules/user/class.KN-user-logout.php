<?php

class KN_user_logout{

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$home_url_ = home_url();
		$logout_page_id_ = $settings_["user_logout_page_id"];
		$logout_redirect_url_ = $settings_["user_logout_redirect_url"];

		if(strlen($logout_page_id_) && ((string)$logout_page_id_) === ((string)$current_page_id_)){
			KN_user::signout();
			wp_redirect((strlen($logout_redirect_url_) ? $logout_redirect_url_ : $home_url_));
			exit;
		}
	}
}

add_action("kn_user_prevent_page", array("KN_user_logout", "_prevent_page"));

?>