<?php

class KN_user_myinfo_accounts extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){


		// $sidemenu_->add_sidemenu("accounts", "KN_user_myinfo_accounts", 998);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();
		// wp_register_script("kn-user-myinfo-changeinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN	-user-myinfo-changeinfo.js"), array("kn-user"));

		// wp_localize_script("kn-user-myinfo-changeinfo", "kn_user_ci_msg", array(
			
		// ));

		// wp_enqueue_script("kn-user-myinfo-changeinfo");
	}

	static function draw_sidemenu(){
		$html_ = '계좌관리';
		return $html_;
	}

	static function draw_title(){
		$html_ = '계좌관리';
		return $html_;
	}
	static function draw_desc(){
		$html_ = '내 계좌를 관리합니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';

		//$user_data_ = kn_current_user();
		//$html_ .= '<h4>'.$user_data_["user_name"].'회원님 환영합니다!</h4>';

		return $html_;
	}

	static function draw_content(){
		$user_data_ = kn_current_user();

		ob_start();
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

}

add_action("kn_user_myinfo_sidemenu", array("KN_user_myinfo_accounts","_add_sidemenu"));

?>