<?php

class KN_user_myinfo_addinfo extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("addinfo", "KN_user_myinfo_addinfo", 110);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("kn-listtable");
		kn_load_library("main-all");
		kn_load_library("kn-utils");
		kn_load_library("kn-maps");
		kn_load_library("kn-fileupload");
		kn_load_library("kn-user-children-table");

		wp_enqueue_style("kn-user-myinfo-addinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-myinfo-addinfo.css"));
		wp_register_script("kn-user-myinfo-addinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-myinfo-addinfo.js"), array("kn-user"));

		wp_localize_script("kn-user-myinfo-addinfo", "kn_user_addinfo_var", array(
			"changeinfo_ajax_error" => "저장 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changeinfo_ajax_failed" => "저장에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changeinfo_ajax_success" => "정상적으로 변경되었습니다.",
			"changeinfo_ajax_notvalid" => "암호가 정확하지 않습니다.",
		));

		wp_enqueue_script("kn-user-myinfo-addinfo");
	}

	static function draw_sidemenu(){
		$html_ = '추가정보 변경';
		return $html_;
	}

	static function draw_title(){
		$html_ = '추가정보 변경';
		return $html_;
	}
	static function draw_desc(){
		$html_ = '추가정보를 수정할 수 있습니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';

		//$user_data_ = kn_current_user();
		//$html_ .= '<h4>'.$user_data_["user_name"].'회원님 환영합니다!</h4>';

		return $html_;
	}

	static function draw_content(){
		$user_data_ = kn_current_user();

		ob_start();
?>

<div class="kn-addinfo-frame kn-form">

<form class="kn-form" id="kn-addinfo-form">
<input type="hidden" name="addr_lat" value="<?=$user_data_["addr_lat"]?>">
<input type="hidden" name="addr_lng" value="<?=$user_data_["addr_lng"]?>">
	<table class="kn-form-table">
		<tr>
			<th>닉네임</th>
			<td>
				<input type="text" name="nick_name" placeholder="닉네임 입력" class="full-width" data-accessible="true" value="<?=$user_data_["nick_name"]?>">
			</td>
		</tr>
		<tr>
			<th>거주정보</th>
			<td>
				<input type="text" name="address" placeholder="여기를 클릭하여 주소를 검색하세요" readonly class="full-width" value="<?=$user_data_["address"]?>">
				<input type="text" name="address_dtl" placeholder="상세주소" class="full-width" data-accessible="true" value="<?=$user_data_["address_dtl"]?>">
			</td>
		</tr>
		<tr>
			<th>놀이활동<br/>진행경험</th>
			<td>
				<?= kn_gcode_make_select("U0005",array(
					"name" => "play_exp",
					"default" => $user_data_["play_exp"],
					"class" => "full-width",
					"attrs" => array(
						"data-accessible" => "true"
					)
				)) ?>
			</td>
		</tr>
		<tr>
			<th>나의소개</th>
			<td>
				<textarea name="intro_txt" class="full-width" placeholder="자기소개 입력" data-accessible="true"><?=$user_data_["intro_txt"]?></textarea>
			</td>
		</tr>
	</table>
</form>
<h4>내아이정보</h4>
<?=KN_user::draw_children_table($user_data_["ID"], true);?>
<br/>

<div class="button-area">
	<span><span class="kn-indicator"></span></span>
	<input type="button" class="knbtn knbtn-medium knbtn-primary" value="저장하기" data-accessible="true" id="kn-addinfo-submit-button">
</div>


</div>

<?=KN_maps::draw_address_popup()?>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_change_password(){
		$user_id_ = kn_current_user_id();
		$user_data_ = $_POST["user_data"];

		$result_ = KN_user::verify($user_id_, $user_data_["user_pass"]);

		if($result_ === false){
			echo json_encode(array(
				"success" => $result_,
				"notvalid" => true,
			));
			die();
			return;
		}

		$result_ = KN_user::change_password($user_id_, $user_data_["new_user_pass"]);
		echo json_encode(array(
			"success" => $result_,
		));
		die();
	}

	static function _ajax_user_exists(){
		$user_login_ = $_REQUEST["key"];
		$user_id_ = kn_user_id($user_login_);
		$result_ = (strlen($user_id_) ? false : true);

		if($user_id_ == kn_current_user_id()){
			$result_ = true;			
		}

		echo json_encode($result_);
		die();
	}

	static function _ajax_change_info(){
		$user_id_ = kn_current_user_id();
		$user_data_ = $_POST["user_data"];

		$result_ = KN_user::verify($user_id_, $user_data_["user_pass"]);

		if($result_ === false){
			echo json_encode(array(
				"success" => $result_,
				"notvalid" => true,
			));
			die();
			return;
		}

		$result_ = kn_update_user($user_id_, $user_data_);

		echo json_encode(array(
			"success" => $result_,
		));
		die();
	}

	static function _ajax_update(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$current_user_id_ = kn_current_user_id();
		$addinfo_ = $_POST["addinfo"];
		$children_data_ = isset($_POST["children_data"]) ? $_POST["children_data"] : array();

		$updated_children_data_ = (isset($children_data_["data"]) ? $children_data_["data"] : array());
		$removed_children_data_ = (isset($children_data_["removed_data"]) ? $children_data_["removed_data"] : array());

		// print_r($addinfo_);
		KN_user::update($current_user_id_, $addinfo_);

		foreach($updated_children_data_ as $child_data_){
			$is_new_ = (isset($child_data_["is_new"]) && $child_data_["is_new"] === "Y");

			unset($child_data_["is_new"]);

			if($is_new_){
				KN_user::add_child($current_user_id_, $child_data_);
			}else{
				KN_user::update_child($current_user_id_, $child_data_["seq"], $child_data_);
			}
		}

		foreach($removed_children_data_ as $child_data_){
			KN_user::delete_child($current_user_id_, $child_data_["seq"], $child_data_);
		}

		echo json_encode(array(
			"success" => true
		));
		die();
	}
}

kn_add_ajax("kn-user-myinfo-addinfo-update", array("KN_user_myinfo_addinfo", "_ajax_update"), true);
// kn_add_ajax("kn-user-myinfo-user-exists",array("KN_user_myinfo_addinfo", "_ajax_user_exists"), true);
// kn_add_ajax("kn-user-myinfo-changeinfo", array("KN_user_myinfo_addinfo", "_ajax_change_info"), true);

add_action("kn_user_myinfo_sidemenu", array("KN_user_myinfo_addinfo","_add_sidemenu"));

?>