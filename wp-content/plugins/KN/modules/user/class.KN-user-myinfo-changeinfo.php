<?php

class KN_user_myinfo_changeinfo extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("myinfo", "KN_user_myinfo_changeinfo", 100);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();
		wp_register_script("kn-user-myinfo-changeinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN	-user-myinfo-changeinfo.js"), array("kn-user"));

		wp_localize_script("kn-user-myinfo-changeinfo", "kn_user_ci_msg", array(
			"user_pass_required" => "암호를 입력하여 주세요.",
			"user_pass_minlength" => "암호는 {0}자 이상으로 입력하여 주세요.",
			"user_pass_maxlength" => "암호는 {0}자 이하로 입력하여 주세요.",
			"user_pass_c_equalsto" => "암호가 일치하지 않습니다.",
			"changepassword_ajax_error" => "비밀번호 변경 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changepassword_ajax_failed" => "비밀번호 변경에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changepassword_ajax_success" => "정상적으로 변경되었습니다.",
			"changepassword_ajax_notvalid" => "암호가 정확하지 않습니다.",


			"user_email_required" => "이메일주소를 입력하여 주세요.",
			"user_email_email" => "이메일형식이 아닙니다.",
			"user_email_exists" => "이미 사용하고 있는 이메일입니다.",
			"phone2_required" => "휴대폰 2번째자리를 입력하여 주세요.",
			"phone2_number" => "휴대폰 2번째자리가 숫자가 아닙니다.",
			"phone3_required" => "휴대폰 3번째자리를 입력하여 주세요.",
			"phone3_number" => "휴대폰 3번째자리가 숫자가 아닙니다.",
			"changeinfo_ajax_error" => "정보변경 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changeinfo_ajax_failed" => "정보변경에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"changeinfo_ajax_success" => "정상적으로 변경되었습니다.",
			"changeinfo_ajax_notvalid" => "암호가 정확하지 않습니다.",
		));

		wp_enqueue_script("kn-user-myinfo-changeinfo");
	}

	static function draw_sidemenu(){
		$html_ = '내정보/비밀번호 변경';
		return $html_;
	}

	static function draw_title(){
		$html_ = '내정보/비밀번호 변경';
		return $html_;
	}
	static function draw_desc(){
		$html_ = '내 정보를 수정할 수 있습니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';

		//$user_data_ = kn_current_user();
		//$html_ .= '<h4>'.$user_data_["user_name"].'회원님 환영합니다!</h4>';

		return $html_;
	}

	static function draw_content(){
		$user_data_ = kn_current_user();

		$html_ = '';

		$html_ .= '<div id="kn-user-changeinfo" class="kn-sidemenu-talltop">';
		$html_ .= '<form id="kn-user-changepassword-form" class="kn-form">';
		$html_ .= '<input type="hidden" name="ID" value="'.$user_data_["ID"].'">';
		$html_ .= '<h4>비밀번호변경</h4>';
		$html_ .= '<table class="kn-form-table">';
		$html_ .= '<tr>';
		$html_ .= '<th>아이디</th>';
		$html_ .= '<td>'.$user_data_["user_login"].'</td>';
		$html_ .= '</tr>';
		$html_ .= '<tr>';
		$html_ .= '<th class="required">비밀번호</th>';
		$html_ .= '<td>';
		$html_ .= '<input type="password" name="user_pass" placeholder="비밀번호" class="full-width">';
		$html_ .= '<label id="user_pass-error" class="error" for="user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label>';
		$html_ .= '<p class="hint">*새로운 비밀번호로 변경하려면 기존 비밀번호를 입력하세요.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '<tr>';
		$html_ .= '<th class="required">새 비밀번호</th>';
		$html_ .= '<td><div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l50"><input type="password" name="new_user_pass" placeholder="비밀번호" class="full-width"></div>';
		$html_ .= '<div class="kn-column l50"><input type="password" name="new_user_pass_c" placeholder="비밀번호" class="full-width" data-accessible="true"></div>';
		$html_ .= '</div><label id="new_user_pass-error" class="error" for="new_user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label>';
		$html_ .= '<p class="hint">*비밀번호는 8~25자 사용이 가능합니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '</table>';
		
		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="비밀번호변경" data-accessible="true">';
		$html_ .= '</div>';

		$html_ .= '</form><br/>';

		$html_ .= '<form id="kn-user-changeinfo-form" class="kn-form">';
		$html_ .= '<input type="hidden" name="ID" value="'.$user_data_["ID"].'">';
		$html_ .= '<h4>내정보변경</h4>';
		$html_ .= '<table class="kn-form-table">';
		$html_ .= '<tr>';
		$html_ .= '<th class="required">이메일주소</th>';
		$html_ .= '<td><div><input type="text" name="user_email" placeholder="이메일주소" class="full-width" data-accessible="true" value="'.$user_data_["user_email"].'"></div>';
		$html_ .= '<label for="kn-user-email_rev_yn" class="for-input"><input id="kn-user-email_rev_yn" type="checkbox" name="email_rev_yn" data-accessible="true" '.($user_data_["email_rev_yn"] === "Y" ? "checked" : "").'>이메일로 새로운 소식 및 정보 받기</label>';
		$html_ .= '<label id="user_email-error" class="error" for="user_email" style="display:none;"></label>';
		$html_ .= '<p class="hint">*주요 공지사항 등, 중요한 정보는 수신여부에 관계없이 발송됩니다.</p>';
		$html_ .= '<p class="hint">*비밀번호변경 및 재발급 시 기입한 이메일로 안내문을 발송합니다. 반드시 사용하시는 이메일로 기입하여 주십시오.</p></td>';
		$html_ .= '</tr>';
		$html_ .= '<tr>';
		$html_ .= '<th class="required">휴대폰</th>';
		$html_ .= '<td><div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l33 fix-width">';

		$html_ .= kn_gcode_make_select("Z01",array(
			"name" => "phone1",
			"add_title" => false,
			"default" => $user_data_["phone1"],
			"attrs" => array(
				"data-accessible" => "true"
			)
		));

		$html_ .= '</div>';
		$html_ .= '<div class="kn-column l33 fix-width"><input type="text" name="phone2" class="full-width" data-accessible="true" maxlength="4" value="'.$user_data_["phone2"].'"></div>';
		$html_ .= '<div class="kn-column l33 fix-width"><input type="text" name="phone3" class="full-width" data-accessible="true" maxlength="4" value="'.$user_data_["phone3"].'"></div>';

		$html_ .= '</div>';
		$html_ .= '<label for="kn_user_sms_rev_yn" class="for-input"><input type="checkbox" id="kn_user_sms_rev_yn" name="sms_rev_yn" data-accessible="true" '.($user_data_["sms_rev_yn"] === "Y" ? "checked" : "").'>SMS로 새로운 소식 및 정보 받기</label>';
		$html_ .= '<label id="phone2-error" class="error" for="phone2" style="display:none;"></label>';
		$html_ .= '<label id="phone3-error" class="error" for="phone3" style="display:none;"></label>';
		$html_ .= '<p class="hint">*주요 공지사항 등, 중요한 정보는 수신여부에 관계없이 발송됩니다.</p>';
		
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '<tr>';
		$html_ .= '<th>생일</th>';
		$html_ .= '<td><div class="kn-column-frame kn-column-padding">';
		$html_ .= '<div class="kn-column l50 fix-width"><select name="birth_mm" data-accessible="true">';
		for($mm_ = 1; $mm_ <13; ++$mm_){
			$mm_str_ = str_pad($mm_,2,'0',STR_PAD_LEFT);
			$html_ .= '<option value="'.$mm_str_.'" '.($mm_str_ === $user_data_["birth_mm"] ? "selected" : "").'>'.$mm_.'월</option>';
		}
		$html_ .= '</select></div>';
		$html_ .= '<div class="kn-column l50 fix-width"><select name="birth_dd" data-accessible="true">';
		for($dd_ = 1; $dd_ <32; ++$dd_){
			$dd_str_ = str_pad($dd_,2,'0',STR_PAD_LEFT);
			$html_ .= '<option value="'.$dd_str_.'" '.($dd_str_ === $user_data_["birth_dd"] ? "selected" : "").'>'.$dd_.'일</option>';
		}
		$html_ .= '</select></div>';
		$html_ .= '</div><p class="hint">*생일에 관련된 이벤트 및 쿠폰발급에 참고합니다.</p></td>';
		$html_ .= '</tr>';

		$html_ .= '<tr>';
		$html_ .= '<th class="required">비밀번호</th>';
		$html_ .= '<td>';
		$html_ .= '<input type="password" name="user_pass" placeholder="비밀번호" class="full-width">';
		$html_ .= '<label id="user_pass-error" class="error" for="user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label>';
		$html_ .= '<p class="hint">*회원님의 정보를 변경하려면 기존 비밀번호가 필요합니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';

		$html_ .= '</table>';

		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="내정보변경" data-accessible="true">';
		$html_ .= '</div>';

		$html_ .= '</form>';
		$html_ .= '</div><br/>';

		return $html_;
	}

	static function _ajax_change_password(){
		$user_id_ = kn_current_user_id();
		$user_data_ = $_POST["user_data"];

		$result_ = KN_user::verify($user_id_, $user_data_["user_pass"]);

		if($result_ === false){
			echo json_encode(array(
				"success" => $result_,
				"notvalid" => true,
			));
			die();
			return;
		}

		$result_ = KN_user::change_password($user_id_, $user_data_["new_user_pass"]);
		echo json_encode(array(
			"success" => $result_,
		));
		die();
	}

	static function _ajax_user_exists(){
		$user_login_ = $_REQUEST["key"];
		$user_id_ = kn_user_id($user_login_);
		$result_ = (strlen($user_id_) ? false : true);

		if($user_id_ == kn_current_user_id()){
			$result_ = true;			
		}

		echo json_encode($result_);
		die();
	}

	static function _ajax_change_info(){
		$user_id_ = kn_current_user_id();
		$user_data_ = $_POST["user_data"];

		$result_ = KN_user::verify($user_id_, $user_data_["user_pass"]);

		if($result_ === false){
			echo json_encode(array(
				"success" => $result_,
				"notvalid" => true,
			));
			die();
			return;
		}

		$result_ = kn_update_user($user_id_, $user_data_);

		echo json_encode(array(
			"success" => $result_,
		));
		die();
	}
}

kn_add_ajax("kn-user-myinfo-changepass", array("KN_user_myinfo_changeinfo", "_ajax_change_password"), true);
kn_add_ajax("kn-user-myinfo-user-exists",array("KN_user_myinfo_changeinfo", "_ajax_user_exists"), true);
kn_add_ajax("kn-user-myinfo-changeinfo", array("KN_user_myinfo_changeinfo", "_ajax_change_info"), true);

add_action("kn_user_myinfo_sidemenu", array("KN_user_myinfo_changeinfo","_add_sidemenu"));

?>