<?php

class KN_user_myinfo_leave extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("memberleave", "KN_user_myinfo_leave", 999);
	}

	static function add_content_library(){
		$settings_ = KN_settings::get_settings();
		wp_enqueue_style("kn-user-leave",(KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-myinfo-leave.css"));
		wp_register_script("kn-user-myinfo-leave", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-myinfo-leave.js"), array("kn-user"));

		wp_localize_script("kn-user-myinfo-leave", "kn_user_lv_msg", array(
			"user_pass_required" => "암호를 입력하여 주세요.",
			"term001_agree_checkequals" => "약관에 동의하여 주세요",
			"leave_confirm_message" => "위 모든 사항을 숙지,동의하였으며, 회원탈퇴를 진행합니다.",
			"leave_ajax_error" => "회원탈퇴 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"leave_ajax_failed" => "회원탈퇴에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
			"leave_ajax_success" => "탈퇴되었습니다. 이용해주셔서 감사합니다.",
			"leave_ajax_notvalid" => "암호가 옮바르지 않습니다.",
			"leave_redirect_url" => get_permalink($settings_["user_login_page_id"]),
		));

		wp_enqueue_script("kn-user-myinfo-leave");
	}

	static function draw_sidemenu(){
		$html_ = '회원탈퇴';
		return $html_;
	}

	static function draw_title(){
		$html_ = '회원탈퇴';
		return $html_;
	}
	static function draw_desc(){
		$html_ = get_bloginfo("name").' 회원탈퇴를 위한 화면입니다.';
		return $html_;
	}

	static function draw_main(){
		$html_ = '';



		return $html_;
	}

	static function draw_content(){
		$settings_ = KN_settings::get_settings();
		$user_data_ = kn_current_user();

		$html_ = '';

		$html_ .= '<div id="kn-user-leave" class="kn-sidemenu-talltop">';
		$html_ .= '<form id="kn-user-leave-form" class="kn-form">';
		$html_ .= '<input type="hidden" name="ID" value="'.$user_data_["ID"].'">';
		$html_ .= '<div class="kn-term-table">';
		$html_ .= '<h4 class="term-title">'.get_bloginfo("name").' 서비스 해지에 관한 약관동의</h4>';
		$html_ .= '<div class="term-content kn-form-table kn-leave-term-content">'.stripslashes($settings_["user_leave_term001_content"]).'</div>';
		$html_ .= '<div class="term-agree-box">';
		$html_ .= '<label id="term001_agree-error" class="error" for="term001_agree"></label>';
		$html_ .= '<input type="radio" name="term001_agree" id="kn-user-term001-no" checked="checked" value="N" title="약관에 동의하셔야 합니다."><label for="kn-user-term001-no">동의하지 않습니다.</label>';
		$html_ .= '<input type="radio" name="term001_agree" id="kn-user-term001-yes" value="Y" data-agreement ><label for="kn-user-term001-yes">동의합니다.</label>';
		$html_ .= '</div>';
		$html_ .= '</div>';

		$html_ .= '<h4>비밀번호 입력</h4>';
		$html_ .= '<table class="kn-form-table">';
		$html_ .= '<tr>';
		$html_ .= '<th class="required">비밀번호</th>';
		$html_ .= '<td>';
		$html_ .= '<input type="password" name="user_pass" placeholder="비밀번호" class="full-width">';
		$html_ .= '<label id="user_pass-error" class="error" for="user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label>';
		$html_ .= '<p class="hint">*회원탈퇴 시, 회원확인을 위해 비밀번호가 필요합니다.</p>';
		$html_ .= '</td>';
		$html_ .= '</tr>';
		$html_ .= '</table>';

		$html_ .= '<div class="button-area">';
		$html_ .= '<span><span class="kn-indicator"></span></span>';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="'.get_bloginfo("name").' 서비스 해지 및 회원탈퇴" data-accessible="true">';
		$html_ .= '</div>';

		$html_ .= '</form>';
		$html_ .= '</div><br/>';

		return $html_;
	}

	static function _ajax_do_leave(){
		$user_id_ = kn_current_user_id();
		$user_data_ = $_POST["user_data"];

		if(!KN_user::verify($user_id_, $user_data_["user_pass"])){
			print json_encode(array(
				"notvalid" => true,
			));
			die();return;			
		}

		$result_ = KN_user::leave($user_id_);

		print json_encode(array(
			"success" => ($result_ !== false)
		));
		die();
	}

}

add_action("kn_user_myinfo_sidemenu", array("KN_user_myinfo_leave","_add_sidemenu"));

kn_add_ajax("kn-user-leave-do-leave", array("KN_user_myinfo_leave", "_ajax_do_leave"));

?>