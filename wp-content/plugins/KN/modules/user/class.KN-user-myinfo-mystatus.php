<?php

class KN_user_myinfo_mystatus extends KNSidemenu_template{

	static function _add_sidemenu($sidemenu_){
		$sidemenu_->add_sidemenu("mystatus", "KN_user_myinfo_mystatus", 0);
	}

	static function add_main_library(){
		wp_enqueue_style("kn-user-myinfo-mystatus", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-myinfo-mystatus.css"));
	}

	static function draw_sidemenu(){return null;}
	static function draw_title(){return null;}
	static function draw_desc(){return null;}

	static function draw_main(){
		$settings_ = KN_settings::get_settings();
		global $wpdb;

		$point_count_ = KN_point::point_count(kn_current_user_id());
		
		ob_start();
?>

<div class="kn-form kn-form-table kn-mystatus-frame">
	<div class="knicon knicon-point-large kn-animate kn-animate-tossing"></div>
	<div class="number kn-color kn-color-point"><?=kn_pretty_number($point_count_)?></div>
	<!-- <div class="button-area">
		<a href="javascript" class="kn-alink">킨체리내역</a>
	</div> -->
</div>


<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function draw_content(){return null;}
}

add_action("kn_user_myinfo_sidemenu", array("KN_user_myinfo_mystatus","_add_sidemenu"));

?>