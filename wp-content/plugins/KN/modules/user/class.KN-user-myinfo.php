<?php

class KN_user_myinfo_sidemenu extends KNSidemenu{

	function before_sidemenu(){
		$current_user_ = kn_current_user();
		$is_pro_ = KN_user::is_pro($current_user_["ID"]);

		$settings_ = KN_settings::get_settings();
		ob_start();
?>
<div class="kn-picture-frame">
	<input type="file" name="files[]" id="kn-profile-file" accept=".jpg,.jpeg,.gif,.png,.bmp" data-accessible="true" data-default-url="<?=$current_user_["profile_url"]?>">
	<p class='user-name'><b><?=$current_user_["display_name"]?></b><?=$is_pro_ ? "<span class='pro'>전문가</span>" : ""?></p>
</div>

<?php
		$html_ = ob_get_contents();
		ob_end_clean();
		return $html_;
	}
}

class KN_user_myinfo extends KNShortcode{

	static function sidemenu_instance(){
		global $kn_user_myinfo_sidemenu;
		if(!isset($kn_user_myinfo_sidemenu)){
			$settings_ = KN_settings::get_settings();
			$base_url = get_permalink($settings_["user_myinfo_page_id"]);
			$current_user_ = kn_current_user();

			$site_name_ = get_bloginfo("name");

			$kn_user_myinfo_sidemenu = new KN_user_myinfo_sidemenu(array(
				"id" => "kn-user-myinfo",
				"class" => "kn-user-myinfo",
				"base_url" => $base_url,
				"title" => $site_name_,
				"side_title" => "내 ".$site_name_,
				"desc" => "회원님의 $site_name_ 정보를 확인할 수 있습니다.",
			));

			do_action("kn_user_myinfo_sidemenu",$kn_user_myinfo_sidemenu);
		}
		return $kn_user_myinfo_sidemenu;	
	}

	static function shortcode_name(){
		return "kn-user-myinfo";
	}

	static function add_library(){
		kn_load_library("main");
		self::sidemenu_instance()->add_library();

		kn_load_library("kn-fileupload");
		
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));
		wp_enqueue_script("kn-user-myinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-myinfo.js"), array("jquery-validate"));
		
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));
		wp_enqueue_style("kn-user-myinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-myinfo.css"));
	}

	static function _do_shortcode($attrs_){
		$attrs_ = shortcode_atts(array(),$attrs_);
		$settings_ = KN_settings::get_settings();
		return self::sidemenu_instance()->html();
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();

		$login_page_id_ = $settings_["user_login_page_id"]; 
		$myinfo_page_id_ = $settings_["user_myinfo_page_id"]; 

		if(strlen($myinfo_page_id_) && ((string)$current_page_id_) === ((string)$myinfo_page_id_) ){
			if(!kn_is_user_logged_in()){
				$login_url_ = kn_make_url(get_permalink($login_page_id_), array("redirect" => urlencode(get_permalink($myinfo_page_id_))));
				wp_redirect($login_url_);
				exit();
			}

			if($settings_["user_myinfo_need_ssl"] === true && !kn_is_https()){
				wp_redirect(kn_make_https());
				exit;
			}
		}
	}

	static function _ajax_change_profile(){
		if(!kn_is_user_logged_in()){
			echo json_encode(false);
			die();
		}

		$img_url_ = isset($_POST["img_url"]) ? $_POST["img_url"] : null;

		KN_user::update(kn_current_user_id(), array(
			"profile_url" => (strlen($img_url_) ? $img_url_ : KN_QUERY_NULL),
		));
		echo json_encode(array(
			"success" => true
		));
		die();
	}
}

add_shortcode((KN_user_myinfo::shortcode_name()), array("KN_user_myinfo","_do_shortcode"));
add_action("kn_shortcode_filter", array("KN_user_myinfo","filter"));
add_action("kn_user_prevent_page",array("KN_user_myinfo","_prevent_page"));
kn_add_ajax("kn-user-myinfo-change-profile", array("KN_user_myinfo", "_ajax_change_profile"), true);

include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo-changeinfo.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo-addinfo.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo-accounts.php');

?>