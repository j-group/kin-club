<?php

class KN_user_pro_apply_p01{

	static function add_library(){
		wp_register_script("kn-user-pro-apply-phase01", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply-phase01.js"), array("kn-user"));

		wp_localize_script("kn-user-pro-apply-phase01", "kn_user_pro_apply_agree_var",array(
			"formval_user_agree_required" => "약관에 동의하셔야 신청이 가능합니다.",
		));

		wp_enqueue_script("kn-user-pro-apply-phase01");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		
		$site_name_ = get_bloginfo("name");
		$term_title_ =  $settings_["user_pro_apply_term_title"];
		$term_content_ =  $settings_["user_pro_apply_term_content"];

		ob_start();

?>
		<form id="kn-pro-apply-form" action="<?=get_permalink($settings_["user_pro_apply_page_id"])?>" method="POST">

		<!-- Step 2 -->
		<?php
		$qna04_ = KN_POST("qna04", array());
		foreach($qna04_ as $selected_){ ?>
			<input type="hidden" name="qna04[]" value="<?=$selected_?>">
		<?php } ?>
		<input type="hidden" name="qna05" value="<?=KN_POST("qna05")?>">
		<input type="hidden" name="qna06_01" value="<?=KN_POST("qna06_01")?>">
		<input type="hidden" name="qna06_02" value="<?=KN_POST("qna06_02")?>">
		<input type="hidden" name="qna07_01" value="<?=KN_POST("qna07_01")?>">
		<input type="hidden" name="qna07_02" value="<?=KN_POST("qna07_02")?>">
		<input type="hidden" name="qna07_03" value="<?=KN_POST("qna07_03")?>">
		
		<!-- Step 3 -->
		<input type="hidden" name="qna08" value="<?=KN_POST("qna08")?>">
		<input type="hidden" name="qna09" value="<?=KN_POST("qna09")?>">
		<input type="hidden" name="qna10" value="<?=KN_POST("qna10")?>">
		<input type="hidden" name="qna11" value="<?=KN_POST("qna11")?>">
		<?php
		$qna12_ = KN_POST("qna12", array());
		foreach($qna12_ as $selected_){ ?>
			<input type="hidden" name="qna12[]" value="<?=$selected_?>">
		<?php } ?>
		<input type="hidden" name="qna13" value="<?=KN_POST("qna13")?>">
		<?php
		$qna14_ = KN_POST("qna14", array());
		foreach($qna14_ as $selected_){ ?>
			<input type="hidden" name="qna14[]" value="<?=$selected_?>">
		<?php } ?>
		

		<input type="hidden" name="phase" value="1">
		<div class="kn-term-table kn-form">
		<p class="desc">원활한 서비스 이용을 위해 아래 약관사항에 동의하셔야만 전문가신청이 가능합니다.</p>

		<h3 class="required"><?=$site_name_?>을 알게된 경로</h3>
		<div class="wrap">
			<?php
				$qna01_list_ = KN_gcode::gcode_dtl_list("U0007");
				foreach($qna01_list_ as $qna01_){ ?>

					<label><input type="radio" name="qna01" value="<?=$qna01_["code_did"]?>" <?=checked(KN_POST("qna01"), $qna01_["code_did"])?> ><?=KN_user_pro_apply::convert_var_parameter($qna01_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna01-error" class="error" for="qna01" style="display:none;"></label>
		</div>

		<h3 class="required">왜 놀이활동 전문가가 되고 싶으신가요?</h3>
		<div class="wrap">
			<?php
				$qna02_list_ = KN_gcode::gcode_dtl_list("U0009");
				$qna02_default_ = KN_POST("qna02", array());

				foreach($qna02_list_ as $qna02_){
					$selected_ = (array_search($qna02_["code_did"],$qna02_default_) !== false);
				?>
					<label><input type="checkbox" name="qna02[]" value="<?=$qna02_["code_did"]?>" <?=($selected_ ? "checked" : "")?> ><?=KN_user_pro_apply::convert_var_parameter($qna02_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna02[]-error" class="error" for="qna02[]" style="display:none;"></label>
		</div>

		<h3 class="required">본인의 교육관은?</h3>
		<div class="wrap">
			<textarea class="full-width" name="qna03"><?=KN_POST("qna03")?></textarea>
			<p class="hint">*아이를 키우시고 싶은 방향과, 현재 육아방식에 대해 적어주세요. 의뢰인의 참여율을 높일 수 있습니다.</p>
		</div>

		<h3 class="term-title required"><?=stripslashes($term_title_)?></h3>
		<div class="term-content kn-form-table"><?=stripslashes($term_content_)?></div>
		
		<div class="term-agree-box">
		<label id="term_agree-error" class="error" for="term_agree"></label>
		<input type="radio" name="term_agree" id="kn-user-pro-apply-term-no" <?=checked(KN_POST("term_agree"), "N")?> value="N" title="<?=$term_title_?> 항목에 동의하셔야 합니다."><label for="kn-user-pro-apply-term-no">동의하지 않습니다.</label>
		<input type="radio" name="term_agree" id="kn-user-pro-apply-term-yes" <?=checked(KN_POST("term_agree"), "Y")?> value="Y" data-agreement ><label for="kn-user-pro-apply-term-yes">동의합니다.</label>
		</div>

		<div class="term-agree-button button-area">
		 <a class="knbtn knbtn-medium knbtn-secondary" href="<?=home_url()?>">취소</a>
		 <input type="submit" class="knbtn knbtn-medium knbtn-primary" value="다음단계">
		</div>
		
		</div></form>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>