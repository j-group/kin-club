<?php

class KN_user_pro_apply_p02{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		wp_enqueue_script("kn-user-pro-apply", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply.js"), array("kn-user"));

		wp_register_script("kn-user-pro-apply-phase02", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply-phase02.js"), array("kn-user"));
		wp_localize_script("kn-user-pro-apply-phase02", "kn_user_pro_apply_var",array(
			"formval_user_agree_required" => "약관에 동의하셔야 신청이 가능합니다.",
		));
		wp_enqueue_script("kn-user-pro-apply-phase02");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		
		$site_name_ = get_bloginfo("name");

		ob_start();
?>
		<form id="kn-pro-apply-form" action="<?=get_permalink($settings_["user_pro_apply_page_id"])?>" method="POST" class="kn-form">
		<input type="hidden" name="phase" value="2">

		<!-- Step 1 -->
		<input type="hidden" name="qna01" value="<?=KN_POST("qna01")?>">
		<?php
		$qna02_ = KN_POST("qna02", array());
		foreach($qna02_ as $selected_){ ?>
			<input type="hidden" name="qna02[]" value="<?=$selected_?>">
		<?php } ?>
	
		<input type="hidden" name="qna03" value="<?=KN_POST("qna03")?>">
		<input type="hidden" name="term_agree" value="<?=KN_POST("term_agree")?>">
		
		<!-- Step 3 -->
		<input type="hidden" name="qna08" value="<?=KN_POST("qna08")?>">
		<input type="hidden" name="qna09" value="<?=KN_POST("qna09")?>">
		<input type="hidden" name="qna10" value="<?=KN_POST("qna10")?>">
		<input type="hidden" name="qna11" value="<?=KN_POST("qna11")?>">
		<?php
		$qna12_ = KN_POST("qna12", array());
		foreach($qna12_ as $selected_){ ?>
			<input type="hidden" name="qna12[]" value="<?=$selected_?>">
		<?php } ?>
		<input type="hidden" name="qna13" value="<?=KN_POST("qna13")?>">
		<?php
		$qna14_ = KN_POST("qna14", array());
		foreach($qna14_ as $selected_){ ?>
			<input type="hidden" name="qna14[]" value="<?=$selected_?>">
		<?php } ?>
		
		
		<div>
		<p class="desc"><?=$site_name_?> 이용을 위한 몇가지 정보를 입력하여 회원가입을 완료합니다.</p>

		<h3 class="required">어떠한 분야에 놀이활동 전문가가 되고 싶으세요?</h3>
		<div class="wrap">
			<?php
				$qna04_list_ = KN_gcode::gcode_dtl_list("U0011");
				$qna04_default_ = KN_POST("qna04", array());

				foreach($qna04_list_ as $qna04_){
					$selected_ = (array_search($qna04_["code_did"],$qna04_default_) !== false);
				?>
					<label><input type="checkbox" name="qna04[]" value="<?=$qna04_["code_did"]?>" <?=($selected_ ? "checked" : "")?> ><?=KN_user_pro_apply::convert_var_parameter($qna04_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna04[]-error" class="error" for="qna04[]" style="display:none;"></label>
		</div>

		<h3 class="required">놀이활동 진행 경험이 얼마나 있으십니까?</h3>
		<div class="wrap">
			<?php
				$qna05_list_ = KN_gcode::gcode_dtl_list("U0013");
				foreach($qna05_list_ as $qna05_){ ?>
					<label><input type="radio" name="qna05" value="<?=$qna05_["code_did"]?>" <?=checked(KN_POST("qna05"), $qna05_["code_did"])?> ><?=KN_user_pro_apply::convert_var_parameter($qna05_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna05-error" class="error" for="qna05" style="display:none;"></label>
		</div>

		<h3 class="required">놀이활동 전문가 활동이 가능한 시간은?</h3>
		<div class="wrap">
			<input 
					type="text" name="qna06_01" class="kn-input-inset l30-l l40-s first-child" value="<?=KN_POST("qna06_01")?>" data-time-format><span 
					type="button" class="kn-input-inset l10-l l20-s kn-input-inset-button" readonly>~</span><input 
					type="text" name="qna06_02" class="kn-input-inset l30-l l40-s last-child" value="<?=KN_POST("qna06_02")?>" data-time-format>

			<label id="qna06_01-error" class="error" for="qna06_01" style="display:none;"></label>
			<label id="qna06_02-error" class="error" for="qna06_02" style="display:none;"></label>
		</div>

		<h3>내 놀이활동에 참여가능한 아이의 연령대는?</h3>
		<div class="wrap">
			<input 
					type="text" name="qna07_01" data-accessible="true" class="kn-input-inset l20-l l20-s first-child" value="<?=KN_POST("qna07_01")?>"><span 
					type="button" class="kn-input-inset l10-l l15-s kn-input-inset-button" readonly>~</span><input 
					type="text" name="qna07_02" data-accessible="true" class="kn-input-inset l20-l l20-s" value="<?=KN_POST("qna07_02")?>"><select name="qna07_03" class="kn-input-inset l30-l l33-s last-child">
					<option value="">-개월선택-</option>
					<?php
					for($index_=0;$index_<12; ++$index_){
						echo "<option value='".($index_+1)."'>".($index_+1).개월."</option>";
					}
					?>
				</select>
			<label id="qna07_01-error" class="error" for="qna07_01" style="display:none;"></label>
			<label id="qna07_02-error" class="error" for="qna07_02" style="display:none;"></label>
			<p class="hint">*예) 만2~만5세 등</p>
		</div>

		<div class="button-area">
		<span><span class="kn-indicator"></span></span>
		<input type="button" class="knbtn knbtn-medium knbtn-secondary" value="전단계로" data-accessible="true" onclick="kn_user_pro_pre_step(0);">
		<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="다음단계로" data-accessible="true">
		</div>

		</div>

		</form>
<?php
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>