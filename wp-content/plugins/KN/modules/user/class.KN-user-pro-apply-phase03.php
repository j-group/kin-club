<?php

class KN_user_pro_apply_p03{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("main-all");
		kn_load_library("kn-utils");
		kn_load_library("kn-fileupload");

		wp_enqueue_script("kn-user-pro-apply", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply.js"), array("kn-user"));

		wp_register_script("kn-user-pro-apply-phase03", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply-phase03.js"), array("kn-user"));
		wp_localize_script("kn-user-pro-apply-phase03", "kn_user_pro_apply_var",array(
			"formval_user_agree_required" => "약관에 동의하셔야 신청이 가능합니다.",
		));
		wp_enqueue_script("kn-user-pro-apply-phase03");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		
		$site_name_ = get_bloginfo("name");

		ob_start();
?>
		<form id="kn-pro-apply-form" action="<?=get_permalink($settings_["user_pro_apply_page_id"])?>" method="POST" class="kn-form">
		<input type="hidden" name="phase" value="3">

		<!-- Step 1 -->
		<input type="hidden" name="qna01" value="<?=KN_POST("qna01")?>">
		<?php
		$qna02_ = KN_POST("qna02", array());
		foreach($qna02_ as $selected_){ ?>
			<input type="hidden" name="qna02[]" value="<?=$selected_?>">
		<?php } ?>
		<input type="hidden" name="qna03" value="<?=KN_POST("qna03")?>">
		<input type="hidden" name="term_agree" value="<?=KN_POST("term_agree")?>">

		<!-- Step 2 -->
		<?php
		$qna04_ = KN_POST("qna04", array());
		foreach($qna04_ as $selected_){ ?>
			<input type="hidden" name="qna04[]" value="<?=$selected_?>">
		<?php } ?>
		<input type="hidden" name="qna05" value="<?=KN_POST("qna05")?>">
		<input type="hidden" name="qna06_01" value="<?=KN_POST("qna06_01")?>">
		<input type="hidden" name="qna06_02" value="<?=KN_POST("qna06_02")?>">
		<input type="hidden" name="qna07_01" value="<?=KN_POST("qna07_01")?>">
		<input type="hidden" name="qna07_02" value="<?=KN_POST("qna07_02")?>">
		<input type="hidden" name="qna07_03" value="<?=KN_POST("qna07_03")?>">
		<input type="hidden" name="qna07" value="<?=KN_POST("qna07")?>">
		
		<div>
		<p class="desc"><?=$site_name_?> 이용을 위한 몇가지 정보를 입력하여 전문가신청을 완료합니다.</p>

		<h3 class="required">놀이활동 전문가 활동 형태를 선택해 주세요.</h3>
		<div class="wrap">
			<?php
				$qna08_list_ = KN_gcode::gcode_dtl_list("U0015");
				foreach($qna08_list_ as $qna08_){ ?>
					<label><input type="radio" name="qna08" value="<?=$qna08_["code_did"]?>" <?=checked(KN_POST("qna08"), $qna08_["code_did"])?> ><?=KN_user_pro_apply::convert_var_parameter($qna08_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna08-error" class="error" for="qna08" style="display:none;">답변을 선택하세요</label>
		</div>

		<div class="qna08-options option-00001 option-00009" style="display:none;">
		<h3 class="required"><?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</h3>
		<div class="wrap">
			<textarea class="full-width" name="qna09" ><?=KN_POST("qna09")?></textarea>
			<p class="hint">*놀이활동 종류, 소요시간, 참여가능 인원 및 연령, 장소, 기타 <?=$site_name_?>에서 도움 받고 싶은 사항 등</p>
		</div>
		</div>

		<div class="qna08-options option-00003" style="display:none;">
		<h3 class="required">현재 운영하시는 사업과 <?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</h3>
		<div class="wrap">
			<textarea class="full-width" name="qna10"><?=KN_POST("qna10")?></textarea>
			<p class="hint">*사업장 이름, 주소, 홈페이지(SNS, 까페) 등</p>
		</div>
		</div>

		<div class="qna08-options option-00005" style="display:none;">
		<h3 class="required">현재 활동하시는 분야와 <?=$site_name_?>을 통해 진행 가능한 놀이활동을 자세히 소개해 주세요.</h3>
		<div class="wrap">
			<textarea class="full-width" name="qna11"><?=KN_POST("qna11")?></textarea>
			<p class="hint">*과거 진행 수업, 참여 연령대, 지역, 기관 등</p>
		</div>
		</div>

		<h3>나와 나의 재능을 보여줄 수 있는 링크가 있을경우 입력해주세요.</h3>
		<div class="wrap">
			<input type="text" name="qna13" value="<?=KN_POST("qna13")?>" >
			<p class="hint">*SNS(페이스북, 트위터, 유투브, 블로그 등) 또는 홈페이지</p>
		</div>

		<h3>놀이활동 진행 장소를 선택해 주세요</h3>
		<div class="wrap">
			<?php
				$qna14_list_ = KN_gcode::gcode_dtl_list("U0017");
				$qna14_default_ = KN_POST("qna14", array());

				foreach($qna14_list_ as $qna14_){
					$selected_ = (array_search($qna14_["code_did"],$qna14_default_) !== false);
				?>
					<label><input type="checkbox" name="qna14[]" value="<?=$qna14_["code_did"]?>" <?=($selected_ ? "checked" : "")?> ><?=KN_user_pro_apply::convert_var_parameter($qna14_["code_dnm"])?></label>
			<?php } ?>
			<label id="qna14[]-error" class="error" for="qna14[]" style="display:none;">답변을 선택하세요</label>
		</div>

		</div>

		</form>

		<h3>놀이활동을 진행한 사진이 있을경우 업로드 하여 주세요.</h3>
		<div class="wrap kn-form">
			<div id="kn-main-image-uploader">
			<!-- 기존이미지 설정 -->
			<?php

			$qna12_ = KN_POST("qna12", array());
			foreach($qna12_ as $img_url_){
				echo "<img style='display:none;' src='".$img_url_."'>";
			}
			
			?>
			</div>
			<p class="hint">*최대 5장</p>
		</div>

		<div class="kn-form"><div class="button-area kn-form" id="kn-pro-apply-button">
		<span><span class="kn-indicator"></span></span>
		<input type="button" class="knbtn knbtn-medium knbtn-secondary" value="전단계로" data-accessible="true" onclick="kn_user_pro_pre_step(1);">
		<input type="button" class="knbtn knbtn-medium knbtn-primary" value="신청하기" data-accessible="true" onclick="kn_user_pro_phase03_submit();">
		</div></div>

		<form id="kn-apply-form-complete" style="display:none;" action="<?=get_permalink($settings_["user_pro_apply_page_id"])?>" method="POST">
		<input type="hidden" name="phase" value="3">
		</form>
<?php
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>