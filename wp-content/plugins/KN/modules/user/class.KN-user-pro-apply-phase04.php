<?php

class KN_user_pro_apply_p04{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		kn_load_library("main-all");

		wp_enqueue_script("kn-user-pro-apply", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply.js"), array("kn-user"));

		wp_register_script("kn-user-pro-apply-phase04", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply-phase04.js"), array("kn-user"));
		wp_localize_script("kn-user-pro-apply-phase04", "kn_user_pro_apply_var",array(
			"formval_user_agree_required" => "약관에 동의하셔야 신청이 가능합니다.",
		));
		wp_enqueue_script("kn-user-pro-apply-phase03");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		$site_name_ = get_bloginfo("name");

		$did_phase_ = strlen(KN_POST("phase",null));
		$user_id_ = kn_current_user_id();
		$is_pro_ = KN_user::is_pro($user_id_);

		ob_start();
?>

<div class="kn-user-pro-apply-complete kn-form-table">

<?php if($is_pro_){ ?>

<h3 class="title">전문가승인완료</h3>
<p class="desc"><?=$site_name_?>의 전문가로 정상등록 되었습니다.</p>

<?php }else{ ?>

<h3 class="title"><?=($did_phase_ ? "전문가신청완료!" : "전문가승인대기중입니다")?></h3>
<p class="desc"><?=$site_name_?>에서 승인절차를 걸쳐 전문가로 등록될 것입니다.</p>

<?php } ?>

</div>

<?php
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>