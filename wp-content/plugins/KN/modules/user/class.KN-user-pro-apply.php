<?php

class KN_user_pro_apply extends KNShortcode{

	static function shortcode_name(){
		return "kn-user-pro-apply";
	}

	static function _current_class(){
		$phase_ = (int)KN_POST("phase", 0);
		
		$draw_form_class_ = null;
		$user_id_ = kn_current_user_id();
		$is_pro_ = KN_user::is_pro($user_id_);
		$did_apply_ = KN_user::did_apply_pro($user_id_);

		if(!strlen(KN_POST("phase",null)) && ($did_apply_ || $is_pro_)){
			$draw_form_class_ = "KN_user_pro_apply_p04";
		}else if($phase_ == 0){
			$draw_form_class_ = "KN_user_pro_apply_p01";
		}else if($phase_ == 1){
			$draw_form_class_ = "KN_user_pro_apply_p02";
		}else if($phase_ == 2){
			$draw_form_class_ = "KN_user_pro_apply_p03";
		}else if($phase_ == 3){
			$draw_form_class_ = "KN_user_pro_apply_p04";
		}
		
		return $draw_form_class_;
	}

	static function add_library(){
		kn_load_library("main");
		kn_load_library("timepicker");
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));

		wp_enqueue_script("kn-user-pro-apply", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-pro-apply.js"), array("jquery-validate"));
		wp_enqueue_style("kn-user-pro-apply", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-pro-apply.css"));
			
		call_user_func(array(self::_current_class(),"add_library"));
	}

	static function convert_var_parameter($str_){
		$parameter_ = array(
			"site_name" => get_bloginfo("name"),
		);

		foreach($parameter_ as $key_ => $value_){
			$str_ = str_replace("{".$key_."}", $value_, $str_);
		}

		return $str_;
	}


	static function _do_shortcode($attrs_){
		$attrs_ = shortcode_atts(array(),$attrs_);
		$settings_ = KN_settings::get_settings();

		$back_img_url_ = $settings_["user_pro_apply_page_back_img_url"];			
		
		$draw_form_class_ = self::_current_class();

		$html_ = "<div class='kn-user kn-user-pro-apply kn-user-register' style='background-image:url(".$back_img_url_.");'><div class='wrap'><div class='register-float'>";

		if($draw_form_class_ !== "KN_user_pro_apply_p04"
			|| strlen(KN_POST("phase",null))){

			$html_ .= '<ol class="top-nav">';
			$html_ .=	'<li '.($draw_form_class_ === "KN_user_pro_apply_p01" ? "class='current-phase'" : "").'>신청1단계<span class="knicon kn-arrow-r"></span></li>';
			$html_ .=	'<li '.($draw_form_class_ === "KN_user_pro_apply_p02" ? "class='current-phase'" : "").'>신청2단계<span class="knicon kn-arrow-r"></span></li>';
			$html_ .=	'<li '.($draw_form_class_ === "KN_user_pro_apply_p03" ? "class='current-phase'" : "").'>신청3단계<span class="knicon kn-arrow-r"></span></li>';
			$html_ .=	'<li '.($draw_form_class_ === "KN_user_pro_apply_p04" ? "class='current-phase'" : "").'>신청완료</li>';
			$html_ .= '</ol>';
		}

		$html_ .= call_user_func_array(array($draw_form_class_,"draw_form"),array($attrs_));

		$html_ .= "</div></div></div>";

		return do_shortcode($html_);
	}

	static function _prevent_page($current_page_id_){
		if(!kn_is_user_logged_in()){
			$settings_ = KN_settings::get_settings();
			$pro_apply_page_id_ = (string)$settings_["user_pro_apply_page_id"];

			if(strlen($pro_apply_page_id_) && $pro_apply_page_id_ === $current_page_id_){
				$login_url_ = kn_make_url(get_permalink($settings_["user_login_page_id"]), array("redirect" => urlencode(get_permalink())));
				wp_redirect($login_url_);
				exit();
			}
		}
	}
	static function _ajax_apply(){
		if(!kn_is_user_logged_in()){
			echo json_encode(array(
				"reject" => true
			));
			die();
		}

		$apply_data_ = KN_POST("apply_data", array());

		$result_ = KN_user::insert_pro(array_merge($apply_data_, array(
			"ID" => kn_current_user_id(),
			"status" => "00001",
		)));

		echo json_encode(array(
			"success" => ($result_ !== false),
		));
		die();
	}
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-pro-apply-phase01.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-pro-apply-phase02.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-pro-apply-phase03.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-pro-apply-phase04.php');

add_shortcode((KN_user_pro_apply::shortcode_name()), array("KN_user_pro_apply","_do_shortcode"));

add_action("kn_shortcode_filter",array("KN_user_pro_apply","filter"));
add_action("kn_user_prevent_page",array("KN_user_pro_apply", "_prevent_page"));

kn_add_ajax("kn-user-pro-apply", array("KN_user_pro_apply" , "_ajax_apply"), true);

?>