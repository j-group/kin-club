<?php

class KN_user_register_p01{

	static function add_library(){
		wp_register_script("kn-user-agree", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-agree.js"), array("kn-user"));

		wp_localize_script("kn-user-agree", "kn_user_agree_msg"
		,array(
			"formval_user_agree_required" => "모든 약관에 동의하셔야 회원가입이 가능합니다.",
		));

		wp_enqueue_script("kn-user-agree");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		
		$term001_title_ =  $settings_["user_term001_title"];
		$term001_content_ =  $settings_["user_term001_content"];
		$term002_title_ =  $settings_["user_term002_title"];
		$term002_content_ =  $settings_["user_term002_content"];
		$term003_title_ =  $settings_["user_term003_title"];
		$term003_content_ =  $settings_["user_term003_content"];
		$term004_title_ =  $settings_["user_term004_title"];
		$term004_content_ =  $settings_["user_term004_content"];

		$html_ = '<form id="kn-register-agree-form" action="'.get_permalink($settings_["user_register_page_id"]).'" method="POST">';
		$html_ .= '<input type="hidden" name="did_agree" value="Y">';
		$html_ .= '<div class="kn-term-table kn-form">';
		$html_ .= '<p class="desc">원활한 서비스 이용을 위해 아래 약관사항에 동의하셔야만 회원가입이 가능합니다.</p>';
		
		if(strlen($term001_title_)){
			$html_ .= '<h3 class="term-title">'.stripslashes($term001_title_).'</h3>';
			$html_ .= '<div class="term-content kn-form-table">'.stripslashes($term001_content_).'</div>';
			
			$html_ .= '<div class="term-agree-box">';
			$html_ .= '<label id="term001_agree-error" class="error" for="term001_agree"></label>';
			$html_ .= '<input type="radio" name="term001_agree" id="kn-user-term001-no" checked="checked" value="N" title="'.$term001_title_.' 항목에 동의하셔야 합니다."><label for="kn-user-term001-no">동의하지 않습니다.</label>';
			$html_ .= '<input type="radio" name="term001_agree" id="kn-user-term001-yes" value="Y" data-agreement ><label for="kn-user-term001-yes">동의합니다.</label>';
			$html_ .= '</div>';
			
		}
		if(strlen($term002_title_)){
			$html_ .= '<h3 class="term-title">'.stripslashes($term002_title_).'</h3>';
			$html_ .= '<div class="term-content kn-form-table">'.stripslashes($term002_content_).'</div>';

			$html_ .= '<div class="term-agree-box">';
			$html_ .= '<label id="term002_agree-error" class="error" for="term002_agree"></label>';
			$html_ .= '<input type="radio" name="term002_agree" id="kn-user-term002-no" checked="checked" value="N" title="'.$term002_title_.' 항목에 동의하셔야 합니다."><label for="kn-user-term002-no">동의하지 않습니다.</label>';
			$html_ .= '<input type="radio" name="term002_agree" id="kn-user-term002-yes" value="Y" data-agreement><label for="kn-user-term002-yes">동의합니다.</label>';
			$html_ .= '</div>';
		}
		if(strlen($term003_title_)){
			$html_ .= '<h3 class="term-title">'.stripslashes($term003_title_).'</h3>';
			$html_ .= '<div class="term-content kn-form-table">'.stripslashes($term003_content_).'</div>';

			$html_ .= '<div class="term-agree-box">';
			$html_ .= '<label id="term003_agree-error" class="error" for="term003_agree"></label>';
			$html_ .= '<input type="radio" name="term003_agree" id="kn-user-term003-no" checked="checked" value="N" title="'.$term003_title_.' 항목에 동의하셔야 합니다."><label for="kn-user-term003-no">동의하지 않습니다.</label>';
			$html_ .= '<input type="radio" name="term003_agree" id="kn-user-term003-yes" value="Y" data-agreement><label for="kn-user-term003-yes">동의합니다.</label>';
			$html_ .= '</div>';
		}
		if(strlen($term004_title_)){
			$html_ .= '<h3 class="term-title">'.stripslashes($term004_title_).'</h3>';
			$html_ .= '<div class="term-content kn-form-table">'.stripslashes($term004_content_).'</div>';

			$html_ .= '<div class="term-agree-box">';
			$html_ .= '<label id="term004_agree-error" class="error" for="term004_agree"></label>';
			$html_ .= '<input type="radio" name="term004_agree" id="kn-user-term004-no" checked="checked" value="N" title="'.$term004_title_.' 항목에 동의하셔야 합니다."><label for="kn-user-term004-no">동의하지 않습니다.</label>';
			$html_ .= '<input type="radio" name="term004_agree" id="kn-user-term004-yes" value="Y" data-agreement><label for="kn-user-term004-yes">동의합니다.</label>';
			$html_ .= '</div>';
		}
		$html_ .= '<div class="term-agree-button button-area">';
		$html_ .= '<label for="kn-term-all-agree" class="for-input"><input type="checkbox" id="kn-term-all-agree" data-accessible="true">위에 모든 약관에 동의합니다.</label><br/>';
		$html_ .= '</div>';

		$html_ .= '<div class="term-agree-button button-area">';
		$html_ .= '<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="동의">';
		$html_ .= ' <a class="knbtn knbtn-medium knbtn-secondary" href="'.get_permalink($settings_["user_login_page_id"]).'">취소</a>';
		$html_ .= '</div>';
		
		$html_ .= '</div>';

		$html_ .= '</form>';

		return $html_;
	}
}

?>