<?php

class KN_user_register_p02{

	static function add_library(){
		kn_load_library("kn-siren24");
		wp_register_script("kn-user-auth", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-auth.js"), array("kn-user"));

		wp_localize_script("kn-user-auth", "kn_user_register_msg"
		,array(
			"auth failed" => "본인인증에 실패하였습니다.",
			"popup blocked" => "본인인증 팝업창이 차단되었습니다. 차단해제 후 다시 시도해주세요.",
		));
		wp_enqueue_script("kn-user-auth");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();

		$auth_req_data_ = KN_siren24::enc_request_info("001006");
			
		ob_start();
?>
		<form id="kn-user-certify-form" class="kn-form" action="<?=get_permalink($settings_["user_register_page_id"])?>" method="POST">
		<input type="hidden" name="did_agree" value="Y">
		<input type="hidden" name="did_certify" value="Y">

		<input type="hidden" name="auth_name" value="" data-auth-column="name">
		<input type="hidden" name="auth_sex" value="" data-auth-column="sex">
		<input type="hidden" name="auth_cell_num" value="" data-auth-column="cell_num">
		<input type="hidden" name="auth_cell_corp" value="" data-auth-column="cell_corp">
		<input type="hidden" name="auth_birth_ymd" value="" data-auth-column="birth_ymd">
		<input type="hidden" name="auth_di" value="" data-auth-column="di">
		<input type="hidden" name="auth_ci1" value="" data-auth-column="ci1">
		<input type="hidden" name="auth_ci2" value="" data-auth-column="ci2">
		<input type="hidden" name="auth_ci_ver" value="" data-auth-column="ci_ver">

		<p class="desc">회원가입을 통한 명의도용방지 및 예방을 위해 본인인증을 진행합니다.</p>

		<table class="kn-form-table">
		<tr>
		<th>인증방식선택</th>
		<td>
		<input type="button" value="핸드폰" class="knbtn knbtn-medium knbtn-primary" onclick="kn_user_do_auth('H');" data-accessible="true">
		<!-- <input type="button" value="아이핀" class="knbtn knbtn-medium knbtn-primary"> -->
		<span class="kn-indicator"></span>
		</td>
		</tr>
		</table>
		
<?php
		if($settings_["user_register_test_mode"]){
			echo '<input type="submit" value="임시통과" class="knbtn knbtn-medium knbtn-secondary">';
		}
?>

		</form>
		<form id="kn-user-auth-req-form">
<?php
		$ignore_popup_ = (kn_is_mobile() && kn_is_ios());
		if($ignore_popup_){
			$html_ .= '<input type="hidden" name="popup" value="false">';
		}
?>
		<input type="hidden" name="enc_data" value="<?=$auth_req_data_["enc_data"]?>">
		<input type="hidden" name="return_url" value="<?=($ignore_popup_ ? get_permalink(get_the_ID()) : $auth_req_data_["return_url"])?>">
		<input type="hidden" name="request_num" value="<?=$auth_req_data_["request_num"]?>">
		</form>
<?php
		$html_ = ob_get_contents();
		ob_end_clean();
		return $html_;
	}
}

?>