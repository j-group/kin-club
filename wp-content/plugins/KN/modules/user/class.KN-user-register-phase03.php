<?php

class KN_user_register_p03{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		wp_register_script("kn-user-register", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-register.js"), array("kn-user"));

		wp_localize_script("kn-user-register", "kn_user_register_msg"
		,array(
			"formval_user_login_required" => "아이디를 입력하여 주세요.",
			"formval_user_login_minlength" => "아이디는 {0}자 이상이여야 합니다.",
			"formval_user_login_maxlength" => "아이디는 {0}자 이하여야 합니다.",
			"formval_user_login_exists" => "이미 사용하고 있는 아이디입니다.",
			"formval_user_login_alphanum" => "아이디는 영문,숫자로만 사용가능합니다.",
			"formval_user_email_required" => "이메일주소를 입력하여 주세요.",
			"formval_user_email_email" => "이메일형식이 아닙니다.",
			"formval_user_email_exists" => "이미 사용하고 있는 이메일입니다.",
			"formval_user_pass_required" => "비밀번호를 입력하여 주세요.",
			"formval_user_pass_minlength" => "비밀번호는 {0}자 이상으로 입력하여 주세요.",
			"formval_user_pass_maxlength" => "비밀번호는 {0}자 이하로 입력하여 주세요.",
			"formval_user_pass_c_equalsto" => "비밀번호가 일치하지 않습니다.",
			"formval_phone2_required" => "휴대폰 2번째자리를 입력하여 주세요.",
			"formval_phone2_number" => "휴대폰 2번째자리가 숫자가 아닙니다.",
			"formval_phone3_required" => "휴대폰 3번째자리를 입력하여 주세요.",
			"formval_phone3_number" => "휴대폰 3번째자리가 숫자가 아닙니다.",
			"signup_cancel_confirm" => "회원가입을 중단하고 로그인페이지로 이동합니다.",
			"signup_cancel_url" => get_permalink($settings_["user_login_page_id"]),
			"signup_error_message" => "회원가입에 실패하였습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
		));

		wp_enqueue_script("kn-user-register");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();

		$is_test_mode_ = $settings_["user_register_test_mode"];

		if(!$is_test_mode_ && (!isset($_POST["auth_di"]) || !strlen($_POST["auth_di"]))){
			wp_die("잘못된 접근입니다");
		}
		
		if(!$is_test_mode_){
			$name_ = urldecode($_POST["auth_name"]);
			$sex_ = $_POST["auth_sex"];
			$cell_num_ = $_POST["auth_cell_num"];
			$cell_corp_ = $_POST["auth_cell_corp"];
			$birth_yyyy_ = substr($_POST["auth_birth_ymd"],0,4);
			$birth_mm_ = substr($_POST["auth_birth_ymd"],4,2);
			$birth_dd_ = substr($_POST["auth_birth_ymd"],6,2);
			$di_ = $_POST["auth_di"];
			$ci1_ = $_POST["auth_ci1"];
			$ci2_ = $_POST["auth_ci2"];
			$ci_ver_ = $_POST["auth_ci_ver"];
		}else{
			$name_ = "홍길동(TEST)";
			$sex_ = "M";
			$cell_num_ = "01012345678";
			$cell_corp_ = "KTF";
			$birth_yyyy_ = "1986";
			$birth_mm_ = "07";
			$birth_dd_ = "06";
			$di_ = kn_random_string(20);
			$ci1_ = kn_random_string(20);
			$ci2_ = kn_random_string(20);
			$ci_ver_ = "0.0.1";
		}

		if(!KN_user::check_di_for_signup($di_)){
			$html_ = '<script type="text/javascript">';
			$html_ .= 'alert("입력하신 신원으로 가입했거나, 가입한 이력이 존재합니다.");document.location = "'.get_permalink(get_the_ID()).'";';
			$html_ .= '</script>';

			return $html_;
		}

		$site_name_ = get_bloginfo("name");

		ob_start();
?>
		<form id="kn-register-form" class="kn-form">
		<input type="hidden" name="user_name" value="<?=$name_?>">
		<input type="hidden" name="auth_di" value="<?=$di_?>">
		<input type="hidden" name="auth_ci1" value="<?=$ci1_?>">
		<input type="hidden" name="auth_ci2" value="<?=$ci2_?>">
		<input type="hidden" name="auth_ci_ver" value="<?=$ci_ver_?>">
		<input type="hidden" name="auth_sex" value="<?=$sex_?>">
		<input type="hidden" name="auth_cell_corp" value="<?=$cell_corp_?>">
		<input type="hidden" name="auth_cell_num" value="<?=$cell_num_?>">

		<div>

		<p class="desc"><?=$site_name_?> 이용을 위한 몇가지 정보를 입력하여 회원가입을 완료합니다.</p>

		<h3>기본정보</h3>
		<table class="kn-form-table">
		
		<tr>
		<th class="required">아이디</th>
		<td><input type="text" name="user_login" placeholder="아이디" class="full-width" data-accessible="true"><p class="hint">*아이디는 5~15자, 영문 및 숫자만 사용가능합니다.</p></td>
		</tr>

		<tr>
		<th class="required">비밀번호</th>
		<td><div class="kn-column-frame kn-column-padding">
		<div class="kn-column l50"><input type="password" name="user_pass" placeholder="비밀번호" class="full-width" title="비밀번호는 8~25자 사용이 가능합니다."></div>
		<div class="kn-column l50"><input type="password" name="user_pass_c" placeholder="비밀번호 확인" class="full-width" title="비밀번호가 정확하지 않습니다." data-accessible="true"></div>
		</div><label id="user_pass-error" class="error" for="user_pass" style="display:none;"></label><label id="user_pass_c-error" class="error" for="user_pass_c" style="display:none;"></label><p class="hint">*비밀번호는 8~25자 사용이 가능합니다.</p></td>
		</tr>

		<tr>
		<th class="required">이메일</th>
		<td><div><input type="text" name="user_email" placeholder="이메일주소" class="full-width" title="이메일주소을 입력하여 주세요" data-accessible="true"></div>
		<label for="kn-user-email_rev_yn" class="for-input"><input id="kn-user-email_rev_yn" type="checkbox" name="email_rev_yn" data-accessible="true">이메일로 새로운 소식 및 정보 받기</label>
		<label id="user_email-error" class="error" for="user_email" style="display:none;"></label>
		<p class="hint">*주요 공지사항 등, 중요한 정보는 수신여부에 관계없이 발송됩니다.</p>
		<p class="hint">*비밀번호변경 및 재발급 시 기입한 이메일로 안내문을 발송합니다. 반드시 사용하시는 이메일로 기입하여 주십시오.</p></td>
		</tr>

		</table>

		<h3>개인정보입력</h3>
		<table class="kn-form-table">
		
		<tr>
		<th>이름</th>
		<td><?=$name_?></td>
		</tr>
		<tr>
		<th>생일</th>
		<td><div class="kn-column-frame kn-column-padding">
		<div class="kn-column l33 fix-width"><select name="birth_yyyy" data-accessible="true">
<?php
		$current_year_ = date('Y');
		$start_year_ = $current_year_ - 65;
		$limit_year_ = $current_year_ - 15;

		for($yyyy_ = $start_year_; $yyyy_ <$limit_year_; ++$yyyy_){
			$yyyy_str_ = str_pad($yyyy_,4,'0',STR_PAD_LEFT);
			echo '<option value="'.$yyyy_str_.'" '.($yyyy_str_ === $birth_yyyy_ ? "selected" : "").'>'.$yyyy_.'년</option>';
		}
?>		
		</select></div>
		<div class="kn-column l33 fix-width"><select name="birth_mm" data-accessible="true">
<?php

		for($mm_ = 1; $mm_ <13; ++$mm_){
			$mm_str_ = str_pad($mm_,2,'0',STR_PAD_LEFT);
			echo '<option value="'.$mm_str_.'" '.($mm_str_ === $birth_mm_ ? "selected" : "").'>'.$mm_.'월</option>';
		}
?>
		</select></div>
		<div class="kn-column l33 fix-width"><select name="birth_dd" data-accessible="true">
<?php

		for($dd_ = 1; $dd_ <32; ++$dd_){
			$dd_str_ = str_pad($dd_,2,'0',STR_PAD_LEFT);
			echo '<option value="'.$dd_str_.'" '.($dd_str_ === $birth_dd_ ? "selected" : "").'>'.$dd_.'일</option>';
		}
?>
		</select></div>
		</div><p class="hint">*생일에 관련된 이벤트 및 쿠폰발급에 참고합니다.</p></td>
		</tr>
		<tr>
		<th class="required">휴대폰</th>
		<td><div class="kn-column-frame kn-column-padding">
		<div class="kn-column l33 fix-width">
		<?= kn_gcode_make_select("Z0001",array(
			"name" => "phone1",
			"add_title" => false,
			"default" => substr($cell_num_, 0,3),
			"attrs" => array(
				"data-accessible" => "true"
			)
		)) ?>
<?php


		$phone_del_ = (strlen($cell_num_) > 10 ? 4 : 3);
		$phone2_ = substr($cell_num_, 3,$phone_del_);
		$phone3_ = substr($cell_num_, 3+$phone_del_, 4);

?>
		</div>
		<div class="kn-column l33 fix-width"><input type="text" name="phone2" class="full-width" data-accessible="true" maxlength="4" value="<?=$phone2_?>"></div>
		<div class="kn-column l33 fix-width"><input type="text" name="phone3" class="full-width" data-accessible="true" maxlength="4" value="<?=$phone3_?>"></div>

		</div>
		<label for="kn_user_sms_rev_yn" class="for-input"><input type="checkbox" id="kn_user_sms_rev_yn" name="sms_rev_yn" data-accessible="true">SMS로 새로운 소식 및 정보 받기</label>
		<label id="phone2-error" class="error" for="phone2" style="display:none;"></label>
		<label id="phone3-error" class="error" for="phone3" style="display:none;"></label>
		<p class="hint">*주요 공지사항 등, 중요한 정보는 수신여부에 관계없이 발송됩니다.</p>
		
		</td>
		</tr>

		</table>

		<div class="button-area">
		<span><span class="kn-indicator"></span></span>
		<input type="submit" class="knbtn knbtn-medium knbtn-primary" value="가입하기" data-accessible="true">
		 <input type="button" class="knbtn knbtn-medium knbtn-secondary" value="취소" data-accessible="true" onclick="kn_user_cancel_register();">
		</div>

		</div>

		</form>

		<form id="kn-register-form-complete" style="display:none;" action="<?=get_permalink($settings_["user_register_page_id"])?>" method="POST">
		<input type="hidden" name="did_agree" value="Y">
		<input type="hidden" name="did_certify" value="Y">
		<input type="hidden" name="did_input" value="Y">
		<input type="hidden" name="ID" value="">
		</form>
<?php
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}
}

?>