<?php

class KN_user_register_p04{

	static function add_library(){
		$settings_ = KN_settings::get_settings();

		$id_ = isset($_POST["ID"]) ? $_POST["ID"] : "";

		if(!strlen($id_)){
			wp_die("잘못된 접근입니다.");
		}

		KN_user::_signin($id_);

		$user_data_ = kn_user($id_);

		// $login_url_ = kn_make_url(get_permalink($settings_["user_login_page_id"]), array(
		// 	"user_login" => $user_data_["user_login"]
		// ));

		kn_load_library("main-all");
		kn_load_library("kn-listtable");
		kn_load_library("kn-maps");
		kn_load_library("kn-utils");
		kn_load_library("kn-fileupload");
		kn_load_library("kn-user-children-table");

		wp_enqueue_style("kn-user-register-addinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-register-addinfo.css"));
		
		wp_register_script("kn-user-register-addinfo", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user-register-addinfo.js"), array("kn-user"));

		wp_localize_script("kn-user-register-addinfo", "kn_addinfo_var"
		,array(
			
			"login_url" => home_url(),
			"update_success_message" => "추가정보가 저장되었습니다. 홈페이지로 이동합니다.",
			"update_error_message" => "저장 중 에러가 발생했습니다. ".$settings_["email_sender"]." 를 통하여 문의바랍니다. 불편을 드려 죄송합니다.",
		));

		wp_enqueue_script("kn-user-register-addinfo");
	}

	static function draw_form($attrs_){
		$settings_ = KN_settings::get_settings();
		$site_name_ = get_bloginfo("name");

		$user_id_ = $_POST["ID"];

		$intro_text_placeholder_txt_ = "Loopkin은 사라들간의 관계를 기반으로 만들어졌습니다.\r\n";
		$intro_text_placeholder_txt_ .= "회원님이 어떤 사람인지 알려주세요.\r\n\r\n";

		$intro_text_placeholder_txt_ .= "당신의 경험과 재능은 무엇입니까?\r\n";
		$intro_text_placeholder_txt_ .= "나를 특별하게 만드는 요소는 무엇인가요?\r\n\r\n";

		$intro_text_placeholder_txt_ .= "나와 내 아이가 좋아하는 활동들을 공유해 주세요.\r\n";
		$intro_text_placeholder_txt_ .= "가장 좋아하는 여행지, 책, 영화, TV, 프로그램, 음악, 음식 등 뭐든지 좋습니다.\r\n\r\n";

		$intro_text_placeholder_txt_ .= "회원님은 어떤 스타일의 놀이활동전문가 또는 참여자 인가요?\r\n";
		$intro_text_placeholder_txt_ .= "인생의 자우명은 무엇인가요?";

		ob_start();
?>

<div class="signup-complete kn-form-table">
<h3 class="title"><?=$site_name_?> 회원가입완료!</h3>
<p class="desc">이제 회원님을 소개할 추가정보를 입력할 차례입니다!</p>
</div>

<div class="kn-addinfo-frame kn-form">

	<div class="picture-area">
	<h3>프로필 사진을 등록해주세요!</h3>
	<div class="kn-form-table picture-frame">
		<input type="file" name="files[]" id="kn-profile-file" accept=".jpg,.jpeg,.gif,.png,.bmp" data-accessible="true">
		<p class="desc">그림을 눌러 프로필 사진을 등록하세요!</p>

		<h4 class="kn-nick-name-title">닉네임</h4>
		<input type="text" name="nick_name" placeholder="닉네임 입력" id="kn-profile-nickname">
	</div>
</div>

<form class="kn-form" id="kn-addinfo-form">
<input type="hidden" name="ID" value="<?=$user_id_?>">
<input type="hidden" name="profile_url" value="">
<input type="hidden" name="addr_lat" value="">
<input type="hidden" name="addr_lng" value="">
<input type="hidden" name="nick_name">


	<h3>회원님을 소개해주세요!</h3>
	<table class="kn-form-table">
		<tr>
			<th>거주정보</th>
			<td>
				<input type="text" name="address" placeholder="여기를 클릭하여 주소를 검색하세요" readonly class="full-width">
				<input type="text" name="address_dtl" placeholder="상세주소" class="full-width">
			</td>
		</tr>
		<tr>
			<th>놀이활동진행경험</th>
			<td>
				<?= kn_gcode_make_select("U0005",array(
					"name" => "play_exp",
					"class" => "full-width",
					"attrs" => array(
						"data-accessible" => "true"
					)
				)) ?>
			</td>
		</tr>
		<tr>
			<th>나의소개</th>
			<td>
				<textarea name="intro_txt" class="full-width" placeholder="<?=$intro_text_placeholder_txt_?>" data-accessible="true"></textarea>
			</td>
		</tr>

	</table>
</form>
<br/>
	<h4>내아이정보</h4>
		<?=KN_user::draw_children_table($user_id_, true);?>
	<br/>


<div class="button-area">
	<span><span class="kn-indicator"></span></span>
	<input type="button" class="knbtn knbtn-medium knbtn-secondary" value="건너띄기" data-accessible="true" onclick="kn_user_skip_addinfo();">
	<input type="button" class="knbtn knbtn-medium knbtn-primary" value="저장하기" data-accessible="true" id="kn-user-addinfo-submit-btn">
</div>

</div>

<?=KN_maps::draw_address_popup()?>

<?php
		
		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_update_addinfo(){
		$addinfo_ = $_POST["addinfo"];
		$current_user_id_ = kn_current_user_id();

		if($current_user_id_ !== $addinfo_["ID"]){
			echo json_encode(array("reject" => true));
			die();
		}

		KN_user::update($current_user_id_, $addinfo_);

		$children_data_ = isset($_POST["children_data"]) ? $_POST["children_data"] : array();
		$updated_children_data_ = (isset($children_data_["data"]) ? $children_data_["data"] : array());
		$removed_children_data_ = (isset($children_data_["removed_data"]) ? $children_data_["removed_data"] : array());

		foreach($updated_children_data_ as $child_data_){
			$is_new_ = (isset($child_data_["is_new"]) && $child_data_["is_new"] === "Y");

			unset($child_data_["is_new"]);

			if($is_new_){
				KN_user::add_child($current_user_id_, $child_data_);
			}else{
				KN_user::update_child($current_user_id_, $child_data_["seq"], $child_data_);
			}
		}

		foreach($removed_children_data_ as $child_data_){
			KN_user::delete_child($current_user_id_, $child_data_["seq"], $child_data_);
		}

		echo json_encode(array(
			"success" => true,
		));
		die();
	}
}

kn_add_ajax("kn-user-update-addinfo", array("KN_user_register_p04", "_ajax_update_addinfo"), true);

?>