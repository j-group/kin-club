<?php

class KN_user_register extends KNShortcode{

	static function shortcode_name(){
		return "kn-user-register";
	}

	static function _current_class(){

		$did_agree_ = KN_POST("did_agree", null) === "Y";
		$did_certify_ = KN_POST("did_certify", null) === "Y";
		$did_input_ = KN_POST("did_input", null) === "Y";

		$draw_form_class_ = null;
		
		if($did_agree_ && $did_certify_ && $did_input_){
			$draw_form_class_ = "KN_user_register_p04";
		}else if($did_agree_ && $did_certify_){
			$draw_form_class_ = "KN_user_register_p03";
		}else if($did_agree_){
			$draw_form_class_ = "KN_user_register_p02";
		}else{
			$draw_form_class_ = "KN_user_register_p01";
		}

		return $draw_form_class_;
	}

	static function add_library(){
		kn_load_library("main");
		wp_enqueue_script("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-user.js"), array("jquery-validate"));
		wp_enqueue_style("kn-user", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user.css"));
		wp_enqueue_style("kn-user-register", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-user-register.css"));
			
		call_user_func(array(self::_current_class(),"add_library"));
	}

	static function _check_siren24(){
		if(isset($_REQUEST["retInfo"])){
			
			$dec_data_ = KN_siren24::decode_enc_data();

			if(KN_siren24::is_error($dec_data_)){
				wp_die("잘못된 접근입니다.");
				return;
			}

			?>
			<script type="text/javascript">
			var $ = jQuery;

			var form_html_ = '<form id="kn-user-certify-form" action="<?= get_permalink($settings_["user_register_page_id"]); ?>" method="POST">';
				form_html_ += '<input type="hidden" name="did_agree" value="Y">';
				form_html_ += '<input type="hidden" name="did_certify" value="Y">';
				
				form_html_ += '<input type="hidden" name="auth_name" value="<?= $dec_data_["name"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_sex" value="<?= $dec_data_["sex"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_cell_num" value="<?= $dec_data_["cell_num"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_cell_corp" value="<?= $dec_data_["cell_corp"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_birth_ymd" value="<?= $dec_data_["birth_ymd"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_di" value="<?= $dec_data_["di"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_ci1" value="<?= $dec_data_["ci1"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_ci2" value="<?= $dec_data_["ci2"]; ?>">';
				form_html_ += '<input type="hidden" name="auth_ci_ver" value="<?= $dec_data_["ci_cer"]; ?>">';
				form_html_ += '</form>';

			var form_element_ = $(form_html_);
			$("body").append(form_element_);
			form_element_.submit();

			</script>
			<?php
		}
	}

	static function _do_shortcode($attrs_){
		$attrs_ = shortcode_atts(array(),$attrs_);
		$settings_ = KN_settings::get_settings();

		$is_test_mode_ = $settings_["user_register_test_mode"];
		$back_img_url_ = $settings_["user_register_page_back_img_url"];

		if(!$is_test_mode_){
			self::_check_siren24();
		}
			
		
		$draw_form_class_ = self::_current_class();

		$html_ = "<div class='kn-user kn-user-register' style='background-image:url(".$back_img_url_.");'><div class='wrap'><div class='register-float'>";
		$html_ .= '<ol class="top-nav">';
		$html_ .=	'<li '.($draw_form_class_ === "KN_user_register_p01" ? "class='current-phase'" : "").'>약관동의<span class="knicon kn-arrow-r"></span></li>';
		$html_ .=	'<li '.($draw_form_class_ === "KN_user_register_p02" ? "class='current-phase'" : "").'>본인인증<span class="knicon kn-arrow-r"></span></li>';
		$html_ .=	'<li '.($draw_form_class_ === "KN_user_register_p03" ? "class='current-phase'" : "").'>회원정보입력<span class="knicon kn-arrow-r"></span></li>';
		$html_ .=	'<li '.($draw_form_class_ === "KN_user_register_p04" ? "class='current-phase'" : "").'>가입완료</li>';
					
		$html_ .= '</ol>';

		if(KN_siren24::has_enc_data()){
			$attrs_["auth_data"] = KN_siren24::decode_enc_data();
		}
		$html_ .= call_user_func_array(array($draw_form_class_,"draw_form"),array($attrs_));

		$html_ .= "</div></div></div>";

		return do_shortcode($html_);
	}

	static function _add_style(){
		
	}

	static function _ajax_user_exists(){
		$user_login_ = $_REQUEST["key"];
		$reverse_ = isset($_REQUEST["reverse"]) ? ($_REQUEST["reverse"] === "true" ? true : false) : false;
		$result_ = (strlen(kn_user_id($user_login_)) ? false : true);

		if($reverse_) $result_ = !$result_;

		echo json_encode($result_);
		die();
	}

	static function _ajax_signup(){
		$user_data_ = $_POST["user_data"];

		$result_ = KN_user::signup($user_data_);

		if(!$result_){
			echo json_encode(false);
			die();
		}
		echo json_encode(array(
			"success" => true,
			"ID" => $result_
		));
		die();
	}

	static function _prevent_page($current_page_id_){
		$settings_ = KN_settings::get_settings();
		$register_page_id_ = $settings_["user_register_page_id"];

		if(strlen($register_page_id_) && ((string)$register_page_id_) === ((string)$current_page_id_)){
			if(kn_is_user_logged_in() && self::_current_class() !== "KN_user_register_p04"){
				wp_redirect(home_url());
				exit;
			}

			if($settings_["user_register_need_ssl"] === true && !kn_is_https()){
				wp_redirect(kn_make_https());
				exit;
			}
		}
	}
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-register-phase01.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-register-phase02.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-register-phase03.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-register-phase04.php');

kn_add_ajax("kn-user-register-user-exists",array("KN_user_register", "_ajax_user_exists"), true);
kn_add_ajax("kn-user-register-signup",array("KN_user_register", "_ajax_signup"), true);

add_shortcode((KN_user_register::shortcode_name()), array("KN_user_register","_do_shortcode"));
add_action("wp_footer", array("KN_user_register","_add_style"));

add_action("kn_shortcode_filter",array("KN_user_register","filter"));
add_action("kn_user_prevent_page",array("KN_user_register", "_prevent_page"));

?>