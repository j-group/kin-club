<?php

class KN_user_settings{
	static function _add_default_settings($settings_){
		return array_merge(array(
			"user_term001_title" => "",
			"user_term001_content" => "",
			"user_term002_title" => "",
			"user_term002_content" => "",
			"user_term003_title" => "",
			"user_term003_content" => "",
			"user_term004_title" => "",
			"user_term004_content" => "",

			"user_leave_term001_content" => "",

			"user_login_page_id" => "",
			"user_login_page_back_img_url" => "",
			"user_login_need_ssl" => false,
			"user_login_redirect_url" => "",
			"user_login_captcha_count" => 3,
			"user_logout_page_id" => "",
			"user_logout_redirect_url" => "",
			"user_register_page_id" => "",
			"user_register_page_back_img_url" => "",
			"user_register_need_ssl" => "",
			"user_forgotpassword_page_id" => "",
			"user_forgotpassword_need_ssl" => "",
			"user_changepassword_page_id" => "",
			"user_changepassword_need_ssl" => "",
			"user_myinfo_page_id" => "",
			"user_myinfo_need_ssl" => "",

			"user_register_test_mode" => false,

			"user_login_header_action" => "",

			"user_find_id_email_title" => "",
			"user_find_id_email_content" => "",

			"user_forgotpassword_email_title" => "",
			"user_forgotpassword_email_content" => "",

			"user_pro_apply_page_id" => "",
			"user_pro_apply_page_back_img_url" => "",
			"user_pro_apply_term_title" => "",
			"user_pro_apply_term_content" => "",
			
		),$settings_);
	}
}

add_filter("kn_default_settings", array("KN_user_settings", "_add_default_settings"));

?>