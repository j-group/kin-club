<?php

class KN_user{

	/**
	사용자 기본정보
	*/
	static function _filter_encode_user_columns($data_){
		return $data_;
	}
	static function _filter_decode_user_columns($data_){
		return $data_;
	}

	static function signup($data_){
		global $wpdb;

		if(!isset($data_["user_login"]) || !isset($data_["user_email"])
			|| !isset($data_["user_pass"])) return false;

		$data_ = array_merge(array(
			"birth_yyyy" => KN_QUERY_NULL,
			"birth_mm" => KN_QUERY_NULL,
			"birth_dd" => KN_QUERY_NULL,
			
			"phone1" => KN_QUERY_NULL,
			"phone2" => KN_QUERY_NULL,
			"phone3" => KN_QUERY_NULL,
			
			"sms_rev_yn" => 'N',
			"email_rev_yn" => 'N',
			
			"status" => '00001',
			
			"auth_di" => KN_QUERY_NULL,
			"auth_ci1" => KN_QUERY_NULL,
			"auth_ci2" => KN_QUERY_NULL,
			"auth_ci_ver" => KN_QUERY_NULL,
			"auth_sex" => KN_QUERY_NULL,
			"auth_cell_corp" => KN_QUERY_NULL,
			"auth_cell_num" => KN_QUERY_NULL,
		),$data_);

		$data_ = apply_filters("kn_user_encode_columns",$data_);

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_users", array(
			"user_login" => $data_["user_login"],
			"user_email" => $data_["user_email"],
			"user_pass" => kn_crypt_hash($data_["user_pass"]),
			"user_name" => $data_["user_name"],

			"birth_yyyy" => $data_["birth_yyyy"],
			"birth_mm" => $data_["birth_mm"],
			"birth_dd" => $data_["birth_dd"],
			
			"phone1" => $data_["phone1"],
			"phone2" => $data_["phone2"],
			"phone3" => $data_["phone3"],
			
			"sms_rev_yn" => $data_["sms_rev_yn"],
			"email_rev_yn" => $data_["email_rev_yn"],
			
			"status" => $data_["status"],

			"auth_di" => $data_["auth_di"],
			"auth_ci1" => $data_["auth_ci1"],
			"auth_ci2" => $data_["auth_ci2"],
			"auth_ci_ver" => $data_["auth_ci_ver"],
			"auth_sex" => $data_["auth_sex"],
			"auth_cell_corp" => $data_["auth_cell_corp"],
			"auth_cell_num" => $data_["auth_cell_num"],
		),array(
			"LOWER(%s)",
			"LOWER(%s)",
			"%s",
			"%s",

			"%s",
			"%s",
			"%s",
			
			"%s",
			"%s",
			"%s",
			
			"%s",
			"%s",

			"%s",

			"%s",
			"%s",
			"%s",
			"%s",
			"%s",
			"%s",
			"%s",
		));

		if(!$wpdb->query($query_)) return false;

		$id_ = $wpdb->insert_id;
		do_action("kn_user_signup",$id_);

		return $id_;
	}

	static function check_di_for_signup($di_){
		global $wpdb;
		$query_ = "SELECT COUNT(*) CNT FROM {$wpdb->base_prefix}kn_users WHERE AUTH_DI = %s";
		$check_count_ = $wpdb->get_row($wpdb->prepare($query_,$di_))->CNT;
		return ($check_count_ == 0);
	}

	static function verify($id_, $pass_){
		global $wpdb;
		$query_ = "SELECT user_pass FROM {$wpdb->base_prefix}KN_USERS
			WHERE ID = %d
			AND   STATUS = '00001'";

		$checkdata_ = $wpdb->get_row($wpdb->prepare($query_, $id_),ARRAY_A);
		if(isset($checkdata_) && isset($checkdata_["user_pass"])){
			if(kn_crypt_verify($pass_, $checkdata_["user_pass"])){
				return $id_;
			}
			return false;
		}else return false;	
	}

	static function _signin($id_){
		kn_session_put("kn_current_user_id",$id_);
		do_action("kn_user_signin",$id_);
	}

	static function signin($key_, $pass_){
		$id_ = self::user_id($key_);
		if(!$id_) return false;

		$result_ = self::verify($id_, $pass_);

		if($result_ !== false){
			self::_signin($id_);
			return $result_;
		}else return $result_;
	}

	static function signout(){
		$id_ = self::current_user_id();
		if(!$id_) return false;

		kn_session_remove("kn_current_user_id");
		do_action("kn_user_signout",$id_);
		return true;
	}

	static function update($id_, $data_){
		global $wpdb;

		unset($data_["id"]);
		unset($data_["user_pass"]);

		$query_ = kn_query_update("{$wpdb->base_prefix}kn_users", apply_filters("kn_user_encode_columns", $data_),array(
				"ID" => $id_
			),null, array("%d"));

		$result_ = $wpdb->query($query_);

		if($result_ === false) return false;

		do_action("kn_user_update",$id_);
		return $id_;
	}

	static function change_password($id_, $pass_){
		global $wpdb;
		$pass_ = kn_crypt_hash($pass_);
		$result_ = $wpdb->update("{$wpdb->base_prefix}kn_users", array(
			"user_pass" => $pass_
		),array(
			"ID" => $id_
		),null, array("%d"));
		do_action("kn_user_change_password");
		return $result_;
	}

	static function leave($id_){
		global $wpdb;

		$wpdb->query("UPDATE {$wpdb->base_prefix}kn_users SET
				status = '00009'
			WHERE 1=1
			AND   ID = $id_");
			
		$result_ = $wpdb->update("{$wpdb->base_prefix}kn_users",array(
			"LEV_DATE" => KN_QUERY_FUNC("CURRENT_TIMESTAMP"),
		),array(
			"ID" => $id_,
		),array("%s"),array("%d"));

		self::signout();

		return $result_;
	}
	static function block($id_, $reason_ = null){
		$result_ = KN_user::update($id_,array(
			"status" => "90001",
		));

		global $wpdb;
		$wpdb->update("{$wpdb->base_prefix}kn_users",array(
			"BLK_DATE" => KN_QUERY_FUNC("CURRENT_TIMESTAMP"),
			"BLK_RSN" => $reason_,
		),array(
			"ID" => $id_,
		),array("%s", "%s"),array("%d"));

		return $result_;
	}
	static function unblock($id_){
		$result_ = KN_user::update($id_,array(
			"status" => "00001",
		));

		global $wpdb;
		$wpdb->update("{$wpdb->base_prefix}kn_users",array(
			"BLK_DATE" => KN_QUERY_NULL,
			"BLK_RSN" => KN_QUERY_NULL,
		),array(
			"ID" => $id_,
		),array("%s", "%s"),array("%d"));

		return $result_;
	}

	static function query_make_conditions($conditions_ = array()){
		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_USR_TMP";

		if(isset($conditions_["status"])){
			if(gettype($conditions_["status"]) === "string")
				$conditions_["status"] = array($conditions_["status"]);

			$query_ .= " AND {$table_alias_}.status IN (-999 ";
			foreach($conditions_["status"] as $row_index_ => $status_){
				$query_ .= ",'".$status_."' ";
			}
			$query_ .= ") ";
		}

		if(isset($conditions_["verified"]) && $conditions_["verified"] === true){
			$query_ .= " AND {$table_alias_}.status IN ('00001') ";
		}

		if(isset($conditions_["user_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["user_id"]." ";
		}

		if(isset($conditions_["sms_rev_yn"])){
			$query_ .= " AND {$table_alias_}.sms_rev_yn = '".($conditions_["sms_rev_yn"] ? "Y" : "N")."' ";
		}

		if(isset($conditions_["email_rev_yn"])){
			$query_ .= " AND {$table_alias_}.email_rev_yn = '".($conditions_["email_rev_yn"] ? "Y" : "N")."' ";
		}

		if(isset($conditions_["auth_di"])){
			$query_ .= " AND {$table_alias_}.auth_di = '".$conditions_["auth_di"]."' ";
		}

		if(isset($conditions_["search_text"])){
			$query_ .= " AND CONCAT({$table_alias_}.user_email,' ',{$table_alias_}.user_name, ' ', {$table_alias_}.user_name,' ', {$table_alias_}.user_login, ' ', {$table_alias_}.id) LIKE '%".$conditions_["search_text"]."%' ";
		}

		return $query_;
	}

	static function user_list($conditions_ = array()){
		$conditions_["table_alias"] = "KN_USR_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY ID DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		global $wpdb;

		$query_ = "SELECT 
				{$table_alias_}.ID ID,
				user_login user_login,
				user_email user_email,
				user_login user_login,
				{$table_alias_}.status status,
				".kn_query_gcode_dtl_nm("U0001","{$table_alias_}.status")." status_nm,
				
				IF({$table_alias_}1.STATUS = '00003', '00003', '00001') user_ctg,
				".kn_query_gcode_dtl_nm("U0003","(IF({$table_alias_}1.STATUS = '00003', '00003', '00001'))")." user_ctg_nm,

				user_name user_name,
				nick_name nick_name,
				IF(nick_name IS NULL OR nick_name = '', user_name, nick_name) display_name,
				birth_yyyy birth_yyyy,
				birth_mm birth_mm,
				birth_dd birth_dd,
				IF(DATE_FORMAT(NOW(),'%m') = birth_mm,'Y', 'N') is_birth_mm,
				
				phone1 phone1,
				phone2 phone2,
				phone3 phone3,
				concat(phone1,phone2,phone3) phone,

				sms_rev_yn sms_rev_yn,
				email_rev_yn email_rev_yn,

				address address,
				address_dtl address_dtl,
				addr_lat addr_lat,
				addr_lng addr_lng,

				play_exp play_exp,
				".kn_query_gcode_dtl_nm("U0005","{$table_alias_}.play_exp")." play_exp_nm,

				profile_url profile_url, 
				intro_txt intro_txt,

				auth_sex auth_sex,
				auth_cell_num auth_cell_num,
				auth_cell_corp auth_cell_corp,

				{$table_alias_}.reg_date reg_date,
				{$table_alias_}.mod_date mod_date,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";

		if(kn_is_admin()){
			$query_ .= ",lev_date lev_date,
						blk_date,
						DATE_FORMAT({$table_alias_}.lev_date,'%Y.%c.%e %H:%i') lev_date_txt,
						DATE_FORMAT({$table_alias_}.blk_date,'%Y.%c.%e %H:%i') blk_date_txt,
						blk_rsn blk_rsn";
		}
		
		$query_ .= " FROM {$wpdb->base_prefix}KN_USERS {$table_alias_}";
		$query_ .= " LEFT OUTER JOIN {$wpdb->base_prefix}KN_USERS_PRO {$table_alias_}1";
		$query_ .= " ON {$table_alias_}.ID = {$table_alias_}1.ID ";
		$query_ .= " WHERE 1=1 ";
		$query_ .= self::query_make_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		return $wpdb->get_results($query_,ARRAY_A);
	}

	static function user($id_){
		$user_ = self::user_list(array("user_id" => $id_));
		if(!isset($user_) || count($user_) <= 0) return false;
		return $user_[0];
	}

	static function user_id($key_){
		global $wpdb;

		$query_ = "SELECT ID FROM {$wpdb->base_prefix}KN_USERS WHERE 1=1";
		$query_ .= " AND (USER_LOGIN = LOWER(%s) OR USER_EMAIL = LOWER(%s))";

		$result_ = $wpdb->get_row($wpdb->prepare($query_, $key_, $key_));

		if(isset($result_)) return $result_->ID;
		else return false;
	}

	static function user_id_by_phone($phone_){
		global $wpdb;

		$query_ = "SELECT ID FROM {$wpdb->base_prefix}KN_USERS WHERE 1=1";
		$query_ .= " AND CONCAT(PHONE1,PHONE2,PHONE3) = %s";

		$result_ = $wpdb->get_row($wpdb->prepare($query_, $phone_));

		if(isset($result_)) return $result_->ID;
		else return false;
	}

	static function send_email_for_find_id($key_){
		$id_ = self::user_id($key_);
		if(!$id_) return $id_;

		$settings_ = KN_settings::get_settings();
		$user_data_ = self::user($id_);

		$email_title_ = $settings_["user_find_id_email_title"];
		$email_content_ = $settings_["user_find_id_email_content"];

		$email_title_ = kn_map_string($email_title_, $user_data_);
		$email_content_ = kn_map_string($email_content_, $user_data_);

		return kn_mail_send($user_data_["user_email"], $email_title_,
			array(
				"message" => $email_content_
			), array('Content-Type: text/html; charset=UTF-8'));
	}

	static function send_email_for_change_password($key_){
		$id_ = self::user_id($key_);
		if(!$id_) return $id_;

		$change_key_ = kn_random_string(40);

		global $wpdb;
		$did_updated_ = $wpdb->update("{$wpdb->base_prefix}KN_USERS",array(
			"change_key" => $change_key_
		),array(
			"id" => $id_
		),
		array("%s"),
		array("%d"));

		if(!$did_updated_) return $did_updated_;

		$settings_ = KN_settings::get_settings();
		$user_data_ = self::user($id_);

		$email_title_ = $settings_["user_forgotpassword_email_title"];
		$email_content_ = $settings_["user_forgotpassword_email_content"];

		$email_title_ = kn_map_string($email_title_, $user_data_);
		$email_content_ = kn_map_string($email_content_, $user_data_);

		$changepassword_url_ = kn_make_url(get_permalink($settings_["user_changepassword_page_id"]),array(
			"user_login" => $user_data_["user_login"],
			"key" => $change_key_,
		));

		return kn_mail_send($user_data_["user_email"], $email_title_,
			array(
				"message" => str_replace("{changepassword_url}",$changepassword_url_,$email_content_)
			), array('Content-Type: text/html; charset=UTF-8'));
	}

	static function current_user_id(){
		return kn_session_get("kn_current_user_id");
	}

	static function current_user(){
		$id_ = self::current_user_id();
		if(!strlen($id_)) return false;
		return self::user($id_);
	}

	static function user_profile_tag($id_, $class_){
		$user_ = self::user($id_);
		if(!isset($user_)) return false;

		return '<span class="kn-img-frame kn-profile '.$class_.'"><span class="kn-profile-image" style="background-image:url('.$user_["profile_url"].');"></span></span>';
	}

	/**
	내아이정보
	*/
	static function add_child($user_id_, $data_){
		global $wpdb;

		if(!isset($data_["child_name"]) || !isset($data_["child_nick"]) || !isset($data_["sex"])){
			return false;
		}

		$data_ = array_merge(array(
			"child_name" => KN_QUERY_NULL,
			"child_nick" => KN_QUERY_NULL,
			"child_img_url" => KN_QUERY_NULL,
			"sex" => KN_QUERY_NULL,
			"age" => KN_QUERY_NULL,
			"rmk" => KN_QUERY_NULL,
		),$data_);

		unset($data_["ID"]);

		$seq_ = $wpdb->get_row("SELECT IFNULL((SELECT MAX(CHD.SEQ) FROM {$wpdb->base_prefix}kn_users_chd CHD WHERE CHD.ID = $user_id_),0)+1 SEQ FROM DUAL")->SEQ;

		$data_["seq"] = $seq_;

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_users_chd",array(
			"ID" => $user_id_,
			"seq" => $data_["seq"],
			"child_name" => $data_["child_name"],
			"child_nick" => $data_["child_nick"],
			"child_img_url" => $data_["child_img_url"],
			"sex" => $data_["sex"],
			"age" => $data_["age"],
		));

		if(!$wpdb->query($query_)) return false;

		do_action("kn_user_child_added",$seq_);

		return $seq_;
	}
	static function update_child($user_id_, $seq_, $data_){
		global $wpdb;

		unset($data_["ID"]);
		unset($data_["seq"]);

		$data_ = array_merge(array(
			"child_name" => KN_QUERY_NULL,
			"child_nick" => KN_QUERY_NULL,
			"child_img_url" => KN_QUERY_NULL,
			"sex" => KN_QUERY_NULL,
			"age" => KN_QUERY_NULL,
			"rmk" => KN_QUERY_NULL,
		),$data_);

		$query_ = kn_query_update("{$wpdb->base_prefix}kn_users_chd",array(
			"child_name" => $data_["child_name"],
			"child_nick" => $data_["child_nick"],
			"child_img_url" => $data_["child_img_url"],
			"sex" => $data_["sex"],
			"age" => $data_["age"],
		),array("ID"=>$user_id_,"seq"=>$seq_));

		$result_ = $wpdb->query($query_);
		if($result_ === false) return $result_;

		do_action("kn_user_child_updated", $user_id_, $seq_);
		return $result_;
	}
	static function delete_child($user_id_, $seq_){
		global $wpdb;

		$result_ = $wpdb->delete("{$wpdb->base_prefix}kn_users_chd",array("ID"=>$user_id_,"seq"=>$seq_));
		if($result_ === false) return $result_;

		do_action("kn_user_child_deleted", $user_id_, $seq_);
		return $result_;
	}

	static function query_make_children_conditions($conditions_ = array()){
		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_CHD_TMP";

		if(isset($conditions_["user_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["user_id"]." ";
		}

		if(isset($conditions_["seq"])){
			$query_ .= " AND {$table_alias_}.seq = ".$conditions_["seq"]." ";
		}

		if(isset($conditions_["sex"])){
			$query_ .= " AND {$table_alias_}.sex = '".$conditions_["sex"]."' ";
		}

		if(isset($conditions_["age"])){
			if(gettype($conditions_["age"]) !== "array"){
				$temp_age_ = $conditions_["age"];
				$conditions_["age"] = array(
					"from" => $temp_age_,
					"to" => $temp_age_,
				);
			}

			if(isset($conditions_["age"]["from"])) $query_ .= " AND {$table_alias_}.age >= ".$conditions_["age"]["from"]." ";
			if(isset($conditions_["age"]["to"])) $query_ .= " AND {$table_alias_}.age <= ".$conditions_["age"]["to"]." ";
		}

		if(isset($conditions_["search_text"])){
			$query_ .= " AND CONCAT({$table_alias_}.child_name,' ',{$table_alias_}.child_nick) LIKE '%".$conditions_["search_text"]."%' ";
		}

		return $query_;
	}

	static function children($conditions_ = array()){
		$conditions_["table_alias"] = "KN_CHD_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY ID, SEQ DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		global $wpdb;

		$query_ = "SELECT 
				ID ID,
				seq seq,
				child_name child_name,
				child_nick child_nick,

				concat(child_name, ' (',child_nick,')') display_name,
			
				sex sex,
				age age,
				rmk rmk,

				child_img_url child_img_url,
			
				reg_date reg_date,
				mod_date mod_date,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";

		$query_ .= " FROM {$wpdb->base_prefix}KN_USERS_CHD {$table_alias_} WHERE 1=1 ";
		$query_ .= self::query_make_children_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		return $wpdb->get_results($query_,ARRAY_A);
	}

	static function child($user_id_, $seq_){
		$child_ = self::children(array("user_id" => $user_id_, "seq" => $seq_));
		if(!isset($child_) || count($child_) <= 0) return false;
		return $child_[0];
	}

	static function draw_children_table($user_id_, $editable_ = false){
		// $children_ = self::children(array("user_id"=> kn_current_user_id()));


		ob_start();
?>
<table class="kn-form-table fix-width kn-list-table <?=($editable_ ? "editable" : "")?>" id="kn-user-children-table">
	<thead><tr>
		<th class="child-img-url">사진</th>
		<th class="child-name">이름(애칭)</th>
		<th class="button-frame"></th>
	</tr></thead>
	<tbody></tbody>
</table>
<div id="kn-user-children-table-popup" class="mfp-hide kn-mg-popup with-button">
	<input type="hidden" name="seq" value="">
	<input type="hidden" name="key" value="">
	<input type="hidden" name="completed" value="N">
	<h3>아이정보수정</h3>
	<div class="wrap kn-form">

		<div class="kn-child-main-img-frame">
			<input type="file" name="files[]" class="kn-child-img-file" accept=".jpg,.jpeg,.gif,.png,.bmp" data-accessible="true">
		</div>
		<table class="kn-form-table fix-width">
			<tr>
				<th>이름</th>
				<td><input type="text" name="child_name" data-accessible="true" placeholder="아이이름"></td>
			</tr>
			<tr>
				<th>애칭</th>
				<td><input type="text" name="child_nick" data-accessible="true" placeholder="아이애칭"></td>
			</tr>
			<tr>
				<th>성별</th>
				<td><?=KN_gcode::make_select("Z0003", array(
						"name" => "sex",
						"class" => "full-width",
						"attrs" => array("data-accessible" => "true")
					))?>
				</td>
			</tr>
			<tr>
				<th>나이</th>
				<td><input type="number" name="age" data-accessible="true" placeholder="아이나이"></td>
			</tr>

		</table>
	</div>
	<div class="button-area">
		<a class="knbtn knbtn-primary modify-btn" href="javascript:;">수정하기</a>
		<a class="knbtn knbtn-delete delete-btn" href="javascript:;">삭제</a>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	var children_table_ = $("#kn-user-children-table");
	children_table_.kn_user_children_table({
		editable : <?=$editable_ ? "true" : "false"?>,
		user_id : "<?=$user_id_?>"
	});
});
</script>

<?php

		$html_ = ob_get_contents();
		ob_end_clean();

		return $html_;
	}

	static function _ajax_load_user_children(){
		$user_id_ = $_POST["user_id"];
		$current_user_ = kn_current_user();

		if(!kn_is_admin() && $current_user_["ID"] !== $user_id_){
			echo json_encode(array(
				"reject" => true,
			));
			die();
		}

		$children_ = self::children(array("user_id" => $user_id_));

		echo json_encode(array(
			"success" => true,
			"data" => $children_,
		));
		die();
	}

	/**
	전문가
	*/
	static function insert_pro($data_){
		global $wpdb;

		$data_ = apply_filters("kn_user_pro_encode_columns",$data_);

		$query_ = kn_query_insert("{$wpdb->base_prefix}kn_users_pro",$data_, null, true);

		if(!$wpdb->query($query_)) return false;

		do_action("kn_user_pro_inserted",$data_["ID"]);

		return $data_["ID"];
	}
	static function update_pro($user_id_, $data_){
		global $wpdb;

		unset($data_["ID"]);

		$data_ = apply_filters("kn_user_pro_encode_columns",$data_);

		$query_ = kn_query_update("{$wpdb->base_prefix}kn_users_pro",$data_, array("ID" => $user_id_));

		if(!$wpdb->query($query_)) return false;

		do_action("kn_user_pro_updated",$user_id_);

		return $user_id_;
	}

	static function delete_pro($user_id_){
		global $wpdb;

		$query_ = kn_query_delete("{$wpdb->base_prefix}kn_users_pro",array("ID" => $user_id_));

		if(!$wpdb->query($query_)) return false;

		do_action("kn_user_pro_deleted",$user_id_);

		return $user_id_;
	}

	static function query_make_pro_conditions($conditions_){
		$query_ = "";

		$table_alias_ = isset($conditions_["table_alias"]) ? $conditions_["table_alias"] : "KN_PRO_TMP";

		if(isset($conditions_["user_id"])){
			$query_ .= " AND {$table_alias_}.ID = ".$conditions_["user_id"]." ";
		}
		if(isset($conditions_["status"])){
			$query_ .= " AND {$table_alias_}.status = ".$conditions_["status"]." ";
		}

		if(isset($conditions_["qna01"])){
			$query_ .= " AND {$table_alias_}.qna01 = '".$conditions_["qna01"]."' ";	
		}

		if(isset($conditions_["qna02"])){
			$temp_key_ = "-9999";
			foreach($conditions_["qna02"] as $key_){
				$temp_key_ .= ",'".$key_."'";
			}

			$query_ .= " AND jsn_contains({$table_alias_}.qna02, jsn_array({$temp_key_})) ";
		}

		if(isset($conditions_["qna03"])){
			$query_ .= " AND {$table_alias_}.qna03 = '".$conditions_["qna03"]."' ";	
		}

		if(isset($conditions_["qna04"])){
			$temp_key_ = "-9999";
			foreach($conditions_["qna04"] as $key_){
				$temp_key_ .= ",'".$key_."'";
			}

			$query_ .= " AND jsn_contains({$table_alias_}.qna04, jsn_array({$temp_key_})) ";
		}

		if(isset($conditions_["qna05"])){
			$query_ .= " AND {$table_alias_}.qna05 = '".$conditions_["qna05"]."' ";	
		}

		if(isset($conditions_["qna06"])){
			$query_ .= " AND {$table_alias_}.qna06 = '".$conditions_["qna06"]."' ";	
		}

		if(isset($conditions_["qna07"])){
			$query_ .= " AND {$table_alias_}.qna07 = '".$conditions_["qna07"]."' ";	
		}

		if(isset($conditions_["qna08"])){
			$query_ .= " AND {$table_alias_}.qna08 = '".$conditions_["qna08"]."' ";	
		}

		if(isset($conditions_["qna09"])){
			$query_ .= " AND {$table_alias_}.qna09 = '".$conditions_["qna09"]."' ";	
		}

		if(isset($conditions_["qna10"])){
			$query_ .= " AND {$table_alias_}.qna10 = '".$conditions_["qna10"]."' ";	
		}

		if(isset($conditions_["qna11"])){
			$query_ .= " AND {$table_alias_}.qna11 = '".$conditions_["qna11"]."' ";	
		}

		if(isset($conditions_["qna12"])){
			$query_ .= " AND {$table_alias_}.qna12 = '".$conditions_["qna12"]."' ";	
		}

		if(isset($conditions_["qna13"])){
			$query_ .= " AND {$table_alias_}.qna14 = '".$conditions_["qna13"]."' ";	
		}

		if(isset($conditions_["qna14"])){
			$temp_key_ = "-9999";
			foreach($conditions_["qna14"] as $key_){
				$temp_key_ .= ",'".$key_."'";
			}

			$query_ .= " AND jsn_contains({$table_alias_}.qna14, jsn_array({$temp_key_})) ";
		}

		return $query_;
	}

	static function _filter_encode_user_pro_columns($data_){
		if(isset($data_["qna02"])){
			$data_["qna02"] = json_encode($data_["qna02"]);
		}

		if(isset($data_["qna04"])){
			$data_["qna04"] = json_encode($data_["qna04"]);
		}

		if(isset($data_["qna06"])){
			$data_["qna06"] = json_encode($data_["qna06"]);
		}
		if(isset($data_["qna07"])){
			$data_["qna07"] = json_encode($data_["qna07"]);
		}

		if(isset($data_["qna12"])){
			$data_["qna12"] = json_encode($data_["qna12"]);
		}

		if(isset($data_["qna14"])){
			$data_["qna14"] = json_encode($data_["qna14"]);
		}

		return $data_;
	}
	static function _filter_decode_user_pro_columns($data_){
		if(isset($data_["qna02"])){
			$data_["qna02"] = json_decode($data_["qna02"]);
		}else $data_["qna02"] = array();

		if(isset($data_["qna04"])){
			$data_["qna04"] = json_decode($data_["qna04"]);
		}else $data_["qna04"] = array();

		if(isset($data_["qna06"])){
			$data_["qna06"] = json_decode($data_["qna06"]);
		}else $data_["qna06"] = array();

		if(isset($data_["qna07"])){
			$data_["qna07"] = json_decode($data_["qna07"]);
		}else $data_["qna07"] = array();

		if(isset($data_["qna12"])){
			$data_["qna12"] = json_decode($data_["qna12"]);
		}else $data_["qna12"] = array();

		if(isset($data_["qna14"])){
			$data_["qna14"] = json_decode($data_["qna14"]);
		}else $data_["qna14"] = array();

		return $data_;
	}

	static function pro_list($conditions_ = array()){
		$conditions_["table_alias"] = "KN_PRO_TMP";
		$table_alias_ = $conditions_["table_alias"];
		
		$query_limit_ = "";
		if(isset($conditions_["limit"])){
			$offset_ = $conditions_["limit"]["offset"];
			$length_ = $conditions_["limit"]["length"];
			$query_limit_ = " LIMIT $offset_, $length_ ";
		}

		$query_sort_ = " ORDER BY ID DESC ";
		if(isset($conditions_["sort"])){
			$query_sort_ = " ORDER BY ".$conditions_["sort"];
		}

		global $wpdb;

		$query_ = "SELECT 
				{$table_alias_}.ID ID,
				{$table_alias_}.status status,
				".kn_query_gcode_dtl_nm("U0019","{$table_alias_}.status")." status_nm,

				{$table_alias_}1.user_login user_login,
				{$table_alias_}1.user_name user_name,
				{$table_alias_}1.profile_url profile_url,
				{$table_alias_}1.birth_yyyy birth_yyyy,
				{$table_alias_}1.birth_mm birth_mm,
				{$table_alias_}1.birth_dd birth_dd,
				{$table_alias_}1.birth_dd,
				{$table_alias_}1.auth_sex sex,
				
				qna01 qna01,
				qna02 qna02,
				qna03 qna03,
				qna04 qna04,
				qna05 qna05,
				qna06 qna06,
				qna07 qna07,
				qna08 qna08,
				qna09 qna09,
				qna10 qna10,
				qna11 qna11,
				qna12 qna12,
				qna13 qna13,
				qna14 qna14,
				
				{$table_alias_}.accept_date accept_date,
				{$table_alias_}.reg_date reg_date,
				{$table_alias_}.mod_date mod_date,
				DATE_FORMAT({$table_alias_}.accept_date,'%Y.%c.%e %H:%i') accept_date_txt,
				DATE_FORMAT({$table_alias_}.REG_DATE,'%Y.%c.%e %H:%i') reg_date_txt,
				DATE_FORMAT({$table_alias_}.mod_date,'%Y.%c.%e %H:%i') mod_date_txt";

		$query_ .= " FROM {$wpdb->base_prefix}KN_USERS_PRO {$table_alias_} ";
		$query_ .= " LEFT OUTER JOIN {$wpdb->base_prefix}KN_USERS {$table_alias_}1 ";
		$query_ .= " ON {$table_alias_}.ID = {$table_alias_}1.ID ";
		$query_ .= " WHERE 1=1 ";
		$query_ .= self::query_make_pro_conditions($conditions_);
		$query_ .= $query_sort_;
		$query_ .= $query_limit_;

		$results_ = $wpdb->get_results($query_,ARRAY_A);
		foreach($results_ as $row_index_ => $result_){
			$results_[$row_index_] = apply_filters("kn_user_pro_decode_columns", $result_);
		}

		return $results_;
	}
	static function pro($user_id_){
		$pro_ = self::pro_list(array("user_id" => $user_id_));
		if(!isset($pro_) || count($pro_) <= 0) return false;
		return $pro_[0];
	}

	static function did_apply_pro($user_id_){
		$pro_ = self::pro($user_id_);
		return ($pro_ !== false);
	}

	static function is_pro($user_id_){
		$pro_ = self::pro($user_id_);
		if($pro_ === false) return false;
		return ($pro_["status"] === "00003");
	}

	/**
	사용자시스템
	*/
	static function _prevent_page(){
		do_action("kn_user_prevent_page",((string)get_the_ID()));
	}

	static function _register_header(){
		$settings_ = KN_settings::get_settings();
		if(strlen($settings_["user_login_header_action"])){
			add_action($settings_["user_login_header_action"], array("KN_user", "_add_header"));
			kn_load_library("main");
			wp_enqueue_style("kn-login-header", (KN_PLUGIN_DIR_URL."modules/user/lib/css/class.KN-login-header.css"));
			wp_enqueue_script("kn-login-header", (KN_PLUGIN_DIR_URL."modules/user/lib/js/class.KN-login-header.js"), array("jquery"));
		}
	}

	static function _add_header(){
		$settings_ = KN_settings::get_settings();
		$current_page_id_ = (string)get_the_ID();
	?>
	<div class="kn-login-header-frame">
	<ul id="kn-login-header">

		<?php
			do_action("kn_user_before_login_header");
			if(kn_is_user_logged_in()){
				$current_user_ = kn_current_user();

				do_action("kn_user_before_login_header_logged_in");
		?>
		<li><a href="<?php echo get_permalink($settings_["play_register_page_id"]); ?>" class="">놀이활동등록</a></li>
		<li>
			<span data-kn-user-dropdown class="<?= ($current_page_id_ === $settings_["user_myinfo_page_id"] ? "current" : ""); ?>"><?=self::user_profile_tag(kn_current_user_id(), "xxsmall")?> <?=$current_user_["display_name"]?>
				<ul class="submenu">
					<li><a href="<?php echo get_permalink($settings_["user_myinfo_page_id"]); ?>">내정보</a></li>
					<li><a href="<?php echo get_permalink($settings_["user_logout_page_id"]); ?>">로그아웃</a></li>
				</ul>
			</span>
		</li>
		<!-- <li><a href="<?php echo get_permalink($settings_["user_logout_page_id"]); ?>">로그아웃</a></li> -->
		<?php
			do_action("kn_user_after_login_header_logged_in");
			}else{ 

			do_action("kn_user_before_login_header_logged_out");
		?>
		<li><a href="<?php echo get_permalink($settings_["user_login_page_id"]); ?>" class="<?= ($current_page_id_ === $settings_["user_login_page_id"] ? "current" : ""); ?>">로그인</a></li>
		<li><a href="<?php echo get_permalink($settings_["user_register_page_id"]); ?>" class="<?= ($current_page_id_ === $settings_["user_register_page_id"] ? "current" : ""); ?>">회원가입</a></li>
		<?php
			do_action("kn_user_after_login_header_logged_out");
			
			}

			do_action("kn_user_after_login_header");
		?>
	</ul>
	</div>
	<?php
	}

	static function register_library(){
		$settings_ = KN_settings::get_settings();
	
		wp_register_style("kn-user-children-table", (KN_PLUGIN_DIR_URL. 'lib/js/kn.user-chlidren-table/kn.user-chlidren-table.css'));
		
		wp_register_script("kn-user-children-table", (KN_PLUGIN_DIR_URL. 'lib/js/kn.user-chlidren-table/kn.user-chlidren-table.js'), array("jquery","kn-main"));
		wp_localize_script("kn-user-children-table","kn_user_children_table_var",array(
			"norows_message" => "등록된 아이정보가 없습니다",
			"validate_child_name" => "이름을 입력하세요",
			"validate_child_nick" => "애칭을 입력하세요",
			"validate_sex" => "성별을 입력하세요",
			"validate_age" => "나이을 입력하세요",
			"title_new" => "내아이정보추가",
			"title_modify" => "내아이정보수정",
			"button_new" => "추가하기",
			"button_modify" => "수정하기",
			"button_delete" => "삭제하기",
			"button_add_child" => "아이정보추가",
		));

		KN_Library::register("kn-user-children-table",array(
			"script" => array(
				"jquery",
				"kn-main",
				"kn-user-children-table",
			),"style" => array(
				"kn-user-children-table",
			),
		));
	}
}

function kn_signup($data_){
	return KN_user::signup($data_);
}
function kn_signin($key_, $pass_){
	return KN_user::signin($key_, $pass_);
}
function kn_signout(){
	return KN_user::signout();
}
function kn_update_user($id_, $data_){
	return KN_user::update($id_, $data_);
}
function kn_user_list($conditions_ = array()){
	return KN_user::user_list($conditions_);
}
function kn_user($id_){
	return KN_user::user($id_);
}
function kn_user_id($key_){
	return KN_user::user_id($key_);
}
function kn_user_id_by_phone($phone_){
	return KN_user::user_id_by_phone($phone_);
}
function kn_current_user_id(){
	return KN_user::current_user_id();
}
function kn_current_user(){
	return KN_user::current_user();	
}
function kn_is_user_logged_in(){
	return (strlen(kn_current_user_id()) ? true : false);
}

include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-settings.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-register.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-login.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-logout.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-forgotpassword.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-changepassword.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo-leave.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-myinfo-mystatus.php');
include_once(KN_PLUGIN_DIR_PATH . 'modules/user/class.KN-user-pro-apply.php');

add_action("template_redirect", array("KN_user", "_prevent_page"));
add_action("init", array("KN_user", "_register_header"));
add_filter("kn_user_encode_columns", array("KN_user", "_filter_encode_user_columns"));
add_filter("kn_user_decode_columns", array("KN_user", "_filter_decode_user_columns"));
add_filter("kn_user_pro_encode_columns", array("KN_user", "_filter_encode_user_pro_columns"));
add_filter("kn_user_pro_decode_columns", array("KN_user", "_filter_decode_user_pro_columns"));
add_action("kn_library_init", array("KN_user","register_library"));
kn_add_ajax("kn-user-children-table-load-list", array("KN_user", "_ajax_load_user_children"), true);

?>