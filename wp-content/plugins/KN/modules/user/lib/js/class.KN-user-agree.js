(function($){

$(document).ready(function($){
	var agree_form_ = $("#kn-register-agree-form");
	var validate_roles_ = {};
	var agree_fields_ = agree_form_.find("input[data-agreement]");
	var all_agree_field_ = agree_form_.find("#kn-term-all-agree");

	agree_fields_.each(function(){
		var agree_column_ = $(this).attr("name");
		validate_roles_[agree_column_] = {
			checkequals : "Y"
		}
	});

	agree_form_.find("input[type='radio']").change(function(){
		kn_user_check_agreement();
	});

	all_agree_field_.change(function(event_){
		var that_ = $(event_.target);
		if(that_.prop("checked")){
			agree_form_.find("input[data-agreement]").prop("checked",that_.prop("checked"));
		}
	});

	agree_form_.validate({
		rules : validate_roles_,
		invalidHandler : function(){
			alert(kn_user_agree_msg["formval_user_agree_required"]);
		}
	});
});

function kn_user_check_agreement(){
	var agree_form_ = $("#kn-register-agree-form");
	var agree_fields_ = agree_form_.find("input[data-agreement]");
	var all_agree_field_ = agree_form_.find("#kn-term-all-agree");

	var all_checked_ = agree_form_.find("input[data-agreement]:checked").length === agree_fields_.length;
	all_agree_field_.prop("checked",all_checked_);
}

})(jQuery);