function kn_user_do_auth(cert_gb_){
	$ = jQuery;
	var auth_form_ = $("#kn-user-certify-form");
	var auth_inputs_ = auth_form_.find("input[data-auth-column]");
	auth_form_.formWorking(true);

	$.each(auth_inputs_, function(){
		$(this).val("");
	});

	var req_data_ = $("#kn-user-auth-req-form").serializeObject();

	if(req_data_["popup"] !== undefined && req_data_["popup"] === "false"){
		req_data_["popup"] = false;
	}

	KN.siren24.auth(req_data_,function(result_, auth_data_){
		auth_form_.formWorking(false);
		if(!result_){
			return;
		}
		
		if(auth_data_.result !== "Y"){
			alert(kn_user_register_msg["auth failed"]);
			location.reload();
			return;
		}
		
		$.each(auth_inputs_, function(){
			var column_name_ = $(this).data("auth-column");
			$(this).val(auth_data_[column_name_]);
		});

		auth_form_.submit();
	},function(){
		auth_form_.formWorking(false);
		alert(kn_user_register_msg["popup blocked"]);
	});
}