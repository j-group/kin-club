(function($){

$(document).ready(function($){
	var changepass_form_ = $("#kn-changepassword-form");
	var first_input_ = $(changepass_form_.find("input[type='password']")[0]);

	changepass_form_.validate({
		rules : {
			"user_pass" : {
				"required" : true,
				"minlength" : 8,
				"maxlength" : 25
			},
			"user_pass_c" : {
				"equalTo" : "#kn-changepassword-form [name='user_pass']"
			}
		},messages : {
			"user_pass" : {
				"required" : kn_user_cp_msg["formval_user_pass_required"],
				"minlength" : kn_user_cp_msg["formval_user_pass_minlength"],
				"maxlength" : kn_user_cp_msg["formval_user_pass_maxlength"]
			},
			"user_pass_c" : {
				"equalTo" : kn_user_cp_msg["formval_user_pass_c_equalsto"]
			}
		},submitHandler : function(){
			kn_user_changepassword();
		}
	});

	first_input_.focus();
});

function kn_user_changepassword(){
	var changepass_form_ = $("#kn-changepassword-form");
	var all_inputs_ = changepass_form_.find("[data-accessible='true']");

	changepass_form_.formWorking(true);

	KN.post({
		action : "kn-user-changepassword",
		user_data : changepass_form_.serializeObject()
	},function(result_, response_text_){
		changepass_form_.formWorking(false);

		if(!result_ || response_text_.success === undefined){
			alert(kn_user_cp_msg["changepassword_ajax_error"]);
			return;
		}

		if(!response_text_.success){
			alert(kn_user_cp_msg["changepassword_ajax_failed"]);
			return;	
		}

		alert(kn_user_cp_msg["changepassword_ajax_success"]);
		document.location = kn_user_cp_msg["changepassword_ajax_redirect_url"];
	});
}

})(jQuery)