(function($){

$(document).ready(function($){
	var forgot_pass_form_ = $("#kn-forgotpass-form");

	forgot_pass_form_.validate({
		rules : {
			"user_login" : {
				"required" : true
			}	
		},
		messages : {
			"user_login" : {
				"required" : kn_forgotpassword_msg["formval_pass_user_login_required"]
			}
		},
		submitHandler : kn_user_send_email_for_pass
	});

	var user_login_field_ = forgot_pass_form_.find("input[name='user_login']");	
	if(user_login_field_.val().length > 0){
		user_login_field_.focus();
	}
});

function kn_user_send_email_for_pass(){
	var forgot_pass_form_ = $("#kn-forgotpass-form");
	var user_login_field_ = forgot_pass_form_.find("[name='user_login']");
	var inputs_ = forgot_pass_form_.find("[data-accessible='true']");
	var user_login_ = forgot_pass_form_.serializeObject();
		user_login_ = user_login_["user_login"];
	
	forgot_pass_form_.formWorking(true);
	
	KN.post({
		action : "kn-user-forgotpass-send-email",
		user_login : user_login_
	},function(result_, response_text_){
		forgot_pass_form_.formWorking(false);

		if(!result_ || response_text_.success !== true){
			alert(kn_forgotpassword_msg["send_email_failed"]);
			user_login_field_.focus();
			return;
		}

		if(!response_text_.result){
			alert(kn_forgotpassword_msg["cannot_finded_email"]);
			user_login_field_.focus();
			return;	
		}
		alert(kn_forgotpassword_msg["email_send_completed"]);
	});
}

})(jQuery);

function kn_user_find_id(cert_gb_){
	$ = jQuery;
	var auth_form_ = $("#kn-forgotid-form");
	var req_data_ = $("#kn-user-auth-req-form").serializeObject();
	auth_form_.formWorking(true);
	KN.siren24.auth(req_data_,function(result_, auth_data_){
		if(!result_){
			auth_form_.formWorking(false);
			return;
		}

		if(auth_data_ === null || auth_data_ === undefined || auth_data_.result !== "Y"){
			auth_form_.formWorking(false);
			alert(kn_forgotpassword_msg["auth_failed"]);
			location.reload();
			return;
		}

		kn_user_find_id_p1(auth_data_);
	});
}

function kn_user_find_id_p1(auth_data_){
	$ = jQuery;
	var auth_form_ = $("#kn-forgotid-form");
	KN.post({
		action :"kn-user-findid-send-email",
		auth_di : auth_data_["di"]
	},function(result_, response_json_, cause_){
		auth_form_.formWorking(false);

		if(!result_ || response_json_.not_found === true){
			alert(kn_forgotpassword_msg["cannot_finded_user"]);
			return;
		}

		if(!response_json_.result){
			alert(kn_forgotpassword_msg["send_email_failed"]);
			return;	
		}
		alert(kn_forgotpassword_msg["findid_email_send_completed"]);
	});
}