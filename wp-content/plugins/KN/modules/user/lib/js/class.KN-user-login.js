(function($){

$(document).ready(function($){
	var login_form_ = $("#kn-user-login-form");
	var validate_rules_ = {
		"user_login" : {
			"required" : true
		},
		"user_pass" : {
			"required" : true
		},
		"captcha" : {
			"required" : true,
			"remote" : {
				url : ajaxurl,
				method : "GET",
				data : {
					action : "kn_user_signin_check_captcha",
					captcha : function(){return $("#kn-user-login-form [name='captcha']").val();}
				}
			}
		}
	}
	var validate_messages_ = {
		"user_login" : {
			"required" : kn_user_login_msg["formval_user_login_required"],
			"remote" : kn_user_login_msg["formval_user_login_exists"]
		},
		"user_pass" : {
			"required" : kn_user_login_msg["formval_user_pass_required"]
		},
		"captcha" : {
			"required" : kn_user_login_msg["formval_captcha_required"],
			"remote" : kn_user_login_msg["formval_captcha_remote"]
		}
	}

	login_form_.validate({
		rules : validate_rules_,
		messages : validate_messages_,
		submitHandler : function(){
			kn_user_login();
		}
	});

	var user_login_ = login_form_.find("[name='user_login']");
	var user_pass_ = login_form_.find("[name='user_pass']");

	if(user_login_.val() !== "" && user_login_.val() !== null)
		user_pass_.focus();
	else user_login_.focus();
});

function kn_user_login(){
	var login_form_ = $("#kn-user-login-form");
	var inputs_ = login_form_.find("[data-accessible='true']");
	var user_data_ = login_form_.serializeObject();

	user_data_["id_remember_yn"] = (user_data_["id_remember_yn"] !== undefined && user_data_["id_remember_yn"] === "on" ? "Y" : "N");
	
	login_form_.formWorking(true);

	KN.post({
		action : "kn_user_signin",
		user_data : user_data_
	},function(result_, response_text_){
		if(!result_ || response_text_.result === undefined){
			alert(kn_user_login_msg["login_error"]);
			login_form_.formWorking(false);
			return;
		}

		if(!response_text_.result){
			alert(kn_user_login_msg["login_failed"]);
			login_form_.formWorking(false);
			login_form_.find("[name='user_pass']").focus();

			if(response_text_["show_captcha"] === true){
				document.location = kn_user_login_msg["login_url"];
			}

			return;
		}
		
		document.location = kn_user_login_msg["redirect_url"];
	});
}

})(jQuery);