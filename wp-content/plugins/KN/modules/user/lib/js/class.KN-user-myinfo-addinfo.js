(function($){

$(document).ready(function($){
	var addinfo_form_ = $("#kn-addinfo-form");
	var submit_btn_ = $("#kn-addinfo-submit-button");

	addinfo_form_.find("[name='address']").click(function(){
		kn_user_addinfo_open_address_popup();
	});


	addinfo_form_.validate({
		submitHandler : function(){
			kn_user_addinfo_update();
		}
	});

	submit_btn_.click(function(){addinfo_form_.submit();});
});


function kn_user_addinfo_open_address_popup(){
	$ = jQuery;
	var addinfo_form_ = $("#kn-addinfo-form");
	
	KN.maps.address_picker({
		callback : function(address_){
			addinfo_form_.find("[name='address']").val(address_["address"]);
			addinfo_form_.find("[name='addr_lat']").val(address_["lat"]);
			addinfo_form_.find("[name='addr_lng']").val(address_["lng"]);
			addinfo_form_.find("[name='address_dtl']").focus();
		},
		secondary_btn_title : "현재위치선택",
		secondary_btn_callback : function(){
			$ = jQuery;
			$.magnificPopup.close();
			KN.maps.geolocation(function(result_){
				addinfo_form_.find("[name='address']").val(result_["address"]["jibunAddress"]["name"]);
				addinfo_form_.find("[name='addr_lat']").val(result_["lat"]);
				addinfo_form_.find("[name='addr_lng']").val(result_["lng"]);
				addinfo_form_.find("[name='address_dtl']").focus();
				
			});
		}
	});
}

function kn_user_addinfo_update(){
	var addinfo_frame_ = $(".kn-addinfo-frame");	
	var addinfo_form_ = $("#kn-addinfo-form");	
	var addinfo_ = addinfo_form_.serializeObject();
	var child_table_ = $("#kn-user-children-table");
	var child_data_ = child_table_.kn_user_children_table("json_string");

	addinfo_frame_.formWorking(true);
	KN.post({
		action : "kn-user-myinfo-addinfo-update",
		addinfo : addinfo_,
		children_data : child_data_
	},function(result_, response_json_){
		addinfo_frame_.formWorking(false);
		if(response_json_.success !== true){
			alert(kn_user_addinfo_var["changeinfo_ajax_error"]);
			return;
		}

		alert(kn_user_addinfo_var["changeinfo_ajax_success"]);
		document.location = document.location;
	});
}

})(jQuery);