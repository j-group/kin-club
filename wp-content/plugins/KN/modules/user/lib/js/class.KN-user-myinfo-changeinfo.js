(function($){

$(document).ready(function($){
	var changepassword_form_ = $("#kn-user-changepassword-form");
	var changeinfo_form_ = $("#kn-user-changeinfo-form"); 

	changepassword_form_.validate({
		groups: {
			new_user_pass : "new_user_pass new_user_pass_c"
		},
		rules : {
			user_pass : {
				required : true
			},
			new_user_pass : {
				required : true,
				minlength : 8,
				maxlength : 25
			},
			new_user_pass_c : {
				equalTo : "#kn-user-changepassword-form [name='new_user_pass']"
			}
		},messages : {
			user_pass : {
				required : kn_user_ci_msg["user_pass_required"]
			},
			new_user_pass : {
				required : kn_user_ci_msg["user_pass_required"],
				minlength : kn_user_ci_msg["user_pass_minlength"],
				maxlength : kn_user_ci_msg["user_pass_maxlength"]
			},
			new_user_pass_c : {
				equalTo : kn_user_ci_msg["user_pass_c_equalsto"]
			}
		},submitHandler : function(){
			kn_user_myinfo_changepassword();
		}
	});

	changeinfo_form_.validate({
		rules : {
			"user_email" : {
				"required" : true,
				"email" : true,
				"remote" : {
					url : ajaxurl,
					method : "GET",
					data : {
						"action" : "kn-user-myinfo-user-exists",
						"key" : function(){return $("#kn-user-changeinfo-form [name='user_email']").val();}
					}
				}
			},
			"phone2" : {
				"required" : true,
				"number" : true
			},
			"phone3" : {
				"required" : true,
				"number" : true
			},
			"user_pass" : {
				"required" : true
			}
		},
		messages : {
			"user_email" : {
				"required" : kn_user_ci_msg["user_email_required"],
				"email" : kn_user_ci_msg["user_email_email"],
				"remote" : kn_user_ci_msg["user_email_exists"]
			},
			"phone2" : {
				"required" : kn_user_ci_msg["phone2_required"],
				"number" : kn_user_ci_msg["phone2_number"]
			},
			"phone3" : {
				"required" : kn_user_ci_msg["phone3_required"],
				"number" : kn_user_ci_msg["phone3_number"]
			},
			user_pass : {
				required : kn_user_ci_msg["user_pass_required"]
			}
		},submitHandler : function(){
			kn_user_myinfo_changeinfo();
		}
	});

});

function kn_user_myinfo_changepassword(){
	var changepassword_form_ = $("#kn-user-changepassword-form");
	changepassword_form_.formWorking(true);
	KN.post({
		action : "kn-user-myinfo-changepass",
		user_data : changepassword_form_.serializeObject()
	},function(result_, response_text_, cause_){
		changepassword_form_.formWorking(false);
		if(!result_){
			alert(kn_user_ci_msg["changepassword_ajax_error"]);
			return;
		}

		if(response_text_.notvalid !== undefined && response_text_.notvalid){
			alert(kn_user_ci_msg["changepassword_ajax_notvalid"]);
			changepassword_form_.find("[name='user_pass']").focus();
			return;
		}

		if(response_text_.success === false){
			alert(kn_user_ci_msg["changepassword_ajax_failed"]);
			return;
		}

		alert(kn_user_ci_msg["changepassword_ajax_success"]);
		document.location = document.location;
	});
}

function kn_user_myinfo_changeinfo(){
	var changeinfo_form_ = $("#kn-user-changeinfo-form");
	var user_data_ = changeinfo_form_.serializeObject();
	
	changeinfo_form_.formWorking(true);

	user_data_["email_rev_yn"] = (user_data_["email_rev_yn"] !== undefined && user_data_["email_rev_yn"] === "on" ? "Y" : "N");
	user_data_["sms_rev_yn"] = (user_data_["sms_rev_yn"] !== undefined && user_data_["sms_rev_yn"] === "on" ? "Y" : "N");

	KN.post({
		action : "kn-user-myinfo-changeinfo",
		user_data : user_data_
	},function(result_, response_text_, cause_){
		changeinfo_form_.formWorking(false);
		if(!result_){
			alert(kn_user_ci_msg["changeinfo_ajax_error"]);
			return;
		}

		if(response_text_.notvalid !== undefined && response_text_.notvalid){
			alert(kn_user_ci_msg["changeinfo_ajax_notvalid"]);
			changeinfo_form_.find("[name='user_pass']").focus();
			return;
		}

		if(response_text_.success === false){
			alert(kn_user_ci_msg["changeinfo_ajax_failed"]);
			return;
		}

		alert(kn_user_ci_msg["changeinfo_ajax_success"]);
		document.location = document.location;
	});
}

})(jQuery);