(function($){

$(document).ready(function($){
	var leave_form_ = $("#kn-user-leave-form");
	
	leave_form_.validate({
		rules : {
			term001_agree : {
				checkequals : "Y"
			},
			user_pass : {
				required : true
			}
		},
		messages : {
			/*term001_agree : {
				checkequals : kn_user_lv_msg["term001_agree_checkequals"]
			},*/
			user_pass : {
				required : kn_user_lv_msg["user_pass_required"]
			}
		},submitHandler : function(){
			if(confirm(kn_user_lv_msg["leave_confirm_message"])){
				kn_user_leave_submit();
			}
		}
	});
});

function kn_user_leave_submit(){
	var leave_form_ = $("#kn-user-leave-form");
	leave_form_.formWorking(true);
	KN.post({
		action : "kn-user-leave-do-leave",
		user_data : leave_form_.serializeObject()
	},function(result_, response_text_, cause_){
		leave_form_.formWorking(false);
		if(!result_){
			alert(kn_user_lv_msg["leave_ajax_error"]);
			return;
		}

		if(response_text_.notvalid !== undefined && response_text_.notvalid){
			alert(kn_user_lv_msg["leave_ajax_notvalid"]);
			leave_form_.find("[name='user_pass']").focus();
			return;
		}

		if(response_text_.success === false){
			alert(kn_user_lv_msg["leave_ajax_failed"]);
			return;
		}

		alert(kn_user_lv_msg["leave_ajax_success"]);
		document.location = kn_user_lv_msg["leave_redirect_url"];
	});
}

})(jQuery);