(function($){

	var profile_file_input_;

$(document).ready(function($){
	profile_file_input_ = $("#kn-profile-file");
	profile_file_input_.kn_profile_uploader({
		class : "small",
		upload_confirm : function(){
			return confirm("프로필사진을 변경하시겠습니까?");
		},
		uploaded : function(url_){
			profile_file_input_.wrap().addClass("disabled");

			KN.post({
				action : "kn-user-myinfo-change-profile",
				img_url : url_
			},function(result_, response_text_){
				profile_file_input_.wrap().removeClass("disabled");
			});

		},remove_confirm : function(){
			return confirm("프로필 사진을 삭제하시겠습니까?");
		},removed : function(){
			KN.post({
				action : "kn-user-myinfo-change-profile",
				img_url : null
			},function(result_, response_text_){
				profile_file_input_.wrap().removeClass("disabled");
			});			
		}
	});
});


})(jQuery);