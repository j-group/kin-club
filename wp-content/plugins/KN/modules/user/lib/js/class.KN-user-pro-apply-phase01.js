(function($){

$(document).ready(function($){
	var agree_form_ = $("#kn-pro-apply-form");
	var validate_roles_ = {};
	var agree_fields_ = agree_form_.find("input[data-agreement]");
	var all_agree_field_ = agree_form_.find("#kn-term-all-agree");

	agree_fields_.each(function(){
		var agree_column_ = $(this).attr("name");
		validate_roles_[agree_column_] = {
			checkequals : "Y"
		}
	});

	validate_roles_["qna01"] = {
		required : true
	}
	validate_roles_["qna02"] = {
		required : true
	}
	validate_roles_["qna03"] = {
		required : true
	}

	agree_form_.find("input[type='radio']").change(function(){
		kn_user_check_agreement();
	});

	all_agree_field_.change(function(event_){
		var that_ = $(event_.target);
		if(that_.prop("checked")){
			agree_form_.find("input[data-agreement]").prop("checked",that_.prop("checked"));
		}
	});

	agree_form_.validate({
		rules : validate_roles_,
		messages : {
			qna01 : "답변을 선택하세요",
			qna02 : "답변을 선택하세요",
			qna03 : "답변을 입력하세요"
		},
		invalidHandler : function(){
			// alert(kn_user_pro_apply_agree_var["formval_user_agree_required"]);
		}
	});
});

function kn_user_check_agreement(){
	var agree_form_ = $("#kn-pro-apply-form");
	var agree_fields_ = agree_form_.find("input[data-agreement]");
	var all_agree_field_ = agree_form_.find("#kn-term-all-agree");

	var all_checked_ = agree_form_.find("input[data-agreement]:checked").length === agree_fields_.length;
	all_agree_field_.prop("checked",all_checked_);
}

})(jQuery);