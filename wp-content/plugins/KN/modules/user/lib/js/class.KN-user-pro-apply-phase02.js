(function($){

$(document).ready(function($){
	var apply_form_ = $("#kn-pro-apply-form");

	apply_form_.find("[data-time-format]").timepicker({"timeFormat" : "H:i", "step" : 60});

	apply_form_.validate({
		rules : {
			"qna04[]": {
				required : true
			},qna05 : {
				required : true
			},qna06_01 : {
				required : true
			},qna06_02 : {
				required : true
			},qna07_01 : {
				required : true
			},qna07_02 : {
				required : true
			}
		},
		messages : {
			"qna04[]" : "답변을 선택하세요",
			qna05 : "답변을 선택하세요",
			qna06_01 : "시간을 입력하세요",
			qna06_02 : "시간을 입력하세요",
			qna07_01 : "연령대를 입력하세요",
			qna07_02 : "연령대를 입력하세요"
		},
		invalidHandler : function(){
			// alert(kn_user_pro_apply_agree_var["formval_user_agree_required"]);
		}
	});
		
});

})(jQuery);