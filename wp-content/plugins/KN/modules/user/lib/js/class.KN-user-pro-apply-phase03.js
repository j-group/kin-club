(function($){

$(document).ready(function($){
	var apply_form_ = $("#kn-pro-apply-form");

	apply_form_.find("[name='qna08']").change(function(){
		var value_ = $(this).val();

		var options_ = apply_form_.find(".qna08-options");
		options_.each(function(){
			var option_ = $(this);
			option_.toggle((option_.hasClass("option-"+value_)));
		})
	});
	apply_form_.find("[name='qna08']:checked").trigger("change");

	var image_uploader_ = $("#kn-main-image-uploader");
	image_uploader_.kn_image_uploader({
		max_length : 5,
		uploaded : function(){
			// apply_form_.find("input[name='img_attach_bool']").val("Y");
			
			_update_images();
		},removed : function(){
			_update_images();
			// apply_form_.find("input[name='img_attach_bool']").val((image_uploader_.kn_image_uploader("length") > 0 ? "Y" : ""));
		}
	});

	_update_images();

	apply_form_.validate({
		rules : {
			qna08 : {
				required : true
			},qna09 : {
				required : true
			},qna10 : {
				required : true
			},qna11 : {
				required : true
			},qna12 : {
				required : true
			},qna13 : {
				required : true
			},"qna14[]" : {
				required : true
			},"img_attach_bool" : {
				"required" : true
			}
		},
		messages : {
			qna08 : "답변을 선택하세요",
			qna09 : "답변을 입력하세요",
			qna10 : "답변을 입력하세요",
			qna11 : "답변을 입력하세요",
			qna12 : "답변을 선택하세요",
			qna13 : "답변을 입력하세요",
			"qna14[]" : "답변을 선택하세요",
			img_attach_bool : "이미지를 선택하세요"
		},
		invalidHandler : function(){

		},submitHandler : function(){
			kn_user_pro_apply();
		}
	});

	function kn_user_pro_apply(){
		KN.confirm({
			title : "전문가신청",
			content : "전문가신청을 진행합니다. 계속하시겠습니까?",
			OK : "신청하기",
			cancel : "취소",
			callback : function(confirmed_){
				if(!confirmed_) return;

				KN.indicator(true, function(){
					kn_user_pro_apply_p1();
				});
			}
		});
	}

	function _update_images(){
		var apply_form_ = $("#kn-pro-apply-form");
		var image_uploader_ = $("#kn-main-image-uploader");
		var image_json_ = image_uploader_.kn_image_uploader("toJSON");

		apply_form_.find("[name='qna12[]']").remove();

		var length_ = image_json_.length;
		for(var index_=0;index_<length_;++index_){
			apply_form_.append($("<input type='hidden' name='qna12[]' value='"+image_json_[index_]+"'>"));
		}
	}
		
	function kn_user_pro_apply_p1(){
		$ = jQuery;
		var apply_form_ = $("#kn-pro-apply-form");
		var apply_data_ = apply_form_.serializeObject();

		if($.type(apply_data_["qna02[]"]) === "string"){
			apply_data_["qna02[]"] = [apply_data_["qna02[]"]];
		}

		if($.type(apply_data_["qna04[]"]) === "string"){
			apply_data_["qna04[]"] = [apply_data_["qna04[]"]];
		}
		if($.type(apply_data_["qna12[]"]) === "string"){
			apply_data_["qna12[]"] = [apply_data_["qna12[]"]];
		}

		if($.type(apply_data_["qna14[]"]) === "string"){
			apply_data_["qna14[]"] = [apply_data_["qna14[]"]];
		}

		apply_data_["qna02"] = apply_data_["qna02[]"];
		apply_data_["qna04"] = apply_data_["qna04[]"];
		apply_data_["qna06"] = {
			"from" : apply_data_["qna06_01"],
			"to" : apply_data_["qna06_02"]
		};
		apply_data_["qna07"] = {
			"from" : apply_data_["qna07_01"],
			"to" : apply_data_["qna07_02"],
			"month" : apply_data_["qna07_03"]
		};
		apply_data_["qna12"] = apply_data_["qna12[]"];
		apply_data_["qna14"] = apply_data_["qna14[]"];

		delete apply_data_["qna02[]"];
		delete apply_data_["qna04[]"];
		delete apply_data_["qna06_01"];
		delete apply_data_["qna06_02"];
		delete apply_data_["qna07_01"];
		delete apply_data_["qna07_02"];
		delete apply_data_["qna07_03"];
		delete apply_data_["qna12[]"];
		delete apply_data_["qna14[]"];
		delete apply_data_["phase"];
		delete apply_data_["term_agree"];

		KN.post({
			action : "kn-user-pro-apply",
			apply_data : apply_data_
		},function(result_, response_json_,cause_){
			KN.indicator(false);
			if(!result_ || response_json_.success !== true){
				KN.alert({
					title : "에러발생",
					content : "신청 중 에러가 발생했습니다.",
					OK : "확인"
				});
				return;
			}

			$("#kn-apply-form-complete").submit();
		});
	}
});

})(jQuery);

function kn_user_pro_phase03_submit(){
	$ = jQuery;
	var apply_form_ = $("#kn-pro-apply-form");
	apply_form_.submit();
}