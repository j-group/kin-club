(function($){

	var addinfo_frame_, addinfo_form_, profile_upload_file_, profile_url_input_;
	var intro_txt_input_,addr_lat_input_,addr_lng_input_,address_input_, address_dtl_input_;

	$(document).ready(function($){
		addinfo_frame_ = $(".kn-addinfo-frame"); 
		addinfo_form_ = $("#kn-addinfo-form");
		profile_url_input_ = addinfo_form_.find("[name='profile_url']");
		profile_upload_file_ = $("#kn-profile-file");
		intro_txt_input_ = addinfo_form_.find("[name='intro_txt']");
		
		addr_lat_input_ = addinfo_form_.find("[name='addr_lng']");
		addr_lng_input_ = addinfo_form_.find("[name='addr_lat']");
		address_input_ = addinfo_form_.find("[name='address']");
		address_dtl_input_ = addinfo_form_.find("[name='address_dtl']");

		profile_upload_file_.kn_profile_uploader({
			uploaded : function(url_){
				profile_url_input_.val(url_);
			},removed : function(){
				profile_url_input_.val("");
			}
		});

		addinfo_form_.validate({
			submitHandler : function(){
				kn_user_save_addinfo();
			}
		});

		address_input_.click(function(){
			kn_user_register_open_address_popup();
		});

		$("#kn-user-addinfo-submit-btn").click(function(){addinfo_form_.submit()});
	});

function kn_user_save_addinfo(){
	var inputs_ = addinfo_form_.find("[data-accessible='true']");
	var addinfo_data_ = addinfo_form_.serializeObject();
		addinfo_data_["nick_name"] = $("#kn-profile-nickname").val();
	var child_table_ = $("#kn-user-children-table");
	var child_data_ = child_table_.kn_user_children_table("json_string");
		
	addinfo_frame_.formWorking(true);

	KN.post({
		action : "kn-user-update-addinfo",
		addinfo : addinfo_data_,
		children_data : child_data_
	},function(result_, response_text_){
		if(!result_ || response_text_.success === undefined){
			alert(kn_addinfo_var["update_error_message"]);
			addinfo_frame_.formWorking(false);
			return;
		}

		alert(kn_addinfo_var["update_success_message"]);
		document.location = kn_addinfo_var["login_url"];
	});
}

function kn_user_register_open_address_popup(){
	$ = jQuery;
	
	KN.maps.address_picker({
		callback : function(address_){
			console.log(address_);
			address_input_.val(address_["address"]);
			addr_lat_input_.val(address_["lat"]);
			addr_lng_input_.val(address_["lng"]);
			address_dtl_input_.focus();
		},
		secondary_btn_title : "현재위치선택",
		secondary_btn_callback : function(){
			$ = jQuery;
			$.magnificPopup.close();
			KN.maps.geolocation(function(result_){
				console.log(result_);
			});
		}
	});
}

})(jQuery);

function kn_user_skip_addinfo(){
	document.location = kn_addinfo_var["login_url"];
}
