(function($){

$(document).ready(function($){
	var register_form_ = $("#kn-register-form");

	register_form_.validate({
		rules : {
			"user_login" : {
				"required" : true,
				"alphanum" : true,
				"minlength" : 5,
				"maxlength" : 15,
				"remote" : {
					url : ajaxurl,
					method : "GET",
					data : {
						"action" : "kn-user-register-user-exists",
						"key" : function(){return $("#kn-register-form [name='user_login']").val();}
					}
				}
			},
			"user_email" : {
				"required" : true,
				"email" : true,
				"remote" : {
					url : ajaxurl,
					method : "GET",
					data : {
						"action" : "kn-user-register-user-exists",
						"key" : function(){return $("#kn-register-form [name='user_email']").val();}
					}
				}
			},
			"user_pass" : {
				"required" : true,
				"minlength" : 8,
				"maxlength" : 25
			},
			"user_pass_c" : {
				"equalTo" : "#kn-register-form [name='user_pass']"
			},
			"phone2" : {
				"required" : true,
				"number" : true
			},
			"phone3" : {
				"required" : true,
				"number" : true
			}
		},messages : {
			"user_login" : {
				"required" : kn_user_register_msg["formval_user_login_required"],
				"minlength" : kn_user_register_msg["formval_user_login_minlength"],
				"maxlength" : kn_user_register_msg["formval_user_login_maxlength"],
				"remote" : kn_user_register_msg["formval_user_login_exists"],
				"alphanum" : kn_user_register_msg["formval_user_login_alphanum"]
			},
			"user_email" : {
				"required" : kn_user_register_msg["formval_user_email_required"],
				"email" : kn_user_register_msg["formval_user_email_email"],
				"remote" : kn_user_register_msg["formval_user_email_exists"]
			},
			"user_pass" : {
				"required" : kn_user_register_msg["formval_user_pass_required"],
				"minlength" : kn_user_register_msg["formval_user_pass_minlength"],
				"maxlength" : kn_user_register_msg["formval_user_pass_maxlength"]
			},
			"user_pass_c" : {
				"equalTo" : kn_user_register_msg["formval_user_pass_c_equalsto"]
			},
			"phone2" : {
				"required" : kn_user_register_msg["formval_phone2_required"],
				"number" : kn_user_register_msg["formval_phone2_number"]
			},
			"phone3" : {
				"required" : kn_user_register_msg["formval_phone3_required"],
				"number" : kn_user_register_msg["formval_phone3_number"]
			}
		},submitHandler : function(){
			kn_user_register();
		}
	});
});

function kn_user_register(){
	var register_form_ = $("#kn-register-form");
	var inputs_ = register_form_.find("[data-accessible='true']");
	var user_data_ = register_form_.serializeObject();
		
	register_form_.formWorking(true);

	user_data_["email_rev_yn"] = (user_data_["email_rev_yn"] !== undefined && user_data_["email_rev_yn"] === "on" ? "Y" : "N");
	user_data_["sms_rev_yn"] = (user_data_["sms_rev_yn"] !== undefined && user_data_["sms_rev_yn"] === "on" ? "Y" : "N");
	
	KN.post({
		action : "kn-user-register-signup",
		user_data : user_data_
	},function(result_, response_text_){
		console.log(response_text_);
		if(!result_ || response_text_.success === undefined){
			alert(kn_user_register_msg["signup_error_message"]);
			register_form_.formWorking(false);
			return;
		}
		var complete_form_ = $("#kn-register-form-complete");
		complete_form_.find("[name='ID']").val(response_text_["ID"]);
		complete_form_.submit();
	});
}

})(jQuery);

function kn_user_cancel_register(url_){
	if(confirm(kn_user_register_msg["signup_cancel_confirm"])){
		document.location = kn_user_register_msg["signup_cancel_url"];
	}
}