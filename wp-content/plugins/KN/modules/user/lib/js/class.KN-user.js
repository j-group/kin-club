(function($){
	jQuery.validator.addMethod("checkequals", function(value_, element_, param_){ 
		return this.optional(element_) || (value_ === param_);
	}, "error");

})(jQuery);