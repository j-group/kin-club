<?php

if(!defined('WP_UNINSTALL_PLUGIN'))
	exit();

//drop a custom db table
global $wpdb;
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

dbDelta("DROP TABLE `{$table_prefix_}kn_users`");

dbDelta("DROP TABLE `{$table_prefix_}kn_gcode_dtl`");
dbDelta("DROP TABLE `{$table_prefix_}kn_gcode`");

remove_option(KN_KEY_SETTINGS);
remove_option('KN_installed');

?>
